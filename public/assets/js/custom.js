/* Banner Section Slider S */
    var owlContactSlideNumber = 1, owlContactClassName = '.banner_section';

    var owlContactLoop = false,

        owlContactItemLength = $(owlContactClassName + ' .owl-carousel .item').length,

        owlContactBanner = $(owlContactClassName + ' .owl-carousel');



    if (owlContactItemLength < owlContactSlideNumber) { $(owlContactClassName).find('.owl-controls').css('display', 'none');}

    if (owlContactItemLength > owlContactSlideNumber) { owlContactLoop = true; } else { owlContactLoop = false; }



    owlContactBanner.owlCarousel({

        animateOut: 'fadeOut',

        animateIn: 'fadeIn',

        loop: owlContactLoop,

        mouseDrag: false,

        touchDrag: true,

        margin: 0,

        navText: ["<i class='icon-left_arrow' title='Previous'></i>","<i class='icon-right-arrow' title='Next'></i>"],

        autoplay: true,

        autoplayHoverPause: false,

        autoplayTimeout: 5000,

        smartSpeed: 2000,           

        responsive: {

            0: { items: 1, dots: true, nav: false },

            480: { items: 1, dots: true, nav: false },

            768: { items: 1, dots: true, nav: false },

            1024: { items: 1, dots: true, nav: false },

            1025: { items: 1, dots: false, nav: true }

        }

    });
/* Banner Section Slider E */

/* Tab Content Section - About TrippyWords S */
    function openCity(evt, cityName) {
        var i, tabcontent, tablinks;
        tabcontent = document.getElementsByClassName("tabcontent");
        for (i = 0; i < tabcontent.length; i++) {
            tabcontent[i].style.display = "none";
        }
        tablinks = document.getElementsByClassName("tablinks");
        for (i = 0; i < tablinks.length; i++) {
            tablinks[i].className = tablinks[i].className.replace(" active", "");
        }
        document.getElementById(cityName).style.display = "block";
        evt.currentTarget.className += " active";
    }

    // Get the element with id="defaultOpen" and click on it
    document.getElementById("defaultOpen").click();
/* Tab Content Section - About TrippyWords E */

/* Login - Modal S */
    $(document).ready(function(){
        $('.login_signup_modal.modal .form_section .input-group').css({
            "border-bottom": "1px solid #cfced3",
            "border-left" : "1px solid transparent",
            "border-right" : "1px solid transparent",
            "border-top" : "1px solid transparent",
            
        });

        $("input.login_signup_input").focus(function(){
            $(this).parent('.login_signup_modal.modal .form_section .form-group > .input-group').css({"border":"1px solid rgba(33, 150, 243, 0.32)","padding-left": "10px","transition" : "0.5s"});
        });

        $("input").focusout(function(){
            $(this).parent('.login_signup_modal.modal .form_section .form-group > .input-group').css({
                "border-bottom": "1px solid #cfced3",
                "border-left" : "1px solid transparent",
                "border-right" : "1px solid transparent",
                "border-top" : "1px solid transparent",
                "padding-left": "0px",
                "transition" : "0.5s",  
            });
        });
    });
/* Login - Modal E */

/* Blog-slider S */
    var owlContactSlideNumber = 1, owlContactClassName = '.blog_slider';

    var owlContactLoop = false,

        owlContactItemLength = $(owlContactClassName + ' .owl-carousel .item').length,

        owlContactBanner = $(owlContactClassName + ' .owl-carousel');



    if (owlContactItemLength < owlContactSlideNumber) { $(owlContactClassName).find('.owl-controls').css('display', 'none');}

    if (owlContactItemLength > owlContactSlideNumber) { owlContactLoop = true; } else { owlContactLoop = false; }



    owlContactBanner.owlCarousel({

        animateOut: 'fadeOut',

        animateIn: 'fadeIn',

        loop: owlContactLoop,

        mouseDrag: false,

        touchDrag: true,

        margin: 0,

        navText: ["<i class='icon-left_arrow' title='Previous'></i>","<i class='icon-right-arrow' title='Next'></i>"],

        autoplay: true,

        autoplayHoverPause: false,

        autoplayTimeout: 5000,

        smartSpeed: 2000,           

        responsive: {

            0: { items: 1, dots: true, nav: false },

            480: { items: 1, dots: true, nav: false },

            768: { items: 1, dots: true, nav: false },

            1024: { items: 1, dots: true, nav: false },

            1025: { items: 1, dots: true, nav: true }

        }

    });
/* Blog-slider E */


/* Ck-Editor S */



/* Ck-Editor E */