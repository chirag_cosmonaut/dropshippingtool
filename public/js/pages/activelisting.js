
$(function() {
    $('#active-table').DataTable({
        processing: true,
        serverSide: true,
        responsive: true,
        ordering: false,
        ajax: '{!! route('activelisting.data') !!}',
        columns: [
            { data: 'BuyItNowPrice', name: 'BuyItNowPrice' },
            { data: 'ItemID',"searchable":true, name: 'ItemID' },
            { data: 'ListingDetails',"searchable":true, name: 'ListingDetails' },
            
            
        ]
    });
});