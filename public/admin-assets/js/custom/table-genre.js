//////////////////////////////////////////////////////
//  Template Name: octAdmin
//  Author: octathemes
//  Email: octathemes@gmail.com
//  File: table-datatable-example.js
///////////////////////////////////////////////////

$(function () {
    "use strict";

    $('#table-genre').DataTable({
        "scrollX": true,
        processing: true,
        serverSide: true,
        dom: 'Bfrtip',
        "ajax": ADMIN_URL+'/genre/getdata',
        columns: [
            {data: 'id', name: 'id'},
            {data: 'img', name: 'Img'},
            {data: 'name', name: 'Name'},
            {data: 'parent_genre_id', name: 'parent_genre_id'},
            {data: 'is_deleted', name: 'is_deleted'},
            {data: 'is_published', name: 'is_published'},
            {data: 'created_at', name: 'created_at'},
            {data: 'updated_at', name: 'updated_at'},
            {data: 'action', name: 'Actions'},
        ],
        buttons: [
            'copyHtml5',
            'excelHtml5',
            'csvHtml5',
            'pdfHtml5'
        ]
    });
});


