$(function() {
	$('#example').DataTable({
		processing: true,
        serverSide: true,
		responsive: true,
		ordering: false,
		destroy: true,
		scrollX:true,
		pageLength:10,
		bStateSave: true,
        ajax: "{!! route('activelisting.data') !!}",
        columns: [
			{ data: 'bulk',"searchable":false,name:'bulk' },
			{ data: 'edit',"searchable":false,name:'edit' },
            { data: 'sell_id',"searchable":false, name: 'sell_id', },
			{ data: 'source_id',"searchable":false, name: 'source_id' },
			{ data: 'picture',"searchable":false, name: 'picture' },
            { data: 'title',"searchable":true, name: 'title' },
            { data: 'quantity_available',"searchable":true, name: 'quantity_available'},
            { data: 'current_price',"searchable":false, name: 'current_price'},
			{ data: 'source_price',"searchable":false,name:'source_price' },
			{ data: 'amount_sold',"searchable":false,name:'amount_sold' },
			{ data: 'hit_count',"searchable":false,name:'hit_count' },
			{ data: 'watch_count',"searchable":false,name:'watch_count' },
			{ data: 'profit',"searchable":false,name:'profit' },
            { data: 'upload_date',"searchable":false, name: 'upload_date' },
            { data: 'days_left',"searchable":false,name:'days_left' },
            { data: 'oos_day',"searchable":false,name:'oos_day' },
            { data: 'dws',"searchable":false,name:'dws' },
            { data: 'status',"searchable":false, name: 'status' },
        ]

    });
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': '{{ csrf_token() }}'
		}
	});
})