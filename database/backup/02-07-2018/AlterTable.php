ALTER TABLE `orders_monitor` ADD `is_start` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 : stop 1 : start' AFTER `min_drop_price_amount`;


ALTER TABLE `order_processing` ADD `is_start` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 : stop 1 : start ' AFTER `set_ontrack_shipping_carrier`;


ALTER TABLE `products_monitor` ADD `is_start` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 : stop 1 : start ' AFTER `automatic_sku_filling`;


ALTER TABLE `profitable_monitor` ADD `is_start` TINYINT(1) NOT NULL DEFAULT '0' COMMENT '0 : stop 1 : start ' AFTER `listing_min_views`;