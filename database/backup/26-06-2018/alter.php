ALTER TABLE `orders` DROP `order_name`, DROP `description`, DROP `competitor_link`, DROP `competitor_price`, DROP `competitor_shipping`, DROP `item_price`, DROP `quantity`, DROP `warehouse`;


ALTER TABLE `item` DROP `supplier_sku`, DROP `brand`, DROP `menufacturer`, DROP `auto_pay`, DROP `buyer_protection`, DROP `gift_icon`, DROP `seller`, DROP `buyer_responsible_for_shipping`, DROP `proxy_item`, DROP `intangible_item`, DROP `post_checkout_experience_enabled`, DROP `relist_parent_id`, DROP `ebay_plus`, DROP `ebay_plus_eligible`, DROP `is_secure_description`, DROP `best_offer_count`, DROP `best_offer_enable`, DROP `new_best_offer`, DROP `shipping_profile_id`, DROP `shipping_profile_name`, DROP `return_profile_id`, DROP `return_profile_name`, DROP `payment_profile_id`, DROP `payment_profile_name`, DROP `product_identifier`, DROP `lot_size`, DROP `best_offer_auto_accept_price`, DROP `minimum_best_offer_price`, DROP `supplier`, DROP `identifier`, DROP `competitor_link`, DROP `competitor_price`, DROP `competitor_shipping`, DROP `supplier_order_id`, DROP `donate_to_charity`, DROP `donation_percentage`;


ALTER TABLE `item_shipping_detail` DROP `apply_shipping_discount`, DROP `weight_major`, DROP `weight_minor`, DROP `expedited_service`, DROP `third_party_checkout`, DROP `shipping_discount_profile_id`, DROP `international_shipping_discount_profile_id`, DROP `seller_exclude_ship_to_locations_preference`;

ALTER TABLE `listing_details` DROP `adult`, DROP `bidding_auction`, DROP `checkout_enabled`, DROP `has_unanswered_questions`;

ALTER TABLE `selling_status` DROP `bid_count`, DROP `bid_increment`, DROP `lead_count`, DROP `minimum_to_bid`, DROP `reserve_met`, DROP `second_chance_eligible`, DROP `quantity_sold_by_pickup_in_store`;