-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 01, 2018 at 04:40 AM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `latest_dropship`
--

-- --------------------------------------------------------

--
-- Table structure for table `amazon_account`
--

CREATE TABLE `amazon_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `purchase_account` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `country` varchar(13) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `Full_name` varchar(50) DEFAULT NULL,
  `prime_account` varchar(5) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amazon_account`
--

INSERT INTO `amazon_account` (`id`, `user_id`, `account_id`, `purchase_account`, `zip_code`, `country`, `phone_number`, `Full_name`, `prime_account`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 380013, 'DE', '123465789', 'Chirag Parmar', 'false', '2018-06-30 16:40:26', '2018-06-30 19:29:24');

-- --------------------------------------------------------

--
-- Table structure for table `orders_monitor`
--

CREATE TABLE `orders_monitor` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `when_product_sold` varchar(16) DEFAULT NULL,
  `quantity_to_raise_to` decimal(10,3) DEFAULT NULL,
  `when_product_sold_change_price` varchar(5) DEFAULT NULL,
  `start_price_raise_after_x_sold` decimal(10,3) DEFAULT NULL,
  `raise_only_for_first_sell` tinyint(1) DEFAULT NULL,
  `raise_for_each_sale` tinyint(1) DEFAULT NULL,
  `raise_additional_per` decimal(10,3) DEFAULT NULL,
  `raise_additional_dollar` decimal(10,3) DEFAULT NULL,
  `max_per_raise_limit` decimal(10,3) DEFAULT NULL,
  `max_price_raise_limit` decimal(10,3) DEFAULT NULL,
  `drop_price_product_not_sold` varchar(5) DEFAULT NULL,
  `drop_price_afterX_days` decimal(10,3) DEFAULT NULL,
  `drop_price_per` decimal(10,3) DEFAULT NULL,
  `drop_price_amount` decimal(10,3) DEFAULT NULL,
  `min_drop_price_per` decimal(10,3) DEFAULT NULL,
  `min_drop_price_amount` decimal(10,3) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders_monitor`
--

INSERT INTO `orders_monitor` (`id`, `user_id`, `account_id`, `when_product_sold`, `quantity_to_raise_to`, `when_product_sold_change_price`, `start_price_raise_after_x_sold`, `raise_only_for_first_sell`, `raise_for_each_sale`, `raise_additional_per`, `raise_additional_dollar`, `max_per_raise_limit`, `max_price_raise_limit`, `drop_price_product_not_sold`, `drop_price_afterX_days`, `drop_price_per`, `drop_price_amount`, `min_drop_price_per`, `min_drop_price_amount`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Raise Quantity', NULL, 'true', NULL, 0, 1, NULL, NULL, NULL, NULL, '1', NULL, NULL, NULL, NULL, NULL, '2018-06-30 11:47:50', '2018-06-30 11:49:37');

-- --------------------------------------------------------

--
-- Table structure for table `order_processing`
--

CREATE TABLE `order_processing` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `auto_order` varchar(5) DEFAULT NULL,
  `bundle_add_on` varchar(5) DEFAULT NULL,
  `oder_only_prime` varchar(5) DEFAULT NULL,
  `hipshipper` varchar(5) DEFAULT NULL,
  `max_product_price_auto_order` decimal(10,3) DEFAULT NULL,
  `max_lose_amount` decimal(10,3) DEFAULT NULL,
  `set_order_shipped` varchar(25) DEFAULT NULL,
  `gift_message` varchar(255) DEFAULT NULL,
  `set_ontrack_shipping_carrier` varchar(5) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_processing`
--

INSERT INTO `order_processing` (`id`, `user_id`, `account_id`, `auto_order`, `bundle_add_on`, `oder_only_prime`, `hipshipper`, `max_product_price_auto_order`, `max_lose_amount`, `set_order_shipped`, `gift_message`, `set_ontrack_shipping_carrier`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'true', 'true', 'true', 'true', NULL, NULL, 'After Purchase', NULL, '1', '2018-06-30 16:38:08', '2018-06-30 19:37:19');

-- --------------------------------------------------------

--
-- Table structure for table `products_monitor`
--

CREATE TABLE `products_monitor` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `default_break_event` decimal(10,3) DEFAULT NULL,
  `additional_dollar` decimal(10,3) DEFAULT NULL,
  `additional_per` decimal(10,3) DEFAULT NULL,
  `end_untracked_listing` varchar(5) DEFAULT NULL,
  `not_available_listing` int(18) DEFAULT NULL,
  `when_product_available` int(16) DEFAULT NULL,
  `quantity_to_raise_to` decimal(10,3) DEFAULT NULL,
  `dynamic_policy_creation` varchar(5) DEFAULT NULL,
  `min_allowed_quantity_in_stock` decimal(10,3) DEFAULT NULL,
  `min_profit_$per_product` decimal(10,3) DEFAULT NULL,
  `when_product_vero` varchar(12) DEFAULT NULL,
  `when_product_blocked` varchar(12) DEFAULT NULL,
  `max_shipping_time` varchar(5) DEFAULT NULL,
  `automatic_sku_filling` varchar(5) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `products_monitor`
--

INSERT INTO `products_monitor` (`id`, `user_id`, `account_id`, `default_break_event`, `additional_dollar`, `additional_per`, `end_untracked_listing`, `not_available_listing`, `when_product_available`, `quantity_to_raise_to`, `dynamic_policy_creation`, `min_allowed_quantity_in_stock`, `min_profit_$per_product`, `when_product_vero`, `when_product_blocked`, `max_shipping_time`, `automatic_sku_filling`, `created_at`, `updated_at`) VALUES
(10, 1, 1, '12.000', '123.200', '50.000', 'false', 0, 0, '41.000', 'true', NULL, '20.000', 'End Listing', 'Alert Me', '1 day', 'true', '2018-06-29 16:21:38', '2018-06-30 18:52:20');

-- --------------------------------------------------------

--
-- Table structure for table `profitable_monitor`
--

CREATE TABLE `profitable_monitor` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `listing_not_profitable` varchar(12) DEFAULT NULL,
  `listing_left_days` int(11) DEFAULT NULL,
  `listing_min_sold_quantity` int(11) DEFAULT NULL,
  `listing_min_watchers` int(11) DEFAULT NULL,
  `listing_min_views` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `profitable_monitor`
--

INSERT INTO `profitable_monitor` (`id`, `user_id`, `account_id`, `listing_not_profitable`, `listing_left_days`, `listing_min_sold_quantity`, `listing_min_watchers`, `listing_min_views`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Alert Me', 20, 45, 10, 12, '2018-06-30 12:31:20', '0000-00-00 00:00:00');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `amazon_account`
--
ALTER TABLE `amazon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `orders_monitor`
--
ALTER TABLE `orders_monitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_processing`
--
ALTER TABLE `order_processing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_monitor`
--
ALTER TABLE `products_monitor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `profitable_monitor`
--
ALTER TABLE `profitable_monitor`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `amazon_account`
--
ALTER TABLE `amazon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `orders_monitor`
--
ALTER TABLE `orders_monitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `order_processing`
--
ALTER TABLE `order_processing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `products_monitor`
--
ALTER TABLE `products_monitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `profitable_monitor`
--
ALTER TABLE `profitable_monitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
