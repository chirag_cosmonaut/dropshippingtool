-- phpMyAdmin SQL Dump
-- version 4.8.0.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Jul 11, 2018 at 10:57 PM
-- Server version: 10.1.32-MariaDB
-- PHP Version: 7.1.17

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `latest_dropship`
--

-- --------------------------------------------------------

--
-- Table structure for table `accounts`
--

CREATE TABLE `accounts` (
  `id` int(11) NOT NULL,
  `int_user_id` int(11) NOT NULL,
  `email` varchar(150) DEFAULT NULL,
  `user_id` varchar(150) DEFAULT NULL,
  `token` text NOT NULL,
  `fees` varchar(150) DEFAULT NULL,
  `country` varchar(150) DEFAULT NULL,
  `fulfillment_centers` varchar(150) DEFAULT NULL,
  `image` varchar(150) DEFAULT NULL,
  `eiast_token` text,
  `feedback_score` int(11) DEFAULT NULL,
  `unique_negative_feedback_count` int(11) DEFAULT NULL,
  `unique_positive_feedback_count` int(11) DEFAULT NULL,
  `positive_feedback_percent` double(4,2) DEFAULT NULL,
  `feedback_private` varchar(25) DEFAULT NULL,
  `feeddback_rating_star` varchar(25) DEFAULT NULL,
  `id_verified` varchar(25) DEFAULT NULL,
  `ebay_good_standing` varchar(25) DEFAULT NULL,
  `new_user` varchar(25) DEFAULT NULL,
  `registration_date` varchar(55) DEFAULT NULL,
  `site` varchar(25) DEFAULT NULL,
  `status` varchar(25) DEFAULT NULL,
  `user_id_changed` varchar(25) DEFAULT NULL,
  `user_id_last_changed` varchar(55) DEFAULT NULL,
  `allow_payment_edit` varchar(25) DEFAULT NULL,
  `checkeout_enabled` varchar(25) DEFAULT NULL,
  `cip_bank_account_stored` varchar(25) DEFAULT NULL,
  `good_standing` varchar(25) DEFAULT NULL,
  `live_auction_authorized` varchar(25) DEFAULT NULL,
  `merchandizing_pref` varchar(25) DEFAULT NULL,
  `qualifies_for_b2b_vat` varchar(25) DEFAULT NULL,
  `seller_guarantee_level` varchar(25) DEFAULT NULL,
  `store_owner` varchar(25) DEFAULT NULL,
  `payment_method` varchar(25) DEFAULT NULL,
  `charity_registered` varchar(25) DEFAULT NULL,
  `safe_payment_exempt` varchar(25) DEFAULT NULL,
  `max_scheduled_minutes` varchar(25) DEFAULT NULL,
  `min_scheduled_minutes` int(11) DEFAULT NULL,
  `max_scheduled_items` int(11) DEFAULT NULL,
  `transaction_percent` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `accounts`
--

INSERT INTO `accounts` (`id`, `int_user_id`, `email`, `user_id`, `token`, `fees`, `country`, `fulfillment_centers`, `image`, `eiast_token`, `feedback_score`, `unique_negative_feedback_count`, `unique_positive_feedback_count`, `positive_feedback_percent`, `feedback_private`, `feeddback_rating_star`, `id_verified`, `ebay_good_standing`, `new_user`, `registration_date`, `site`, `status`, `user_id_changed`, `user_id_last_changed`, `allow_payment_edit`, `checkeout_enabled`, `cip_bank_account_stored`, `good_standing`, `live_auction_authorized`, `merchandizing_pref`, `qualifies_for_b2b_vat`, `seller_guarantee_level`, `store_owner`, `payment_method`, `charity_registered`, `safe_payment_exempt`, `max_scheduled_minutes`, `min_scheduled_minutes`, `max_scheduled_items`, `transaction_percent`, `created_at`, `updated_at`) VALUES
(1, 1, 'test.cosmonautgroup@gmail.com', 'testuser_cosmonautgroup', 'AgAAAA**AQAAAA**aAAAAA**J8gjWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ**gHMEAA**AAMAAA**Ej0uxSLGchr9zf4E+8gtDKq+cL1GihgaD8l0B8/5cgChnzRY5VRrNu7PcxVZuPHM0WTdPYZ5rb4IvdCY6zbIZKAa4XELsUVoMYMD3cbIi2pZmuRPFsin4aup2Cuwxx5FajkNwpiSueWbiKjWpBJJNuZVGL6+TQDCtjGpI4oV+Rm/A9eYmIlfq1oriH9IURiQLUD/hPKXasfeyJTgr6E8Zu3wf/lDELesnpwDqYCk1FFbBUEuHniesfdU3Zx7EXI1RDsA01ndUmjCEY0QLSsDQF459KNaKdXvyF1X3GUGV+QEgnicmP5YVfA5HTIahl+5iIj6sZW5e6yMkrz5VJOKMHtI0lBmjHa91AASY9PvlOMNV2nz0pQzaT2/xOAKal9xJSEhqXLSXHLH0SPb0EBOacmaJ3wTHBaiaOpJ5i5hLNhNeRTjp+KMzOR9STKcLE3oIt6OYI4fo7uFfuyq+vYzyGz890TdaEC2EPNUtubK2VfuCIYMcKltCv8IUxvorfa2u1xzlBF+TMtPvbfh1w3Y5Z3nWSvnb15EZzvQR9X2Vq+Ea2Jh2k6cGWqOXJrCbbqA/hVTXDCJI25sAWn/WELhXfbovncikdJN/KOG3k3yWlBKj/tm/2HtAbqeygr7zQvxILkOY2eeXQGSakPXWhazzClo7rqcEiKWDhuPjL7Jy+v9hj7nOL0l48EyqTR68O+H25ESuVO55C/VE416O8iEXyO1n88eT7qdyzCcC+Mank58rH68U0UWR4DLEyCAIMy7', NULL, NULL, NULL, NULL, 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 500, 0, 0, 0.00, 'false', 'Purple', 'true', 'true', 'false', '2006-01-01T00:00:00.000Z', 'India', 'Confirmed', 'false', '2018-01-16T12:14:45.000Z', 'true', 'true', 'false', 'true', 'false', 'OptIn', 'false', 'NotEligible', 'false', 'NothingOnFile', 'false', 'true', '30240', 0, 3000, 75, '2018-06-15 14:07:39', '2018-06-15 14:07:39'),
(3, 1, 'Invalid Request', 'testuser_kalpesh', 'AgAAAA**AQAAAA**aAAAAA**DPAoWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ**gHMEAA**AAMAAA**8wdbt8tMP5P67DNrZeLGh+k6RBRvWrq8dRwxuydLipPzi8mBndbr7/4m4RvE+9Ss1sXzlqyYO3nwExr6NfmN72kEqQP+qNzlyA1xN8I7LejHB5bWyS0QuA9FYBoOeVZWHtpzMUbXZPD4j7G5Q9a09tbHmRQC7yfGeQOUlOUtNXUqSowsnmeomgGPqKuhwmCjobmhYY2Gz357pqsWcIQ995z+GaTsC+iGl4BYVA6/zXYVZoRwcgFAljIpWMegCfu1REW/7Mh0olREEfqWCfi05CRZIPxLeyltadwJAJLkgMOepUTsKVAGy3AB4c7PTgkh2GlVagsbCtyjfjA9wQWfFIvUrSI8Jy9xZMZ587A2itGPNuLXi83ptR+mA3m4FckeMI4clq8zYhR59MCcLyr2bjZgSFKCZ31iXSoekbhWBgnKh8zEYAdAWh8JijxdLI/0+Ia+AcI0RCVTSeSlto0h4INofjtW0CCm5asiWr0Hnv2wjBx7sAMGEeAxixKLHwWkAbruOdXkA1emGkkXW/6zjMEB8vL+Bk+DmQ8Q2bC82tQjlnLvzMkqqd1xlCzFiis6lW/6ypPhnV797+k82+3hIqOfPXtYu8H8+BxmeGi5rxWfTOZh0R3z3FJPX3WZdITWVvsywngjBxAEneuhEJ4SMTUiMY3i44djsSkuZYnfId5t4q7nrUsA0Nw0L0Usc4Y4NVOaQGDGoHP4XmHA0aTtbmJ/9nF1WzN6hj/GGm7A44h95GD6mgrflzT8c37byFRO', NULL, NULL, NULL, NULL, 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 0, 0, 0, 0.00, 'false', 'None', 'false', 'true', 'false', '2018-01-17T10:44:00.000Z', 'India', 'Confirmed', 'false', '2018-01-17T10:44:00.000Z', 'true', 'true', 'false', 'true', 'false', 'OptIn', 'false', 'NotEligible', 'false', '', 'false', 'false', '30240', 0, 3000, 0, '2018-06-19 11:59:14', '2018-06-19 11:59:14'),
(4, 8, 'test.cosmonautgroup@gmail.com', 'testuser_cosmonautgroup', 'AgAAAA**AQAAAA**aAAAAA**7wMpWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ**gHMEAA**AAMAAA**Ej0uxSLGchr9zf4E+8gtDKq+cL1GihgaD8l0B8/5cgChnzRY5VRrNu7PcxVZuPHM0WTdPYZ5rb4IvdCY6zbIZKAa4XELsUVoMYMD3cbIi2pZmuRPFsin4aup2Cuwxx5FajkNwpiSueWbiKjWpBJJNuZVGL6+TQDCtjGpI4oV+Rm/A9eYmIlfq1oriH9IURiQLUD/hPKXasfeyJTgr6E8Zu3wf/lDELesnpwDqYCk1FFbBUEuHniesfdU3Zx7EXI1RDsA01ndUmjCEY0QLSsDQF459KNaKdXvyF1X3GUGV+QEgnicmP5YVfA5HTIahl+5iIj6sZW5e6yMkrz5VJOKMHtI0lBmjHa91AASY9PvlOMNV2nz0pQzaT2/xOAKal9xJSEhqXLSXHLH0SPb0EBOacmaJ3wTHBaiaOpJ5i5hLNhNeRTjp+KMzOR9STKcLE3oIt6OYI4fo7uFfuyq+vYzyGz890TdaEC2EPNUtubK2VfuCIYMcKltCv8IUxvorfa2u1xzlBF+TMtPvbfh1w3Y5Z3nWSvnb15EZzvQR9X2Vq+Ea2Jh2k6cGWqOXJrCbbqA/hVTXDCJI25sAWn/WELhXfbovncikdJN/KOG3k3yWlBKj/tm/2HtAbqeygr7zQvxILkOY2eeXQGSakPXWhazzClo7rqcEiKWDhuPjL7Jy+v9hj7nOL0l48EyqTR68O+H25ESuVO55C/VE416O8iEXyO1n88eT7qdyzCcC+Mank58rH68U0UWR4DLEyCAIMy7', NULL, NULL, NULL, NULL, 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 500, 0, 0, 0.00, 'false', 'Purple', 'true', 'true', 'false', '2006-01-01T00:00:00.000Z', 'India', 'Confirmed', 'false', '2018-01-16T12:14:45.000Z', 'true', 'true', 'false', 'true', 'false', 'OptIn', 'false', 'NotEligible', 'false', 'NothingOnFile', 'false', 'true', '30240', 0, 3000, 75, '2018-06-19 13:24:04', '2018-06-19 13:24:04'),
(5, 8, 'Invalid Request', 'testuser_kalpesh', 'AgAAAA**AQAAAA**aAAAAA**YQQpWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ**gHMEAA**AAMAAA**8wdbt8tMP5P67DNrZeLGh+k6RBRvWrq8dRwxuydLipPzi8mBndbr7/4m4RvE+9Ss1sXzlqyYO3nwExr6NfmN72kEqQP+qNzlyA1xN8I7LejHB5bWyS0QuA9FYBoOeVZWHtpzMUbXZPD4j7G5Q9a09tbHmRQC7yfGeQOUlOUtNXUqSowsnmeomgGPqKuhwmCjobmhYY2Gz357pqsWcIQ995z+GaTsC+iGl4BYVA6/zXYVZoRwcgFAljIpWMegCfu1REW/7Mh0olREEfqWCfi05CRZIPxLeyltadwJAJLkgMOepUTsKVAGy3AB4c7PTgkh2GlVagsbCtyjfjA9wQWfFIvUrSI8Jy9xZMZ587A2itGPNuLXi83ptR+mA3m4FckeMI4clq8zYhR59MCcLyr2bjZgSFKCZ31iXSoekbhWBgnKh8zEYAdAWh8JijxdLI/0+Ia+AcI0RCVTSeSlto0h4INofjtW0CCm5asiWr0Hnv2wjBx7sAMGEeAxixKLHwWkAbruOdXkA1emGkkXW/6zjMEB8vL+Bk+DmQ8Q2bC82tQjlnLvzMkqqd1xlCzFiis6lW/6ypPhnV797+k82+3hIqOfPXtYu8H8+BxmeGi5rxWfTOZh0R3z3FJPX3WZdITWVvsywngjBxAEneuhEJ4SMTUiMY3i44djsSkuZYnfId5t4q7nrUsA0Nw0L0Usc4Y4NVOaQGDGoHP4XmHA0aTtbmJ/9nF1WzN6hj/GGm7A44h95GD6mgrflzT8c37byFRO', NULL, NULL, NULL, NULL, 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 0, 0, 0, 0.00, 'false', 'None', 'false', 'true', 'false', '2018-01-17T10:44:00.000Z', 'India', 'Confirmed', 'false', '2018-01-17T10:44:00.000Z', 'true', 'true', 'false', 'true', 'false', 'OptIn', 'false', 'NotEligible', 'false', '', 'false', 'false', '30240', 0, 3000, 0, '2018-06-19 13:25:56', '2018-06-19 13:25:56'),
(6, 9, 'test.cosmonautgroup@gmail.com', 'testuser_cosmonautgroup', 'AgAAAA**AQAAAA**aAAAAA**YggpWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ**gHMEAA**AAMAAA**Ej0uxSLGchr9zf4E+8gtDKq+cL1GihgaD8l0B8/5cgChnzRY5VRrNu7PcxVZuPHM0WTdPYZ5rb4IvdCY6zbIZKAa4XELsUVoMYMD3cbIi2pZmuRPFsin4aup2Cuwxx5FajkNwpiSueWbiKjWpBJJNuZVGL6+TQDCtjGpI4oV+Rm/A9eYmIlfq1oriH9IURiQLUD/hPKXasfeyJTgr6E8Zu3wf/lDELesnpwDqYCk1FFbBUEuHniesfdU3Zx7EXI1RDsA01ndUmjCEY0QLSsDQF459KNaKdXvyF1X3GUGV+QEgnicmP5YVfA5HTIahl+5iIj6sZW5e6yMkrz5VJOKMHtI0lBmjHa91AASY9PvlOMNV2nz0pQzaT2/xOAKal9xJSEhqXLSXHLH0SPb0EBOacmaJ3wTHBaiaOpJ5i5hLNhNeRTjp+KMzOR9STKcLE3oIt6OYI4fo7uFfuyq+vYzyGz890TdaEC2EPNUtubK2VfuCIYMcKltCv8IUxvorfa2u1xzlBF+TMtPvbfh1w3Y5Z3nWSvnb15EZzvQR9X2Vq+Ea2Jh2k6cGWqOXJrCbbqA/hVTXDCJI25sAWn/WELhXfbovncikdJN/KOG3k3yWlBKj/tm/2HtAbqeygr7zQvxILkOY2eeXQGSakPXWhazzClo7rqcEiKWDhuPjL7Jy+v9hj7nOL0l48EyqTR68O+H25ESuVO55C/VE416O8iEXyO1n88eT7qdyzCcC+Mank58rH68U0UWR4DLEyCAIMy7', NULL, NULL, NULL, NULL, 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 500, 0, 0, 0.00, 'false', 'Purple', 'true', 'true', 'false', '2006-01-01T00:00:00.000Z', 'India', 'Confirmed', 'false', '2018-01-16T12:14:45.000Z', 'true', 'true', 'false', 'true', 'false', 'OptIn', 'false', 'NotEligible', 'false', 'NothingOnFile', 'false', 'true', '30240', 0, 3000, 75, '2018-06-19 13:43:06', '2018-06-19 13:43:06'),
(7, 9, 'Invalid Request', 'testuser_kalpesh', 'AgAAAA**AQAAAA**aAAAAA**DQopWw**nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ**gHMEAA**AAMAAA**8wdbt8tMP5P67DNrZeLGh+k6RBRvWrq8dRwxuydLipPzi8mBndbr7/4m4RvE+9Ss1sXzlqyYO3nwExr6NfmN72kEqQP+qNzlyA1xN8I7LejHB5bWyS0QuA9FYBoOeVZWHtpzMUbXZPD4j7G5Q9a09tbHmRQC7yfGeQOUlOUtNXUqSowsnmeomgGPqKuhwmCjobmhYY2Gz357pqsWcIQ995z+GaTsC+iGl4BYVA6/zXYVZoRwcgFAljIpWMegCfu1REW/7Mh0olREEfqWCfi05CRZIPxLeyltadwJAJLkgMOepUTsKVAGy3AB4c7PTgkh2GlVagsbCtyjfjA9wQWfFIvUrSI8Jy9xZMZ587A2itGPNuLXi83ptR+mA3m4FckeMI4clq8zYhR59MCcLyr2bjZgSFKCZ31iXSoekbhWBgnKh8zEYAdAWh8JijxdLI/0+Ia+AcI0RCVTSeSlto0h4INofjtW0CCm5asiWr0Hnv2wjBx7sAMGEeAxixKLHwWkAbruOdXkA1emGkkXW/6zjMEB8vL+Bk+DmQ8Q2bC82tQjlnLvzMkqqd1xlCzFiis6lW/6ypPhnV797+k82+3hIqOfPXtYu8H8+BxmeGi5rxWfTOZh0R3z3FJPX3WZdITWVvsywngjBxAEneuhEJ4SMTUiMY3i44djsSkuZYnfId5t4q7nrUsA0Nw0L0Usc4Y4NVOaQGDGoHP4XmHA0aTtbmJ/9nF1WzN6hj/GGm7A44h95GD6mgrflzT8c37byFRO', NULL, NULL, NULL, NULL, 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 0, 0, 0, 0.00, 'false', 'None', 'false', 'true', 'false', '2018-01-17T10:44:00.000Z', 'India', 'Confirmed', 'false', '2018-01-17T10:44:00.000Z', 'true', 'true', 'false', 'true', 'false', 'OptIn', 'false', 'NotEligible', 'false', '', 'false', 'false', '30240', 0, 3000, 0, '2018-06-19 13:50:10', '2018-06-19 13:50:10');

-- --------------------------------------------------------

--
-- Table structure for table `action_log`
--

CREATE TABLE `action_log` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `action` varchar(255) NOT NULL,
  `ip_address` varchar(55) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `action_log`
--

INSERT INTO `action_log` (`id`, `user_id`, `account_id`, `action`, `ip_address`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 'Order Detail Page Opened.', '192.168.1.5', '2018-06-25 19:22:41', NULL),
(2, 1, 1, 'Order Detail Page Opened.', '192.168.1.5', '2018-06-25 19:23:00', NULL),
(3, 1, 1, 'Order Detail Page Opened.', '192.168.1.5', '2018-06-25 19:24:07', NULL),
(4, 1, 1, 'Order Imported.', '192.168.1.5', '2018-06-26 17:59:58', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `amazon_account`
--

CREATE TABLE `amazon_account` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `purchase_account` int(11) DEFAULT NULL,
  `zip_code` int(11) DEFAULT NULL,
  `country` varchar(13) DEFAULT NULL,
  `phone_number` varchar(20) DEFAULT NULL,
  `Full_name` varchar(50) DEFAULT NULL,
  `prime_account` varchar(5) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `amazon_account`
--

INSERT INTO `amazon_account` (`id`, `user_id`, `account_id`, `purchase_account`, `zip_code`, `country`, `phone_number`, `Full_name`, `prime_account`, `created_at`, `updated_at`) VALUES
(1, 1, 1, 0, 380013, 'DE', '123465789', 'Chirag Parmar', 'false', '2018-06-30 16:40:26', '2018-06-30 19:29:24');

-- --------------------------------------------------------

--
-- Table structure for table `buyer`
--

CREATE TABLE `buyer` (
  `id` int(11) NOT NULL,
  `user_first_name` varchar(255) DEFAULT NULL,
  `user_last_name` varchar(255) DEFAULT NULL,
  `email` varchar(50) DEFAULT NULL,
  `buyer_user_id` varchar(50) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `contact_name` varchar(50) DEFAULT NULL,
  `street1` varchar(255) DEFAULT NULL,
  `street2` varchar(255) DEFAULT NULL,
  `city` varchar(255) DEFAULT NULL,
  `state` varchar(255) DEFAULT NULL,
  `zip` varchar(255) DEFAULT NULL,
  `country` varchar(255) DEFAULT NULL,
  `phone2` varchar(50) DEFAULT NULL,
  `secondary_email` varchar(255) DEFAULT NULL,
  `eiast_token` text,
  `feedback_score` int(11) DEFAULT NULL,
  `unique_negative_feedback_count` int(11) DEFAULT NULL,
  `unique_positive_feedback_count` int(11) DEFAULT NULL,
  `positive_feedback_percent` double(10,2) DEFAULT NULL,
  `feedback_private` varchar(10) DEFAULT NULL,
  `feeddback_rating_star` varchar(10) DEFAULT NULL,
  `id_verified` varchar(10) DEFAULT NULL,
  `ebay_good_standing` varchar(10) DEFAULT NULL,
  `new_user` varchar(10) DEFAULT NULL,
  `registration_date` varchar(100) DEFAULT NULL,
  `site` varchar(11) DEFAULT NULL,
  `user_id_changed` varchar(10) DEFAULT NULL,
  `user_id_last_changed` varchar(100) DEFAULT NULL,
  `about_me_page` varchar(10) DEFAULT NULL,
  `vat_status` varchar(25) DEFAULT NULL,
  `business_role` varchar(100) DEFAULT NULL,
  `ebay_wiki_read_only` varchar(10) DEFAULT NULL,
  `motors_dealer` varchar(10) DEFAULT NULL,
  `unique_neutral_feedback_count` int(11) DEFAULT NULL,
  `enterprise_seller` varchar(10) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP,
  `status` int(11) NOT NULL DEFAULT '1'
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `buyer`
--

INSERT INTO `buyer` (`id`, `user_first_name`, `user_last_name`, `email`, `buyer_user_id`, `phone`, `contact_name`, `street1`, `street2`, `city`, `state`, `zip`, `country`, `phone2`, `secondary_email`, `eiast_token`, `feedback_score`, `unique_negative_feedback_count`, `unique_positive_feedback_count`, `positive_feedback_percent`, `feedback_private`, `feeddback_rating_star`, `id_verified`, `ebay_good_standing`, `new_user`, `registration_date`, `site`, `user_id_changed`, `user_id_last_changed`, `about_me_page`, `vat_status`, `business_role`, `ebay_wiki_read_only`, `motors_dealer`, `unique_neutral_feedback_count`, `enterprise_seller`, `created_at`, `updated_at`, `status`) VALUES
(1, 'kalpesh', 'patel', NULL, 'testuser_kalpesh', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 'test.cosmonautgroup@gmail.com', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 0, 0, 0, 0.00, 'false', 'None', 'false', 'true', 'false', '2018-01-17T10:44:00.000Z', 'India', 'false', '2018-01-17T10:44:00.000Z', 'false', 'VATTax', 'FullMarketPlaceParticipant', 'false', 'false', 0, 'false', '2018-06-15 07:07:58', '2018-06-26 12:29:50', 0);

-- --------------------------------------------------------

--
-- Table structure for table `check_out_status`
--

CREATE TABLE `check_out_status` (
  `id` int(11) NOT NULL,
  `ebay_payment_status` varchar(50) DEFAULT NULL,
  `last_modified_time` varchar(80) DEFAULT NULL,
  `payment_method` varchar(20) DEFAULT NULL,
  `status` varchar(20) DEFAULT NULL,
  `integrated_merchant_credit_card_enabled` varchar(20) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `check_out_status`
--

INSERT INTO `check_out_status` (`id`, `ebay_payment_status`, `last_modified_time`, `payment_method`, `status`, `integrated_merchant_credit_card_enabled`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 'NoPaymentFailure', '2018-02-24T04:12:55.000Z', 'None', 'Incomplete', 'false', 1, '2018-06-15 07:07:58.600916', '2018-06-15 07:07:58.600916'),
(2, 'NoPaymentFailure', '2018-02-25T09:27:35.000Z', 'None', 'Incomplete', 'false', 2, '2018-06-15 07:07:59.974393', '2018-06-15 07:07:59.974393'),
(3, 'NoPaymentFailure', '2018-02-25T09:50:23.000Z', 'None', 'Incomplete', 'false', 3, '2018-06-15 07:08:01.396173', '2018-06-15 07:08:01.396173'),
(4, 'PayPalPaymentInProcess', '2018-03-04T10:34:31.000Z', 'PayPal', 'Complete', 'false', 4, '2018-06-15 07:08:02.709631', '2018-06-15 07:08:02.709631'),
(5, 'NoPaymentFailure', '2018-04-11T09:44:54.000Z', 'None', 'Incomplete', 'false', 5, '2018-06-15 07:08:04.193654', '2018-06-15 07:08:04.193654'),
(6, 'NoPaymentFailure', '2018-04-11T09:47:07.000Z', 'None', 'Incomplete', 'false', 6, '2018-06-15 07:08:05.607648', '2018-06-15 07:08:05.607648'),
(7, 'NoPaymentFailure', '2018-04-24T11:10:45.000Z', 'None', 'Incomplete', 'false', 7, '2018-06-15 07:08:07.013486', '2018-06-15 07:08:07.013486'),
(8, 'NoPaymentFailure', '2018-04-28T07:27:34.000Z', 'None', 'Incomplete', 'false', 8, '2018-06-15 07:08:08.428447', '2018-06-15 07:08:08.428447'),
(9, 'PayPalPaymentInProcess', '2018-05-04T09:41:56.000Z', 'PayPal', 'Complete', 'false', 9, '2018-06-15 07:08:09.870763', '2018-06-15 07:08:09.870763'),
(10, 'NoPaymentFailure', '2018-05-10T05:32:11.000Z', 'None', 'Incomplete', 'false', 10, '2018-06-15 07:08:11.271313', '2018-06-15 07:08:11.271313'),
(11, 'NoPaymentFailure', '2018-05-22T06:25:02.000Z', 'None', 'Incomplete', 'false', 11, '2018-06-15 07:08:12.680125', '2018-06-15 07:08:12.680125'),
(12, 'NoPaymentFailure', '2018-05-25T08:27:50.000Z', 'None', 'Incomplete', 'false', 12, '2018-06-15 07:08:14.040238', '2018-06-15 07:08:14.040238'),
(13, 'NoPaymentFailure', '2018-05-25T09:02:54.000Z', 'None', 'Incomplete', 'false', 13, '2018-06-15 07:08:15.387528', '2018-06-15 07:08:15.387528'),
(14, 'NoPaymentFailure', '2018-06-20T04:38:25.000Z', 'None', 'Incomplete', 'false', 14, '2018-06-15 07:08:16.736833', '2018-06-15 07:08:16.736833'),
(15, 'NoPaymentFailure', '2018-05-27T06:41:22.000Z', 'None', 'Incomplete', 'false', 15, '2018-06-15 07:08:18.069898', '2018-06-15 07:08:18.069898'),
(16, 'NoPaymentFailure', '2018-05-30T06:58:52.000Z', 'None', 'Incomplete', 'false', 16, '2018-06-15 07:08:19.495646', '2018-06-15 07:08:19.495646'),
(17, 'NoPaymentFailure', '2018-06-20T04:56:44.000Z', 'None', 'Incomplete', 'false', 17, '2018-06-15 07:08:20.923190', '2018-06-15 07:08:20.923190');

-- --------------------------------------------------------

--
-- Table structure for table `email_templates`
--

CREATE TABLE `email_templates` (
  `id` bigint(20) NOT NULL,
  `title` varchar(255) NOT NULL,
  `subject` text NOT NULL,
  `body` blob NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `is_deleted` tinyint(1) NOT NULL DEFAULT '0',
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `feedback`
--

CREATE TABLE `feedback` (
  `id` int(11) NOT NULL,
  `commenting_user` varchar(55) DEFAULT NULL,
  `commenting_user_score` int(11) DEFAULT NULL,
  `comment_text` text,
  `comment_time` varchar(55) DEFAULT NULL,
  `comment_type` varchar(55) DEFAULT NULL,
  `item_id` varchar(22) DEFAULT NULL,
  `role` varchar(22) DEFAULT NULL,
  `feedback_id` varchar(22) DEFAULT NULL,
  `transaction_id` varchar(22) DEFAULT NULL,
  `order_line_item_id` varchar(55) DEFAULT NULL,
  `item_title` text,
  `item_price` varchar(22) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `item`
--

CREATE TABLE `item` (
  `id` int(11) NOT NULL,
  `item_id` varchar(50) DEFAULT NULL,
  `site` varchar(10) DEFAULT 'Australia',
  `title` varchar(255) DEFAULT NULL,
  `condition_id` varchar(20) DEFAULT NULL,
  `condition_display_name` text,
  `buy_it_now_price` varchar(50) DEFAULT NULL,
  `listing_duration` varchar(20) DEFAULT NULL,
  `listing_type` varchar(50) DEFAULT NULL,
  `time_left` varchar(20) DEFAULT NULL,
  `quantity_available` int(50) DEFAULT NULL,
  `sku` varchar(50) DEFAULT NULL,
  `picture_details` varchar(255) DEFAULT NULL,
  `status` int(11) DEFAULT '1',
  `description` text,
  `country` varchar(20) DEFAULT NULL,
  `currency` varchar(20) DEFAULT NULL,
  `hit_counter` varchar(50) DEFAULT NULL,
  `location` varchar(150) DEFAULT NULL,
  `payment_methods` varchar(255) DEFAULT NULL,
  `paypal_email_address` varchar(50) DEFAULT NULL,
  `category` varchar(20) DEFAULT NULL,
  `private_listing` varchar(20) DEFAULT NULL,
  `reserve_price` varchar(20) DEFAULT NULL,
  `revise_status` varchar(20) DEFAULT NULL,
  `ship_to_locations` varchar(50) DEFAULT NULL,
  `start_price` varchar(50) DEFAULT NULL,
  `hit_count` varchar(10) DEFAULT '0',
  `watch_count` int(50) NOT NULL DEFAULT '0',
  `postal_code` varchar(20) DEFAULT NULL,
  `dispatch_time_max` varchar(20) DEFAULT NULL,
  `buyer_guarantee_price` varchar(50) DEFAULT NULL,
  `ship_to_register_country` varchar(10) DEFAULT NULL,
  `condition_description` varchar(255) DEFAULT NULL,
  `hide_from_search` varchar(10) DEFAULT NULL,
  `out_of_stock_control` varchar(10) DEFAULT NULL,
  `returns_accepted_option` varchar(100) DEFAULT NULL,
  `returns_accepted` varchar(100) DEFAULT NULL,
  `shipping_irregular` varchar(10) DEFAULT NULL,
  `shipping_package` varchar(100) DEFAULT NULL,
  `weight_major` varchar(20) DEFAULT NULL,
  `weight_minor` varchar(20) DEFAULT NULL,
  `sub_title` varchar(255) DEFAULT NULL,
  `secondary_category` varchar(50) DEFAULT NULL,
  `length` varchar(50) DEFAULT NULL,
  `return_within` varchar(50) DEFAULT NULL,
  `refund_type` varchar(50) DEFAULT NULL,
  `return_paid_by` varchar(50) DEFAULT NULL,
  `restock_fee` varchar(20) DEFAULT NULL,
  `return_policy_detail` text,
  `handle_time` varchar(50) DEFAULT NULL,
  `cod_cost` varchar(20) DEFAULT NULL,
  `depth` varchar(10) DEFAULT NULL,
  `width` varchar(50) DEFAULT NULL,
  `weight` varchar(50) DEFAULT NULL,
  `vat` varchar(11) DEFAULT NULL,
  `payment_instruction` text,
  `restricted_to_business` varchar(10) DEFAULT NULL,
  `image` varchar(255) DEFAULT NULL,
  `international_shipping_service` varchar(500) DEFAULT NULL,
  `international_cost` varchar(20) DEFAULT NULL,
  `internationl_handling_cost` varchar(20) DEFAULT NULL,
  `international_free_shipping` varchar(20) DEFAULT NULL,
  `international_shipping_country` text,
  `supplier_product_link` text,
  `supplier_price` varchar(20) DEFAULT NULL,
  `supplier_shipping` varchar(20) DEFAULT NULL,
  `folder` int(11) DEFAULT NULL,
  `isOutOfStock` tinyint(1) DEFAULT '0',
  `outOfStockFrom` date DEFAULT NULL,
  `start_shell` varchar(11) DEFAULT NULL,
  `shell_start_date` varchar(55) DEFAULT NULL,
  `is_delete` int(11) NOT NULL DEFAULT '0',
  `user_id` int(11) NOT NULL DEFAULT '0',
  `account_id` int(11) NOT NULL,
  `is_block` int(11) NOT NULL DEFAULT '0' COMMENT '0 : unBlocked 1 : Blocked',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item`
--

INSERT INTO `item` (`id`, `item_id`, `site`, `title`, `condition_id`, `condition_display_name`, `buy_it_now_price`, `listing_duration`, `listing_type`, `time_left`, `quantity_available`, `sku`, `picture_details`, `status`, `description`, `country`, `currency`, `hit_counter`, `location`, `payment_methods`, `paypal_email_address`, `category`, `private_listing`, `reserve_price`, `revise_status`, `ship_to_locations`, `start_price`, `hit_count`, `watch_count`, `postal_code`, `dispatch_time_max`, `buyer_guarantee_price`, `ship_to_register_country`, `condition_description`, `hide_from_search`, `out_of_stock_control`, `returns_accepted_option`, `returns_accepted`, `shipping_irregular`, `shipping_package`, `weight_major`, `weight_minor`, `sub_title`, `secondary_category`, `length`, `return_within`, `refund_type`, `return_paid_by`, `restock_fee`, `return_policy_detail`, `handle_time`, `cod_cost`, `depth`, `width`, `weight`, `vat`, `payment_instruction`, `restricted_to_business`, `image`, `international_shipping_service`, `international_cost`, `internationl_handling_cost`, `international_free_shipping`, `international_shipping_country`, `supplier_product_link`, `supplier_price`, `supplier_shipping`, `folder`, `isOutOfStock`, `outOfStockFrom`, `start_shell`, `shell_start_date`, `is_delete`, `user_id`, `account_id`, `is_block`, `created_at`, `updated_at`) VALUES
(1, '110273865775', 'Australia', 'Men Slim Biker Denim Jeans Skinny Frayed Pants', '1000', 'Brand new', '13.99', 'GTC', 'FixedPriceItem', 'P23DT15H9M35S', 2000, '1456146fvfdv', 'http://ebayinventory.cosmonautgroup.com/public/images/products/1519015884.jpg', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Perth, WA', '{\"0\":\"PayPal\",\"1\":\"COD\"}', '', '171228', 'false', '0.0', '', 'AU', '13.99', '0', 0, '6000', '0', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Seller', 'Percent_15', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:07:51', '2018-07-06 01:56:34'),
(2, '110273866024', 'Australia', 'Men Slim Biker Denim Jeans Skinny Frayed Pants', '1000', 'Brand new', '10013.99', 'GTC', 'FixedPriceItem', 'P23DT15H24M32S', 2000, '1456146fvfdv', 'http://ebayinventory.cosmonautgroup.com/public/images/products/1519015884.jpg', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Perth, WA', '{\"0\":\"PayPal\",\"1\":\"COD\"}', '', '171228', 'false', '0.0', '', 'AU', '10013.99', '0', 0, '6000', '0', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'MailingBoxes', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Seller', 'Percent_15', '', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:07:52', '2018-06-26 12:53:19'),
(3, '110311440699', 'Australia', 'product on 22 may 2018', '1000', 'Brand New', '199.0', 'GTC', 'FixedPriceItem', 'P24DT19H51M15S', 18, '', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Gwabegar, NSW', '{\"0\":\"PayPal\"}', '', '45453', 'false', '0.0', '', 'AU', '199.0', '0', 0, '2356', '2', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'We offer a 30 days return policy from the receiving date.\nAll items purchased must be return in the same condition as they were delivered in and in original packaging. They can not have been assembled and disassembled.\nItems defective upon receipt must be packaged in their retail packaging as if new and returned with a detailed description of the problem.\nReturn shipping fees are not refundable.\nBuyer pays shipping both ways. 20% handling/restocking fee for returns - starting at time of sale.\nBuyers remorse, finding the item on sale or price variation is not a valid reason for return.\nReturn shipping is the responsibility of buyer unless the return is a result of our mistake .\nWe will pay for shipping replacement back to you if an exchange is requested.\n\nIf you have any questions please contact us through \"Ask seller a question\" link. \nWe will respond within 1 business day or less.\n\nAll returns must be done within 30 days of the date of purchase.\nPlease note that we cannot give refunds after the 30 day limit has expired.\nWhen sending in a return, please note that it can take up to 7 business days for the return to be processed.\nWe do our best to process as quickly as possible.', '2', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:07:54', '2018-07-06 01:56:28'),
(4, '110311869497', 'Australia', 'DELL MS116 USB OPTICAL MOUSE OEM FOR LAPTOP AND DESKTOP With 1 Years Waranty', '1000', 'Brand New', '265.0', 'GTC', 'FixedPriceItem', 'P26DT17H41M5S', 313, 'DELL MS116', '', 1, NULL, 'AT', 'AUD', 'NoHitCounter', 'Guntramsdorf', '{\"0\":\"PayPal\"}', '', '46693', 'false', '0.0', '', 'AU', '265.0', '0', 0, '2356', '0', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_14', 'MoneyBack', 'Buyer', 'NoRestockingFee', '3453b5 353245 dfgkjd gijdgfkdkg', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:07:55', '2018-06-26 12:53:24'),
(5, '110284009561', 'Australia', 'Set of 2 Premium Folding Rotatable Boat Seats Fishing Seat with Swivel Base Grey', '1000', 'Brand new', '236.68', 'GTC', 'FixedPriceItem', 'P1DT20H25M37S', 2, '272897824726', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'NSW,', '{\"0\":\"PayPal\",\"1\":\"COD\"}', '', '171228', 'false', '0.0', '', 'Worldwide', '236.68', '0', 0, '', '1', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Seller', 'Percent_20', 'There will be 20% restocking fee. Item have to be in original condition.', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:07:57', '2018-06-26 12:51:59'),
(6, '110284013864', 'Australia', 'Set of 2 Premium Folding Rotatable Boat Seats Fishing Seat with Swivel Base', '1000', 'Brand new', '236.68', 'GTC', 'FixedPriceItem', 'P1DT20H31M39S', 2, '272897824726', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'NSW,', '{\"0\":\"PayPal\",\"1\":\"COD\"}', '', '171228', 'false', '0.0', '', 'Worldwide', '236.68', '0', 0, '', '1', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Seller', 'Percent_20', 'There will be 20% restocking fee. Item have to be in original condition.', '1', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:07:58', '2018-06-26 12:52:02'),
(7, '110268877238', 'Australia', 'Canon EOS 700D 18MP DSLR Camera 51', '4000', 'Very good', '6000.0', 'GTC', 'FixedPriceItem', 'P17DT15H49M28S', 5, '272917790583', 'http://localhost/bitbucket/ebay-inventory/public/images/products/1518581213.jpg', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Goombungee, QLD', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '6000.0', '0', 0, '4354', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_60', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'tsatst', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:07:58', '2018-06-26 12:53:15'),
(8, '110258438609', 'Australia', 'Maternity Pillow Pregnancy Nursing Sleeping Body Support Feeding New', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P3DT20H15M', 15, '123456', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:07:59', '2018-06-26 12:52:06'),
(9, '110274449122', 'Australia', 'Lenovo Yoga 920 13.9\" Convertible i7 Quad 8th', '4000', 'Very good', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:07:59', '2018-06-22 04:36:33'),
(10, '110258439323', 'Australia', 'updateProduct', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P3DT20H23M34S', 15, '123456', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:01', '2018-06-26 12:52:09'),
(11, '110258789906', 'Australia', 'cosmonautproduct', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P4DT23H2M11S', 10, '1212', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:02', '2018-06-26 12:52:14'),
(12, '110276407048', 'Australia', 'iRobot Roomba 980 Robot Vacuum with Wi-Fi', '4000', 'Very good', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:02', '2018-06-26 12:28:31'),
(13, '110258789936', 'Australia', 'cosmo product', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P4DT23H10M3S', 15, '123456', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:03', '2018-06-26 12:52:16'),
(14, '110276418065', 'Australia', 'Demo for Duc', '1000', 'Brand new', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:04', '2018-06-26 12:28:44'),
(15, '110258790742', 'Australia', 'Bajaj GX1 500 W Mixer Grinder  (Black, 3 Jars)', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P5DT54M41S', 10, '1212', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:05', '2018-06-26 12:52:18'),
(16, '110258961759', 'Australia', 'Samsung Galaxy J7 - 6 (New 2015 Edition) (gold, 16 GB)  (2 GB RAM)', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P5DT19H51M14S', 25, 'yhj', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:06', '2018-06-26 12:52:21'),
(17, '110261379103', 'Australia', 'Fashion On Sky Synthetic Printed Salwar Suit Dupatta Material', '5000', 'Good', '499.0', 'GTC', 'FixedPriceItem', 'P13DT17H41M38S', 210, 'mkl45', 'http://localhost/bitbucket/ebay-inventory/public/images/products/1518580699.jpg', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Goombungee, QLD', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '499.0', '0', 0, '4354', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'PaddedBags', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Seller', 'Percent_20', 'nop', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:07', '2018-06-26 12:53:07'),
(18, '110258963221', 'Australia', 'Product for Duc', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P5DT20H26M56S', 10, 'product_duc_demo', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '16135', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'PaddedBags', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:07', '2018-06-26 12:52:23'),
(19, '110259313412', 'Australia', 'Honor 9 Lite (Midnight Black, 64 GB)  (4 GB RAM)', '5000', 'Good', '11555.0', 'GTC', 'FixedPriceItem', 'P8DT15H25M42S', 101, 'yhj', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '279', 'false', '0.0', '', 'AU', '11555.0', '0', 0, '2212', '3', '20000.0', NULL, 'Like new', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'PaddedBags', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:08', '2018-06-26 12:52:40'),
(20, '110258974469', 'Australia', 'Moto G5 Plus (Fine Gold, 32 GB)  (4 GB RAM)', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P5DT23H15M26S', 100, 'adeg', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:09', '2018-06-26 12:52:26'),
(21, '110258974544', 'Australia', 'Fogg Fresh Oriental Body Spray - For Men  (150 ml)', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P5DT23H24M56S', 15, '343574', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:10', '2018-06-26 12:52:28'),
(22, '110307922955', 'Australia', 'MI note 5 pro', '1000', 'Brand New', NULL, NULL, NULL, NULL, NULL, NULL, NULL, 1, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, '0', 0, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:11', '2018-06-26 12:29:08'),
(23, '110258974571', 'Australia', 'Mi A1 (Black, 64 GB)  (4 GB RAM)', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P5DT23H29M11S', 50, 'gfp34', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:12', '2018-06-26 12:52:30'),
(24, '110309097670', 'Australia', 'kusfgkeuryqiwum fd,wkqjrhlwqihrwejhr', '1500', 'New without box', '26.99', 'GTC', 'FixedPriceItem', 'P14DT21H10M30S', 452, '', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Gwabegar, NSW', '{\"0\":\"PayPal\",\"1\":\"CashOnPickup\"}', '', '57929', 'false', '0.0', '', 'AU', '26.99', '0', 0, '2356', '3', '20000.0', NULL, '', 'false', '', 'ReturnsNotAccepted', 'ReturnsNotAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, '', '', '', '', '', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:12', '2018-06-26 12:53:09'),
(25, '110258974708', 'Australia', 'Moto C Plus (Starry Black, 16 GB)  (2 GB RAM)', '4000', 'Very good', '60.0', 'GTC', 'FixedPriceItem', 'P6DT4M36S', 15, '343574', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:13', '2018-06-26 12:52:33'),
(26, '110258983594', 'Australia', 'Moto E4 Plus', '2750', 'Like new', '500.0', 'GTC', 'FixedPriceItem', 'P6DT2H3M22S', 5, '123456', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '279', 'false', '0.0', '', 'AU', '500.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:14', '2018-06-26 12:52:35'),
(27, '110259098984', 'Australia', 'Test Product Saturday', '1000', 'Brand new', '60.0', 'GTC', 'FixedPriceItem', 'P6DT16H20M4S', 50, 'test23234234', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Revesby, NSW', '{\"0\":\"PayPal\"}', '', '171228', 'false', '0.0', '', 'AU', '60.0', '0', 0, '2212', '3', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'If you are not satisfied, return the Product for refund.', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:16', '2018-06-26 12:52:37'),
(28, '110259895973', 'Australia', 'demo productsdf', '2000', 'Manufacturer refurbished', '6999.0', 'GTC', 'FixedPriceItem', 'P11DT17H22M53S', 60, '56435xdvc', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Ngaanyatjarra-Giles, NT', '{\"0\":\"PayPal\"}', '', '111422', 'false', '0.0', '', 'AU', '6999.0', '0', 0, '0872', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'ToughBags', '21', '0', NULL, NULL, NULL, 'Days_60', 'MoneyBack', 'Seller', 'NoRestockingFee', 'sdfsfgdfgdf', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:18', '2018-06-26 12:52:42'),
(29, '110308791011', 'Australia', 'new product for test on 10-05-2018', '1000', 'Brand New', '65.0', 'GTC', 'FixedPriceItem', 'P12DT20H59M22S', 10, '', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Gwabegar, NSW', '{\"0\":\"PayPal\"}', '', '179697', 'false', '0.0', '', 'AU', '65.0', '0', 0, '2356', '0', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', 'MoneyBack', 'Buyer', 'Percent_10', 'We offer a 30 days return policy from the receiving date.\nAll items purchased must be return in the same condition as they were delivered in and in original packaging. They can not have been assembled and disassembled.\nItems defective upon receipt must be packaged in their retail packaging as if new and returned with a detailed description of the problem.\nReturn shipping fees are not refundable.\nBuyer pays shipping both ways. 20% handling/restocking fee for returns - starting at time of sale.\nBuyers remorse, finding the item on sale or price variation is not a valid reason for return.\nReturn shipping is the responsibility of buyer unless the return is a result of our mistake .\nWe will pay for shipping replacement back to you if an exchange is requested.\n\nIf you have any questions please contact us through \"Ask seller a question\" link. \nWe will respond within 1 business day or less.\n\nAll returns must be done within 30 days of the date of purchase.\nPlease note that we cannot give refunds after the 30 day limit has expired.\nWhen sending in a return, please note that it can take up to 7 business days for the return to be processed.\nWe do our best to process as quickly as possible.', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:19', '2018-06-26 12:53:04'),
(30, '110259896186', 'Australia', 'demo productsdf nsdfg', '2000', 'Manufacturer refurbished', '6999.0', 'GTC', 'FixedPriceItem', 'P11DT17H26M29S', 60, '56435xdvc', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Ngaanyatjarra-Giles, NT', '{\"0\":\"PayPal\"}', '', '111422', 'false', '0.0', '', 'AU', '6999.0', '0', 0, '0872', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'ToughBags', '21', '0', NULL, NULL, NULL, 'Days_14', 'MoneyBack', 'Seller', 'NoRestockingFee', 'sdfsfgdfgdf', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:20', '2018-06-26 12:52:45'),
(31, '110259896220', 'Australia', 'productsdf nsdfg', '2000', 'Manufacturer refurbished', '6999.0', 'GTC', 'FixedPriceItem', 'P11DT17H27M8S', 60, '56435xdvc', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Ngaanyatjarra-Giles, NT', '{\"0\":\"PayPal\"}', '', '111422', 'false', '0.0', '', 'AU', '6999.0', '0', 0, '0872', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'ToughBags', '21', '0', NULL, NULL, NULL, 'Days_14', 'MoneyBack', 'Seller', 'NoRestockingFee', 'sdfsfgdfgdf', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:21', '2018-06-26 12:52:47'),
(32, '110259896427', 'Australia', 'productsdf nsfg', '2000', 'Manufacturer refurbished', '6999.0', 'GTC', 'FixedPriceItem', 'P11DT17H30M28S', 60, '56435xdvc', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Ngaanyatjarra-Giles, NT', '{\"0\":\"PayPal\"}', '', '111422', 'false', '0.0', '', 'AU', '6999.0', '0', 0, '0872', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'ToughBags', '21', '0', NULL, NULL, NULL, 'Days_14', 'MoneyBack', 'Seller', 'NoRestockingFee', 'sdfsfgdfgdf', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:22', '2018-06-26 12:52:49'),
(33, '110259896494', 'Australia', 'productsdf fg', '2000', 'Manufacturer refurbished', '6999.0', 'GTC', 'FixedPriceItem', 'P11DT17H31M42S', 60, '56435xdvc', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Ngaanyatjarra-Giles, NT', '{\"0\":\"PayPal\"}', '', '111422', 'false', '0.0', '', 'AU', '6999.0', '0', 0, '0872', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'ToughBags', '21', '0', NULL, NULL, NULL, 'Days_14', 'MoneyBack', 'Seller', 'NoRestockingFee', 'sdfsfgdfgdf', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:24', '2018-06-26 12:52:52'),
(34, '110259896555', 'Australia', 'productsdfju', '2000', 'Manufacturer refurbished', '6999.0', 'GTC', 'FixedPriceItem', 'P11DT17H33M29S', 60, '56435xdvc', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Ngaanyatjarra-Giles, NT', '{\"0\":\"PayPal\"}', '', '111422', 'false', '0.0', '', 'AU', '6999.0', '0', 0, '0872', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'ToughBags', '21', '0', NULL, NULL, NULL, 'Days_14', 'MoneyBack', 'Seller', 'NoRestockingFee', 'sdfsfgdfgdf', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:25', '2018-06-26 12:52:54'),
(35, '110259896600', 'Australia', 'product on 12th feb', '1000', 'Brand New', '6999.0', 'GTC', 'FixedPriceItem', 'P11DT17H36M51S', 60, '56435xdvc', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Ngaanyatjarra-Giles, NT', '{\"0\":\"PayPal\"}', '', '111422', 'false', '0.0', '', 'AU', '6999.0', '0', 0, '0872', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'ToughBags', '21', '0', NULL, NULL, NULL, 'Days_14', 'MoneyBack', 'Seller', 'NoRestockingFee', 'sdfsfgdfgdf', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:26', '2018-06-26 12:52:57'),
(36, '110259925992', 'Australia', 'prctsfjury', '1000', 'Brand New', '5999.0', 'GTC', 'FixedPriceItem', 'P11DT21H18M34S', 35, '56435xdvc', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Ngaanyatjarra-Giles, NT', '{\"0\":\"PayPal\"}', '', '116674', 'false', '0.0', '', 'AU', '5999.0', '0', 0, '0872', '5', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'PaddedBags', '21', '0', NULL, NULL, NULL, 'Days_14', 'MoneyBack', 'Seller', 'NoRestockingFee', 'sdfsfgdfgdf', '5', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:28', '2018-06-26 12:52:59'),
(37, '110308790228', 'Australia', 'new add form testing 123456789', '2000', 'Manufacturer refurbished', '75.0', 'GTC', 'FixedPriceItem', 'P12DT20H38M47S', 10, '', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Gwabegar, NSW', '{\"0\":\"PayPal\",\"1\":\"CashOnPickup\"}', '', '133708', 'false', '0.0', '', 'AU', '75.0', '0', 0, '2356', '0', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_14', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Maecenas feugiat consequat diam. Maecenas metus. Vivamus diam purus, cursus a, commodo non, facilisis vitae, nulla. Aenean dictum lacinia tortor. Nunc iaculis, nibh non iaculis aliquam, orci felis euismod neque.', '0', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:29', '2018-06-26 12:53:02'),
(38, '110268834000', 'Australia', 'Canon EOS 700D 18MP DSLR Camera', '5000', 'Good', '6000.0', 'GTC', 'FixedPriceItem', 'P17DT15H16M30S', 8, '272917790583', 'http://localhost/bitbucket/ebay-inventory/public/images/products/1518584730.jpg', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Goombungee, QLD', '{\"0\":\"PayPal\",\"1\":\"COD\"}', '', '171228', 'false', '0.0', '', 'AU', '6000.0', '0', 0, '4354', '3', '20000.0', NULL, 'item in good condition. with defacts', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'ToughBags', '0', '0', NULL, NULL, NULL, 'Days_60', 'MoneyBack', 'Buyer', 'NoRestockingFee', 'tsatst', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-15 14:08:35', '2018-06-26 12:53:12'),
(39, '110329661815', 'Australia', 'new product for variation123', '1500', 'New: Never Used', '25000.0', 'Days_7', 'FixedPriceItem', 'PT22H24M56S', 10, '', '', 1, NULL, 'AU', 'AUD', 'NoHitCounter', 'Gwabegar, NSW', '{\"0\":\"PayPal\",\"1\":\"CashOnPickup\"}', '', '9355', 'false', '0.0', '', 'AU', '25000.0', '0', 0, '2356', '3', '20000.0', NULL, 'New: Never Used', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', '', 'Buyer', 'NoRestockingFee', '', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-22 04:36:28', '2018-06-26 12:51:54'),
(40, '110329677846', 'Australia', 'Apple iPhone 6 (Gold, 32GB)', '1000', 'Brand New', '23000.0', 'Days_7', 'FixedPriceItem', 'P1DT1H14M51S', 16, '', '', 1, NULL, 'AT', 'AUD', 'NoHitCounter', 'Guntramsdorf', '{\"0\":\"PayPal\",\"1\":\"CashOnPickup\"}', '', '9355', 'false', '0.0', '', 'AU', '23000.0', '0', 0, '2356', '4', '20000.0', NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', '', 'Buyer', 'NoRestockingFee', '', '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-22 04:36:29', '2018-06-26 12:51:57'),
(41, '110329921482', 'Australia', 'product for template testing', '1000', 'Brand New', '50.0', 'Days_7', 'FixedPriceItem', 'P2DT23H36M39S', 1, '', '', 1, NULL, 'AT', 'AUD', NULL, 'Guntramsdorf', '{\"0\":\"PayPal\",\"1\":\"CashOnPickup\"}', '', '185039', 'false', '0.0', '', 'AU', '50.0', '0', 0, '2356', '3', NULL, NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', '', 'Buyer', 'NoRestockingFee', '', '3', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-26 12:52:03', '2018-06-26 12:52:04'),
(42, '110330105180', 'Australia', 'SanDisk Ultra 32 GB MicroSDHC Class 10 98 MB/s Memory Card', '1000', 'Brand New', '260.0', 'Days_7', 'FixedPriceItem', 'P4DT1H13M7S', 25, '', '', 1, NULL, 'AU', 'AUD', NULL, 'Gwabegar, NSW', '{\"0\":\"PayPal\",\"1\":\"CashOnPickup\"}', '', '185039', 'false', '0.0', '', 'AU', '260.0', '0', 0, '2356', '4', NULL, NULL, '', 'false', '', 'ReturnsAccepted', 'ReturnsAccepted', 'false', 'None', '0', '0', NULL, NULL, NULL, 'Days_30', '', 'Buyer', 'NoRestockingFee', '', '4', NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, NULL, 0, NULL, NULL, NULL, 0, 1, 1, 0, '2018-06-26 12:52:10', '2018-06-26 12:52:11');

-- --------------------------------------------------------

--
-- Table structure for table `item_has_price`
--

CREATE TABLE `item_has_price` (
  `id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `item_id` varchar(20) NOT NULL,
  `old_price` double(10,2) DEFAULT '0.00',
  `new_price` double(10,2) NOT NULL DEFAULT '0.00',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_has_price`
--

INSERT INTO `item_has_price` (`id`, `account_id`, `item_id`, `old_price`, `new_price`, `created_at`, `updated_at`) VALUES
(1, 1, '110273865775', 10.54, 13.99, '2018-06-29 15:38:26', '0000-00-00 00:00:00'),
(2, 1, '110311440699', 165.00, 150.00, '2018-06-29 15:38:32', '2018-06-29 15:57:22'),
(3, 1, '110274449122', NULL, 1399.99, '2018-06-29 15:39:26', '0000-00-00 00:00:00'),
(4, 1, '110276407048', NULL, 499.00, '2018-06-29 15:39:30', '0000-00-00 00:00:00'),
(5, 1, '110276418065', NULL, 500.00, '2018-06-29 15:39:33', '0000-00-00 00:00:00'),
(6, 1, '110307922955', NULL, 250.00, '2018-06-29 15:39:36', '0000-00-00 00:00:00'),
(7, 1, '110274449122', NULL, 1399.99, '2018-06-29 15:40:21', '0000-00-00 00:00:00'),
(8, 1, '110276407048', NULL, 499.00, '2018-06-29 15:40:24', '0000-00-00 00:00:00'),
(9, 1, '110276418065', NULL, 500.00, '2018-06-29 15:40:28', '0000-00-00 00:00:00'),
(10, 1, '110307922955', NULL, 250.00, '2018-06-29 15:40:31', '0000-00-00 00:00:00'),
(11, 1, '110274449122', NULL, 1399.99, '2018-06-29 16:38:28', '0000-00-00 00:00:00'),
(12, 1, '110276407048', NULL, 499.00, '2018-06-29 16:38:31', '0000-00-00 00:00:00'),
(13, 1, '110276418065', NULL, 500.00, '2018-06-29 16:38:34', '0000-00-00 00:00:00'),
(14, 1, '110307922955', NULL, 250.00, '2018-06-29 16:38:37', '0000-00-00 00:00:00'),
(15, 1, '110273865775', 10.54, 13.99, '2018-06-30 10:37:14', '0000-00-00 00:00:00'),
(16, 1, '110273866024', 12000.99, 10013.99, '2018-06-30 10:37:17', '0000-00-00 00:00:00'),
(17, 1, '110311440699', 205.00, 199.00, '2018-06-30 10:37:20', '0000-00-00 00:00:00'),
(18, 1, '110284009561', 220.68, 236.68, '2018-06-30 10:37:25', '0000-00-00 00:00:00'),
(19, 1, '110274449122', NULL, 1399.99, '2018-06-30 10:38:46', '0000-00-00 00:00:00'),
(20, 1, '110276407048', NULL, 499.00, '2018-06-30 10:38:49', '0000-00-00 00:00:00'),
(21, 1, '110276418065', NULL, 500.00, '2018-06-30 10:38:52', '0000-00-00 00:00:00'),
(22, 1, '110307922955', NULL, 250.00, '2018-06-30 10:38:54', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `item_shipping_detail`
--

CREATE TABLE `item_shipping_detail` (
  `id` int(11) NOT NULL,
  `sales_tax_percent` varchar(10) DEFAULT NULL,
  `shipping_included_in_tax` varchar(10) DEFAULT NULL,
  `shipping_service` varchar(150) DEFAULT NULL,
  `shipping_service_cost` varchar(10) DEFAULT '0.00',
  `shipping_service_priority` varchar(10) DEFAULT NULL,
  `free_shipping` varchar(10) DEFAULT NULL,
  `shipping_type` varchar(100) DEFAULT NULL,
  `additional_cost` varchar(20) DEFAULT NULL,
  `offer_local_pickup` varchar(10) DEFAULT NULL,
  `local_pickup_fee` varchar(25) DEFAULT NULL,
  `domestic_shipping` varchar(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `created_st` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `item_shipping_detail`
--

INSERT INTO `item_shipping_detail` (`id`, `sales_tax_percent`, `shipping_included_in_tax`, `shipping_service`, `shipping_service_cost`, `shipping_service_priority`, `free_shipping`, `shipping_type`, `additional_cost`, `offer_local_pickup`, `local_pickup_fee`, `domestic_shipping`, `item_id`, `created_st`, `updated_at`) VALUES
(1, '0.0', 'false', 'AU_CouriersPlease', '2.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 1, '2018-06-15 07:07:51', NULL),
(2, '0.0', 'false', 'AU_CouriersPlease', '2.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 2, '2018-06-15 07:07:52', NULL),
(3, '0.0', 'false', 'AU_StandardDelivery', '5.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 3, '2018-06-15 07:07:54', NULL),
(4, '0.0', 'false', 'AU_Regular', '0.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 4, '2018-06-15 07:07:55', NULL),
(5, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 5, '2018-06-15 07:07:57', NULL),
(6, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 6, '2018-06-15 07:07:58', NULL),
(7, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 8, '2018-06-15 07:07:59', NULL),
(8, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 10, '2018-06-15 07:08:01', NULL),
(9, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 11, '2018-06-15 07:08:02', NULL),
(10, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 13, '2018-06-15 07:08:03', NULL),
(11, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 15, '2018-06-15 07:08:05', NULL),
(12, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 16, '2018-06-15 07:08:06', NULL),
(13, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 18, '2018-06-15 07:08:07', NULL),
(14, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 20, '2018-06-15 07:08:09', NULL),
(15, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 21, '2018-06-15 07:08:10', NULL),
(16, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 23, '2018-06-15 07:08:12', NULL),
(17, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 25, '2018-06-15 07:08:13', NULL),
(18, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 26, '2018-06-15 07:08:14', NULL),
(19, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 27, '2018-06-15 07:08:16', NULL),
(20, '0.0', 'false', 'AU_StandardDelivery', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 19, '2018-06-15 07:08:17', NULL),
(21, '0.0', 'false', 'AU_StandardDelivery', '50.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 28, '2018-06-15 07:08:18', NULL),
(22, '0.0', 'false', 'AU_StandardDelivery', '50.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 30, '2018-06-15 07:08:20', NULL),
(23, '0.0', 'false', 'AU_StandardDelivery', '50.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 31, '2018-06-15 07:08:21', NULL),
(24, '0.0', 'false', 'AU_StandardDelivery', '50.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 32, '2018-06-15 07:08:22', NULL),
(25, '0.0', 'false', 'AU_StandardDelivery', '50.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 33, '2018-06-15 07:08:24', NULL),
(26, '0.0', 'false', 'AU_StandardDelivery', '50.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 34, '2018-06-15 07:08:25', NULL),
(27, '0.0', 'false', 'AU_StandardDelivery', '50.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 35, '2018-06-15 07:08:27', NULL),
(28, '0.0', 'false', 'AU_StandardDeliveryRegisted', '50.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 36, '2018-06-15 07:08:28', NULL),
(29, '0.0', 'false', 'AU_OfficeworksMailman', '5.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 37, '2018-06-15 07:08:29', NULL),
(30, '0.0', 'false', 'AU_StandardDeliveryRegisted', '0.0', '1', 'true', 'Flat', NULL, NULL, NULL, NULL, 29, '2018-06-15 07:08:30', NULL),
(31, '0.0', 'false', 'AU_RegisteredParcelPostPrepaidSatchel3kg', '60.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 17, '2018-06-15 07:08:32', NULL),
(32, '0.0', 'false', 'AU_OfficeworksMailman', '25.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 24, '2018-06-15 07:08:33', NULL),
(33, '0.0', 'false', 'AU_OfficeworksMailman', '20.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 38, '2018-06-15 07:08:35', NULL),
(34, '0.0', 'false', 'AU_OfficeworksMailman', '20.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 7, '2018-06-15 07:08:36', NULL),
(35, '0.0', 'false', 'AU_OfficeworksMailman', '5.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 39, '2018-06-21 21:36:28', NULL),
(36, '0.0', 'false', 'AU_StandardDeliveryRegisted', '50.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 40, '2018-06-21 21:36:29', NULL),
(37, '0.0', 'false', 'AU_OfficeworksMailman', '5.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 41, '2018-06-26 18:22:03', NULL),
(38, '0.0', 'false', 'AU_OfficeworksMailman', '5.0', '1', '', 'Flat', NULL, NULL, NULL, NULL, 42, '2018-06-26 18:22:11', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `listing_details`
--

CREATE TABLE `listing_details` (
  `id` int(50) NOT NULL,
  `converted_buy_it_now_price` varchar(50) DEFAULT NULL,
  `start_time` varchar(50) DEFAULT NULL,
  `view_item_url` varchar(255) DEFAULT NULL,
  `view_item_url_for_natural_search` varchar(255) DEFAULT NULL,
  `item_id` int(50) DEFAULT NULL,
  `converted_start_price` varchar(20) DEFAULT NULL,
  `converted_reserve_price` varchar(20) DEFAULT NULL,
  `has_reserve_price` varchar(10) DEFAULT NULL,
  `end_time` varchar(50) DEFAULT NULL,
  `has_public_message` varchar(10) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `listing_details`
--

INSERT INTO `listing_details` (`id`, `converted_buy_it_now_price`, `start_time`, `view_item_url`, `view_item_url_for_natural_search`, `item_id`, `converted_start_price`, `converted_reserve_price`, `has_reserve_price`, `end_time`, `has_public_message`, `created_at`, `updated_at`) VALUES
(1, '0.0', '2018-02-20T04:02:50.000Z', 'http://cgi.sandbox.ebay.com/Men-Slim-Biker-Denim-Jeans-Skinny-Frayed-Pants-/110273865775', 'http://cgi.sandbox.ebay.com/Men-Slim-Biker-Denim-Jeans-Skinny-Frayed-Pants-/110273865775', 1, '10.32', '0.0', 'false', '2018-07-20T04:02:50.000Z', 'false', '2018-06-15 07:07:51.630631', '2018-06-15 07:07:51.630631'),
(2, '0.0', '2018-02-20T04:17:49.000Z', 'http://cgi.sandbox.ebay.com/Men-Slim-Biker-Denim-Jeans-Skinny-Frayed-Pants-/110273866024', 'http://cgi.sandbox.ebay.com/Men-Slim-Biker-Denim-Jeans-Skinny-Frayed-Pants-/110273866024', 2, '7384.82', '0.0', 'false', '2018-07-20T04:17:49.000Z', 'false', '2018-06-15 07:07:52.953361', '2018-06-15 07:07:52.953361'),
(3, '0.0', '2018-05-22T08:44:35.000Z', 'http://cgi.sandbox.ebay.com/product-22-may-2018-/110311440699', 'http://cgi.sandbox.ebay.com/product-22-may-2018-/110311440699', 3, '146.75', '0.0', 'false', '2018-07-21T08:44:35.000Z', 'false', '2018-06-15 07:07:54.327816', '2018-06-15 07:07:54.327816'),
(4, '0.0', '2018-05-24T06:34:27.000Z', 'http://cgi.sandbox.ebay.com/DELL-MS116-USB-OPTICAL-MOUSE-OEM-LAPTOP-AND-DESKTOP-1-Years-Waranty-/110311869497', 'http://cgi.sandbox.ebay.com/DELL-MS116-USB-OPTICAL-MOUSE-OEM-LAPTOP-AND-DESKTOP-1-Years-Waranty-/110311869497', 4, '195.42', '0.0', 'false', '2018-07-23T06:34:27.000Z', 'false', '2018-06-15 07:07:55.779689', '2018-06-15 07:07:55.779689'),
(5, '0.0', '2018-03-30T09:17:34.000Z', 'http://cgi.sandbox.ebay.com/Set-2-Premium-Folding-Rotatable-Boat-Seats-Fishing-Seat-Swivel-Base-Grey-/110284009561', 'http://cgi.sandbox.ebay.com/Set-2-Premium-Folding-Rotatable-Boat-Seats-Fishing-Seat-Swivel-Base-Grey-/110284009561', 5, '174.54', '0.0', 'false', '2018-06-28T09:17:34.000Z', 'false', '2018-06-15 07:07:57.096068', '2018-06-15 07:07:57.096068'),
(6, '0.0', '2018-03-30T09:23:39.000Z', 'http://cgi.sandbox.ebay.com/Set-2-Premium-Folding-Rotatable-Boat-Seats-Fishing-Seat-Swivel-Base-/110284013864', 'http://cgi.sandbox.ebay.com/Set-2-Premium-Folding-Rotatable-Boat-Seats-Fishing-Seat-Swivel-Base-/110284013864', 6, '174.54', '0.0', 'false', '2018-06-28T09:23:39.000Z', 'false', '2018-06-15 07:07:58.498469', '2018-06-15 07:07:58.498469'),
(7, '0.0', '2018-01-31T09:07:04.000Z', 'http://cgi.sandbox.ebay.com/Maternity-Pillow-Pregnancy-Nursing-Sleeping-Body-Support-Feeding-New-/110258438609', 'http://cgi.sandbox.ebay.com/Maternity-Pillow-Pregnancy-Nursing-Sleeping-Body-Support-Feeding-New-/110258438609', 8, '44.25', '0.0', 'false', '2018-06-30T09:07:04.000Z', 'false', '2018-06-15 07:07:59.831154', '2018-06-15 07:07:59.831154'),
(8, '0.0', '2018-01-31T09:15:41.000Z', 'http://cgi.sandbox.ebay.com/updateProduct-/110258439323', 'http://cgi.sandbox.ebay.com/updateProduct-/110258439323', 10, '44.25', '0.0', 'false', '2018-06-30T09:15:41.000Z', 'false', '2018-06-15 07:08:01.187090', '2018-06-15 07:08:01.187090'),
(9, '0.0', '2018-02-01T11:54:23.000Z', 'http://cgi.sandbox.ebay.com/cosmonautproduct-/110258789906', 'http://cgi.sandbox.ebay.com/cosmonautproduct-/110258789906', 11, '44.25', '0.0', 'false', '2018-07-01T11:54:23.000Z', 'false', '2018-06-15 07:08:02.642154', '2018-06-15 07:08:02.642154'),
(10, '0.0', '2018-02-01T12:02:17.000Z', 'http://cgi.sandbox.ebay.com/cosmo-product-/110258789936', 'http://cgi.sandbox.ebay.com/cosmo-product-/110258789936', 13, '44.25', '0.0', 'false', '2018-07-01T12:02:17.000Z', 'false', '2018-06-15 07:08:03.964036', '2018-06-15 07:08:03.964036'),
(11, '0.0', '2018-02-01T13:46:57.000Z', 'http://cgi.sandbox.ebay.com/Bajaj-GX1-500-W-Mixer-Grinder-Black-3-Jars-/110258790742', 'http://cgi.sandbox.ebay.com/Bajaj-GX1-500-W-Mixer-Grinder-Black-3-Jars-/110258790742', 15, '44.25', '0.0', 'false', '2018-07-01T13:46:57.000Z', 'false', '2018-06-15 07:08:05.275683', '2018-06-15 07:08:05.275683'),
(12, '0.0', '2018-02-02T08:43:32.000Z', 'http://cgi.sandbox.ebay.com/Samsung-Galaxy-J7-6-New-2015-Edition-gold-16-GB-2-GB-RAM-/110258961759', 'http://cgi.sandbox.ebay.com/Samsung-Galaxy-J7-6-New-2015-Edition-gold-16-GB-2-GB-RAM-/110258961759', 16, '44.25', '0.0', 'false', '2018-07-02T08:43:32.000Z', 'false', '2018-06-15 07:08:06.643084', '2018-06-15 07:08:06.643084'),
(13, '0.0', '2018-02-02T09:19:17.000Z', 'http://cgi.sandbox.ebay.com/Product-Duc-/110258963221', 'http://cgi.sandbox.ebay.com/Product-Duc-/110258963221', 18, '44.25', '0.0', 'false', '2018-07-02T09:19:17.000Z', 'false', '2018-06-15 07:08:07.984721', '2018-06-15 07:08:07.984721'),
(14, '0.0', '2018-02-02T12:07:49.000Z', 'http://cgi.sandbox.ebay.com/Moto-G5-Plus-Fine-Gold-32-GB-4-GB-RAM-/110258974469', 'http://cgi.sandbox.ebay.com/Moto-G5-Plus-Fine-Gold-32-GB-4-GB-RAM-/110258974469', 20, '44.25', '0.0', 'false', '2018-07-02T12:07:49.000Z', 'false', '2018-06-15 07:08:09.351705', '2018-06-15 07:08:09.351705'),
(15, '0.0', '2018-02-02T12:17:22.000Z', 'http://cgi.sandbox.ebay.com/Fogg-Fresh-Oriental-Body-Spray-Men-150-ml-/110258974544', 'http://cgi.sandbox.ebay.com/Fogg-Fresh-Oriental-Body-Spray-Men-150-ml-/110258974544', 21, '44.25', '0.0', 'false', '2018-07-02T12:17:22.000Z', 'false', '2018-06-15 07:08:10.691629', '2018-06-15 07:08:10.691629'),
(16, '0.0', '2018-02-02T12:21:39.000Z', 'http://cgi.sandbox.ebay.com/Mi-A1-Black-64-GB-4-GB-RAM-/110258974571', 'http://cgi.sandbox.ebay.com/Mi-A1-Black-64-GB-4-GB-RAM-/110258974571', 23, '44.25', '0.0', 'false', '2018-07-02T12:21:39.000Z', 'false', '2018-06-15 07:08:12.092252', '2018-06-15 07:08:12.092252'),
(17, '0.0', '2018-02-02T12:57:07.000Z', 'http://cgi.sandbox.ebay.com/Moto-C-Plus-Starry-Black-16-GB-2-GB-RAM-/110258974708', 'http://cgi.sandbox.ebay.com/Moto-C-Plus-Starry-Black-16-GB-2-GB-RAM-/110258974708', 25, '44.25', '0.0', 'false', '2018-07-02T12:57:07.000Z', 'false', '2018-06-15 07:08:13.449515', '2018-06-15 07:08:13.449515'),
(18, '0.0', '2018-02-02T14:55:55.000Z', 'http://cgi.sandbox.ebay.com/Moto-E4-Plus-/110258983594', 'http://cgi.sandbox.ebay.com/Moto-E4-Plus-/110258983594', 26, '368.73', '0.0', 'false', '2018-07-02T14:55:55.000Z', 'false', '2018-06-15 07:08:14.809943', '2018-06-15 07:08:14.809943'),
(19, '0.0', '2018-02-03T05:12:39.000Z', 'http://cgi.sandbox.ebay.com/Test-Product-Saturday-/110259098984', 'http://cgi.sandbox.ebay.com/Test-Product-Saturday-/110259098984', 27, '44.25', '0.0', 'false', '2018-07-03T05:12:39.000Z', 'false', '2018-06-15 07:08:16.195536', '2018-06-15 07:08:16.195536'),
(20, '0.0', '2018-02-05T04:18:19.000Z', 'http://cgi.sandbox.ebay.com/Honor-9-Lite-Midnight-Black-64-GB-4-GB-RAM-/110259313412', 'http://cgi.sandbox.ebay.com/Honor-9-Lite-Midnight-Black-64-GB-4-GB-RAM-/110259313412', 19, '8521.23', '0.0', 'false', '2018-07-05T04:18:19.000Z', 'false', '2018-06-15 07:08:17.564942', '2018-06-15 07:08:17.564942'),
(21, '0.0', '2018-02-08T06:15:33.000Z', 'http://cgi.sandbox.ebay.com/demo-productsdf-/110259895973', 'http://cgi.sandbox.ebay.com/demo-productsdf-/110259895973', 28, '5161.41', '0.0', 'false', '2018-07-08T06:15:33.000Z', 'false', '2018-06-15 07:08:18.870579', '2018-06-15 07:08:18.870579'),
(22, '0.0', '2018-02-08T06:19:11.000Z', 'http://cgi.sandbox.ebay.com/demo-productsdf-nsdfg-/110259896186', 'http://cgi.sandbox.ebay.com/demo-productsdf-nsdfg-/110259896186', 30, '5161.41', '0.0', 'false', '2018-07-08T06:19:11.000Z', 'false', '2018-06-15 07:08:20.170808', '2018-06-15 07:08:20.170808'),
(23, '0.0', '2018-02-08T06:19:53.000Z', 'http://cgi.sandbox.ebay.com/productsdf-nsdfg-/110259896220', 'http://cgi.sandbox.ebay.com/productsdf-nsdfg-/110259896220', 31, '5161.41', '0.0', 'false', '2018-07-08T06:19:53.000Z', 'false', '2018-06-15 07:08:21.490577', '2018-06-15 07:08:21.490577'),
(24, '0.0', '2018-02-08T06:23:15.000Z', 'http://cgi.sandbox.ebay.com/productsdf-nsfg-/110259896427', 'http://cgi.sandbox.ebay.com/productsdf-nsfg-/110259896427', 32, '5161.41', '0.0', 'false', '2018-07-08T06:23:15.000Z', 'false', '2018-06-15 07:08:22.867464', '2018-06-15 07:08:22.867464'),
(25, '0.0', '2018-02-08T06:24:32.000Z', 'http://cgi.sandbox.ebay.com/productsdf-fg-/110259896494', 'http://cgi.sandbox.ebay.com/productsdf-fg-/110259896494', 33, '5161.41', '0.0', 'false', '2018-07-08T06:24:32.000Z', 'false', '2018-06-15 07:08:24.241099', '2018-06-15 07:08:24.241099'),
(26, '0.0', '2018-02-08T06:26:21.000Z', 'http://cgi.sandbox.ebay.com/productsdfju-/110259896555', 'http://cgi.sandbox.ebay.com/productsdfju-/110259896555', 34, '5161.41', '0.0', 'false', '2018-07-08T06:26:21.000Z', 'false', '2018-06-15 07:08:25.637899', '2018-06-15 07:08:25.637899'),
(27, '0.0', '2018-02-08T06:29:45.000Z', 'http://cgi.sandbox.ebay.com/product-12th-feb-/110259896600', 'http://cgi.sandbox.ebay.com/product-12th-feb-/110259896600', 35, '5161.41', '0.0', 'false', '2018-07-08T06:29:45.000Z', 'false', '2018-06-15 07:08:26.998345', '2018-06-15 07:08:26.998345'),
(28, '0.0', '2018-02-08T10:11:31.000Z', 'http://cgi.sandbox.ebay.com/prctsfjury-/110259925992', 'http://cgi.sandbox.ebay.com/prctsfjury-/110259925992', 36, '4423.96', '0.0', 'false', '2018-07-08T10:11:31.000Z', 'false', '2018-06-15 07:08:28.267621', '2018-06-15 07:08:28.267621'),
(29, '0.0', '2018-05-10T09:31:46.000Z', 'http://cgi.sandbox.ebay.com/new-add-form-testing-123456789-/110308790228', 'http://cgi.sandbox.ebay.com/new-add-form-testing-123456789-/110308790228', 37, '55.31', '0.0', 'false', '2018-07-09T09:31:46.000Z', 'false', '2018-06-15 07:08:29.559004', '2018-06-15 07:08:29.559004'),
(30, '0.0', '2018-05-10T09:52:24.000Z', 'http://cgi.sandbox.ebay.com/new-product-test-10-05-2018-/110308791011', 'http://cgi.sandbox.ebay.com/new-product-test-10-05-2018-/110308791011', 29, '47.93', '0.0', 'false', '2018-07-09T09:52:24.000Z', 'false', '2018-06-15 07:08:30.912904', '2018-06-15 07:08:30.912904'),
(31, '0.0', '2018-02-10T06:34:42.000Z', 'http://cgi.sandbox.ebay.com/Fashion-Sky-Synthetic-Printed-Salwar-Suit-Dupatta-Material-/110261379103', 'http://cgi.sandbox.ebay.com/Fashion-Sky-Synthetic-Printed-Salwar-Suit-Dupatta-Material-/110261379103', 17, '367.99', '0.0', 'false', '2018-07-10T06:34:42.000Z', 'false', '2018-06-15 07:08:32.326276', '2018-06-15 07:08:32.326276'),
(32, '0.0', '2018-05-12T10:03:37.000Z', 'http://cgi.sandbox.ebay.com/kusfgkeuryqiwum-fd-wkqjrhlwqihrwejhr-/110309097670', 'http://cgi.sandbox.ebay.com/kusfgkeuryqiwum-fd-wkqjrhlwqihrwejhr-/110309097670', 24, '19.9', '0.0', 'false', '2018-07-11T10:03:37.000Z', 'false', '2018-06-15 07:08:33.616734', '2018-06-15 07:08:33.616734'),
(33, '0.0', '2018-02-14T04:09:40.000Z', 'http://cgi.sandbox.ebay.com/Canon-EOS-700D-18MP-DSLR-Camera-/110268834000', 'http://cgi.sandbox.ebay.com/Canon-EOS-700D-18MP-DSLR-Camera-/110268834000', 38, '4424.7', '0.0', 'false', '2018-07-14T04:09:40.000Z', 'false', '2018-06-15 07:08:35.006593', '2018-06-15 07:08:35.006593'),
(34, '0.0', '2018-02-14T04:42:40.000Z', 'http://cgi.sandbox.ebay.com/Canon-EOS-700D-18MP-DSLR-Camera-51-/110268877238', 'http://cgi.sandbox.ebay.com/Canon-EOS-700D-18MP-DSLR-Camera-51-/110268877238', 7, '4424.7', '0.0', 'false', '2018-07-14T04:42:40.000Z', 'false', '2018-06-15 07:08:36.310353', '2018-06-15 07:08:36.310353'),
(35, '0.0', '2018-06-20T11:16:48.000Z', 'http://cgi.sandbox.ebay.com/new-product-variation123-/110329661815', 'http://cgi.sandbox.ebay.com/new-product-variation123-/110329661815', 39, '18436.25', '0.0', 'false', '2018-06-27T11:16:48.000Z', 'false', '2018-06-21 21:36:28.347928', '2018-06-21 21:36:28.347928'),
(36, '0.0', '2018-06-20T14:06:45.000Z', 'http://cgi.sandbox.ebay.com/Apple-iPhone-6-Gold-32GB-/110329677846', 'http://cgi.sandbox.ebay.com/Apple-iPhone-6-Gold-32GB-/110329677846', 40, '16961.35', '0.0', 'false', '2018-06-27T14:06:45.000Z', 'false', '2018-06-21 21:36:29.788357', '2018-06-21 21:36:29.788357'),
(37, '0.0', '2018-06-22T12:28:41.000Z', 'http://cgi.sandbox.ebay.com/product-template-testing-/110329921482', 'http://cgi.sandbox.ebay.com/product-template-testing-/110329921482', 41, '36.87', '0.0', 'false', '2018-06-29T12:28:41.000Z', NULL, '2018-06-26 18:22:03.815922', '2018-06-26 18:22:03.815922'),
(38, '0.0', '2018-06-23T14:05:16.000Z', 'http://cgi.sandbox.ebay.com/SanDisk-Ultra-32-GB-MicroSDHC-Class-10-98-MB-s-Memory-Card-/110330105180', 'http://cgi.sandbox.ebay.com/SanDisk-Ultra-32-GB-MicroSDHC-Class-10-98-MB-s-Memory-Card-/110330105180', 42, '191.74', '0.0', 'false', '2018-06-30T14:05:16.000Z', NULL, '2018-06-26 18:22:10.956330', '2018-06-26 18:22:10.956330');

-- --------------------------------------------------------

--
-- Table structure for table `messages`
--

CREATE TABLE `messages` (
  `id` int(11) NOT NULL,
  `item_id` int(20) DEFAULT NULL,
  `item_title` text,
  `question_type` varchar(50) DEFAULT NULL,
  `message_type` varchar(50) DEFAULT NULL,
  `display_to_public` varchar(20) DEFAULT NULL,
  `sender_id` varchar(50) DEFAULT NULL,
  `sender_email` varchar(100) DEFAULT NULL,
  `recipient_id` varchar(50) DEFAULT NULL,
  `subject` text,
  `body` text,
  `message_id` varchar(55) DEFAULT NULL,
  `parrent_messageid` int(20) DEFAULT NULL,
  `message_status` varchar(50) DEFAULT NULL,
  `creation_date` varchar(50) DEFAULT NULL,
  `last_modified_date` varchar(50) DEFAULT NULL,
  `attachment` text,
  `created_at` datetime DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `messages`
--

INSERT INTO `messages` (`id`, `item_id`, `item_title`, `question_type`, `message_type`, `display_to_public`, `sender_id`, `sender_email`, `recipient_id`, `subject`, `body`, `message_id`, `parrent_messageid`, `message_status`, `creation_date`, `last_modified_date`, `attachment`, `created_at`, `updated_at`) VALUES
(1, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'sergiminolett-0', 'sergim_mx6806rh@members.ebay.com.au', 'dealshub_au', 'sergiminolett-0 has sent a question about item #272944026626, ending on 18-May-18 21:34:04 AEST - 38\" Wooden Acoustic Guitar Black Student Maple Wood Fingerboard 3/4 Beginner', 'Do you have a number to check transport, please?', '1710941807017', NULL, 'Unanswered', '2018-05-08T02:21:47.000Z', '2018-05-08T02:21:47.000Z', NULL, '2018-06-07 20:13:47', '2018-06-07 21:21:33'),
(2, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'g2h7', 'g2h7_lw5107lcj@members.ebay.com.au', 'dealshub_au', 'g2h7 has sent a question about item #273040190569, ending on 25-May-18 13:32:10 AEST - Winged Clothes Airer with Garment Rack Dryer Horse Drying Laundry Line Hanger', 'I am slightly concerned I’ve made a mistake. Why does description say this? Makes me worried you may send me not a washing rack at all....\n\n\n\n\n“Cigweld TIG Torch 17V, Gas Valve, 4m lead To Suit Transmig 175i #W4013801\n\nThis winged clothes airer with garment rack will ensure that your clothes stay well organised......\n\n\nAlso please ship (clothes airer not TIG torch valve) ASAP as my other one broke and I need another soon. Thanks. I paid you immediately and hope you offer me the same level of service. This will also guarantee good feedback. \n\nRegards,\nStacy g2h7 Host.', '1711836516017', NULL, 'Answered', '2018-05-09T12:47:17.000Z', '2018-05-10T05:13:30.000Z', NULL, '2018-06-07 20:13:47', '2018-06-07 21:21:33'),
(3, 2147483647, '', 'None', 'AskSellerQuestion', 'false', 'outside**thebox', 'thedec_eu2148lgj@members.ebay.com.au', 'dealshub_au', 'Other: outside**thebox sent a message about 54 Piece Dining Set New Start Starter Black Solid Dinnerware Plates Bowls Mugs + #272939042783', 'hello dealshub  do you have any of these 54 piece black kitchen sets available ?   if you use kmart then they are out online .   so I just want to see if you use a different supplier if yes id like to buy a set   cheers mate   Cam', '1720374252014', NULL, 'Answered', '2018-05-12T22:44:18.000Z', '2018-05-13T00:19:10.000Z', NULL, '2018-06-07 20:13:47', '2018-06-07 21:21:34'),
(4, 2147483647, '', 'Shipping', 'AskSellerQuestion', 'false', 'swarzys', 'swarzy_eq4145pbz@members.ebay.com.au', 'dealshub_au', 'Postage: swarzys sent a message about 4Mini Exercise Black Bike Pedal Fitness Exerciser Leg Workout Floor Gym FOLDING #272891974175', 'Do you deliver to An address C/- caravan Park, Forster NSW?', '1729298373011', NULL, 'Unanswered', '2018-05-14T05:09:17.000Z', '2018-05-14T05:09:17.000Z', NULL, '2018-06-07 20:13:47', '2018-06-07 21:21:34'),
(5, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'do0do0d', 'do0do0_isd4857rb@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about Winged Clothes Airer with Garment Rack Dryer Horse Drying Laundry Line Hanger #273040190569', 'Mate, parcel collection at a post office can have big items.\r\n\r\nI am not home everyday to wait for your parcel', '1727050504010', NULL, 'Unanswered', '2018-05-15T12:27:29.000Z', '2018-05-15T12:27:29.000Z', NULL, '2018-06-07 20:13:47', '2018-06-07 21:21:34'),
(6, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'jaddarmod_0', 'jaddar_mt3206oejc@members.ebay.com.au', 'dealshub_au', 'jaddarmod_0 has sent a question about item #272883859685, ending on 09-Jun-18 05:20:00 AEST - Expandable Wooden Cutlery Tray Organizer Drawer Storage Kitchen Holder Large', 'I would like my money back as you just sent me a $15 cutlery tray from Kmart. It even had Kmart on the label on the box. Please refund my money before I report you', '1712930447019', NULL, 'Answered', '2018-05-17T03:40:39.000Z', '2018-05-18T04:47:35.000Z', NULL, '2018-06-07 20:13:47', '2018-06-07 21:21:34'),
(7, 2147483647, '', 'None', 'ContactTransactionPartner', 'false', 'won.maxwe', 'wonmax_kws4709qjo@members.ebay.com.au', 'dealshub_au', 'I have a question about using my item or I want to send the seller a message: won.maxwe sent a message about 91 Pcs Metric Spanner Socket Wrench Ratchet Car Set Repair Kit Mechanic Tool Box #272892145582', 'hey do you have this stock ? can you check for me? \r\nthank you', '1725359829013', NULL, 'Answered', '2018-05-19T10:20:51.000Z', '2018-05-19T10:35:01.000Z', NULL, '2018-06-07 20:13:48', '2018-06-07 21:21:34'),
(8, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'won.maxwe', 'wonmax_kws4709qjo@members.ebay.com.au', 'dealshub_au', 'I have a question about using my item or I want to send the seller a message: won.maxwe sent a message about 91 Pcs Metric Spanner Socket Wrench Ratchet Car Set Repair Kit Mechanic Tool Box #272892145582', 'please double check. coz i bought this last time. you said to me no stock . thank you ', '1725370929013', NULL, 'Answered', '2018-05-19T10:59:31.000Z', '2018-05-19T11:51:59.000Z', NULL, '2018-06-07 20:13:48', '2018-06-07 21:21:34'),
(9, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'walkingcouch', 'walkin_kxnu4196rit@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272892145582, ending on 15-Jun-18 19:32:14 AEST - 91 Pcs Metric Spanner Socket Wrench Ratchet Car Set Repair Kit Mechanic Tool Box', 'Hi Mary,\r\n\r\nYes, charge the additional $10 for postage.  \r\n\r\nThank you for letting me know\r\n', '1732762304011', NULL, 'Answered', '2018-05-20T02:38:43.000Z', '2018-05-20T03:08:34.000Z', NULL, '2018-06-07 20:13:48', '2018-06-07 21:21:34'),
(10, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'walkingcouch', 'walkin_kxnu4196rit@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272892145582, ending on 15-Jun-18 19:32:14 AEST - 91 Pcs Metric Spanner Socket Wrench Ratchet Car Set Repair Kit Mechanic Tool Box', 'Hi,\r\n\r\nThe extra $10 was sent. Please let me know if you have received it?\r\n\r\nThank you', '1733147921011', NULL, 'Unanswered', '2018-05-21T02:01:22.000Z', '2018-05-21T02:01:22.000Z', NULL, '2018-06-07 20:13:48', '2018-06-07 21:21:35'),
(11, 2147483647, '', 'General', 'ContacteBayMemberViaAnonymousEmail', 'false', 'blkeker.dfsflvmra', 'blkeke_et6156thtx@members.ebay.com.au', 'dealshub_au', 'Re: dealshub_au has sent a question about item #273220141509, ending\r\n on 17-Jun-18 12:48:51 AEST - TV Stand Entertainment Unit High Gloss Cabinet\r\n Media Storage Furniture White', 'Hi Mary\n\n$ 10 carriage fee is ok how should I pay for this ?\n\nWhen will you despatch and how quickly can I expect arrival in WA 6038 ?\n\nRegards\n\nKerry\n\nSent from my iPhone\n\n\nOn 22 May 2018, at 11:52, eBay - dealshub_au &lt;&gt; wrote:\n\n', '1726693513013', NULL, 'Answered', '2018-05-22T04:30:37.000Z', '2018-05-22T04:39:45.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:23'),
(12, 2147483647, '', 'General', 'ContacteBayMemberViaAnonymousEmail', 'false', 'blkeker.dfsflvmra', 'blkeke_et6156thtx@members.ebay.com.au', 'dealshub_au', 'Re: dealshub_au has sent a question about item #273220141509, ending\r\n on 17-Jun-18 12:48:51 AEST - TV Stand Entertainment Unit High Gloss Cabinet\r\n Media Storage Furniture White', 'Hi Mary\n\nPaid by PayPal a few minutes ago !\n\nRegards\n\nKerry\n\n\n\nSent from my iPhone\n\n\nOn 22 May 2018, at 12:39, eBay - dealshub_au &lt;&gt; wrote:\n\n', '1726699543013', NULL, 'Answered', '2018-05-22T04:57:26.000Z', '2018-05-22T06:44:15.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:23'),
(13, 2147483647, '', 'General', 'AskSellerQuestion', 'false', '836lacey', '836lac_jzd6305pafg@members.ebay.com.au', 'dealshub_au', '836lacey has sent a question about item #273039339106, ending on 25-May-18 01:06:30 AEST - Cigweld TIG Torch 17V, Gas Valve, 4m lead To Suit Transmig 175i #W4013801', 'Hi dealshub_au\nJust wondering if you match prices\nTotal tools and Sydney tools are selling the tig torch\nW4013801 for $135.00.\nThanks 836lacey.', '1726746625013', NULL, 'Unanswered', '2018-05-22T07:08:05.000Z', '2018-05-22T07:08:05.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:23'),
(14, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'jaddarmod_0', 'jaddar_mt3206oejc@members.ebay.com.au', 'dealshub_au', 'jaddarmod_0 has sent a question about item #272883859685, ending on 09-Jun-18 05:20:00 AEST - Expandable Wooden Cutlery Tray Organizer Drawer Storage Kitchen Holder Large', 'What a crock of shit.', '1715702731019', NULL, 'Answered', '2018-05-22T10:49:56.000Z', '2018-05-22T10:49:56.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(15, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'ashleyw220', 'ashley_jr3796la@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272931498575, ending on 12-Jun-18 13:15:33 AEST - 16-18FT UV WATERPROOF CARAVAN CAMPERVAN COVER HEAVYDUTY TRAILER 4 LAYER + 4 SIDE', 'No problems.\r\n\r\nI understand.\r\n\r\nAsh', '1716109884019', NULL, 'Unanswered', '2018-05-23T01:10:13.000Z', '2018-05-23T01:10:13.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(16, 2147483647, '', 'None', 'ContactTransactionPartner', 'false', 'bbal-84', 'bbal84_eww2157uif@members.ebay.com.au', 'dealshub_au', 'bbal-84 has sent a question about item #273222923921, ending on 18-Jun-18 16:56:10 AEST - WAHL Hair Clippers Haircut Set Cordless Beard Trimmer Shaver Mens Groomer 21Pcs', 'Hello Mary. That is correct. I would also like to add the Post Office Box number.\n\nP.O. Box 326\n43 Marool Loop \nDampier Peninsula\nWestern Australia, 6725 \n\nCurrently postal services cannot recognise house numbers where I live but are able to recognise street names and P.O. Box s. \n\nLet me know if I can provide more info.\n\nCheers, Bradley', '1716604537019', NULL, 'Answered', '2018-05-23T19:44:23.000Z', '2018-05-23T20:01:48.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(17, 2147483647, '', 'None', 'ContactTransactionPartner', 'false', 'bbal-84', 'bbal84_eww2157uif@members.ebay.com.au', 'dealshub_au', 'bbal-84 has sent a question about item #273222923921, ending on 18-Jun-18 16:56:10 AEST - WAHL Hair Clippers Haircut Set Cordless Beard Trimmer Shaver Mens Groomer 21Pcs', 'Yes', '1716731293019', NULL, 'Unanswered', '2018-05-24T01:35:15.000Z', '2018-05-24T01:35:15.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(18, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'moods9188', 'moods9_lu6198ng@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272885610555, ending on 10-Jun-18 14:40:57 AEST - Pie Maker 4-piece Non Stick Plate Easy Cooking Pastry Bakes Effortless Nonstick', 'The fee would be ok\nPlease send', '1731901210010', NULL, 'Answered', '2018-05-24T03:59:57.000Z', '2018-05-24T04:20:04.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(19, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'moods9188', 'moods9_lu6198ng@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272885610555, ending on 10-Jun-18 14:40:57 AEST - Pie Maker 4-piece Non Stick Plate Easy Cooking Pastry Bakes Effortless Nonstick', 'Done', '1731908106010', NULL, 'Answered', '2018-05-24T04:24:44.000Z', '2018-05-24T04:38:07.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(20, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'ministryofbeauty', 'kk5_krd4756tf@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #273054737317, ending on 04-Jun-18 16:33:48 AEST - Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer', 'Hi Mary\nThanks for this. Please send to: \n5 Towner road \nGalston NSW 2159\nCan leave near letterbox over the fence as no one will be home.\n\nThanks!', '1721986819012', NULL, 'Answered', '2018-05-24T06:30:03.000Z', '2018-05-24T06:30:03.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(21, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'sheenakay27', 'sheena_jxdl3259uj@members.ebay.com.au', 'dealshub_au', 'sheenakay27 has sent a question about item #272885610555, ending on 10-Jun-18 14:40:57 AEST - Pie Maker 4-piece Non Stick Plate Easy Cooking Pastry Bakes Effortless Nonstick', 'Wo deering of your pie maker comes with the pastry cutter and what size pies it makes?', '1728604794016', NULL, 'Unanswered', '2018-05-24T08:47:45.000Z', '2018-05-24T08:47:45.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(22, 2147483647, '', 'None', 'ContacteBayMemberViaAnonymousEmail', 'false', 'focus217t', 'focus2_kxi3256ud@members.ebay.com.au', 'dealshub_au', 'Re: dealshub_au sent a message about Makeup Table With Mirror\r\n Dressing Vanity Desk w/ Drawer Bedroom Furniture White #273220075353', '\nDear Mary, \n\nThank you for your notice. \n\nI’m wondering if I can cancel my order and get a full refund? I don’t like black and the makeup table is not very urgent for me. \n\nLooking forward to hearing from you. \n\nKind Regards \n\nJinfang Tian\nOn Sat, May 26, 2018 at 9:43 PM, eBay - dealshub_au &lt;&gt; wrote:\n--\nYours sincerely,\n\nJinfang Tian (Audrey)', '1729192018013', NULL, 'Unanswered', '2018-05-26T11:57:54.000Z', '2018-05-26T11:57:54.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(23, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'mralexablake', 'mralex_lsn2855uit@members.ebay.com.au', 'dealshub_au', 'mralexablake has sent a question about item #273220189050, ending on 17-Jun-18 13:16:39 AEST - 200 Puppy PottyTraining Dog Cat Pet Animal Pee Pad Wee IndoorToilet Mat PinkBlue', 'Hi I’ve just purchased the pink dog pee mats. \nAny chance we can please change the postal address to:\n Alexa Greene \n2/24 Kohl Street, Upper Coomera 4209 Qld\n\nThanks in advance \nAlexa', '1723457968012', NULL, 'Unanswered', '2018-05-27T04:38:13.000Z', '2018-05-27T04:38:13.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(24, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'niccas_25', 'niccas_husu5847tf@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about Cordless Home Phone Digital Battery WiFi Answering Machine Intercom Black NBN #273090707565', 'Hi\n\nCan you please refund the money\n\nThanks \n\nMatt', '1728262078014', NULL, 'Answered', '2018-05-27T08:07:56.000Z', '2018-05-27T08:07:56.000Z', NULL, '2018-06-07 20:13:48', '2018-06-21 21:56:24'),
(25, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'karhoded', 'karhod_kwn3709li@members.ebay.com.au', 'dealshub_au', 'karhoded has sent a question about item #273040451997, ending on 24-Jun-18 18:58:32 AEST - Water Pump Pressure Switch Garden Farm Clean Rain Tank Irrigation 350W 240v', 'Hi there , I am wanting to hook this pump up to an IBC and the connect the garden hose . Have you got any fittings that I could use for this application ? Many thanks Kelly', '1721566516017', NULL, 'Unanswered', '2018-05-27T11:32:22.000Z', '2018-05-27T11:32:22.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(26, 2147483647, '', 'None', 'ContacteBayMemberViaAnonymousEmail', 'false', 'focus217t', 'focus2_kxi3256ud@members.ebay.com.au', 'dealshub_au', 'Re: dealshub_au sent a message about Makeup Table With Mirror\r\n Dressing Vanity Desk w/ Drawer Bedroom Furniture White #273220075353', '\nHi Mary, \n\nThank you for your assistance.\n\nCheers \n\nJinfang \n\nOn Mon, May 28, 2018 at 12:48 AM, eBay - dealshub_au &lt;&gt; wrote:\n--\nYours sincerely,\n\nJinfang Tian (Audrey)', '1729770270013', NULL, 'Unanswered', '2018-05-27T23:15:42.000Z', '2018-05-27T23:15:42.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(27, 2147483647, '', 'None', 'AskSellerQuestion', 'false', 'cheprecisionoz', 'chepre_mx3206mcz@members.ebay.com.au', 'dealshub_au', 'Other: cheprecisionoz sent a message about Makita Cordless Leaf Blower 18v LXT DUB183Z Lithium Ion for18v Battery Bare New #273049127131', 'Hi Mate,\r\nCan you give me a model or part number for the battery please.\r\n\r\nCheers,\r\nGeorge', '1721915433017', NULL, 'Unanswered', '2018-05-28T06:38:51.000Z', '2018-05-28T06:38:51.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(28, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'qtoose11', 'qtoose_ewwy2305qb@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about 15L Cooler Drink Jug New Large Ice Keg Tap Water Dispenser Camping Picnic Bucket #272922738955', 'Hi Mary,\n\nOK. Please cancel the current order and resend with the additional postage fee. \n\nThanks', '1730973570016', NULL, 'Answered', '2018-05-29T03:52:31.000Z', '2018-05-30T06:16:04.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(29, 2147483647, '', 'General', 'ContacteBayMemberViaAnonymousEmail', 'false', 'blkeker.dfsflvmra', 'blkeke_et6156thtx@members.ebay.com.au', 'dealshub_au', 'EBAY GUEST ORDER SHIPPED: #273220141509 - Stand Entertainment Unit\r\n High Gloss Cabinet Media Storage Furniture White', '\nHi Mary\n\nThis order should have been received by yesterday latest, as per attached notification..\n\ncan you please check with your couriers and advise an ETA with me in Alkimos WA 6038 please ?\n\nthank you\n\nKerry\n\n0413 361 710\n\n\nFrom: eBay &lt;ebay@ebay.com.au&gt;\nSent: 23 May 2018 09:29\nTo: kerry.blackburn@hotmail.co.uk\nSubject: GUEST ORDER SHIPPED: TV Stand Entertainment Unit High Gloss Cabinet Media Storage Furniture White\n \n\n\nFrom: eBay - dealshub_au &lt;&gt;\nSent: 22 May 2018 14:44\nTo:\nSubject: Re: dealshub_au has sent a question about item #273220141509, ending on 17-Jun-18 12:48:51 AEST - TV Stand Entertainment Unit High Gloss Cabinet Media Storage Furniture White\n \nNew message: Hi Kerry, We&apos;ve received the payme... New message from: dealshub_au (1,040)\nHi Kerry,\n\nWe&apos;ve received the payment, your order is being processed.\n\nRegards,\nMaryReply\nDear blkeker.dfsflvmra,\n\nHi Kerry,\n\nWe&apos;ve received the payment, your order is being processed.\n\nRegards,\nMary\n\n- dealshub_au\nYour previous message\nHi Mary\n\nPaid by PayPal a few minutes ago !\n\nRegards\n\nKerry\n\n\n\nSent from my iPhone\n\n\nFrom: blkeker.dfsflvmra\nTo: dealshub_au\nSubject: Re: dealshub_au has sent a question about item #273220141509, ending on 17-Jun-18 12:48:51 AEST - TV Stand Entertainment Unit High Gloss Cabinet Media Storage Furniture White\nSent Date: 22-May-18 14:57:26 AEST\n\nDear dealshub_au,\n\nHi Mary\n\nPaid by PayPal a few minutes ago !\n\nRegards\n\nKerry\n\n\n\nSent from my iPhone\n\n- blkeker.dfsflvmra\n\nTV Stand Entertainment Unit High Gloss Cabinet Media Storage Furniture WhiteOrder status: PaidPaid AU $167.68 with PAYPAL on 22 May, 2018 11:43 AESTEstimated delivery: Thu. 24 May. - Mon. 28 May.\nView order details \nWe scan messages to enforce policies. Only purchases on eBay are covered by eBay Money Back Guarantee. Asking your trading partner to complete a transaction outside of eBay is not allowed.\nEmail reference ID: [#a03-sm9bck7doj#]_[#26f2e45518b6496babfb2614a54b4838#]\nWe don&apos;t check this mailbox, so please don&apos;t reply to this message. If you have a question, go to Help &amp; Contact.\neBay sent this message to Kerry Blackburn (blkeker.dfsflvmra). Learn more about account protection. eBay is committed to your privacy. Learn more about our privacy notice and user agreement.\n©1995-2018 eBay Inc., eBay International AG Helvetiastrasse 15/17 - P.O. Box 133, 3000 Bern 6, Switzerland', '1730393193013', NULL, 'Unanswered', '2018-05-29T03:56:09.000Z', '2018-05-29T03:56:09.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(30, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'qtoose11', 'qtoose_ewwy2305qb@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about 15L Cooler Drink Jug New Large Ice Keg Tap Water Dispenser Camping Picnic Bucket #272922738955', 'That’s fine! Thank you!', '1731179490016', NULL, 'Answered', '2018-05-29T11:57:54.000Z', '2018-05-29T11:57:54.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(31, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'pastry-chef', 'pastry_ls6306uboc@members.ebay.com.au', 'dealshub_au', 'pastry-chef has sent a question about item #272903500156, ending on 24-Jun-18 16:20:14 AEST - 100cm Bunny Home Rabbit Guinea Pig Animal Cage Hutch Habitat Wheel Stand House', 'Hello\nExited i recieved my cage today and started to put it together right away but then saw that it is a bit chipped/broken on one of the corners! \nWhat can we do? ....but i do need the cage right away as animals need the extra room.\nBut otherwise really happy, thanks!\n\nRegards', '1719849720019', NULL, 'Unanswered', '2018-05-30T01:08:58.000Z', '2018-05-30T01:08:58.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(32, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'pastry-chef', 'pastry_ls6306uboc@members.ebay.com.au', 'dealshub_au', 'pastry-chef has sent a question about item #272903500156, ending on 24-Jun-18 16:20:14 AEST - 100cm Bunny Home Rabbit Guinea Pig Animal Cage Hutch Habitat Wheel Stand House', 'Oh... and ive just seen the thing to hold the hay i believe, its hook to attach it to the cage is also broken off.\nThanks for your hopefully quick reply.', '1719851923019', NULL, 'Unanswered', '2018-05-30T01:16:08.000Z', '2018-05-30T01:16:08.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(33, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'kcojla', 'kcojla_dxw2298rh@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about TV Stand Entertainment Unit High Gloss Cabinet Media Storage Furniture White #273220141509', 'Hi, according to fastway delivery was today. Ive been home all morning but nobody showed up and status is now &quot;your parcel has been delivered&quot;?\nIve followed up on fastway but no response. Do you have any details of delivery status at your end?', '1734949427010', NULL, 'Answered', '2018-05-30T01:36:24.000Z', '2018-05-30T01:36:24.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(34, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'kcojla', 'kcojla_dxw2298rh@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about TV Stand Entertainment Unit High Gloss Cabinet Media Storage Furniture White #273220141509', 'Hi Mary, according to Fastway the above tracking number is not to my address. Did you quote wrong reference number or was  my parcel sent to wrong address?', '1734954345010', NULL, 'Answered', '2018-05-30T01:50:48.000Z', '2018-05-30T01:50:48.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(35, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'kcojla', 'kcojla_dxw2298rh@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about TV Stand Entertainment Unit High Gloss Cabinet Media Storage Furniture White #273220141509', 'Hi Mary, i managed to reach fastway via phone. They said the delivery number above was not to my address. Did you quote wrong referenve number or was my parcel sent to wrong address?', '1734955657010', NULL, 'Answered', '2018-05-30T01:54:47.000Z', '2018-05-30T08:41:05.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(36, 2147483647, '', 'None', 'AskSellerQuestion', 'false', 'justgeorgeous', 'justge_hun6148ubt@members.ebay.com.au', 'dealshub_au', 'Details about item: justgeorgeous sent a message about WAHL Hair Clippers Haircut Set Cordless Beard Trimmer Shaver Professional 21Pcs #273222947353', 'hi. where is it made? usa? germany? china?', '1735051451010', NULL, 'Unanswered', '2018-05-30T06:59:10.000Z', '2018-05-30T06:59:10.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(37, 2147483647, '', 'General', 'ContactTransactionPartner', 'false', 'susuway', 'susuwa_mwn3859th@members.ebay.com.au', 'dealshub_au', 'susuway ?????? 362087434044 ?????????? 2018-06-27 17:35:19–4 In 1 Micro-Needle DERMA 3 PIECE SET ROLLER + Hyaluronic Acid Skin Care ', 'Dear friend,\n\nThanks for your mail.\n\nWe just got the information that  the  hyaluronic Acid Original Fluid  is listed in prohibition list according to custom regulation.therefore we can not ship it to you. So do you accept we refund you  4 AUD ,then we will help you to send the needle out for you ?is it OK  for you to carry on your order ?\n\nWaiting for your early reply.\n\nBest regards.', '1724207088017', NULL, 'Answered', '2018-06-01T01:19:05.000Z', '2018-06-01T03:14:09.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(38, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'kcojla', 'kcojla_dxw2298rh@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about TV Stand Entertainment Unit High Gloss Cabinet Media Storage Furniture White #273220141509', 'Hi, do you have the tracking ID so i can track progress to be sure theres somebody home to receive the delivery.', '1736207005010', NULL, 'Answered', '2018-06-01T05:31:05.000Z', '2018-06-01T05:47:01.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(39, 2147483647, '', 'Shipping', 'AskSellerQuestion', 'false', 'aussiehorse01', 'aussie_eudu4229sdo@members.ebay.com.au', 'dealshub_au', 'aussiehorse01 has sent a question about :P&P for item #273222947353, ending on 18-Jun-18 17:04:04 AEST - WAHL Hair Clippers Haircut Set Cordless Beard Trimmer Shaver Professional 21Pcs', 'Hello. I haven&apos;t yet received this item.  Tracking history says it was sent back to the sender and then back to Adelaide 7 days ago.  I have been in touch with my local courier who says he hasn&apos;t received it.  Can you help? Why was it returned? Thanks', '1739766108015', NULL, 'Answered', '2018-06-01T06:49:41.000Z', '2018-06-05T08:41:14.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(40, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'susuway', 'susuwa_mwn3859th@members.ebay.com.au', 'dealshub_au', 'susuway ?????? 362087434044 ?????????? 2018-06-27 17:35:19–4 In 1 Micro-Needle DERMA 3 PIECE SET ROLLER + Hyaluronic Acid Skin Care ', 'Dear buyer,\nWe are sorry for this inconvenience.\nWe sent the cancel request for your paid order, you can find and accept the cancel request in the message from eBay to you. After you accept the cancel request, the order can be cancelled and refunded in full.\nSincerely hope you can support us.\nYours faithfully,', '1724356931017', NULL, 'Answered', '2018-06-01T08:55:59.000Z', '2018-06-01T08:55:59.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(41, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'nerm-au2014', 'nermau_hvsu2247pd@members.ebay.com.au', 'dealshub_au', 'nerm-au2014 has sent a question about item #273222947353, ending on 18-Jun-18 17:04:04 AEST - WAHL Hair Clippers Haircut Set Cordless Beard Trimmer Shaver Professional 21Pcs', 'Hi\n\nI just wanted to let you know I received the item- thank you.\n\nThe invoice states $29.95\nPostage :$9.95\n\nI was charged $48.95 ....... something is not adding up?', '1720412237018', NULL, 'Unanswered', '2018-06-01T20:57:17.000Z', '2018-06-01T20:57:17.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(42, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'nerm-au2014', 'nermau_hvsu2247pd@members.ebay.com.au', 'dealshub_au', 'nerm-au2014 has sent a question about item #273222947353, ending on 18-Jun-18 17:04:04 AEST - WAHL Hair Clippers Haircut Set Cordless Beard Trimmer Shaver Professional 21Pcs', 'Hi sorry,\n\nFurther to my last message to you, postage\n\nPaid $48.68, not $48.95\n\nPlease advise??', '1720413161018', NULL, 'Unanswered', '2018-06-01T20:59:28.000Z', '2018-06-01T20:59:28.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(43, 2147483647, '', 'General', 'AskSellerQuestion', 'false', '40urteen', 'nathgr_iqsy4855ud@members.ebay.com.au', 'dealshub_au', '40urteen has sent a question about item #272944026626, ending on 17-Jun-18 21:34:04 AEST - 38\" Wooden Acoustic Guitar Black Student Maple Wood Fingerboard 3/4 Beginner', 'Hi, is this guitar nylon string or steel string?\nThanks', '1737189441010', NULL, 'Unanswered', '2018-06-03T07:53:48.000Z', '2018-06-03T07:53:48.000Z', NULL, '2018-06-07 20:13:49', '2018-06-21 21:56:24'),
(44, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'bodhitree74-5', 'bodhit_essy3799tajx@members.ebay.com.au', 'dealshub_au', 'bodhitree74-5 has sent a question about item #272958819750, ending on 26-Jun-18 21:44:59 AEST - Pub Style Dartboard in Wooden Cabinet with Score Board and Game Darts', 'Hi\n\nI just opened the box to install. The cabinet and dart board are in the box but not the darts or the mounting screws/equipment and other accessories if they came with.\n\nWhat is the best way to get the things that are missing out of the box?\n\nStuart', '1740674099011', NULL, 'Answered', '2018-06-04T00:07:15.000Z', '2018-06-04T01:16:23.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:24'),
(45, 2147483647, '', 'None', 'ContactTransactionPartner', 'false', 'tea4lyf', 'wikkan_mzs5849siz@members.ebay.com.au', 'dealshub_au', 'I have a question about using my item or I want to send the seller a message: tea4lyf sent a message about Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer #273054737317', 'This purchase has been VERY weird to say the least.\r\n\r\nOn Saturday 26th May I had a call from Harvey Norman Chirnside Park to tell me my order for the Remington Trimmer was at their store ready to be picked up.\r\n\r\nOn monday I went in to the store and was told Harvey Norman dont have an ebay account and they know nothing about it.\r\n\r\n2 days later I get an email saying the trimmer has been posted. It was meant to arrive before 30th May.\r\n\r\nIts now 4th June and still no item, where is it and explain what is going on please.\r\n\r\nRegards\r\nChris', '1740704090011', NULL, 'Answered', '2018-06-04T01:54:55.000Z', '2018-06-04T11:16:43.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:24'),
(46, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'bodhitree74-5', 'bodhit_essy3799tajx@members.ebay.com.au', 'dealshub_au', 'bodhitree74-5 has sent a question about item #272958819750, ending on 26-Jun-18 21:44:59 AEST - Pub Style Dartboard in Wooden Cabinet with Score Board and Game Darts', 'Ok I will repack ready to be re-sent for exchange. Looking forward to further instructions.\n\nStuart', '1740712454011', NULL, 'Answered', '2018-06-04T02:18:08.000Z', '2018-06-04T02:18:08.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:24'),
(47, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'tea4lyf', 'wikkan_mzs5849siz@members.ebay.com.au', 'dealshub_au', 'tea4lyf has sent a question about item #273054737317, ending on 04-Jul-18 16:33:48 AEST - Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer', 'Can I get a response to my message?! I want to know what’s going on', '1740876014011', NULL, 'Answered', '2018-06-04T09:21:32.000Z', '2018-06-05T08:32:07.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:24'),
(48, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'qtoose11', 'qtoose_ewwy2305qb@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about 15L Cooler Drink Jug New Large Ice Keg Tap Water Dispenser Camping Picnic Bucket #272922738955', 'Hi Mary,\n\nI made the payment last week the day you sent it. I only saw this message just then. \n\nThanks', '1734469284016', NULL, 'Answered', '2018-06-04T11:22:21.000Z', '2018-06-05T08:31:01.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:24'),
(49, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'bodhitree74-5', 'bodhit_essy3799tajx@members.ebay.com.au', 'dealshub_au', 'bodhitree74-5 has sent a question about item #272958819750, ending on 26-Jun-18 21:44:59 AEST - Pub Style Dartboard in Wooden Cabinet with Score Board and Game Darts', 'Hi still awaiting instructions on this.', '1741319910011', NULL, 'Answered', '2018-06-04T22:12:24.000Z', '2018-06-05T08:29:37.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(50, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'coco-cassie', 'cococa_mud3846qg@members.ebay.com.au', 'dealshub_au', 'coco-cassie has sent a question about item #272900035442, ending on 22-Jun-18 00:33:25 AEST - Double Elevated Twin Pet Bowl Dog Cat Feeder Food Water Raised Lifted Stand Pair', 'Hi there,\n\nJust letting you know that I still haven’t received my package as yet. Do you know why it is taking so long?\n\nCheers,\nCarolyn', '1741327344011', NULL, 'Answered', '2018-06-04T22:32:44.000Z', '2018-06-05T06:27:13.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(51, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'tea4lyf', 'wikkan_mzs5849siz@members.ebay.com.au', 'dealshub_au', 'I have a question about using my item or I want to send the seller a message: tea4lyf sent a message about Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer #273054737317', 'Im sorry? The supplier?? You&apos;re the supplier!!\r\n\r\nYou said you had this item in stock, you marked it as posted which it hasnt been!!\r\n\r\nThis is rubbish, i want a full refund today or I&apos;m reporting you to ebay.', '1741341736011', NULL, 'Unanswered', '2018-06-04T23:16:41.000Z', '2018-06-04T23:16:41.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(52, 2147483647, '', 'None', 'ResponseToASQQuestion', 'false', 'jenniferw8966', 'jennif_exsy5749pb@members.ebay.com.au', 'dealshub_au', 'dealshub_au sent a message about Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer #273054737317', 'Please send my purchase to 12 Wedge Court, Kyneton 3444. Thank you for your message.', '1741770758015', NULL, 'Unanswered', '2018-06-05T00:28:14.000Z', '2018-06-05T00:28:14.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(53, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'bodhitree74-5', 'bodhit_essy3799tajx@members.ebay.com.au', 'dealshub_au', 'bodhitree74-5 has sent a question about item #272958819750, ending on 26-Jun-18 21:44:59 AEST - Pub Style Dartboard in Wooden Cabinet with Score Board and Game Darts', 'Thanks\n\nCan you please send the attachment to bodhitree74@gmail.com as I can’t seem to open it to print. \n\nStuart', '1741904895011', NULL, 'Answered', '2018-06-05T20:13:46.000Z', '2018-06-06T06:48:16.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(54, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'tea4lyf', 'wikkan_mzs5849siz@members.ebay.com.au', 'dealshub_au', 'tea4lyf has sent a question about item #273054737317, ending on 04-Jul-18 16:33:48 AEST - Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer', 'you pretend in ebay that YOU are the supplier! saying you had them in stock!', '1742011892011', NULL, 'Answered', '2018-06-06T01:02:45.000Z', '2018-06-06T06:52:26.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(55, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'tda6626_dqdxcx', 'tda662_jt2109pfzg@members.ebay.com.au', 'dealshub_au', 'tda6626_dqdxcx has sent a question about item #272939042783, ending on 15-Jun-18 05:56:16 BST - 54 Piece Dining Set New Start Starter Black Solid Dinnerware Plates Bowls Mugs +', 'Hi there , \n\nJust wanted to let you know that I’m having problems with the courier for this package . I was suppose to get the delivery on Friday and when I contacted them they said I would receive the parcel on Monday then when it didn’t arrive they said Tuesday and today is Wednesday and it has missed the delivery van again ... I’m not sure what is going on with TNT but I’m really getting fed dealing with them . \nI really don’t want to arrange a refund but if TNT can’t find my package then I’ll have no choice but to arrange a refund from you \n\nI just wanted to inform you of there terrible service and hopefully I’ll get the parcel tomorrow as I really wanted this item \n\n\nKind regards \nDani', '1738935079010', NULL, 'Answered', '2018-06-06T05:07:55.000Z', '2018-06-06T09:04:03.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(56, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'bngplanes', 'bngpla_mys5126ndo@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272882781817, ending on 08-Jun-18 12:29:18 AEST - Set of 2 Modern Touch Table Desk Bedside Lamps Black Silver Home Decor', 'As Advertised “Free Shipping in Australia “ please despatch immediately or I will have your business for False Advertising. My post code is 4580 Tin Can Bay Queensland Australia. Barry', '1736143257016', NULL, 'Answered', '2018-06-06T22:01:34.000Z', '2018-06-07T01:52:02.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(57, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'coco-cassie', 'cococa_mud3846qg@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272900035442, ending on 22-Jun-18 00:33:25 AEST - Double Elevated Twin Pet Bowl Dog Cat Feeder Food Water Raised Lifted Stand Pair', 'Thank you.\n\nCan you please advise how long this will take and is there a tracking number?\n\nCheers,\nCarolyn', '1742609203011', NULL, 'Answered', '2018-06-06T22:40:48.000Z', '2018-06-07T03:41:50.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(58, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'tea4lyf', 'wikkan_mzs5849siz@members.ebay.com.au', 'dealshub_au', 'tea4lyf has sent a question about item #273054737317, ending on 04-Jul-18 16:33:48 AEST - Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer', 'Whatever! When you buy an item from a seller and they say they have it in stock that’s what I expect. Not a company that drop ships items!\nWhy was it already marked as sent? It was meant to be sent ages ago?', '1742633851011', NULL, 'Answered', '2018-06-07T00:00:08.000Z', '2018-06-07T01:37:55.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(59, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'tda6626_dqdxcx', 'tda662_jt2109pfzg@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272939042783, ending on 15-Jun-18 14:56:16 AEST - 54 Piece Dining Set New Start Starter Black Solid Dinnerware Plates Bowls Mugs +', 'Hi there \n\nAfter waiting for the last 3 days at home wit no delivery \nit comes as no surprise as I have actually received a call today from TNT stating the package has been damaged and some items have been broken \n\nI have asked TNT to send this back for a re package and return to myself so please chase this up ASAP they have told me they sent you photos \n\nCan you please advise me once this has been re sent\n\nThanks very much \nDanielle trent \n0425456526', '1739519799010', NULL, 'Answered', '2018-06-07T01:44:38.000Z', '2018-06-07T05:53:50.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(60, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'bodhitree74-5', 'bodhit_essy3799tajx@members.ebay.com.au', 'dealshub_au', 'bodhitree74-5 has sent a question about item #272958819750, ending on 26-Jun-18 21:44:59 AEST - Pub Style Dartboard in Wooden Cabinet with Score Board and Game Darts', 'Thanks\nI have sent the package in the mail today. Please let me know if you need anything more so I can receive a new one with all the equipment this time. Thanks', '1742692688011', NULL, 'Answered', '2018-06-07T02:59:30.000Z', '2018-06-07T03:45:59.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(61, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'tda6626_dqdxcx', 'tda662_jt2109pfzg@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272939042783, ending on 15-Jun-18 14:56:16 AEST - 54 Piece Dining Set New Start Starter Black Solid Dinnerware Plates Bowls Mugs +', 'Thankyou so much it’s not your fault it’s the shipping TNT so I appreciate all your assistance \n\n\nMany thanks \nDanielle', '1739595504010', NULL, 'Answered', '2018-06-07T06:04:16.000Z', '2018-06-08T09:18:14.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(62, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'tea4lyf', 'wikkan_mzs5849siz@members.ebay.com.au', 'dealshub_au', 'tea4lyf has sent a question about item #273054737317, ending on 04-Jul-18 16:33:48 AEST - Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer', 'Really? Then why was it marked as sent?\r\n\r\nIm sick of your excuses, when is the item arriving?', '1742784270011', NULL, 'Answered', '2018-06-07T07:44:18.000Z', '2018-06-08T01:07:59.000Z', NULL, '2018-06-07 20:13:50', '2018-06-21 21:56:25'),
(63, 2147483647, NULL, NULL, NULL, NULL, 'smart_choice12', 'smartchoicedirectebay@gmail.com', 'testuser_kalpesh', NULL, 'dfgdfgsdfg', NULL, NULL, NULL, '2018-06-08T05:58:45.114Z', NULL, NULL, '2018-06-08 11:28:49', NULL),
(64, 2147483647, '', 'None', 'ContactTransactionPartner', 'false', 'wynstay', 'wynsta_jti4157mcz@members.ebay.com.au', 'dealshub_au', 'Other: wynstay sent a message about Christmas Rope Light Led Lights Multi Colour Party Waterproof 50m Xmas Fairy #272907573243', 'Hello\r\nI purchased 3 of these Fairy lights late last year. 2 sets are fine however 1 set did not work when I got them out to use for a party this long weekend.\r\nI&apos;ve complied with instructions and kept the transformers inside at all times.\r\nPlease advise what the procedure is for refund / replacement.\r\nThank you,\r\nWynstay.', '1744680112011', NULL, 'Unanswered', '2018-06-11T03:08:55.000Z', '2018-06-11T03:08:55.000Z', NULL, '2018-06-21 21:16:58', '2018-06-21 21:56:25'),
(65, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'gemmdous0', 'gemmdo_jrs5196se@members.ebay.com.au', 'dealshub_au', 'gemmdous0 has sent a question about item #273222875751, ending on 18-Jun-18 16:28:05 AEST - Wahl Hair Clippers Beard Trimmer Set Cordless Professional Grooming Shaver New', 'Hello, I was just wondering if you have any tracking details? I was due to get this no later then today &amp; just need it for this weekend so was just checking if you knew when I’ll receive it? Thank you :)', '1737443802014', NULL, 'Answered', '2018-06-12T14:26:39.000Z', '2018-06-12T17:26:45.000Z', NULL, '2018-06-21 21:16:58', '2018-06-21 21:56:25'),
(66, 2147483647, '', 'None', 'ContactTransactionPartner', 'false', 'gemmdous0', 'gemmdo_jrs5196se@members.ebay.com.au', 'dealshub_au', 'gemmdous0 has sent a question about item #273222875751, ending on 18-Jun-18 16:28:05 AEST - Wahl Hair Clippers Beard Trimmer Set Cordless Professional Grooming Shaver New', 'Wow your amazing! Thank you so much for getting back to me so soon. I look forward to hearing back :) sorry for being a pain!', '1737545812014', NULL, 'Unanswered', '2018-06-12T17:29:44.000Z', '2018-06-12T17:29:44.000Z', NULL, '2018-06-21 21:16:58', '2018-06-21 21:56:25'),
(67, 2147483647, '', 'None', 'ContactTransactionPartner', 'false', 'wynstay', 'wynsta_jti4157mcz@members.ebay.com.au', 'dealshub_au', 'Other: wynstay sent a message about Christmas Rope Light Led Lights Multi Colour Party Waterproof 50m Xmas Fairy #272907573243', 'Hello I sent this email on Monday so far no reply. Before I contact ebay I thought I&apos;d try you again. Thank you.\r\n\r\nI purchased 3 of these Fairy lights late last year. 2 sets are fine however 1 set did not work when I got them out to use for a party this long weekend.\r\nI&apos;ve complied with instructions and kept the transformers inside at all times.\r\nPlease advise what the procedure is for refund / replacement.\r\nThank you,\r\nWynstay.', '1745983170011', NULL, 'Answered', '2018-06-13T02:04:13.000Z', '2018-06-13T08:03:32.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(68, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'tda6626_dqdxcx', 'tda662_jt2109pfzg@members.ebay.com.au', 'dealshub_au', 'tda6626_dqdxcx has sent a question about item #272939042783, ending on 15-Jun-18 05:56:16 BST - 54 Piece Dining Set New Start Starter Black Solid Dinnerware Plates Bowls Mugs +', 'Hi there \n\nI have received the package today and I have 1 coffee cup broken and a chipped small plate . Please see photos . Everything else is good , can you replace these 2 items', '1742898825010', NULL, 'Answered', '2018-06-13T05:10:03.000Z', '2018-06-13T08:08:46.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(69, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'long_7788', 'long77_dzi6848oio@members.ebay.com.au', 'dealshub_au', 'long_7788 has sent a question about item #273232998516, ending on 23-Jun-18 06:25:49 AEST - Electric Juicer Citrus Orange Juice Squeezer Press Machine Lemon Fruit Extractor', 'Hello, seller, I haven’t received my goods yet, it should be recive on last Friday? please check', '1743066524010', NULL, 'Answered', '2018-06-13T11:22:14.000Z', '2018-06-13T13:13:11.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(70, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'tda6626_dqdxcx', 'tda662_jt2109pfzg@members.ebay.com.au', 'dealshub_au', 'tda6626_dqdxcx has sent a question about item #272939042783, ending on 15-Jun-18 05:56:16 BST - 54 Piece Dining Set New Start Starter Black Solid Dinnerware Plates Bowls Mugs +', 'Oh really , well I guess that is ok to get some refund but I’m not happy because that the second time TNT have delivered these items and have broken them . Obviously fragile means nothing to that company \n\nThanks very much and I’ll await that 10% refund', '1743069644010', NULL, 'Answered', '2018-06-13T11:29:09.000Z', '2018-06-13T11:51:00.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(71, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'bodhitree74-5', 'bodhit_essy3799tajx@members.ebay.com.au', 'dealshub_au', 'bodhitree74-5 has sent a question about item #272958819750, ending on 26-Jun-18 21:44:59 AEST - Pub Style Dartboard in Wooden Cabinet with Score Board and Game Darts', 'Hi just checking you have recieved the return and sending me a new one?', '1746231162011', NULL, 'Answered', '2018-06-13T12:26:10.000Z', '2018-06-14T00:25:12.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(72, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'gemmdous0', 'gemmdo_jrs5196se@members.ebay.com.au', 'dealshub_au', 'gemmdous0 has sent a question about item #273222875751, ending on 18-Jun-18 16:28:05 AEST - Wahl Hair Clippers Beard Trimmer Set Cordless Professional Grooming Shaver New', 'Hi just wondering if you have any idea when I’ll receive my clippers? I have a full booked Saturday &amp; Sunday with clients that I need them for. Sorry to sound like a pain just seeing if you know when to expect them. Thanks', '1738122317014', NULL, 'Answered', '2018-06-13T16:03:08.000Z', '2018-06-14T12:27:49.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(73, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'long_7788', 'long77_dzi6848oio@members.ebay.com.au', 'dealshub_au', 'long_7788 has sent a question about item #273232998516, ending on 23-Jun-18 06:25:49 AEST - Electric Juicer Citrus Orange Juice Squeezer Press Machine Lemon Fruit Extractor', 'it s ok, i am not looking for something', '1743420152010', NULL, 'Answered', '2018-06-13T23:47:32.000Z', '2018-06-13T23:47:32.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(74, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'long_7788', 'long77_dzi6848oio@members.ebay.com.au', 'dealshub_au', 'long_7788 has sent a question about item #273232998516, ending on 23-Jun-18 06:25:49 AEST - Electric Juicer Citrus Orange Juice Squeezer Press Machine Lemon Fruit Extractor', 'it s ok, i am not looking something in purpose, it is to be last, just in case it lose, i am report that first.', '1743420591010', NULL, 'Answered', '2018-06-13T23:49:27.000Z', '2018-06-14T00:16:03.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(75, 2147483647, '', 'General', 'CustomCode', 'false', 'gemmdous0', 'gemmdo_jrs5196se@members.ebay.com.au', 'dealshub_au', 'Re: gemmdous0 has sent a question about item #273222875751,\r\n ending on 18-Jun-18 16:28:05 AEST - Wahl Hair Clippers Beard Trimmer Set\r\n Cordless Professional Grooming Shaver New', 'Thank you so much for your outstanding customer service! \nI am currently waiting on a personalised football for my nephew that I ordered in November &amp; the company aren’t being at all apologetic that so much has gone wrong with the order &amp; that I’m still waiting so this means the absolute world to me that you have done everything in your power to track this package down for me &amp; replace it with express post! I will be ordering with you guys for sure! Thank You Thank You Thank You! Fingers crossed it arrives today xx\nGem\n\nSent from my iPhone\n\nOn 14 Jun 2018, at 8:27 pm, eBay - dealshub_au &lt;&gt; wrote:\n\n', '1738771750014', NULL, 'Unanswered', '2018-06-14T17:14:58.000Z', '2018-06-14T17:14:58.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(76, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'long_7788', 'long77_dzi6848oio@members.ebay.com.au', 'dealshub_au', 'long_7788 has sent a question about item #273232998516, ending on 23-Jun-18 06:25:49 AEST - Electric Juicer Citrus Orange Juice Squeezer Press Machine Lemon Fruit Extractor', 'thanks for your undertanding too?  I promise that to you, if  secondone is came i, I will send it back to you ASAP…', '1744026335010', NULL, 'Answered', '2018-06-15T00:30:17.000Z', '2018-06-15T00:30:17.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25');
INSERT INTO `messages` (`id`, `item_id`, `item_title`, `question_type`, `message_type`, `display_to_public`, `sender_id`, `sender_email`, `recipient_id`, `subject`, `body`, `message_id`, `parrent_messageid`, `message_status`, `creation_date`, `last_modified_date`, `attachment`, `created_at`, `updated_at`) VALUES
(77, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'sk8ersoft', 'mattee_jss3308rjjc@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #273064694414, ending on 11-Jul-18 18:13:27 AEST - Milwaukee 12V Ratchet Cordless M12 3/8\" SQUARE DRIVE - Tool Only', 'Sorry I started a new job and didn&apos;t have time to get to the post office before 5pm this week. I should make it this afternoon fingers crossed.', '1727961451018', NULL, 'Answered', '2018-06-15T05:10:35.000Z', '2018-06-15T05:10:35.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(78, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'luminatrix', 'veriti_fz3129qh@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272900066737, ending on 22-Jun-18 01:02:10 AEST - Acacia Hardwood Timber Wood Outdoor Storage Chest Box Bench Seat Chair Stow Rest', 'You should state your free freight restrictions up front like all the other responsible sellers so you don&apos;t waste your buyer&apos;s time. Sincerely,', '1739117582014', NULL, 'Unanswered', '2018-06-15T09:04:15.000Z', '2018-06-15T09:04:15.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(79, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'bodhitree74-5', 'bodhit_essy3799tajx@members.ebay.com.au', 'dealshub_au', 'bodhitree74-5 has sent a question about item #272958819750, ending on 26-Jun-18 21:44:59 AEST - Pub Style Dartboard in Wooden Cabinet with Score Board and Game Darts', 'Hi\n\nJust checking you have recieved my return and will be sending a replacement soon.\n\nThanks\nStuart', '1747755033011', NULL, 'Answered', '2018-06-16T04:50:21.000Z', '2018-06-16T04:50:21.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:25'),
(80, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'gemmdous0', 'gemmdo_jrs5196se@members.ebay.com.au', 'dealshub_au', 'gemmdous0 has sent a question about item #273222875751, ending on 18-Jun-18 16:28:05 AEST - Wahl Hair Clippers Beard Trimmer Set Cordless Professional Grooming Shaver New', 'Thank you so much for my clippers!!!! You guys are amazing! Xoxox', '1739696270014', NULL, 'Unanswered', '2018-06-16T13:48:02.000Z', '2018-06-16T13:48:02.000Z', NULL, '2018-06-21 21:16:59', '2018-06-21 21:56:26'),
(81, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'hrpremx2prem', 'hrprem_gqdu2296lgz@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #273064673553, ending on 11-Jul-18 17:48:46 AEST - Makita Angle Grinder 2000W 230mm (9\") MT Series M9001G Powerful Trigger Switch', 'st leonards,7250/////just a suburb of launceston.', '1729125996018', NULL, 'Unanswered', '2018-06-17T19:25:05.000Z', '2018-06-17T19:25:05.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(82, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'mandy7743', 'mandy7_frs3828uej@members.ebay.com.au', 'dealshub_au', 'mandy7743 has sent a question about item #272931201292, ending on 12-Jul-18 09:36:27 AEST - Industrial Hallway Table Console Accent Side Entrance Hall Entry Shelf Storage', 'Do you have this in white with a lighter wood? Thanks', '1740402798014', NULL, 'Unanswered', '2018-06-18T08:18:54.000Z', '2018-06-18T08:18:54.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(83, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'jamalfafi', 'jamalf_iqs5355lbt@members.ebay.com.au', 'dealshub_au', 'jamalfafi has sent a question about item #273054737317, ending on 04-Jul-18 16:33:48 AEST - Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer', 'Are they coming in package or loose thanks', '1730954109019', NULL, 'Answered', '2018-06-18T22:45:18.000Z', '2018-06-19T04:28:11.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(84, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'jamalfafi', 'jamalf_iqs5355lbt@members.ebay.com.au', 'dealshub_au', 'jamalfafi has sent a question about item #273054737317, ending on 04-Jul-18 16:33:48 AEST - Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer', 'Any offer for three item', '1731104499019', NULL, 'Answered', '2018-06-19T06:34:56.000Z', '2018-06-19T06:34:56.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(85, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'aussiehorse01', 'aussie_eudu4229sdo@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #273222947353, ending on 18-Jun-18 17:04:04 AEST - WAHL Hair Clippers Haircut Set Cordless Beard Trimmer Shaver Professional 21Pcs', 'Hello.  I just looked it up and it said its been sent back to the sender.  Have you put the correct address on it? This is ridiculous. It&apos;s been nearly a month. Not happy', '1749983880015', NULL, 'Answered', '2018-06-19T13:31:54.000Z', '2018-06-19T13:31:54.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(86, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'janel6775', 'janel6_dr3127rgfg@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #273054737317, ending on 04-Jul-18 16:33:48 AEST - Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer', 'ok', '1741475291014', NULL, 'Unanswered', '2018-06-19T21:24:55.000Z', '2018-06-19T21:24:55.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(87, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'markphelan75', 'markph_fv4899sg@members.ebay.com.au', 'dealshub_au', 'markphelan75 has sent a question about item #272931413145, ending on 12-Jul-18 03:20:44 BST - NEW Gate Opener Double Automatic Solar Electric Swing Kit Remote Control 1000KG', 'Hi can you get a push button set to be wired in to this gate?  Thanks mark', '1734625986017', NULL, 'Unanswered', '2018-06-19T22:01:46.000Z', '2018-06-19T22:01:46.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(88, 2147483647, '', 'General', 'AskSellerQuestion', 'false', 'salim_au', 'salima_lv6329ui@members.ebay.com.au', 'dealshub_au', 'salim_au has sent a question about item #273054737317, ending on 04-Jul-18 16:33:48 AEST - Remington Beard Trimmer Cordless Hair Clipper Rechargeable Shaver Body Groomer', 'Hi there \nCan I try the shaver before I buy?\nCan I pick up?\nThanks', '1743399191016', NULL, 'Unanswered', '2018-06-19T23:56:26.000Z', '2018-06-19T23:56:26.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(89, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'rwhi2669', 'rwhi26_jw5759sf@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #273232998516, ending on 23-Jun-18 06:25:49 AEST - Electric Juicer Citrus Orange Juice Squeezer Press Machine Lemon Fruit Extractor', 'Thanks Mary.\r\nCheers, Robyn', '1750375777011', NULL, 'Unanswered', '2018-06-21T01:48:21.000Z', '2018-06-21T01:48:21.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(90, 2147483647, '', 'None', 'AskSellerQuestion', 'false', 'opalbee65', 'opalbe_etd5226mifm@members.ebay.com.au', 'dealshub_au', 'Other: opalbee65 sent a message about Makita 6mm Die Grinder Electric 1/4 Inch 240V 380W Corded Straight Mt Series New #273064624643', 'Hi ! \r\nI just bought grinder but I would like to ask you if selling those hi quality bits that can grind steal. \r\nKind Regards ... Goran !', '1751024294015', NULL, 'Unanswered', '2018-06-21T06:34:29.000Z', '2018-06-21T06:34:29.000Z', NULL, '2018-06-21 21:17:00', '2018-06-21 21:56:26'),
(91, 2147483647, '', 'General', 'ResponseToASQQuestion', 'false', 'eturne_4', 'eturne_itw2395tc@members.ebay.com.au', 'dealshub_au', 'dealshub_au has sent a question about item #272910218517, ending on 29-Jun-18 17:12:36 AEST - #DEALS Rabbit Hutch Chicken Coop Guinea Pig Ferret Cage Hen House 2 Storey Run', 'Hi \n\nI don’t mind paying the $20 for postage.. \nAll though I can’t make the payment till Wednesday after 3:00 pm.. \nWill my payed item be held till I am able to pay the excess? \n\nKind regards EJ Turner', '1750604669011', NULL, 'Answered', '2018-06-21T11:52:55.000Z', '2018-06-21T11:52:55.000Z', NULL, '2018-06-21 21:56:26', '2018-06-21 21:56:26');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2014_10_12_000000_create_users_table', 1),
(2, '2014_10_12_100000_create_password_resets_table', 1),
(3, '2018_04_19_120922_create_permission_tables', 1),
(4, '2018_04_19_121027_create_products_table', 1),
(5, '2016_06_01_000001_create_oauth_auth_codes_table', 2),
(6, '2016_06_01_000002_create_oauth_access_tokens_table', 2),
(7, '2016_06_01_000003_create_oauth_refresh_tokens_table', 2),
(8, '2016_06_01_000004_create_oauth_clients_table', 2),
(9, '2016_06_01_000005_create_oauth_personal_access_clients_table', 2),
(12, '2018_04_19_121027_create_genres_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `model_has_permissions`
--

CREATE TABLE `model_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `model_has_roles`
--

CREATE TABLE `model_has_roles` (
  `role_id` int(10) UNSIGNED NOT NULL,
  `model_type` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `model_id` bigint(20) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `model_has_roles`
--

INSERT INTO `model_has_roles` (`role_id`, `model_type`, `model_id`) VALUES
(2, 'App\\User', 2),
(2, 'App\\User', 4),
(2, 'App\\User', 5),
(2, 'App\\User', 6),
(2, 'App\\User', 7),
(2, 'App\\User', 8),
(2, 'App\\User', 9),
(2, 'App\\User', 10),
(2, 'App\\User', 11),
(2, 'App\\User', 12),
(2, 'App\\User', 13),
(2, 'App\\User', 14),
(2, 'App\\User', 15),
(2, 'App\\User', 16),
(2, 'App\\User', 17),
(2, 'App\\User', 18),
(2, 'App\\User', 19),
(2, 'App\\User', 20),
(2, 'App\\User', 21),
(2, 'App\\User', 22),
(2, 'App\\User', 23),
(2, 'App\\User', 24),
(2, 'App\\User', 25),
(2, 'App\\User', 26),
(2, 'App\\User', 27),
(2, 'App\\User', 28),
(2, 'App\\User', 29),
(2, 'App\\User', 30),
(2, 'App\\User', 31),
(2, 'App\\User', 32),
(2, 'App\\User', 33),
(2, 'App\\User', 34),
(2, 'App\\User', 35),
(2, 'App\\User', 37),
(2, 'App\\User', 38),
(2, 'App\\User', 39),
(2, 'App\\User', 40),
(2, 'App\\User', 41),
(2, 'App\\User', 42),
(2, 'App\\User', 43),
(2, 'App\\User', 44),
(2, 'App\\User', 45),
(2, 'App\\User', 46),
(2, 'App\\User', 47),
(2, 'App\\User', 48),
(2, 'App\\User', 49),
(2, 'App\\User', 50),
(4, 'App\\User', 1);

-- --------------------------------------------------------

--
-- Table structure for table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) DEFAULT NULL,
  `order_status` varchar(10) DEFAULT NULL,
  `adjustment_amount` varchar(20) DEFAULT NULL,
  `amount_paid` varchar(20) DEFAULT NULL,
  `amount_saved` varchar(20) DEFAULT NULL,
  `created_time` varchar(50) DEFAULT NULL,
  `payment_methods` varchar(50) DEFAULT NULL,
  `seller_email` varchar(50) DEFAULT NULL,
  `shipping_service_selected` varchar(50) DEFAULT NULL,
  `subtotal` varchar(20) DEFAULT NULL,
  `total` varchar(20) DEFAULT NULL,
  `integrated_merchant_credit_card_enabled` varchar(20) DEFAULT NULL,
  `eias_token` varchar(100) DEFAULT NULL,
  `payment_hold_status` varchar(50) DEFAULT NULL,
  `is_multi_leg_shipping` varchar(20) DEFAULT NULL,
  `seller_user_id` varchar(50) DEFAULT NULL,
  `seller_eias_token` varchar(100) DEFAULT NULL,
  `cancel_status` varchar(50) DEFAULT NULL,
  `contains_ebay_plus_transaction` varchar(20) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `buyer_id` int(11) DEFAULT NULL,
  `supplier` varchar(20) DEFAULT NULL,
  `buyer_note` text,
  `ebay_fee` varchar(20) DEFAULT '0.0',
  `paypal_fee` varchar(20) DEFAULT '0.0',
  `status` varchar(255) DEFAULT NULL,
  `folder` int(11) DEFAULT NULL,
  `ebay_fee_price` varchar(20) DEFAULT '0.00',
  `paypal_fee_price` varchar(20) DEFAULT '0.00',
  `cancel_reason` text,
  `refund_amount` varchar(50) DEFAULT NULL,
  `paid_time` varchar(100) DEFAULT NULL,
  `shipped_time` varchar(100) DEFAULT NULL,
  `order_shipping_cost` varchar(50) NOT NULL DEFAULT '0.00',
  `transaction_fee` varchar(50) NOT NULL DEFAULT '0.30',
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `orders`
--

INSERT INTO `orders` (`id`, `order_id`, `order_status`, `adjustment_amount`, `amount_paid`, `amount_saved`, `created_time`, `payment_methods`, `seller_email`, `shipping_service_selected`, `subtotal`, `total`, `integrated_merchant_credit_card_enabled`, `eias_token`, `payment_hold_status`, `is_multi_leg_shipping`, `seller_user_id`, `seller_eias_token`, `cancel_status`, `contains_ebay_plus_transaction`, `item_id`, `buyer_id`, `supplier`, `buyer_note`, `ebay_fee`, `paypal_fee`, `status`, `folder`, `ebay_fee_price`, `paypal_fee_price`, `cancel_reason`, `refund_amount`, `paid_time`, `shipped_time`, `order_shipping_cost`, `transaction_fee`, `user_id`, `account_id`, `created_at`, `updated_at`) VALUES
(1, '110268877238-28849395001', 'Active', '0.0', '0.0', '0.0', '2018-02-21T04:12:45.000Z', 'PayPal', 'finance@comonautgroup.com', NULL, '6000.0', '6000.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '528.00', '156.00', NULL, NULL, NULL, NULL, '20', '0.30', 1, 1, '2018-07-03 14:07:58', '2018-07-05 11:03:52'),
(2, '110274449122-28850629001', 'Active', '0.0', '0.0', '0.0', '2018-02-22T09:28:52.000Z', 'COD', 'finance@cosmonautgroup.com', NULL, '1399.99', '1399.99', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '123.20', '36.40', NULL, NULL, NULL, NULL, '0', '0.30', 1, 1, '2018-07-05 00:00:00', '2018-07-05 11:52:39'),
(3, '110274449122-28850638001', 'Active', '0.0', '0.0', '0.0', '2018-02-22T09:49:42.000Z', 'COD', 'finance@cosmonautgroup.com', NULL, '1399.99', '1399.99', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '123.20', '36.40', NULL, NULL, NULL, NULL, '0', '0.30', 1, 1, '2018-06-15 14:08:01', '2018-06-22 04:36:33'),
(4, '110276407048-28855788001', 'Completed', '0.0', '549.0', '0.0', '2018-03-01T10:33:11.000Z', 'COD', 'test.cosmonautgroup@gmail.com', NULL, '499.0', '549.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '48.31', '14.27', NULL, NULL, NULL, NULL, '0', '0.30', 1, 1, '2018-06-28 14:08:02', '2018-07-05 11:09:05'),
(5, '110276418065-28855839001', 'Active', '0.0', '0.0', '0.0', '2018-03-01T11:39:32.000Z', 'PayPal', 'test.cosmonautgroup@gmail.com', NULL, '500.0', '500.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'CancelPending', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '44.00', '13.00', NULL, NULL, NULL, NULL, '0', '0.30', 1, 1, '2018-06-15 14:08:04', '2018-06-26 12:28:34'),
(6, '110276418065-28855840001', 'Active', '0.0', '0.0', '0.0', '2018-03-01T11:43:18.000Z', 'PayPal', 'test.cosmonautgroup@gmail.com', NULL, '500.0', '500.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'CancelPending', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '44.00', '13.00', NULL, NULL, '2018-04-10T10:14:53.000Z', NULL, '0', '0.30', 1, 1, '2018-06-15 14:08:05', '2018-06-26 12:28:40'),
(7, '110261379103-28884239001', 'Active', '0.0', '0.0', '0.0', '2018-04-04T09:37:15.000Z', 'PayPal', 'finance@comonautgroup.com', NULL, '499.0', '499.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'CancelPending', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '43.91', '12.97', NULL, NULL, '2018-04-24T11:04:03.000Z', NULL, '60', '0.30', 1, 1, '2018-06-15 14:08:07', '2018-06-26 12:28:46'),
(8, '110259313412-28888100001', 'Active', '0.0', '0.0', '0.0', '2018-04-09T12:22:12.000Z', 'PayPal', 'finance-buyer@cosmonautgroup.com', NULL, '11555.0', '11555.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'CancelPending', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '1,016.84', '300.43', NULL, NULL, NULL, NULL, '0', '0.30', 1, 1, '2018-06-15 14:08:08', '2018-06-26 12:28:52'),
(9, '110284013864-28904560001', 'Completed', '0.0', '248.68', '0.0', '2018-05-01T09:42:32.000Z', 'COD', 'ducn2904@gmail.com', NULL, '236.68', '248.68', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '21.88', '6.47', NULL, NULL, NULL, NULL, '0', '0.30', 1, 1, '2018-06-15 14:08:09', '2018-06-26 12:28:58'),
(10, '110307922955-28908928001', 'Active', '0.0', '0.0', '0.0', '2018-05-07T05:33:29.000Z', 'PayPal', 'finance-buyer@cosmonautgroup.com', NULL, '250.0', '250.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '22.00', '6.50', NULL, NULL, NULL, NULL, '0', '0.30', 1, 1, '2018-07-04 14:08:11', '2018-07-05 11:09:22'),
(11, '110309097670-28915877001', 'Active', '0.0', '0.0', '0.0', '2018-05-15T05:51:16.000Z', 'CashOnPickup', 'ducn2904@gmail.com', NULL, '26.99', '71.99', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '6.34', '1.87', NULL, NULL, '2018-05-22T06:25:02.000Z', NULL, '25', '0.30', 1, 1, '2018-06-15 14:08:12', '2018-06-26 12:29:11'),
(12, '110309097670-28921450001', 'Active', '0.0', '0.0', '0.0', '2018-05-22T08:30:15.000Z', 'CashOnPickup', 'ducn2904@gmail.com', NULL, '26.99', '71.99', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '6.34', '1.87', NULL, NULL, NULL, NULL, '25', '0.30', 1, 1, '2018-06-15 14:08:14', '2018-06-26 12:29:17'),
(13, '110311440699-28921456001', 'Active', '0.0', '0.0', '0.0', '2018-05-22T09:03:08.000Z', 'PayPal', 'ducn2904@gmail.com', NULL, '995.0', '995.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'CancelPending', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '87.56', '25.87', NULL, NULL, '2018-05-22T09:06:29.000Z', NULL, '5', '0.30', 1, 1, '2018-06-15 14:08:15', '2018-06-26 12:29:23'),
(14, '110268877238-28922264001', 'Active', '0.0', '0.0', '0.0', '2018-05-23T06:14:52.000Z', 'PayPal', 'finance@comonautgroup.com', NULL, '6000.0', '6000.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '9', NULL, '52000', '16.00', NULL, NULL, 'NULL', '2018-06-20T04:38:25.644Z', '20', '0.30', 1, 1, '2018-06-03 14:08:16', '2018-07-05 11:48:27'),
(15, '110311869497-28923276001', 'Active', '0.0', '0.0', '0.0', '2018-05-24T06:40:31.000Z', 'PayPal', 'ducn2904@gmail.com', NULL, '265.0', '265.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '23.32', '6.89', NULL, NULL, '2018-05-24T06:53:47.000Z', NULL, '0', '0.30', 1, 1, '2018-06-03 14:08:18', '2018-07-05 11:48:34'),
(16, '110308791011-28922607001', 'Active', '0.0', '0.0', '0.0', '2018-05-23T12:28:14.000Z', 'PayPal', 'finance-buyer@cosmonautgroup.com', NULL, '65.0', '65.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '5.72', '1.69', NULL, NULL, '2018-05-30T06:58:52.000Z', NULL, '0', '0.30', 1, 1, '2018-06-01 14:08:19', '2018-07-05 11:48:40'),
(17, '110258974708-28935237001', 'Active', '0.0', '0.0', '0.0', '2018-06-07T14:05:29.000Z', 'PayPal', 'finance-buyer@cosmonautgroup.com', NULL, '60.0', '60.0', 'false', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAZGGpAidj6x9nY+seQ==', 'None', 'false', 'testuser_cosmonautgroup', 'nY+sHZ2PrBmdj6wVnY+sEZ2PrA2dj6wFk4GmAJiDqQqdj6x9nY+seQ==', 'NotApplicable', 'false', NULL, 1, NULL, NULL, '0.0', '0.0', '8', NULL, '5.30', '1.56', NULL, NULL, '2018-06-30T05:45:11.722Z', '', '0', '0.30', 1, 1, '2018-05-31 14:08:20', '2018-07-05 11:48:52');

-- --------------------------------------------------------

--
-- Table structure for table `orders_monitor`
--

CREATE TABLE `orders_monitor` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `when_product_sold` varchar(16) DEFAULT NULL,
  `quantity_to_raise_to` decimal(10,3) DEFAULT NULL,
  `when_product_sold_change_price` varchar(5) DEFAULT NULL,
  `start_price_raise_after_x_sold` decimal(10,3) DEFAULT NULL,
  `raise_only_for_first_sell` tinyint(1) DEFAULT NULL,
  `raise_for_each_sale` tinyint(1) DEFAULT NULL,
  `raise_additional_per` decimal(10,3) DEFAULT NULL,
  `raise_additional_dollar` decimal(10,3) DEFAULT NULL,
  `max_per_raise_limit` decimal(10,3) DEFAULT NULL,
  `max_price_raise_limit` decimal(10,3) DEFAULT NULL,
  `drop_price_product_not_sold` varchar(5) DEFAULT NULL,
  `drop_price_afterX_days` decimal(10,3) DEFAULT NULL,
  `drop_price_per` decimal(10,3) DEFAULT NULL,
  `drop_price_amount` decimal(10,3) DEFAULT NULL,
  `min_drop_price_per` decimal(10,3) DEFAULT NULL,
  `min_drop_price_amount` decimal(10,3) DEFAULT NULL,
  `is_start` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 : stop 1 : start',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_item`
--

CREATE TABLE `order_item` (
  `id` int(11) NOT NULL,
  `order_id` int(11) DEFAULT NULL,
  `item_id` int(11) DEFAULT NULL,
  `order_line_item_id` varchar(55) DEFAULT NULL,
  `item_price` varchar(11) DEFAULT '0.00',
  `tracking_id` varchar(100) DEFAULT NULL,
  `shipping_company_name` varchar(50) DEFAULT NULL,
  `supplier_product_link` text,
  `supplier_price` varchar(20) DEFAULT NULL,
  `supplier_shipping` varchar(20) DEFAULT NULL,
  `supplier_order_id` varchar(250) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_item`
--

INSERT INTO `order_item` (`id`, `order_id`, `item_id`, `order_line_item_id`, `item_price`, `tracking_id`, `shipping_company_name`, `supplier_product_link`, `supplier_price`, `supplier_shipping`, `supplier_order_id`) VALUES
(1, 1, 7, '110268877238-28849395001', '6000.0', NULL, NULL, NULL, NULL, NULL, NULL),
(2, 2, 9, '110274449122-28850629001', '1399.99', NULL, NULL, NULL, NULL, NULL, NULL),
(3, 3, 9, '110274449122-28850638001', '1399.99', NULL, NULL, NULL, NULL, NULL, NULL),
(4, 4, 12, '110276407048-28855788001', '499.0', NULL, NULL, NULL, NULL, NULL, NULL),
(5, 5, 14, '110276418065-28855839001', '500.0', NULL, NULL, NULL, NULL, NULL, NULL),
(6, 6, 14, '110276418065-28855840001', '500.0', NULL, NULL, NULL, NULL, NULL, NULL),
(7, 7, 17, '110261379103-28884239001', '499.0', NULL, NULL, NULL, NULL, NULL, NULL),
(8, 8, 19, '110259313412-28888100001', '11555.0', NULL, NULL, NULL, NULL, NULL, NULL),
(9, 9, 6, '110284013864-28904560001', '236.68', NULL, NULL, NULL, NULL, NULL, NULL),
(10, 10, 22, '110307922955-28908928001', '250.0', NULL, NULL, NULL, NULL, NULL, NULL),
(11, 11, 24, '110309097670-28915877001', '26.99', NULL, NULL, NULL, NULL, NULL, NULL),
(12, 12, 24, '110309097670-28921450001', '26.99', NULL, NULL, NULL, NULL, NULL, NULL),
(13, 13, 3, '110311440699-28921456001', '199.0', NULL, NULL, NULL, NULL, NULL, NULL),
(14, 14, 7, '110268877238-28922264001', '6000.0', NULL, NULL, 'http://dropshipai.cosmonautgroup.com/monitors', '5560', '50', '453453423'),
(15, 15, 4, '110311869497-28923276001', '265.0', NULL, NULL, NULL, NULL, NULL, NULL),
(16, 16, 29, '110308791011-28922607001', '65.0', NULL, NULL, NULL, NULL, NULL, NULL),
(17, 17, 25, '110258974708-28935237001', '60.0', NULL, NULL, 'https://dropshipai.cosmonautgroup.com/active-listing', '55', '3', '1255487');

-- --------------------------------------------------------

--
-- Table structure for table `order_notes`
--

CREATE TABLE `order_notes` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `note` text NOT NULL,
  `is_active` int(11) DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `update_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_notes`
--

INSERT INTO `order_notes` (`id`, `order_id`, `note`, `is_active`, `created_at`, `update_at`) VALUES
(1, 14, 'test order note', 0, '2018-06-19 03:00:19', '2018-06-19 03:02:23'),
(2, 14, 'test order note', 0, '2018-06-19 03:00:34', '2018-06-19 03:10:19'),
(3, 14, 'note 123', 0, '2018-06-19 03:03:19', '2018-06-19 03:03:33'),
(4, 14, 'fdghfdghfgh', 0, '2018-06-19 03:04:36', '2018-06-19 03:05:21'),
(5, 14, 'fdghfdghfgh', 0, '2018-06-19 03:04:43', '2018-06-19 03:05:34'),
(6, 14, 'cdhdghfgh', 0, '2018-06-19 03:05:27', '2018-06-19 03:05:38'),
(7, 14, 'fgdfgsdfg', 0, '2018-06-19 03:10:24', '2018-06-19 03:10:41'),
(8, 14, 'fghfgh', 0, '2018-06-19 03:10:31', '2018-06-19 03:10:36'),
(9, 14, 'test order note', 0, '2018-06-19 03:10:50', '2018-06-19 04:24:22'),
(10, 17, 'test for order note1', 1, '2018-06-19 03:56:21', '2018-06-19 23:45:07'),
(11, 14, 'fdgdsfgsdfg', 0, '2018-06-19 04:24:28', '2018-06-19 04:24:36'),
(12, 14, 'fghfghfgh', 0, '2018-06-19 04:25:14', '2018-06-19 04:38:51'),
(13, 17, 'test order note', 0, '2018-06-19 06:47:23', '2018-06-19 06:47:41');

-- --------------------------------------------------------

--
-- Table structure for table `order_payment`
--

CREATE TABLE `order_payment` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `payment_status` varchar(10) DEFAULT NULL,
  `payer` varchar(20) DEFAULT NULL,
  `payee` varchar(55) DEFAULT NULL,
  `payment_time` varchar(50) DEFAULT NULL,
  `payment_amount` varchar(50) DEFAULT NULL,
  `reference_id` varchar(50) DEFAULT NULL,
  `fee_or_credit_amount` varchar(50) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_payment`
--

INSERT INTO `order_payment` (`id`, `order_id`, `payment_status`, `payer`, `payee`, `payment_time`, `payment_amount`, `reference_id`, `fee_or_credit_amount`, `created_at`, `updated_at`) VALUES
(1, '4', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-03-01T10:53:52.000Z', '549.0', '8MB658638B8794440', '', '2018-06-15 07:08:02', '2018-06-15 07:08:02'),
(2, '9', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-05-01T09:45:25.000Z', '248.68', '8EU82572YC616083U', '', '2018-06-15 07:08:09', '2018-06-15 07:08:09'),
(3, '4', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-03-01T10:53:52.000Z', '549.0', '8MB658638B8794440', '', '2018-06-15 07:08:52', '2018-06-15 07:08:52'),
(4, '9', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-05-01T09:45:25.000Z', '248.68', '8EU82572YC616083U', '', '2018-06-15 07:08:59', '2018-06-15 07:08:59'),
(5, '4', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-03-01T10:53:52.000Z', '549.0', '8MB658638B8794440', '', '2018-06-19 06:24:23', '2018-06-19 06:24:23'),
(6, '9', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-05-01T09:45:25.000Z', '248.68', '8EU82572YC616083U', '', '2018-06-19 06:24:30', '2018-06-19 06:24:30'),
(7, '4', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-03-01T10:53:52.000Z', '549.0', '8MB658638B8794440', '', '2018-06-19 06:44:33', '2018-06-19 06:44:33'),
(8, '9', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-05-01T09:45:25.000Z', '248.68', '8EU82572YC616083U', '', '2018-06-19 06:44:40', '2018-06-19 06:44:40'),
(9, '4', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-03-01T10:53:52.000Z', '549.0', '8MB658638B8794440', '', '2018-06-21 21:36:35', '2018-06-21 21:36:35'),
(10, '9', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-05-01T09:45:25.000Z', '248.68', '8EU82572YC616083U', '', '2018-06-21 21:36:42', '2018-06-21 21:36:42'),
(11, '4', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-03-01T10:53:52.000Z', '549.0', '8MB658638B8794440', '', '2018-06-26 17:58:31', '2018-06-26 17:58:31'),
(12, '9', 'Pending', 'testuser_kalpesh', 'testuser_cosmonautgroup', '2018-05-01T09:45:25.000Z', '248.68', '8EU82572YC616083U', '', '2018-06-26 17:59:02', '2018-06-26 17:59:02');

-- --------------------------------------------------------

--
-- Table structure for table `order_processing`
--

CREATE TABLE `order_processing` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `auto_order` varchar(5) DEFAULT NULL,
  `bundle_add_on` varchar(5) DEFAULT NULL,
  `oder_only_prime` varchar(5) DEFAULT NULL,
  `hipshipper` varchar(5) DEFAULT NULL,
  `max_product_price_auto_order` decimal(10,3) DEFAULT NULL,
  `max_lose_amount` decimal(10,3) DEFAULT NULL,
  `set_order_shipped` varchar(25) DEFAULT NULL,
  `gift_message` varchar(255) DEFAULT NULL,
  `set_ontrack_shipping_carrier` varchar(5) DEFAULT NULL,
  `is_start` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 : stop 1 : start ',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_refunds`
--

CREATE TABLE `order_refunds` (
  `id` int(11) NOT NULL,
  `order_id` varchar(50) NOT NULL,
  `refundstatus` varchar(20) NOT NULL DEFAULT 'Succeeded',
  `refund_type` varchar(20) NOT NULL DEFAULT 'PaymentRefund',
  `refund_to` varchar(20) NOT NULL,
  `refund_time` varchar(50) NOT NULL,
  `refund_amount` varchar(50) NOT NULL DEFAULT '0',
  `reference_id` varchar(50) NOT NULL,
  `fee_or_credit_amount` varchar(50) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `order_status`
--

CREATE TABLE `order_status` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `status` int(11) DEFAULT '1',
  `order` int(11) NOT NULL DEFAULT '0',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_status`
--

INSERT INTO `order_status` (`id`, `name`, `status`, `order`, `created_at`, `updated_at`) VALUES
(1, 'Active', 0, 0, '2018-02-26 12:14:38', '2018-04-16 07:34:47'),
(2, 'Cancelled', 0, 0, '2018-02-26 12:14:38', '2018-04-16 07:34:45'),
(3, 'Completed', 0, 0, '2018-02-26 12:14:56', '2018-04-16 07:34:40'),
(4, 'InShipping', 0, 0, '2018-02-26 12:14:56', '2018-02-28 05:51:30'),
(5, 'newstatus', 0, 0, '2018-02-26 14:27:53', '2018-02-27 05:03:43'),
(6, 'new', 0, 0, '2018-02-27 05:13:48', '2018-04-16 07:35:47'),
(7, 'In Packing', 0, 0, '2018-02-28 10:59:04', '2018-04-16 07:34:29'),
(8, 'New Order', 1, 5, '2018-04-16 07:36:00', '2018-06-08 08:35:31'),
(9, 'Product ordered - Waiting on tracking', 1, 4, '2018-04-16 07:36:49', '2018-06-08 08:35:31'),
(10, 'Tracking uploaded', 1, 2, '2018-04-16 07:37:06', '2018-06-08 08:35:31'),
(11, 'Order Completed - Delivered', 1, 10, '2018-04-16 07:37:58', '2018-06-08 08:35:31'),
(12, 'Order cancelled - Refunded', 1, 6, '2018-04-16 07:38:33', '2018-06-08 08:35:31'),
(13, 'Product OOS - Check Alternative', 1, 8, '2018-04-16 07:59:06', '2018-06-08 08:35:31'),
(14, 'Supplier cancelled - Need to re-order', 1, 11, '2018-04-16 07:59:34', '2018-06-08 08:35:31'),
(15, 'Parcel Awaiting Collection - Need to message customer', 1, 13, '2018-04-16 08:00:01', '2018-06-08 08:35:31'),
(16, 'Bad Address - Need to message customer', 1, 1, '2018-04-16 08:00:36', '2018-06-08 08:35:31'),
(17, 'Parcel Awaiting Collection - Waiting on customer responses', 1, 12, '2018-04-16 08:01:59', '2018-06-08 08:35:31'),
(18, 'Bad Address - Waiting on customer responses', 1, 7, '2018-04-16 08:02:17', '2018-06-08 08:35:31'),
(19, 'Order In Dispute', 1, 3, '2018-04-16 08:03:12', '2018-06-08 08:35:31'),
(20, 'Order completed - Refunded', 1, 9, '2018-04-16 08:05:15', '2018-06-08 08:35:31'),
(21, 'Order Completed - Partially Refunded', 1, 15, '2018-04-16 08:05:31', '2018-06-08 08:35:31'),
(22, 'Order Completed - With Replacement', 1, 14, '2018-04-16 08:33:32', '2018-06-08 08:35:31');

-- --------------------------------------------------------

--
-- Table structure for table `order_tags`
--

CREATE TABLE `order_tags` (
  `id` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `tag_id` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `order_tags`
--

INSERT INTO `order_tags` (`id`, `order_id`, `tag_id`) VALUES
(9, 14, 58),
(10, 14, 57),
(11, 14, 55);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `token` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `permissions`
--

CREATE TABLE `permissions` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `permissions`
--

INSERT INTO `permissions` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(1, 'role-list', 'web', '2018-04-19 06:47:26', '2018-04-19 06:47:26'),
(2, 'role-create', 'web', '2018-04-19 06:47:26', '2018-04-19 06:47:26'),
(3, 'role-edit', 'web', '2018-04-19 06:47:26', '2018-04-19 06:47:26'),
(4, 'role-delete', 'web', '2018-04-19 06:47:26', '2018-04-19 06:47:26'),
(5, 'product-list', 'web', '2018-04-19 06:47:26', '2018-04-19 06:47:26'),
(6, 'product-create', 'web', '2018-04-19 06:47:26', '2018-04-19 06:47:26'),
(7, 'product-edit', 'web', '2018-04-19 06:47:26', '2018-04-19 06:47:26'),
(8, 'product-delete', 'web', '2018-04-19 06:47:26', '2018-04-19 06:47:26'),
(9, 'edit articles', 'web', '2018-04-19 07:21:47', '2018-04-19 07:21:47'),
(10, 'delete articles', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48'),
(11, 'publish articles', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48'),
(12, 'unpublish articles', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48'),
(13, 'genre-list', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48'),
(14, 'genre-create', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48'),
(15, 'genre-edit', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48'),
(16, 'genre-delete', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48');

-- --------------------------------------------------------

--
-- Table structure for table `products_monitor`
--

CREATE TABLE `products_monitor` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `default_break_event` decimal(10,3) DEFAULT NULL,
  `additional_dollar` decimal(10,3) DEFAULT NULL,
  `additional_per` decimal(10,3) DEFAULT NULL,
  `end_untracked_listing` varchar(5) DEFAULT NULL,
  `not_available_listing` int(18) DEFAULT NULL,
  `when_product_available` int(16) DEFAULT NULL,
  `quantity_to_raise_to` decimal(10,3) DEFAULT NULL,
  `dynamic_policy_creation` varchar(5) DEFAULT NULL,
  `min_allowed_quantity_in_stock` decimal(10,3) DEFAULT NULL,
  `min_profit_$per_product` decimal(10,3) DEFAULT NULL,
  `when_product_vero` varchar(12) DEFAULT NULL,
  `when_product_blocked` varchar(12) DEFAULT NULL,
  `max_shipping_time` varchar(5) DEFAULT NULL,
  `automatic_sku_filling` varchar(5) DEFAULT NULL,
  `is_start` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 : stop 1 : start ',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `profitable_monitor`
--

CREATE TABLE `profitable_monitor` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `account_id` int(11) NOT NULL,
  `listing_not_profitable` varchar(12) DEFAULT NULL,
  `listing_left_days` int(11) DEFAULT NULL,
  `listing_min_sold_quantity` int(11) DEFAULT NULL,
  `listing_min_watchers` int(11) DEFAULT NULL,
  `listing_min_views` int(11) DEFAULT NULL,
  `is_start` tinyint(1) NOT NULL DEFAULT '0' COMMENT '0 : stop 1 : start ',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `roles`
--

CREATE TABLE `roles` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `guard_name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `roles`
--

INSERT INTO `roles` (`id`, `name`, `guard_name`, `created_at`, `updated_at`) VALUES
(2, 'writer', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48'),
(3, 'moderator', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48'),
(4, 'super-admin', 'web', '2018-04-19 07:21:48', '2018-04-19 07:21:48');

-- --------------------------------------------------------

--
-- Table structure for table `role_has_permissions`
--

CREATE TABLE `role_has_permissions` (
  `permission_id` int(10) UNSIGNED NOT NULL,
  `role_id` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `role_has_permissions`
--

INSERT INTO `role_has_permissions` (`permission_id`, `role_id`) VALUES
(1, 4),
(2, 4),
(3, 4),
(4, 4),
(5, 2),
(5, 4),
(6, 2),
(6, 4),
(7, 4),
(8, 4),
(9, 2),
(9, 4),
(10, 4),
(11, 3),
(11, 4),
(12, 3),
(12, 4),
(13, 4),
(14, 4),
(15, 4),
(16, 4);

-- --------------------------------------------------------

--
-- Table structure for table `selling_status`
--

CREATE TABLE `selling_status` (
  `id` int(50) NOT NULL,
  `converted_current_price` varchar(50) DEFAULT NULL,
  `current_price` varchar(50) DEFAULT NULL,
  `item_id` int(50) DEFAULT NULL,
  `quantity_sold` varchar(20) DEFAULT NULL,
  `listing_status` varchar(20) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `selling_status`
--

INSERT INTO `selling_status` (`id`, `converted_current_price`, `current_price`, `item_id`, `quantity_sold`, `listing_status`, `created_at`, `updated_at`) VALUES
(1, '10.32', '13.99', 1, '0', 'Active', '2018-06-15 07:07:51.632039', '2018-06-15 07:07:51.632039'),
(2, '7384.82', '10013.99', 2, '0', 'Active', '2018-06-15 07:07:52.954862', '2018-06-15 07:07:52.954862'),
(3, '146.75', '199', 3, '6', 'Active', '2018-06-15 07:07:54.329372', '2018-06-15 07:07:54.329372'),
(4, '195.42', '265.0', 4, '1', 'Active', '2018-06-15 07:07:55.780830', '2018-06-15 07:07:55.780830'),
(5, '174.54', '236.68', 5, '0', 'Active', '2018-06-15 07:07:57.097229', '2018-06-15 07:07:57.097229'),
(6, '174.54', '236.68', 6, '1', 'Active', '2018-06-15 07:07:58.499581', '2018-06-15 07:07:58.499581'),
(7, '44.25', '60.0', 8, '0', 'Active', '2018-06-15 07:07:59.832307', '2018-06-15 07:07:59.832307'),
(8, '44.25', '60.0', 10, '0', 'Active', '2018-06-15 07:08:01.188427', '2018-06-15 07:08:01.188427'),
(9, '44.25', '60.0', 11, '0', 'Active', '2018-06-15 07:08:02.643889', '2018-06-15 07:08:02.643889'),
(10, '44.25', '60.0', 13, '0', 'Active', '2018-06-15 07:08:03.965204', '2018-06-15 07:08:03.965204'),
(11, '44.25', '60.0', 15, '0', 'Active', '2018-06-15 07:08:05.276896', '2018-06-15 07:08:05.276896'),
(12, '44.25', '60.0', 16, '0', 'Active', '2018-06-15 07:08:06.644197', '2018-06-15 07:08:06.644197'),
(13, '44.25', '60.0', 18, '0', 'Active', '2018-06-15 07:08:07.985678', '2018-06-15 07:08:07.985678'),
(14, '44.25', '60.0', 20, '0', 'Active', '2018-06-15 07:08:09.352848', '2018-06-15 07:08:09.352848'),
(15, '44.25', '60.0', 21, '0', 'Active', '2018-06-15 07:08:10.692825', '2018-06-15 07:08:10.692825'),
(16, '44.25', '60.0', 23, '0', 'Active', '2018-06-15 07:08:12.094030', '2018-06-15 07:08:12.094030'),
(17, '44.25', '60.0', 25, '1', 'Active', '2018-06-15 07:08:13.451172', '2018-06-15 07:08:13.451172'),
(18, '368.73', '500.0', 26, '0', 'Active', '2018-06-15 07:08:14.811285', '2018-06-15 07:08:14.811285'),
(19, '44.25', '60.0', 27, '0', 'Active', '2018-06-15 07:08:16.197241', '2018-06-15 07:08:16.197241'),
(20, '8521.23', '11555.0', 19, '1', 'Active', '2018-06-15 07:08:17.566256', '2018-06-15 07:08:17.566256'),
(21, '5161.41', '6999.0', 28, '0', 'Active', '2018-06-15 07:08:18.872104', '2018-06-15 07:08:18.872104'),
(22, '5161.41', '6999.0', 30, '0', 'Active', '2018-06-15 07:08:20.171846', '2018-06-15 07:08:20.171846'),
(23, '5161.41', '6999.0', 31, '0', 'Active', '2018-06-15 07:08:21.492069', '2018-06-15 07:08:21.492069'),
(24, '5161.41', '6999.0', 32, '0', 'Active', '2018-06-15 07:08:22.868794', '2018-06-15 07:08:22.868794'),
(25, '5161.41', '6999.0', 33, '0', 'Active', '2018-06-15 07:08:24.242478', '2018-06-15 07:08:24.242478'),
(26, '5161.41', '6999.0', 34, '0', 'Active', '2018-06-15 07:08:25.639545', '2018-06-15 07:08:25.639545'),
(27, '5161.41', '6999.0', 35, '0', 'Active', '2018-06-15 07:08:26.999654', '2018-06-15 07:08:26.999654'),
(28, '4423.96', '5999.0', 36, '0', 'Active', '2018-06-15 07:08:28.269196', '2018-06-15 07:08:28.269196'),
(29, '55.31', '75.0', 37, '0', 'Active', '2018-06-15 07:08:29.560320', '2018-06-15 07:08:29.560320'),
(30, '47.93', '65.0', 29, '1', 'Active', '2018-06-15 07:08:30.913893', '2018-06-15 07:08:30.913893'),
(31, '367.99', '499.0', 17, '1', 'Active', '2018-06-15 07:08:32.327458', '2018-06-15 07:08:32.327458'),
(32, '19.9', '26.99', 24, '2', 'Active', '2018-06-15 07:08:33.618175', '2018-06-15 07:08:33.618175'),
(33, '4424.7', '6000.0', 38, '0', 'Active', '2018-06-15 07:08:35.008519', '2018-06-15 07:08:35.008519'),
(34, '4424.7', '6000.0', 7, '2', 'Active', '2018-06-15 07:08:36.311364', '2018-06-15 07:08:36.311364'),
(35, '18436.25', '25000.0', 39, '0', 'Active', '2018-06-21 21:36:28.348949', '2018-06-21 21:36:28.348949'),
(36, '16961.35', '23000.0', 40, '0', 'Active', '2018-06-21 21:36:29.790434', '2018-06-21 21:36:29.790434'),
(37, '36.87', '50.0', 41, '0', 'Active', '2018-06-26 18:22:03.860925', '2018-06-26 18:22:03.860925'),
(38, '191.74', '260.0', 42, '0', 'Active', '2018-06-26 18:22:11.015334', '2018-06-26 18:22:11.015334');

-- --------------------------------------------------------

--
-- Table structure for table `service_option`
--

CREATE TABLE `service_option` (
  `id` int(11) NOT NULL,
  `shipping_service` varchar(50) DEFAULT NULL,
  `shipping_service_cost` varchar(20) DEFAULT NULL,
  `shipping_service_priority` varchar(10) DEFAULT NULL,
  `expedited_service` varchar(20) DEFAULT NULL,
  `shipping_time_min` varchar(20) DEFAULT NULL,
  `shipping_time_max` varchar(20) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `service_option`
--

INSERT INTO `service_option` (`id`, `shipping_service`, `shipping_service_cost`, `shipping_service_priority`, `expedited_service`, `shipping_time_min`, `shipping_time_max`, `order_id`, `created_at`, `updated_at`) VALUES
(1, 'AU_OfficeworksMailman', '20.0', '1', 'false', '2', '14', 1, '2018-06-15 07:07:58.599722', '2018-06-15 07:07:58.599722'),
(2, 'AU_RegisteredParcelPostPrepaidSatchel5kg', '50.0', '1', 'false', '2', '6', 2, '2018-06-15 07:07:59.972938', '2018-06-15 07:07:59.972938'),
(3, 'AU_RegisteredParcelPostPrepaidSatchel5kg', '50.0', '1', 'false', '2', '6', 3, '2018-06-15 07:08:01.394795', '2018-06-15 07:08:01.394795'),
(4, 'AU_RegularParcelWithTracking', '50.0', '1', 'false', '2', '6', 4, '2018-06-15 07:08:02.708275', '2018-06-15 07:08:02.708275'),
(5, 'AU_Regular', '45.0', '1', 'false', '2', '6', 5, '2018-06-15 07:08:04.192548', '2018-06-15 07:08:04.192548'),
(6, 'AU_Regular', '45.0', '1', 'false', '2', '6', 6, '2018-06-15 07:08:05.606310', '2018-06-15 07:08:05.606310'),
(7, 'AU_RegisteredParcelPostPrepaidSatchel3kg', '60.0', '1', 'false', '2', '6', 7, '2018-06-15 07:08:07.012564', '2018-06-15 07:08:07.012564'),
(8, 'AU_StandardDelivery', '0.0', '1', 'false', '1', '6', 8, '2018-06-15 07:08:08.427499', '2018-06-15 07:08:08.427499'),
(9, 'AU_StandardDelivery', '0.0', '1', 'false', '1', '6', 9, '2018-06-15 07:08:09.869475', '2018-06-15 07:08:09.869475'),
(10, 'AU_StandardDelivery', '0.0', '1', 'false', '1', '6', 10, '2018-06-15 07:08:11.270333', '2018-06-15 07:08:11.270333'),
(11, 'AU_OfficeworksMailman', '25.0', '1', 'false', '2', '14', 11, '2018-06-15 07:08:12.679153', '2018-06-15 07:08:12.679153'),
(12, 'AU_OfficeworksMailman', '25.0', '1', 'false', '2', '14', 12, '2018-06-15 07:08:14.039392', '2018-06-15 07:08:14.039392'),
(13, 'AU_StandardDelivery', '5.0', '1', 'false', '1', '6', 13, '2018-06-15 07:08:15.386008', '2018-06-15 07:08:15.386008'),
(14, 'AU_OfficeworksMailman', '20.0', '1', 'false', '2', '14', 14, '2018-06-15 07:08:16.735426', '2018-06-15 07:08:16.735426'),
(15, 'AU_Regular', '0.0', '1', 'false', '2', '6', 15, '2018-06-15 07:08:18.068552', '2018-06-15 07:08:18.068552'),
(16, 'AU_StandardDeliveryRegisted', '0.0', '1', 'false', '1', '6', 16, '2018-06-15 07:08:19.494529', '2018-06-15 07:08:19.494529'),
(17, 'AU_StandardDelivery', '0.0', '1', 'false', '1', '6', 17, '2018-06-15 07:08:20.921800', '2018-06-15 07:08:20.921800');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_address`
--

CREATE TABLE `shipping_address` (
  `id` int(11) NOT NULL,
  `name` varchar(150) DEFAULT NULL,
  `street1` varchar(150) DEFAULT NULL,
  `street2` varchar(150) DEFAULT NULL,
  `city_name` varchar(150) DEFAULT NULL,
  `state_or_provinance` varchar(150) DEFAULT NULL,
  `country_name` varchar(150) DEFAULT NULL,
  `phone` varchar(50) DEFAULT NULL,
  `postal_code` varchar(50) DEFAULT NULL,
  `external_address_id` varchar(150) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping_address`
--

INSERT INTO `shipping_address` (`id`, `name`, `street1`, `street2`, `city_name`, `state_or_provinance`, `country_name`, `phone`, `postal_code`, `external_address_id`, `order_id`, `created_at`, `updated_at`) VALUES
(1, '', '', '', '', '', '', '', '', '', 1, '2018-06-15 07:07:58.603609', '2018-06-15 07:07:58.603609'),
(2, '', '', '', '', '', '', '', '', '', 2, '2018-06-15 07:07:59.976843', '2018-06-15 07:07:59.976843'),
(3, '', '', '', '', '', '', '', '', '', 3, '2018-06-15 07:08:01.398970', '2018-06-15 07:08:01.398970'),
(4, 'Michael C. Oneil', '2 Purcell Place', 'CLIFDEN NSW', 'NSW', 'New South Wales', 'Australia', '0267626495', '2460', '', 4, '2018-06-15 07:08:02.712595', '2018-06-15 07:08:02.712595'),
(5, '', '', '', '', '', '', '', '', '', 5, '2018-06-15 07:08:04.195918', '2018-06-15 07:08:04.195918'),
(6, '', '', '', '', '', '', '', '', '', 6, '2018-06-15 07:08:05.610627', '2018-06-15 07:08:05.610627'),
(7, '', '', '', '', '', '', '', '', '', 7, '2018-06-15 07:08:07.016744', '2018-06-15 07:08:07.016744'),
(8, '', '', '', '', '', '', '', '', '', 8, '2018-06-15 07:08:08.430343', '2018-06-15 07:08:08.430343'),
(9, 'kalpesh patel', 'address', '', 'city', 'NEW DELHI', 'India', '1000031039', '110034', '', 9, '2018-06-15 07:08:09.873445', '2018-06-15 07:08:09.873445'),
(10, '', '', '', '', '', '', '', '', '', 10, '2018-06-15 07:08:11.273382', '2018-06-15 07:08:11.273382'),
(11, '', '', '', '', '', '', '', '', '', 11, '2018-06-15 07:08:12.683232', '2018-06-15 07:08:12.683232'),
(12, '', '', '', '', '', '', '', '', '', 12, '2018-06-15 07:08:14.042010', '2018-06-15 07:08:14.042010'),
(13, '', '', '', '', '', '', '', '', '', 13, '2018-06-15 07:08:15.390398', '2018-06-15 07:08:15.390398'),
(14, '', '', '', '', '', '', '', '', '', 14, '2018-06-15 07:08:16.739199', '2018-06-15 07:08:16.739199'),
(15, '', '', '', '', '', '', '', '', '', 15, '2018-06-15 07:08:18.072621', '2018-06-15 07:08:18.072621'),
(16, '', '', '', '', '', '', '', '', '', 16, '2018-06-15 07:08:19.497765', '2018-06-15 07:08:19.497765'),
(17, '', '', '', '', '', '', '', '', '', 17, '2018-06-15 07:08:20.926589', '2018-06-15 07:08:20.926589');

-- --------------------------------------------------------

--
-- Table structure for table `shipping_details`
--

CREATE TABLE `shipping_details` (
  `id` int(11) NOT NULL,
  `sales_tax` varchar(20) DEFAULT NULL,
  `shipping_service_option` int(11) DEFAULT NULL,
  `selling_manager_sales_record_number` int(10) DEFAULT NULL,
  `get_it_fast` varchar(10) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `shipping_details`
--

INSERT INTO `shipping_details` (`id`, `sales_tax`, `shipping_service_option`, `selling_manager_sales_record_number`, `get_it_fast`, `order_id`, `created_at`, `updated_at`) VALUES
(1, '', 1, 103, 'false', 1, '2018-06-15 07:07:58.602077', '2018-06-15 07:07:58.602077'),
(2, '', 2, 104, 'false', 2, '2018-06-15 07:07:59.975545', '2018-06-15 07:07:59.975545'),
(3, '', 3, 105, 'false', 3, '2018-06-15 07:08:01.397550', '2018-06-15 07:08:01.397550'),
(4, '', 4, 106, 'false', 4, '2018-06-15 07:08:02.710934', '2018-06-15 07:08:02.710934'),
(5, '', 5, 107, 'false', 5, '2018-06-15 07:08:04.194883', '2018-06-15 07:08:04.194883'),
(6, '', 6, 108, 'false', 6, '2018-06-15 07:08:05.608959', '2018-06-15 07:08:05.608959'),
(7, '', 7, 109, 'false', 7, '2018-06-15 07:08:07.014362', '2018-06-15 07:08:07.014362'),
(8, '', 8, 110, 'false', 8, '2018-06-15 07:08:08.429354', '2018-06-15 07:08:08.429354'),
(9, '', 9, 111, 'false', 9, '2018-06-15 07:08:09.872174', '2018-06-15 07:08:09.872174'),
(10, '', 10, 112, 'false', 10, '2018-06-15 07:08:11.272263', '2018-06-15 07:08:11.272263'),
(11, '', 11, 113, 'false', 11, '2018-06-15 07:08:12.681167', '2018-06-15 07:08:12.681167'),
(12, '', 12, 114, 'false', 12, '2018-06-15 07:08:14.041104', '2018-06-15 07:08:14.041104'),
(13, '', 13, 115, 'false', 13, '2018-06-15 07:08:15.389064', '2018-06-15 07:08:15.389064'),
(14, '', 14, 116, 'false', 14, '2018-06-15 07:08:16.737972', '2018-06-15 07:08:16.737972'),
(15, '', 15, 118, 'false', 15, '2018-06-15 07:08:18.071207', '2018-06-15 07:08:18.071207'),
(16, '', 16, 117, 'false', 16, '2018-06-15 07:08:19.496672', '2018-06-15 07:08:19.496672'),
(17, '', 17, 119, 'false', 17, '2018-06-15 07:08:20.924763', '2018-06-15 07:08:20.924763');

-- --------------------------------------------------------

--
-- Table structure for table `smtp_setting`
--

CREATE TABLE `smtp_setting` (
  `id` bigint(20) NOT NULL,
  `smtp_host` text NOT NULL,
  `smtp_user` text NOT NULL,
  `smtp_pass` varchar(20) NOT NULL,
  `smtp_protocol` varchar(20) NOT NULL,
  `smtp_port` text NOT NULL,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `subscription`
--

CREATE TABLE `subscription` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `price` double(10,2) NOT NULL,
  `validity` varchar(55) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription`
--

INSERT INTO `subscription` (`id`, `name`, `price`, `validity`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Subscription', 2000.00, '2 days', 1, '2018-07-11 18:26:56', '2018-07-11 18:26:56');

-- --------------------------------------------------------

--
-- Table structure for table `subscription_access`
--

CREATE TABLE `subscription_access` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `is_active` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `subscription_access`
--

INSERT INTO `subscription_access` (`id`, `name`, `is_active`, `created_at`, `updated_at`) VALUES
(1, 'Test Access', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(2, 'Test Access', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(3, 'Test Access', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(4, 'Test Access', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(5, 'Test user', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(6, 'Only Read', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00'),
(7, 'Only Read', 1, '0000-00-00 00:00:00', '0000-00-00 00:00:00');

-- --------------------------------------------------------

--
-- Table structure for table `subscription_has_access`
--

CREATE TABLE `subscription_has_access` (
  `id` int(11) NOT NULL,
  `sucription_id` int(11) NOT NULL,
  `access_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tags`
--

CREATE TABLE `tags` (
  `id` int(11) NOT NULL,
  `tag_name` varchar(50) NOT NULL,
  `status` int(11) NOT NULL DEFAULT '1',
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tags`
--

INSERT INTO `tags` (`id`, `tag_name`, `status`, `created_at`, `updated_at`) VALUES
(1, 'Special Order', 1, '2018-04-24 05:02:51', NULL),
(2, 'Old Order', 0, '2018-04-24 05:02:51', '2018-05-15 03:24:15'),
(3, 'New Order', 1, '2018-04-24 05:02:51', NULL),
(4, 'cancelled order', 1, '2018-04-24 05:02:51', NULL),
(5, 'Full Refunded', 1, '2018-04-24 05:02:51', NULL),
(6, 'Partially Refunded', 1, '2018-04-24 05:02:51', '2018-05-19 04:43:35'),
(7, 'Order with Dispute', 1, '2018-04-24 05:02:51', NULL),
(8, 'Order with replacement', 1, '2018-04-24 05:02:51', NULL),
(9, 'Order with Return', 1, '2018-04-24 05:02:51', NULL),
(10, 'Need to claim refund with supplier', 1, '2018-04-24 05:02:51', NULL),
(11, 'Need to claim replacement with supplier', 1, '2018-04-24 05:02:51', NULL),
(12, 'Supplier provided full refund', 1, '2018-04-24 05:02:51', NULL),
(13, 'Supplier provided partial refund', 1, '2018-04-24 05:02:51', NULL),
(14, 'Supplier sent replacement', 1, '2018-04-24 05:02:51', NULL),
(15, 'Supplier request product to be returned - Inform c', 1, '2018-04-24 05:02:51', NULL),
(16, 'Supplier request product to be returned - Customer', 1, '2018-04-24 05:02:51', NULL),
(17, 'Customer received replacement', 1, '2018-04-24 05:02:51', NULL),
(18, 'new tag', 0, '2018-04-24 05:02:51', '2018-05-15 03:24:34'),
(19, 'Self Bought & Shipped', 1, '2018-04-24 05:02:51', NULL),
(20, 'Authority to Leave Parcel', 1, '2018-05-08 16:17:36', NULL),
(21, 'WES.com', 1, '2018-05-10 19:38:40', NULL),
(22, 'Kmart.com.au', 1, '2018-05-10 19:39:27', NULL),
(23, 'Dropshipzone.com', 1, '2018-05-10 19:43:18', NULL),
(24, 'Target.com.au', 1, '2018-05-11 00:23:04', NULL),
(25, 'Supplier OOS', 1, '2018-05-11 00:34:07', NULL),
(26, 'JustTools', 1, '2018-05-11 02:23:54', NULL),
(27, 'Ebay', 1, '2018-05-11 02:26:18', NULL),
(28, 'Realsmart', 1, '2018-05-11 02:27:22', NULL),
(29, 'CL TOOL CENTRE', 1, '2018-05-11 02:29:23', NULL),
(30, 'Tool Mart', 1, '2018-05-11 02:47:01', NULL),
(31, 'MIGHTYAPE', 1, '2018-05-12 01:06:50', NULL),
(32, 'BIG W', 1, '2018-05-12 01:52:22', NULL),
(33, 'Appliances Online', 1, '2018-05-12 03:08:15', NULL),
(34, 'Shaver Shop', 1, '2018-05-13 01:30:31', '2018-05-15 03:24:24'),
(35, 'SYDNEY TOOLS', 1, '2018-05-13 02:33:22', NULL),
(36, 'MY DEALS', 1, '2018-05-13 02:41:42', NULL),
(37, 'THE GOOD GUY', 1, '2018-05-13 02:54:13', NULL),
(38, 'TAOBAO', 1, '2018-05-13 03:48:04', NULL),
(39, 'Toy R US', 1, '2018-05-13 09:18:10', NULL),
(40, '2nd Address Requested From Customer', 1, '2018-05-13 09:56:10', NULL),
(41, 'CashBack - Awaiting Confirmation', 1, '2018-05-13 09:59:34', NULL),
(42, 'CashBack - Confirmed', 1, '2018-05-13 09:59:46', NULL),
(43, 'HarveyNorman', 1, '2018-05-13 10:16:47', NULL),
(44, 'Paid in store', 1, '2018-05-14 21:29:59', NULL),
(45, 'TotalTools', 1, '2018-05-14 22:21:08', NULL),
(46, 'KGELECTRONIC', 1, '2018-05-14 23:20:34', NULL),
(47, 'Klika', 1, '2018-05-14 23:51:36', NULL),
(48, 'KULLER.COM.AU', 1, '2018-05-15 00:22:03', NULL),
(49, 'BOURNE ELECTRONIC', 1, '2018-05-15 00:36:00', NULL),
(50, 'Order Arrived Damaged', 1, '2018-05-15 03:25:02', NULL),
(51, 'Orders with delivery rejection/not collected', 1, '2018-05-15 03:45:37', NULL),
(52, 'JBHIFI', 1, '2018-05-15 03:48:55', NULL),
(53, 'Paid With Card', 1, '2018-05-17 23:14:19', NULL),
(54, 'Promoted Listing Sale', 1, '2018-05-19 02:15:17', NULL),
(55, 'Bank Transfer to Supplier', 1, '2018-05-19 02:25:50', NULL),
(56, 'Charged Additional Shipping Cost Orders', 1, '2018-05-20 01:40:53', NULL),
(57, 'Kogan', 1, '2018-05-22 20:40:21', NULL),
(58, 'MYER', 1, '2018-05-30 00:51:55', NULL),
(59, 'sgdsfgsdfgsdfg', 1, '2018-06-19 09:36:10', '2018-06-19 09:36:10'),
(60, 'demo', 1, '2018-07-09 12:04:55', NULL),
(61, 'test', 1, '2018-07-09 14:16:03', NULL),
(62, 'tester', 0, '2018-07-09 16:19:56', '2018-07-10 14:13:33'),
(63, 'test user', 1, '2018-07-10 14:30:54', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `tracking`
--

CREATE TABLE `tracking` (
  `ID` int(11) NOT NULL,
  `order_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `tracking_id` varchar(50) DEFAULT NULL,
  `shipping_company_name` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tracking`
--

INSERT INTO `tracking` (`ID`, `order_id`, `item_id`, `tracking_id`, `shipping_company_name`) VALUES
(1, 8, 19, '235725', 'UPS'),
(2, 9, 6, '569856323465356', 'FedEx'),
(3, 10, 22, '569856329852', 'FedEx'),
(4, 11, 24, '5698532324568999', 'FedEx');

-- --------------------------------------------------------

--
-- Table structure for table `transaction`
--

CREATE TABLE `transaction` (
  `id` int(11) NOT NULL,
  `shipping_details` varchar(20) DEFAULT NULL,
  `created_date` varchar(50) DEFAULT NULL,
  `quantity_purchased` int(11) DEFAULT NULL,
  `transaction_id` varchar(50) DEFAULT NULL,
  `transaction_price` varchar(20) DEFAULT NULL,
  `final_value_fee` varchar(20) DEFAULT '0.00',
  `transaction_site_id` varchar(10) DEFAULT NULL,
  `platform` varchar(50) DEFAULT NULL,
  `order_line_item_id` varchar(50) DEFAULT NULL,
  `ebay_plus_transaction` varchar(20) DEFAULT NULL,
  `order_id` int(11) DEFAULT NULL,
  `item_id` varchar(25) DEFAULT NULL,
  `paypal_transaction_id` varchar(50) DEFAULT NULL,
  `paypal_tansaction_id_supplier` varchar(50) DEFAULT NULL,
  `created_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6),
  `updated_at` datetime(6) NOT NULL DEFAULT CURRENT_TIMESTAMP(6)
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Dumping data for table `transaction`
--

INSERT INTO `transaction` (`id`, `shipping_details`, `created_date`, `quantity_purchased`, `transaction_id`, `transaction_price`, `final_value_fee`, `transaction_site_id`, `platform`, `order_line_item_id`, `ebay_plus_transaction`, `order_id`, `item_id`, `paypal_transaction_id`, `paypal_tansaction_id_supplier`, `created_at`, `updated_at`) VALUES
(1, '103', '2018-02-21T04:12:45.000Z', 1, '28849395001', '6000.0', '', 'US', 'eBay', '110268877238-28849395001', 'false', 1, '110268877238', NULL, NULL, '2018-06-15 07:07:58.596723', '2018-06-15 07:07:58.596723'),
(2, '104', '2018-02-22T09:28:52.000Z', 1, '28850629001', '1399.99', '', 'US', 'eBay', '110274449122-28850629001', 'false', 2, '110274449122', NULL, NULL, '2018-06-15 07:07:59.969672', '2018-06-15 07:07:59.969672'),
(3, '105', '2018-02-22T09:49:42.000Z', 1, '28850638001', '1399.99', '', 'US', 'eBay', '110274449122-28850638001', 'false', 3, '110274449122', NULL, NULL, '2018-06-15 07:08:01.389930', '2018-06-15 07:08:01.389930'),
(4, '106', '2018-03-01T10:33:11.000Z', 1, '28855788001', '499.0', '', 'US', 'eBay', '110276407048-28855788001', 'false', 4, '110276407048', NULL, NULL, '2018-06-15 07:08:02.703601', '2018-06-15 07:08:02.703601'),
(5, '107', '2018-03-01T11:39:32.000Z', 1, '28855839001', '500.0', '', 'Australia', 'eBay', '110276418065-28855839001', 'false', 5, '110276418065', NULL, NULL, '2018-06-15 07:08:04.189338', '2018-06-15 07:08:04.189338'),
(6, '108', '2018-03-01T11:43:18.000Z', 1, '28855840001', '500.0', '', 'Australia', 'eBay', '110276418065-28855840001', 'false', 6, '110276418065', NULL, NULL, '2018-06-15 07:08:05.602227', '2018-06-15 07:08:05.602227'),
(7, '109', '2018-04-04T09:37:15.000Z', 1, '28884239001', '499.0', '', 'US', 'eBay', '110261379103-28884239001', 'false', 7, '110261379103', NULL, NULL, '2018-06-15 07:08:07.009731', '2018-06-15 07:08:07.009731'),
(8, '110', '2018-04-09T12:22:12.000Z', 1, '28888100001', '11555.0', '', 'US', 'eBay', '110259313412-28888100001', 'false', 8, '110259313412', NULL, NULL, '2018-06-15 07:08:08.424782', '2018-06-15 07:08:08.424782'),
(9, '111', '2018-05-01T09:42:32.000Z', 1, '28904560001', '236.68', '', 'US', 'eBay', '110284013864-28904560001', 'false', 9, '110284013864', NULL, NULL, '2018-06-15 07:08:09.866002', '2018-06-15 07:08:09.866002'),
(10, '112', '2018-05-07T05:33:29.000Z', 1, '28908928001', '250.0', '', 'US', 'eBay', '110307922955-28908928001', 'false', 10, '110307922955', NULL, NULL, '2018-06-15 07:08:11.266646', '2018-06-15 07:08:11.266646'),
(11, '113', '2018-05-15T05:51:16.000Z', 1, '28915877001', '26.99', '', 'Australia', 'eBay', '110309097670-28915877001', 'false', 11, '110309097670', NULL, NULL, '2018-06-15 07:08:12.675618', '2018-06-15 07:08:12.675618'),
(12, '114', '2018-05-22T08:30:15.000Z', 1, '28921450001', '26.99', '', 'US', 'eBay', '110309097670-28921450001', 'false', 12, '110309097670', NULL, NULL, '2018-06-15 07:08:14.036795', '2018-06-15 07:08:14.036795'),
(13, '115', '2018-05-22T09:03:08.000Z', 5, '28921456001', '199.0', '', 'Australia', 'eBay', '110311440699-28921456001', 'false', 13, '110311440699', NULL, NULL, '2018-06-15 07:08:15.382336', '2018-06-15 07:08:15.382336'),
(14, '116', '2018-05-23T06:14:52.000Z', 1, '28922264001', '6000.0', '', 'US', 'eBay', '110268877238-28922264001', 'false', 14, '110268877238', NULL, NULL, '2018-06-15 07:08:16.730499', '2018-06-15 07:08:16.730499'),
(15, '118', '2018-05-24T06:40:31.000Z', 1, '28923276001', '265.0', '', 'US', 'eBay', '110311869497-28923276001', 'false', 15, '110311869497', NULL, NULL, '2018-06-15 07:08:18.064130', '2018-06-15 07:08:18.064130'),
(16, '117', '2018-05-23T12:28:14.000Z', 1, '28922607001', '65.0', '', 'US', 'eBay', '110308791011-28922607001', 'false', 16, '110308791011', NULL, NULL, '2018-06-15 07:08:19.491364', '2018-06-15 07:08:19.491364'),
(17, '119', '2018-06-07T14:05:29.000Z', 1, '28935237001', '60.0', '', 'US', 'eBay', '110258974708-28935237001', 'false', 17, '110258974708', NULL, NULL, '2018-06-15 07:08:20.917479', '2018-06-15 07:08:20.917479');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `email` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `profile_image` varchar(255) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `isActive` int(11) NOT NULL COMMENT '0 : inActive , 1 : active',
  `password` varchar(191) COLLATE utf8mb4_unicode_ci NOT NULL,
  `remember_token` varchar(100) COLLATE utf8mb4_unicode_ci DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `profile_image`, `isActive`, `password`, `remember_token`, `created_at`, `updated_at`) VALUES
(1, 'chirag  parmar', 'support@chiragparmar.com', 'chirag-parmar.jpg', 1, '$2y$10$LYQPB45dS.MzUkgWL.pwx.Ipfu77r9bHquCb5KAWRhNauKJjCs3/W', 'kZ9tgsTsTE7tHubxa2CDegSb3ONfJNCOaeK00IsPJ9dBTr6m1JruCk89b5od', '2018-04-19 07:05:55', '2018-06-19 20:03:17'),
(8, 'test', 'test.cosmonautgroup@gmail.com', 'test.jpeg', 0, '$2y$10$6xs.m1ekqC4EfRnfDmNOqOhwLBvOPjMZJct4lfZorhExCLxByzXj.', NULL, '2018-06-19 20:22:39', '2018-06-19 20:30:37'),
(9, 'vipul123', 'vipul.cosmonautgroup@gmail.com', 'vipul123.jpg', 0, '$2y$10$GmsG3Fvn7K5g4w8vOk73i.5LWUtjrotpn18ZqOP6oTpRv6j9R66CC', '9BvqlF9w4HU3JOwkfwvmOWGo9l8mSlU1LzDOOzoxo5p3DHluR9IUFhC1jRUe', '2018-06-19 20:41:26', '2018-06-19 20:49:19'),
(14, 'vipul', 'vipul12@gmail.com', NULL, 0, '$2y$10$N9hgrwu60oZBSfKPoUhCFeOFp9CaFuLwGfqIeO1hI2cdKey6Ger8W', 'm08V3jixI6yCP56eUUUnPA9UMvahMA', '2018-07-04 05:43:21', '2018-07-04 05:43:21'),
(38, 'Chirag Daxini', 'chiragdaxini@gmail.com', NULL, 1, '$2y$10$FgLuUeDlk.JQPvKbr/uiV.q5AFVg7Wxf7DGJtNiGGAvNZTKBBngIK', NULL, '2018-07-04 07:48:06', '2018-07-04 07:48:15'),
(50, 'Sonal Panchal', 'sonali25panchal@gmail.com', NULL, 1, '$2y$10$hjlbBf5cB4e9Bo8U3z8s5uxsCmEF1o2TJhnZnnH1R9phSQ7.c/l2.', '0QM1xoGvgLzo8uT1G6RZx17l9beJWpOr4hdNgCu3rWCKRdBPxPTKvqXTRjlm', '2018-07-05 07:17:09', '2018-07-05 07:17:18');

-- --------------------------------------------------------

--
-- Table structure for table `user_has_subscription`
--

CREATE TABLE `user_has_subscription` (
  `id` int(11) NOT NULL,
  `user_id` int(11) NOT NULL,
  `subscription_id` int(11) NOT NULL,
  `created_at` datetime NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` datetime NOT NULL DEFAULT '0000-00-00 00:00:00' ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `accounts`
--
ALTER TABLE `accounts`
  ADD PRIMARY KEY (`id`),
  ADD KEY `user_id` (`int_user_id`);

--
-- Indexes for table `action_log`
--
ALTER TABLE `action_log`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `amazon_account`
--
ALTER TABLE `amazon_account`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `buyer`
--
ALTER TABLE `buyer`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `check_out_status`
--
ALTER TABLE `check_out_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `email_templates`
--
ALTER TABLE `email_templates`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `feedback`
--
ALTER TABLE `feedback`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item`
--
ALTER TABLE `item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_has_price`
--
ALTER TABLE `item_has_price`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `item_shipping_detail`
--
ALTER TABLE `item_shipping_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `listing_details`
--
ALTER TABLE `listing_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `messages`
--
ALTER TABLE `messages`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`model_id`,`model_type`),
  ADD KEY `model_has_permissions_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD PRIMARY KEY (`role_id`,`model_id`,`model_type`),
  ADD KEY `model_has_roles_model_type_model_id_index` (`model_type`,`model_id`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`),
  ADD KEY `orders_ibfk_2` (`item_id`),
  ADD KEY `orders_ibfk_1` (`buyer_id`) USING BTREE;

--
-- Indexes for table `orders_monitor`
--
ALTER TABLE `orders_monitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_item`
--
ALTER TABLE `order_item`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_notes`
--
ALTER TABLE `order_notes`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_payment`
--
ALTER TABLE `order_payment`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_processing`
--
ALTER TABLE `order_processing`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_refunds`
--
ALTER TABLE `order_refunds`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_status`
--
ALTER TABLE `order_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `order_tags`
--
ALTER TABLE `order_tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`);

--
-- Indexes for table `permissions`
--
ALTER TABLE `permissions`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `products_monitor`
--
ALTER TABLE `products_monitor`
  ADD PRIMARY KEY (`id`),
  ADD KEY `account_id` (`account_id`);

--
-- Indexes for table `profitable_monitor`
--
ALTER TABLE `profitable_monitor`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `roles`
--
ALTER TABLE `roles`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD PRIMARY KEY (`permission_id`,`role_id`),
  ADD KEY `role_has_permissions_role_id_foreign` (`role_id`);

--
-- Indexes for table `selling_status`
--
ALTER TABLE `selling_status`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `service_option`
--
ALTER TABLE `service_option`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_address`
--
ALTER TABLE `shipping_address`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `shipping_details`
--
ALTER TABLE `shipping_details`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `smtp_setting`
--
ALTER TABLE `smtp_setting`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription`
--
ALTER TABLE `subscription`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription_access`
--
ALTER TABLE `subscription_access`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `subscription_has_access`
--
ALTER TABLE `subscription_has_access`
  ADD PRIMARY KEY (`id`),
  ADD KEY `subscription` (`sucription_id`),
  ADD KEY `access` (`access_id`);

--
-- Indexes for table `tags`
--
ALTER TABLE `tags`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `tracking`
--
ALTER TABLE `tracking`
  ADD PRIMARY KEY (`ID`);

--
-- Indexes for table `transaction`
--
ALTER TABLE `transaction`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `users_email_unique` (`email`);

--
-- Indexes for table `user_has_subscription`
--
ALTER TABLE `user_has_subscription`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `accounts`
--
ALTER TABLE `accounts`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `action_log`
--
ALTER TABLE `action_log`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `amazon_account`
--
ALTER TABLE `amazon_account`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `buyer`
--
ALTER TABLE `buyer`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `check_out_status`
--
ALTER TABLE `check_out_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `email_templates`
--
ALTER TABLE `email_templates`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `feedback`
--
ALTER TABLE `feedback`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `item`
--
ALTER TABLE `item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=43;

--
-- AUTO_INCREMENT for table `item_has_price`
--
ALTER TABLE `item_has_price`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `item_shipping_detail`
--
ALTER TABLE `item_shipping_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `listing_details`
--
ALTER TABLE `listing_details`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `messages`
--
ALTER TABLE `messages`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=92;

--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `orders_monitor`
--
ALTER TABLE `orders_monitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_item`
--
ALTER TABLE `order_item`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `order_notes`
--
ALTER TABLE `order_notes`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=14;

--
-- AUTO_INCREMENT for table `order_payment`
--
ALTER TABLE `order_payment`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=13;

--
-- AUTO_INCREMENT for table `order_processing`
--
ALTER TABLE `order_processing`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_refunds`
--
ALTER TABLE `order_refunds`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `order_status`
--
ALTER TABLE `order_status`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;

--
-- AUTO_INCREMENT for table `order_tags`
--
ALTER TABLE `order_tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT for table `permissions`
--
ALTER TABLE `permissions`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;

--
-- AUTO_INCREMENT for table `products_monitor`
--
ALTER TABLE `products_monitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `profitable_monitor`
--
ALTER TABLE `profitable_monitor`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `roles`
--
ALTER TABLE `roles`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `selling_status`
--
ALTER TABLE `selling_status`
  MODIFY `id` int(50) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=39;

--
-- AUTO_INCREMENT for table `service_option`
--
ALTER TABLE `service_option`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `shipping_address`
--
ALTER TABLE `shipping_address`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `shipping_details`
--
ALTER TABLE `shipping_details`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `smtp_setting`
--
ALTER TABLE `smtp_setting`
  MODIFY `id` bigint(20) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `subscription`
--
ALTER TABLE `subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `subscription_access`
--
ALTER TABLE `subscription_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT for table `subscription_has_access`
--
ALTER TABLE `subscription_has_access`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT for table `tags`
--
ALTER TABLE `tags`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=64;

--
-- AUTO_INCREMENT for table `tracking`
--
ALTER TABLE `tracking`
  MODIFY `ID` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `transaction`
--
ALTER TABLE `transaction`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=51;

--
-- AUTO_INCREMENT for table `user_has_subscription`
--
ALTER TABLE `user_has_subscription`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT;

--
-- Constraints for dumped tables
--

--
-- Constraints for table `model_has_permissions`
--
ALTER TABLE `model_has_permissions`
  ADD CONSTRAINT `model_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `model_has_roles`
--
ALTER TABLE `model_has_roles`
  ADD CONSTRAINT `model_has_roles_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `role_has_permissions`
--
ALTER TABLE `role_has_permissions`
  ADD CONSTRAINT `role_has_permissions_permission_id_foreign` FOREIGN KEY (`permission_id`) REFERENCES `permissions` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `role_has_permissions_role_id_foreign` FOREIGN KEY (`role_id`) REFERENCES `roles` (`id`) ON DELETE CASCADE;

--
-- Constraints for table `subscription_has_access`
--
ALTER TABLE `subscription_has_access`
  ADD CONSTRAINT `access` FOREIGN KEY (`access_id`) REFERENCES `subscription_access` (`id`) ON DELETE CASCADE,
  ADD CONSTRAINT `subscription` FOREIGN KEY (`sucription_id`) REFERENCES `subscription` (`id`) ON DELETE CASCADE ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
