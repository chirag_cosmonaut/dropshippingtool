<div class="sidebar" id="sidebar">
    <nav class="sidebar-nav" id="sidebar-nav-scroller">
        <ul class="nav">
            @hasrole('writer')
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('dashboard') }}">
                        <i class="mdi mdi-gauge"></i> Dashboard
                        <span class="badge badge-main badge-boxed badge-warning">New</span>
                    </a>
                </li>

                <li class="nav-title">Listings</li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('active-listing') }}">
                        <i class="fa fa-calendar-check-o"></i> Active Listing
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('untracked-listing') }}">
                        <i class="fa fa-calendar-times-o"></i> Untracked Listing
                    </a>
                </li>

                <li class="nav-title">Sales</li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('order') }}">
                        <i class="fa fa-shopping-cart"></i> Orders
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('customer-service') }}">
                        <i class="fa fa-user-circle-o"></i> Customer Service
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('invoice') }}">
                        <i class="fa fa-credit-card"></i> Invoice
                    </a>
                </li>
              
               
                <li class="nav-title"> Advance Features </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('monitors') }}">
                        <i class="fa fa-line-chart"></i> Monitors
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('finder') }}">
                        <i class="fa fa-bullseye"></i> AutoDs Finder
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ route('uploader') }}">
                        <i class="fa fa-upload"></i> Uploader
                    </a>
                </li>

                <li class="divider"></li>
                <li class="nav-title"> Internal Database</li>
                <li class="nav-item">
                    <a class="nav-link " href="#">
                        <i class="fa fa-address-book"></i> Suppliers
                    </a>
                </li>

                <li class="nav-item nav-dropdown ">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="mdi mdi-file-document"></i> Stock Control</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link " href="#">
                                <i class="fa fa-tasks"></i> Manage Warehouse
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#">
                                <i class="fa fa-truck"></i> Stock Transfer
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#">
                                <i class="fa fa-hand-paper-o"></i> Stock Adjustment
                            </a>
                        </li>

                    </ul>
                </li>

                <li class="nav-title"> Integration</li>
                <li class="nav-item nav-dropdown ">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="mdi mdi-file-document"></i> Channels</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('ebay_channel') }}">
                                <i class="fa fa-tasks"></i> eBay Integration
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="{{ route('amazon_channel') }}" >
                                <i class="fa fa-truck"></i> Amazon Integration
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#">
                                <i class="fa fa-hand-paper-o"></i> WallMart Integration
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="divider"></li>
                <li class="nav-title"> Reports</li>
                <li class="nav-item">
                    <a class="nav-link " href="#">
                        <i class="fa fa-truck"></i> Sales Reports
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">
                        <i class="fa fa-truck"></i> Inventory Reports
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">
                        <i class="fa fa-truck"></i> Out of Stock 
                    </a>
                </li>
                <li class="nav-item nav-dropdown ">
                    <a class="nav-link nav-dropdown-toggle" href="#">
                        <i class="mdi mdi-file-document"></i> Manage Policies</a>
                    <ul class="nav-dropdown-items">
                        <li class="nav-item">
                            <a class="nav-link " href="#">
                                <i class="fa fa-tasks"></i> Return Policies
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#">
                                <i class="fa fa-truck"></i> Shipping Policies
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link " href="#">
                                <i class="fa fa-hand-paper-o"></i> Payment Policies
                            </a>
                        </li>
                    </ul>
                </li>

                <li class="nav-title"> System </li>
                <li class="nav-item">
                    <a class="nav-link " href="{{ route("settings") }}">
                        <i class="mdi mdi-settings"></i> General Settings
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">
                        <i class="mdi mdi-atom"></i> SMTP Configuration
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link " href="#">
                        <i class="mdi mdi-atom"></i> Manage Templates
                    </a>
                </li>
            @else
                <li class="nav-title">Subscription</li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin-panel/subscription') }}">
                        <i class="fa fa-credit-card"></i>Subscription Listing
                    </a>
                </li>
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('admin-panel/subscription_access') }}">
                        <i class="fa fa-check"></i>Access Listing
                    </a>
                </li>
            @endhasrole
            
        </ul>
    </nav>
</div>
<!-- end sidebar -->