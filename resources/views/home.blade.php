@extends('layouts.app')

@section('content')

<!-- main.css | /* Banner Section S */ -->
    <section>
        <div class="banner_section">
            <div class="owl-carousel owl-theme">
                <div class="item banner_one">
                    <div class="container">
                        <div class="row custom-row">
                            <div class="col-xl-6 col-lg-7 col-md-8 col-sm-9 custom-col">
                                <div class="banner_content">
                                    <h1 class="banner_content_title wow fadeInUp">AUTOMATIC DROPSHIPPING TOOLS</h1>
                                    <div class="top-gap-half-padding"></div>
                                    @guest
                                        <div class="banner_section_buttons">
                                            <a href="{{ url('login') }}" title="Login" class="btn btn-default" >Login</a>
                                            <a href="{{ url('register') }}" title="Sign Up" class="btn btn-default signup" >Sign Up</a>
                                        </div>
                                    @else
                                        <div class="banner_section_buttons">
                                            <a href="{{ route('dashboard') }}"  class="btn btn-default" title="Go to Dashboard">Dashboard</a>
                                                @hasrole('super-admin')
                                                    <a href="{{ route('admin-panel') }}" class="btn btn-default signup" title="Go to Admin Panel">Admin Panel</a>
                                                @else
                                                @endhasrole
                                        </div>
                                    @endguest
                                </div>
                            </div>
                            <div class="col-xl-4 offset-xl-2 col-lg-5 col-md-4 col-sm-3 custom-padding">
                                <img src="https://autodstools.com/neon_dashboard/assets/images/Logo_white.png">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
     
    </section>

    <!-- main.css | /* About TrippyWords Section S */ __ custom.js | /* Tab Content Section - About TrippyWords S */ -->
    <section>
        <div class="section-gap-half-padding about_trippywords_section">
            <div class="container">
                <div class="row">
                    <div class="col-md-6">
                        <div class="about_trippywords">
                            <h2 class="title">About AUTODSTOOL</h2>
                            <p class="details">
                                It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, 
                            </p>
                            <div class="about_trippywords_tab_section">
                                <div class="tab">
                                    <div class="tablinks" onclick="openCity(event, 'what_we_do')" id="defaultOpen">What we do?</div>
                                    <div class="tablinks" onclick="openCity(event, 'how_its_works')">How Its Works?</div>
                                </div>
                                <div id="what_we_do" class="tabcontent">
                                    <p class="desc">
                                        Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry's standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book.
                                    </p>
                                </div>
                                <div id="how_its_works" class="tabcontent">
                                    <p class="desc">
                                       It is a long established fact that a reader will be distracted by the readable content of a page when looking at its layout. The point of using Lorem Ipsum is that it has a more-or-less normal distribution of letters, as opposed to using 'Content here, 
                                    </p> 
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="about_trippywords_image">
                            <img src="public/assets/image/about-img.png" alt="About ">
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
