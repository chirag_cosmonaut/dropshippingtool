<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>AutoDSTools - Client Panel</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('public/admin-assets/img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('public/admin-assets/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('public/admin-assets/img/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ URL::asset('public/admin-assets/img/favicon/manifest.json') }}">
    <link rel="mask-icon" href="{{ URL::asset('public/admin-assets/img/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <!-- fonts -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/fonts/md-fonts/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/libs/animate.css/animate.min.css') }}">

     <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/libs/jquery-loading/dist/jquery.loading.min.css') }}">

    <!-- octadmin main style -->

    <link id="pageStyle" rel="stylesheet" href="{{ URL::asset('public/admin-assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/libs/tables-datatables/dist/datatables.min.css')}}">
    <script src="{{ URL::asset('public/admin-assets/libs/jquery/dist/jquery.min.js') }}"></script>

</head>

<body>

    
    <section class="container-server-errors">

        <div class="brand-logo-dark float-left ">
            <a class="" href="#">
                <Strong>Dropship AI</Strong>
            </a>
        </div>

        <div class="server-errors pages-card">
            <div class="status-text-1">Oops!</div>
            <div class="status-error text-theme ">4 0 4</div>
            <div class="text-center status-text-2 text-dark">PAGE NOT FOUND ..!</div>
            <div class="text-center">
                <a href="{{ url('/') }}" class="btn  btn-theme login-btn text-white"> Go to Home Page </a>
            </div>
        </div>
        <!-- end server-error pages -->
    </section>

    <div class="half-circle"></div>
    <div class="small-circle"></div>
    <div class="half-circle-bottom"></div>
    <div class="small-circle-bottom"></div>

    <div id="copyright">
        <a href="#">DropShiping Tools</a> &copy; 2018. </div>


    <script src="{{ URL::asset('public/admin-assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/Bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/PACE/pace.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/nicescroll/jquery.nicescroll.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/jquery-loading/dist/jquery.loading.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/js/app.js') }}"></script>

</body>

</html>