@extends('backend_layout.app') 	
@section('content')

   
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="{{ url('dashboard') }}">Home</a>
    </li>
    <li class="breadcrumb-item active">Edit Profile</li>
</ol>

<div class="container">

    <div class="animated fadeIn">

        <div class="row">
            <div class="col-md-12">
                <div class="card card-accent-theme">
                    <div class="card-body">
                        <h5>{!!Auth::user()->name !!}</h5>
                       <div class="row">
                       		<div class="col-md-2"></div>
                       			<div class="col-md-8">
	                       		<div class="card">
	                           		<form action="{{ route('profile.update')}}" method="POST" class="form-horizontal" enctype="multipart/form-data">
	                           			<div class="card-header text-theme">
	                               	 		<strong>Edit </strong>Profile
	                            		</div>
	                           			<div class="card-body">
	                                    		{{ csrf_field() }}
		                                        <div class="form-group row">
		                                            <label class="col-md-3 form-control-label" for="hf-email">Name</label>
		                                            <div class="col-md-9">
		                                                <input type="text" id="editName" value="{{ $user->name }}" name="editName"  class="form-control" placeholder="Enter your name">
		                                                <span class="help-block">Please enter your name</span>
		                                            </div>
		                                        </div>
		                                        <div class="form-group row">
		                                            <label class="col-md-3 form-control-label" for="hf-email">Email</label>
		                                            <div class="col-md-9">
		                                                <input type="email" id="editEmail" name="editEmail" value="{{ $user->email}}" class="form-control" placeholder="Enter your email">
		                                                <span class="help-block">Please enter your email</span>
		                                            </div>
		                                        </div>
		                                        <div class="form-group row">
	                                            	<label class="col-md-3 form-control-label" for="file-input">Upload Photo</label>
	                                           			<div class="col-md-9">
	                                                		<input type="file" id="uploadPhoto" name="profileImage">
	                                            		</div>
	                                        	</div>
	                                        	@if($user->profile_image)
	                                        	<div class="form-group row">
	                                            	<label class="col-md-3 form-control-label" for="file-input">Image</label>
	                                           			<div class="col-md-3">
	                                                		<img src="{{ URL::asset('public/images/user/'.$user->profile_image) }}"  alt="{!!Auth::user()->name !!}" height="100px" widht="100px" >
	                                            		</div>
	                                        	</div>
	                                        	@endif
	                                	</div>
	                                	<div class="card-footer">
	                                    	<input type="submit" class="btn btn-sm btn-success" />
	                                    	<button type="reset" class="btn btn-sm btn-danger">Reset</button>
	                                    </div>
	                                </form>
                        		</div>
                  			</div>
                  			<div class="col-md-2"></div>	
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
<!-- end container-fluid -->

       

@endsection