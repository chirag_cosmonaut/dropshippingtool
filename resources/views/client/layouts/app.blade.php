<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <meta name="description" content="Admin, Dashboard, Bootstrap" />
    <meta name="viewport" content="width=device-width, initial-scale=1.0" />

    <title>AutoDSTools - Client Panel</title>

    <link rel="apple-touch-icon" sizes="180x180" href="{{ URL::asset('public/admin-assets/img/favicon/apple-touch-icon.png') }}">
    <link rel="icon" type="image/png" sizes="32x32" href="{{ URL::asset('public/admin-assets/img/favicon/favicon-32x32.png') }}">
    <link rel="icon" type="image/png" sizes="16x16" href="{{ URL::asset('public/admin-assets/img/favicon/favicon-16x16.png') }}">
    <link rel="manifest" href="{{ URL::asset('public/admin-assets/img/favicon/manifest.json') }}">
    <link rel="mask-icon" href="{{ URL::asset('public/admin-assets/img/favicon/safari-pinned-tab.svg') }}" color="#5bbad5">
    <meta name="theme-color" content="#ffffff">

    <!-- fonts -->
    <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/fonts/md-fonts/css/materialdesignicons.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/fonts/font-awesome-4.7.0/css/font-awesome.min.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/libs/animate.css/animate.min.css') }}">

     <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/libs/jquery-loading/dist/jquery.loading.min.css') }}">

    <!-- octadmin main style -->

    <link id="pageStyle" rel="stylesheet" href="{{ URL::asset('public/admin-assets/css/style.css') }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/libs/tables-datatables/dist/datatables.min.css')}}">
    <script src="{{ URL::asset('public/admin-assets/libs/jquery/dist/jquery.min.js') }}"></script>
    @yield('header_css')

    <meta name="csrf-token" content="{{ csrf_token() }}">
    <link rel="stylesheet" href="{{ URL::asset('public/admin-assets/css/custom.css') }}">
    <script>
        var url = "{{ url('/') }}/" ;
    </script>
</head>

<body class="app sidebar-fixed aside-menu-off-canvas aside-menu-hidden header-fixed">
    <!-- <div class="text-center">
        <i class="fa fa-cog fa-spin fa-5x fa-fw text-theme"></i>
    </div> -->
    <div class="page_loader">
        <div class="page_loader_container">
            <div class="text-center">
                <i class="fa fa-cog fa-spin fa-5x fa-fw text-theme"></i>
            </div>
        </div>
    </div>
    <?php
        use App\Accounts;
        $accounts = Accounts::select('id','user_id')->where('int_user_id','=',Auth()->user()->id)->get();
        $connectUser =count($accounts);
        if($connectUser == 0){ ?>
           <script>
                $(function() {
                    $("#add_ebay_account").modal('show');
                });
            </script>
       <?php }
             

        $currentAcc = session()->get('account');
    ?>
    @include('client.partials.header')

    <div class="app-body">
        @include('client.partials.sidebar')

        <main class="main">
            @yield('content')
        </main>
    </div>
        <div class="modal fade bs-example-modal-default" data-backdrop="static" keyboard="false" id="add_ebay_account" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
            <div class="modal-dialog modal-lg modal-dialog-centered-custom">
                <div class="modal-content">
                    <div class="modal-header">
                        <h6 class="modal-title" ><p>Connect Ebay User</p></h6>
                    </div>
                    <div class="modal-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="card-body">
                                    <div class="row">
                                        <p class="text-center">
                                            <strong> To Start monitor your ebay user, you must connect it to the monitor.</strong> 
                                        </p>

                                        <p class="text-center">
                                            <strong>Click on "Connect User" to connect your ebay user to the monitor.</strong>
                                        </p>                                
                                    </div>
                                    <div class="row">
                                        <div class="col-md-5">
                                            <select id="site" name="site" class="selectpicker form-control">
                                                <option value="0">ebay US</option> 
                                            </select>
                                        </div>
                                        <div class="col-md-4">
                                            <a href="{{ url('ebay_channel/getSessionId') }}" class="btn btn-success connect-btn text-left">Connect User</a>
                                        </div>
                                    </div>
                                    </div></br>
                                    <div class="col-md-12">
                                        <p style="color: blue;">Your information is secured with top secure protocols (HTTP/SSL)</p>
                                        <p style="color: blue;">All your user information stays private and will never be shared with anyone.</p> 
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    
    <footer class="app-footer">
        <a href="#" class="text-theme">AutoDS</a> &copy; 2018 
        <span class="float-right">Developed by
            <a href="http://www.cosmonautgroup.com" target="_blank" class="text-theme">Cosmonaut Group</a>
        </span>
    </footer>
    <script>
        var ADMIN_URL = "{{ URL('/admin-panel/') }}";
    </script>
    <!-- Bootstrap and necessary plugins -->
    <script src="{{ URL::asset('public/admin-assets/libs/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/popper.js/dist/umd/popper.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/Bootstrap/bootstrap.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/PACE/pace.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/chart.js/dist/Chart.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/nicescroll/jquery.nicescroll.min.js') }}"></script>

    <!-- jquery-loading -->
    
    <script src="{{ URL::asset('public/admin-assets/libs/jquery-loading/dist/jquery.loading.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/tables-datatables/dist/datatables.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/plugins/datatables/jquery.dataTables.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/js/app.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/Alertify/js/alertify.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/js/common.js') }}"></script>
    
    @yield('footer_script')
</body>
</html>