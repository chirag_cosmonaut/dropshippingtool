@extends('backend_layout.app')

@section('header_css')
<link rel="stylesheet" href="{{ URL::asset('public/admin-assets/css/custom.css') }}">
@endsection
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
	<li class="breadcrumb-item ">
		<a href="">Home</a>
	</li>
	<li class="breadcrumb-item">
		<a href="#">Listing</a>
	</li>
	<li class="breadcrumb-item active">AutoDS Finder</li>
</ol>
<div class="row">
	<div class="col-md-12 text-center">
		<h3>
			<p><strong>Remaining AutoDS Finder credits: 5930</strong></p>
			<button id="buy_prefinder_credits" type="button" class="btn btn-primary" data-toggle="modal" data-target="#buy_prefinder_credits_modal">Buy AutoDs Finder Credits</button>
		</h3>
	</div>
	<div class="col-md-12">
		<div class="card-body">
			<h5 class="text-theme">At least one sold with 1$ profit</h5>
			<h6>1 Credit Per Product</h6>
			<div class="slidecontainer">
				<div class="row">
					<div class="col-md-10">
						<input type="range" min="0" max="1000" value="1" class="range-slider" id="myRange">
					</div>
					<div class="col-md-2">
						<input type="number" id="demo" min="0" value="0" max="1000">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card-body">
			<h5 class="text-theme">At least 2 sold with 2$ profit</h5>
			<h6>2 Credit Per Product</h6>
			<div class="slidecontainer">
				<div class="row">
					<div class="col-md-10">
						<input type="range" min="0" max="1000" value="1" class="range-slider" id="myRange1">
					</div>
					<div class="col-md-2">
						<input type="number" id="demo1" min="0" value="0" max="1000">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card-body">
			<h5 class="text-theme">At least 5 sold with 3$ profit</h5>
			<h6>4 Credit Per Product</h6>
			<div class="slidecontainer">
				<div class="row">
					<div class="col-md-10">
						<input type="range" min="0" max="1000" value="1" class="range-slider" id="myRange2">
					</div>
					<div class="col-md-2">
						<input type="number" id="demo2" min="0" value="0" max="1000">
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="col-md-12">
		<div class="card-body">
			<h6>Uploading these products will cost 0 credits.</h6>
			<p>*There are no refunds for credits and credits don't have an expiration date</p>
			<div class="slidecontainer text-center">
				<button type="button" class="btn btn-primary">Upload it!</button>
			</div>
		</div>
	</div>
</div>
@endsection

@section('footer_script')
<script>
	$("#myRange").change(function(){
		$("#demo").val(this.value);
	});

	$("#myRange1").change(function(){
		$("#demo1").val(this.value);
	});

	$("#myRange2").change(function(){
		$("#demo2").val(this.value);
	});
</script>
<!-- slider-range -->
<script src="{{ URL::asset('public/admin-assets/libs/sliderrange/dist/jquery-asRange.min.js') }}"></script>
@endsection