@extends('backend_layout.app')	

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="{{ route('dashboard') }}">Home</a>
    </li>
    <li class="breadcrumb-item active">
        <a href="#">Dashboard</a>
    </li>
</ol>

<div class="container-fluid">
    <div class="animated fadeIn">
        <div class=" row">
            <div class="col-md-3">
                <div class="card ecom-widget-sales">
                    <div class="card-body">
                        <div class="ecom-sales-icon text-center">
                            <i class="mdi mdi-cart-outline"></i>
                        </div>
                        <h5 class="text-center">Active Listing</h5>
                        <ul>
                            <li>Total (all)
                                <span>{{$total}}</span>
                            </li>
                            <li>Live
                                <span>{{$live}}</span>
                            </li>
                            <li>Draft
                                <span>{{$draft}}</span>
                            </li>
                        </ul>

                        <div class="text-center btn-tool-bar">
                            <a href="{{ route('active-listing') }}" class="btn btn-theme btn-round btn-sm">More Details</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 ">
                <div class="card ecom-widget-sales">
                    <div class="card-body">
                        <div class="ecom-sales-icon text-center">
                            <i class="mdi mdi-currency-usd"></i>
                        </div>
                        <h5 class="text-center">Untraked Listing</h5>
                        <ul>
                            <li>All
                                <span>{{ $untracked }}</span>
                            </li>
                            <li>Paid
                                <span>0</span>
                            </li>
                            <li>Unpaid
                                <span>0</span>
                            </li>
                        </ul>

                        <div class="text-center btn-tool-bar">
                            <a href="{{ route('untracked-listing') }}" class="btn btn-theme btn-round btn-sm">More Details</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 ">
                <div class="card ecom-widget-sales">
                    <div class="card-body">
                        <div class="ecom-sales-icon text-center">
                            <i class="mdi mdi-cube-outline"></i>
                        </div>
                        <h5 class="text-center">Orders</h5>
                        <ul>
                            <li>All
                                <span>{{$order}}</span>
                            </li>
                            <li>Completed
                                <span>{{$complete}}</span>
                            </li>
                            <li>Active
                                <span>{{$active}}</span>
                            </li>
                            <li>Cancelled
                                <span>{{$cancel}}</span>
                            </li>
                        </ul>

                        <div class="text-center btn-tool-bar">
                            <a href="{{ route('order') }}" class="btn btn-theme btn-round btn-sm">More Details</a>
                        </div>
                    </div>
                </div>
            </div>

            <div class="col-md-3 ">
                <div class="card ecom-widget-sales">
                    <div class="card-body">
                        <div class="ecom-sales-icon text-center">
                            <i class="mdi mdi-fingerprint"></i>
                        </div>
                        <h5 class="text-center">Profit</h5>
                        <ul>
                            <li>Last 2 hours
                                <span>{{$last2_hours}} $</span>
                            </li>
                            <li> 1 week
                                <span>{{$week_profit}} $</span>
                            </li>
                            <li>1 Month
                                <span>{{$month_profit}} $</span>
                            </li>
                        </ul>

                        <div class="text-center btn-tool-bar">
                            <a href="" class="btn btn-theme btn-round btn-sm">More Details</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

        <div class="row">
            <div class="col-md-12">
                <div class="card card-accent-theme">
                    <div class="card-body">
                        <div class="h5 text-dark">
                            <div class="row">
                                <div class="col-md-3">
                                    <strong>Orders/Profit Chart</strong>
                                </div>
                                <div class="col-md-3">
                                    <span class="badge badge-info" style="background-color: #0358C7;"> </span> Orders(#)
                                    </br>
                                    <span class="badge badge-dark" style="background-color: #1BC9E4;"> </span> Profit($)
                                </div>
                                <div class="col-md-6">
                                    <button class="btn btn-dark btn-round btn-sm week_profit" data-week="1">1 Month</button>
                                    <button class="btn btn-theme btn-round btn-sm week_profit" data-week="3">3 Months</button>
                                    <button class="btn btn-theme btn-round btn-sm week_profit" data-week="6">6 Months</button>
                                    <button class="btn btn-theme btn-round btn-sm week_profit" data-week="12">1 Year</button>
                                </div>
                            </div>
                        </div>
                        <small class="text-theme">Last 10 days -based on the monitor's data</small>
                        <div id="area-chart"></div>
                    </div>
                </div>
            </div>
        </div>
        <!-- start of accont and date -->
        <div class="card card-accent-theme">
            <div class="card-body">
                <div class="row">
                    <div class="col-md-4 mb-3">
                        <label for="validationCustom03"><strong>Accounts</strong></label>
                        <select id="select2" name="select" class="form-control form-control-sm">
                            <!-- <option value="0">Please select Date:</option> -->
                            <option value="1">john</option>
                            <option value="2">sachin</option>
                            <option value="3">jinal</option>
                            <option value="3">rahul</option>
                        </select>
                    </div>

                    <!-- <div class="col-md-3 mb-3" style="margin-top:18px ">
                           <button type="submit" class="btn btn-primary">Show Detail</button>
                    </div> -->

                    <div class="col-md-6">
                        <button class="btn btn-theme btn-round btn-sm">Days</button>
                        <button class="btn btn-theme btn-round btn-sm">Weekly</button>
                        <button class="btn btn-dark btn-round btn-sm">Monthly</button>
                        <button class="btn btn-theme btn-round btn-sm">Yearly</button>
                    </div>
                </div>

                <div class="row">

                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-body">
                                <h4 class="text-theme">User Statistics</h4>
                                <div class="table-responsive">
                                    <table class="table table-bordered">
                                        <thead>
                                            <tr>
                                                <th></th>
                                                <th>Sun 18/3</th>
                                                <th>Sun 25/3</th>
                                                <th>Sun 1/4</th>
                                                <th>Sun 8/4</th>
                                                <th>Sun 15/4</th>
                                                <th>Sun 22/4</th>
                                                <th>Sun 29/4</th>
                                                <th>Sun 6/5</th>
                                                <th>Sun 13/5</th>
                                                <th>Sun 20/5</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <tr>
                                                <td>Orders</td>
                                                <td>181</td>
                                                <td>220</td>
                                                <td>190</td>
                                                <td>204</td>
                                                <td>212</td>
                                                <td>134</td>
                                                <td>116</td>
                                                <td>115</td>
                                                <td>117</td>
                                                <td>86</td>
                                            </tr>
                                            <tr>
                                                <td>Profit</td>
                                                <td>$23.91</td>
                                                <td>$15.32</td>
                                                <td>$37.91</td>
                                                <td>$20.8</td>
                                                <td>$21.5</td>
                                                <td>$-6.51</td>
                                                <td>$85319.8</td>
                                                <td>$10.64</td>
                                                <td>$20.96</td>
                                                <td>$2.455</td>
                                            </tr>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                            <!-- end card-body -->
                        </div>
                        <!-- end card -->
                    </div>
                </div>
            </div>
            <!-- end card-body -->
        </div>

        <div class="col-md-12 summary-widgets">
            <div class="card card-accent-theme">

                <div class="card-body">
                    <h6 class="text-theme">Summary</h6>
                    <div class="row">
                        <div class="col-md-6 summary-widget-1">
                            <div class="card ">
                                <div class="card-body">
                                    <div class="number">1069</div>
                                    <small>AutoDS monitoring products:</small>
                                    <div class="progress xs ">
                                        <div class="progress-bar bg-info" style="width: 80%; " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 summary-widget-1">
                            <div class="card ">
                                <div class="card-body">
                                    <div class="number">1331</div>
                                    <small>AutoDS monitoring product remaining:</small>
                                    <div class="progress xs ">
                                        <div class="progress-bar bg-danger" style="width: 60%; " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 summary-widget-1">
                            <div class="card">
                                <div class="card-body">
                                    <div class="number">1/5</div>
                                    <small>Ebay Accounts Limit:</small>
                                    <div class="progress xs ">
                                        <div class="progress-bar bg-success" style="width: 60%; " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 summary-widget-1">
                            <div class="card">
                                <div class="card-body">
                                    <div class="number">178</div>
                                    <small>Auto orders made:</small>
                                    <div class="progress xs ">
                                        <div class="progress-bar bg-warning" style="width: 60%; " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 summary-widget-1">
                            <div class="card ">
                                <div class="card-body">
                                    <div class="number">67</div>
                                    <small>Out of stock Listings</small>
                                    <div class="progress xs ">
                                        <div class="progress-bar bg-info" style="width: 10%; " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>

                        <div class="col-md-6 summary-widget-1">
                            <div class="card ">
                                <div class="card-body">
                                    <div class="number">0</div>
                                    <small>Pending order:</small>
                                    <div class="progress xs ">
                                        <div class="progress-bar bg-danger" style="width: 20%; " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 summary-widget-1">
                            <div class="card ">
                                <div class="card-body">
                                    <div class="number">2</div>
                                    <small>Failed order:</small>
                                    <div class="progress xs ">
                                        <div class="progress-bar bg-danger" style="width: 20%; " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6 summary-widget-1">
                            <div class="card ">
                                <div class="card-body">
                                    <div class="number">2</div>
                                    <small>All connected users failed orders:</small>
                                    <div class="progress xs ">
                                        <div class="progress-bar bg-danger" style="width: 20%; " role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>




                    </div>
                </div>
            </div>
        </div>

        <div class="col-md-12">
            <div class="card card-accent-theme">
                <div class="card-body">
                    <h4>Ebay Theme</h4>
                    <div class="table table-striped">
                        <table class="table table-bordered">
                            <thead>
                                <tr>
                                    <th>Ebay account</th>
                                    <th>Quantity remaining / Amount remaining</th>

                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td>Karen-Leaf</td>
                                    <td>
                                        14304/$712036
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!-- end row -->
    @endsection

    @section('footer_script')


    <script src="{{ URL::asset('public/admin-assets/libs/jquery-knob/dist/jquery.knob.min.js') }}"></script>
    <!--morris js -->
    <script src="{{ URL::asset('public/admin-assets/libs/raphael/raphael.min.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/charts-morris-chart/morris.min.js') }}"></script>


    <!-- dashboard-ecom script -->
    <script src="{{ URL::asset('public/admin-assets/js/dashboard-ecom-widgets.js') }}"></script>
    <script type="text/javascript">
        $(document).ready(function(){
            var token = $('meta[name="csrf-token"]').attr('content');
            $(".week_profit.btn-theme").click(function(){  
                $('.week_profit').removeClass("btn-dark");  
                $('.week_profit').addClass("btn-theme");  
                $(this).addClass("btn-dark");  
                $(this).removeClass("btn-theme");
                var week = $(this).data("week");  
                $.post(url + 'order-profit',{week:week, _token: token},function(response){
                    if (response!=[]) {
                        $('#area-chart').html('');
                        var res = jQuery.parseJSON(response);
                        Morris.Area({
                            element: 'area-chart',
                            data: res.response,
                            xkey: 'week',
                            ykeys: ['a', 'b'],
                            labels: ['Orders', 'Profits'],
                            hideHover: 'auto',
                            lineColors:['#0358C7', '#1BC9E4'],
                            gridLineColor: '#f0f3f5',
                            resize: true
                        }); 
                    }    
                });
            });  
            $(".week_profit.btn-dark").click(function(){  
                var week = $(this).data("week"); 
                $('.week_profit').removeClass("btn-dark");  
                $('.week_profit').addClass("btn-theme");  
                $(this).addClass("btn-dark");  
                $(this).removeClass("btn-theme");
                $.post(url + 'order-profit',{week:week, _token: token},function(response){
                    if (response!=[]) {
                        $('#area-chart').html('');
                        var res = jQuery.parseJSON(response);
                        Morris.Area({
                            element: 'area-chart',
                            data: res.response,
                            xkey: 'week',
                            ykeys: ['a', 'b'],
                            labels: ['Orders', 'Profits'],
                            hideHover: 'auto',
                            lineColors:['#0358C7', '#1BC9E4'],
                            gridLineColor: '#f0f3f5',
                            resize: true
                        });  
                    }         
                });
            }).trigger('click');
        }); 
    </script>
    @endsection