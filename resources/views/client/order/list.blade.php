@extends('backend_layout.app') 

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
	<li class="breadcrumb-item ">
		<a href="">Home</a>
	</li>
	<li class="breadcrumb-item">
		<a href="#">Listing</a>
	</li>
	<li class="breadcrumb-item active">Orders</li>
</ol>


<div class="container-fluid">

	<div class="animated fadeIn">
		<!-- <h3>Orders</h3>
		<br/> -->
		@if(session('flash_message'))
	        <div class="alert alert-success alert-dismissible">
	            {!! session('flash_message') !!}
	            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
	                <span aria-hidden="true">×</span>
	            </button>
	        </div>
	    @endif
		<a href="javascript:;" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="#bulk_change">Bulk Change</a>
		<a href="#" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="#filter">Filter</a>
		<a href="#" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="#col_customize">ColumnsCustomize</a>
		<a href="#" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="">Export</a>
		<a href="javascript:;" data-toggle="modal" data-target="#import_order" class="btn btn-dark waves-effect text-left">Import</a>
		<a href="#" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="#change_listings">listings bulk changes</a>
		
		<div class="row">
			<div class="col-md-12">
				<div class="card card-accent-theme">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table" id="example">
								<thead>
									<tr>
										<th>
											<input type="checkbox" name="">
										</th>
										<th>Edit</th>
										<th>
											<img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Order ID
										</th>
										<th>
											<img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png"><br>Source Order ID
										</th>
										<th>Picture</th>
										<th>
											<img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Item ID
										</th>
										<th>
											<img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png"><br>Source Item ID
										</th>
										<th>
											<img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Price
										</th>
										<th>
											<img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png"><br>Purchase Price
										</th>
										<th>Tax</th>
										<th>Profit</th>
										<th>Buyer Name</th>
										<th>Buyer Username</th>
										<th>
											<img src="https://autodstools.com/neon_dashboard/assets/images/001-sign.png" style="max-width: 100%; max-height:40px; min-width:30px">
										</th>
										<th>Sold Date</th>
										<th>Status</th>
									</tr>
								</thead>
							</table>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>

<!-- Import Order Dates Modal -->
<div class="modal fade" id="import_order" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <form method="post" action="{{ route('order.import') }}">
            	@csrf
	            <div class="modal-header">
	                <h6 class="modal-title" id="exampleModalLabel">Import Orders</h6>
	                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
	                    <span aria-hidden="true">&times;</span>
	                </button>
	            </div>
	            <div class="modal-body">
	            	<div class="row">
	            		<div class="col-md-12">
	            			<span class="text-center">Select Date in range on 3-4 Days</span>
	            		</div>
	            	</div>
	            	<div class="row">
	            		<div class="col-md-6">
			                <div class="form-group">
			                    <label for="message-text" class="col-form-label">From Date:</label>
			                    <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" name="from">
                                    <div class="input-group-addon">
                                        <span class="mdi mdi-calendar"></span>
                                    </div>
                                </div>
			                </div>
	            		</div>
	            		<div class="col-md-6">
	            			<div class="form-group">
			                    <label for="message-text" class="col-form-label">To Date:</label>
			                    <div class="input-group date" data-provide="datepicker">
                                    <input type="text" class="form-control" name="to">
                                    <div class="input-group-addon">
                                        <span class="mdi mdi-calendar"></span>
                                    </div>
                                </div>
			                </div>
	            		</div>
	            	</div>
	            </div>
	            <div class="modal-footer">
	                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
	                <button type="submit" class="btn btn-success">Import</button>
	            </div>
            </form>
        </div>
    </div>
</div>
<!-- Import Order Dates Modal -->

<!-- sample modal content Notification -->
<div class="modal fade bs-example-modal-default_orders" id="change_listings" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
		        <h6 class="modal-title" style="margin: 12px" >Change listings</h6>
		        <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <div class="row">
                    	<ol style="display: none;" class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                    		<li class="breadcrumb-item ">
                    			<a href="">Home</a>
                    		</li>
                    		<li class="breadcrumb-item">
                    			<a href="#">Listing</a>
                    		</li>
                    		<li class="breadcrumb-item active">Monitors</li>
                    	</ol>
                    	<div class="container-fluid">
							<div class="tab-content" id="myTabContent">
								<div class="tab-pane fade show active" id="price_monitor" role="tabpanel" aria-labelledby="home-tab" style="height: 200px;
						    overflow-y: scroll;">
									<div class="row">
										<div class="col-md-6">
											<div class="card-body">
												<strong>New Break Even:</strong>
												<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<strong>New Addition $ Profit:</strong>
												<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="card-body">
												<strong>New Additional % Profit:</strong>
												<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<strong>New Quantity:</strong>
												<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="card-body">
												<strong>New Price:</strong>
												<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<strong>Change Current $ Profit By X.X$(+-):	</strong>
												<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="card-body">
												<strong>Change Breakeven By X.X %(+-):</strong>
												<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span>
												</div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<strong>Change Additional % Profit By X.X$(+-):</strong>
												<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span>
												</div>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="card-body">
												<strong>Change Private Listings</strong>
													<div class="form-group row">
														<div class="col-md-12">
															<select id="select" name="select" class="form-control">
									                            <option value="0">Please select</option>
									                            <option value="1">Option #1</option>
									                            <option value="2">Option #2</option>
									                            <option value="3">Option #3</option>
									                        </select>
									                    </div>
									                </div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<strong>Template</strong>
												<div class="form-group row">
													<div class="col-md-12">
														<select id="select" name="select" class="form-control">
													        <option value="0">Please select</option>
													        <option value="1">Option #1</option>
													        <option value="2">Option #2</option>
													        <option value="3">Option #3</option>
									           			</select>
									         		 </div>
									         	</div>
									         </div>
									    </div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="card-body">
												<strong>Change Tag</strong>
												<div class="form-group row">
													<div class="col-md-12">
														<select id="select" name="select" class="form-control">
									                        <option value="0">Please select</option>
									                        <option value="1">Option #1</option>
									                        <option value="2">Option #2</option>
									                        <option value="3">Option #3</option>
									                    </select>
									                 </div>
									            </div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<input type="checkbox" id="checkbox1" name="checkbox1" value="option1">
												<strong>Remove Tag</strong>
											</div>
										</div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="card-body">
												<strong>Marketplace</strong>
												<div class="form-group row">
													<div class="col-md-12">
														<select id="select" name="select" class="form-control">
									                        <option value="0">Please select</option>
									                        <option value="1">Option #1</option>
									                        <option value="2">Option #2</option>
									                        <option value="3">Option #3</option>
									                    </select>
									                </div>
									            </div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<strong>Set Policy</strong>
												<div class="form-group row">
													<div class="col-md-12">
														<select id="select" name="select" class="form-control">
															<option value="0">Please select</option>
															<option value="1">Option #1</option>
															<option value="2">Option #2</option>
															<option value="3">Option #3</option>
									           			</select>
									          		</div>
									         	</div>
									        </div>
									    </div>
									</div>



									<div class="row">
										<div class="col-md-6">
											<div class="card-body">
												<strong>Stock Monitoring</strong>
												<div class="form-group row">
													<div class="col-md-12">
														<select id="select" name="select" class="form-control">
									                        <option value="0">Please select</option>
									                        <option value="1">Option #1</option>
									                        <option value="2">Option #2</option>
									                        <option value="3">Option #3</option>
									                    </select>
									                </div>
									            </div>
											</div>
										</div>
										<div class="col-md-6">
											<div class="card-body">
												<strong>Auto Ordering</strong>
												<div class="form-group row">
													<div class="col-md-12">
														<select id="select" name="select" class="form-control">
													        <option value="0">Please select</option>
													        <option value="1">Option #1</option>
													        <option value="2">Option #2</option>
													        <option value="3">Option #3</option>
									           			</select>
									           		</div>
									           	</div>
									        </div>
									    </div>
									</div>

									<div class="row">
										<div class="col-md-6">
											<div class="card-body">
												<strong>Price Monitoring</strong>
												<div class="form-group row">
													<div class="col-md-12">
														<select id="select" name="select" class="form-control">
													        <option value="0">Please select</option>
													        <option value="1">Option #1</option>
													        <option value="2">Option #2</option>
													        <option value="3">Option #3</option>
									           			</select>
									           		</div>
									           	</div>
											</div>
										</div>
									</div>
								</div>
								<div class="modal-footer">
									<button type="button" class="btn btn-default waves-effect text-left" data-dismiss="modal">Close</button>
									<button type="button" class="btn btn-primary">Save Change</button>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>


<div class="modal fade bs-example-modal-default" id="bulk_change" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
	            <h6 class="modal-title" style="margin: 12px" >Change Orders</h6>
	            <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
           	<div class="modal-body">
           		<div class="row">
					<div class="col-md-6">
						<div class="card-body">
						<strong>Change Status:</strong>
							<div class="form-group row">
								<div class="col-md-12">
									<select id="select" name="select" class="form-control">
				                        <option value="0">Change Status</option>
				                        <option value="1">Ordered</option>
				                        <option value="2">Shipped</option>
				                        <option value="3">Completed</option>
				                        <option value="3">Standby</option>
				                        <option value="3">Canceled</option>
                        			</select>
                    			</div>
                			</div>
						</div>
					</div>
					<div class="col-md-6">
						<div class="card-body">
							<input type="checkbox" id="checkbox1" name="checkbox1" value="option1">
							<strong>Send to ordering process</strong>
						</div>
					</div>
				</div>
			</div>
			<div class="modal-footer">
				<button type="button" class="btn btn-default waves-effect text-left" data-dismiss="modal">Close</button>
				<button type="button" class="btn btn-primary">Save Change</button>
			</div>

		</div>
	</div>
</div>




@endsection

@section('footer_script')
<script src="{{ URL::asset('public/admin-assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript">
$(function() {
	$('#example').DataTable({
		processing: true,
		serverSide: true,
		responsive: true,
		ordering: true,
		destroy: true,
		scrollX:true,
		pageLength:25,
		bStateSave:false,
		dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
		ajax: '{!! route('order.data') !!}',
		columns: [
			{ data: 'bulk',"searchable":false, name: 'bulk' },
			{ data: 'edit',"searchable":false,name:'edit' },
			{ data: 'sales_order_id',"searchable":true, name: 'sales_order_id' },
			{ data: 'source_order_id',"searchable":true, name: 'source_order_id' },
			{ data: 'picture',"searchable":false,name:'picture'},
			{ data: 'IID',"searchable":true, name: 'item.item_id' },
			{ data: 'source_item_id',"searchable":false, name: 'source_item_id' },
			{ data: 'total',"searchable":true, name: 'orders.total' },
			{ data: 'purchase_price',"searchable":false, name: 'purchase_price' },
			{ data: 'tax',"searchable":false, name: 'tax' },
			{ data: 'profit',"searchable":false, name: 'profit' },
			{ data: 'buyer_name',"searchable":true, name: 'buyer_name' },
			{ data: 'buyer_user_id',"searchable":true, name: 'b1.buyer_user_id' },
			{ data: 'sold',"searchable":false, name: 'sold' },
			{ data: 'sold_date',"searchable":true, name: 'sold_date' },
			{ data: 'status',"searchable":false, name: 'status' }
		]
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': '{{ csrf_token() }}'
		}
	});
})
</script>
@endsection