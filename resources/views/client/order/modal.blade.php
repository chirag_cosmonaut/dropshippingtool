<!-- Modal for Contact Buyer -->
<div class="modal fade" id="contact_buyer" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Contact Buyer</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    
                    <div class="form-group">
                        <input type="hidden" name="buyer" id="buyer" value="{{$buyerDetail->buyer_user_id}}">
                        <label for="message-text" class="col-form-label">Message:</label>
                        <textarea class="form-control" rows="5" placeholder="Contact Buyer" id="message" name="message"></textarea>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" onclick="contactBuyer(buyer.value,message.value)" class="btn btn-success">Send</button>
                <button type="buton"  onclick="contactBuyerCancelOrder(buyer.value,message.value)" class="btn btn-primary">Continue</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal for Contact Buyer -->

<!-- Modal for Forcr Sync -->
<div class="modal fade" id="force_sync" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Force Synchronization</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form>
                    
                    <div class="form-group">
                    	<label for="message-text" class="col-form-label">Choose Synchronization:</label>
                      	<div class="radio abc-radio abc-radio-success">
                            <input type="radio"  name="syncType" value="EbayToSystem" id="radio-success" checked>
                            <label for="radio-success">Ebay To System</label>
                        </div>
                        <div class="radio abc-radio abc-radio-success">
                            <input type="radio" name="syncType" value="SystemToEbay" id="radio-primary">
                            <label for="radio-primary">System To Ebay</label>
                       	</div>
                    </div>
                </form>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-success" onclick="synchOrder()">System to Ebay</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal for Forcr Sync -->

<!-- Modal for Add Tracking -->
 <div class="modal fade" id="add_tracking" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Add Tracking</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-11">
                        <div class="form-group">
                        	<div class="checkbox">
                                <label for="checkbox1">
                                   	<input type="checkbox" id="checkbox1" name="checkbox1" value="option1">&nbsp;&nbsp;Oppo A57 Mobile
                                </label>
                            </div>
                        </div>
                    </div>
                </div><hr>
                <div class="row" id="addControl">
                	<div class="col-md-5">
                		<div class="form-group">
                            <label for="name">Tracking Id</label>
                            <input type="text" class="form-control" id="track_id" name="track_id[]" placeholder="Tracking Number">
                        </div>
                	</div>
                	<div class="col-md-5">
                		<div class="form-group">
                			<label class="form-control-label" for="select">&nbsp;Shipping Company</label>
                            <select onchange="isCustom(1,this.value)" id="shipping_company_1" name="shipping_company[]" placeholder="Shipping Company" required="" class="form-control">
                                <option value="USPS">USPS</option>
                                <option value="UPS">UPS</option>
                                <option value="FedEx">FedEx</option>
                                <option value="DHL">DHL</option>
                                <option value="Canada Post">Canada Post</option>
                                <option value="Royal Mail">Royal Mail</option>
                                <option value="Australia Post">Australia Post</option>
                                <option value="Couriers Please">Couriers Please</option>
                                <option value="Smart Send">Smart Send</option>
                                <option value="Interparcel">Interparcel</option>
                                <option value="Fastway">Fastway</option>
                                <option value="Courier">Courier</option>
                               	<option value="Other">Other</option>
                            </select>
                        </div>
                        <div class="form-group" id="custom_div1">
                            <label for="name">Company Name</label>
                            <input type="text" class="form-control" id="company_name" name="company_name[]">
                        </div>
                	</div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <label>&nbsp;</label>
                            <a href="javascript:;" onclick="addMoreTracking()">
                                <i class="fa fa-plus"></i>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-secondary" data-dismiss="modal">Close</a>
                <a href="javascript:;" onclick="saveTracking()" class="btn btn-success">Add</a>
            </div>
        </div>
    </div>
</div>
<!-- Modal for Add Tracking -->

<!-- Modal for Edit Order Note -->
 <div class="modal fade" id="edit_note" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Edit Order Note</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <input type="hidden" name="noteid" id="noteid">
                <div class="form-group">
                    <textarea id="editnote" name="editnote" class="form-control" placeholder="Order Note" required=""></textarea>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button" onclick="saveNote(noteid.value,editnote.value)" class="btn btn-success">Update</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal for Edit Order Note -->

<!-- Modal for Contact Buyer -->
 <div class="modal fade" id="update_address" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" id="exampleModalLabel">Contact Buyer</h6>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Contact Name </label>
                            <input type="text" name="contact_name" id="contact_name" value="@if($OrderDetail->name){{$OrderDetail->name}}@endif" class="form-control" placeholder="Name" required="">
                        </div>
                        <div class="form-group">
                            <label>Address <span class="asterisk">*</span> </label>
                            <input type="text" name="street1" id="street1" value="@if($OrderDetail->street1){{$OrderDetail->street1}}@endif" class="form-control" placeholder="Address Line 1" required="">
                        </div>
                        <div class="form-group">
                            <label>City<span class="asterisk">*</span></label>
                            <input type="text" name="city" id="city" value="@if($OrderDetail->city_name){{$OrderDetail->city_name}}@endif" class="form-control" placeholder="City" required="">
                        </div>
                        <div class="form-group">
                            <label>Zip<span class="asterisk">*</span></label>
                            <input type="text" name="zip" id="zip" value="@if($OrderDetail->postal_code){{$OrderDetail->postal_code}}@endif" class="form-control numberOnly" placeholder="Zip" required="">
                        </div>
                        
                    </div>
                    <div class="col-md-6">
                        <div class="form-group">
                            <label>Phone<span class="asterisk">*</span></label>
                            <input type="text" name="phone" id="phone" value="@if($OrderDetail->phone){{$OrderDetail->phone}}@endif" class="form-control" placeholder="Phone" required="">
                        </div>
                        <div class="form-group">
                            <label>Address</label>
                            <input type="text" name="street2" id="street2" value="@if($OrderDetail->street2){{$OrderDetail->street2}}@endif" class="form-control" placeholder="Address Line 2">
                        </div>
                        <div class="form-group">
                            <label>State<span class="asterisk">*</span></label>
                            <input type="text" name="state" id="state" value="@if($OrderDetail->state_or_provinance){{$OrderDetail->state_or_provinance}}@endif" class="form-control" placeholder="State" required="">
                        </div>
                        <div class="form-group">
                            <label>Secondary Email</label>
                            <input type="text" name="secondary_email" id="secondary_email" value="" class="form-control" placeholder="Secondary Email">
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12">
                        <div class="form-group">
                            <label>Country<span class="asterisk">*</span></label>
                            <select name="country" id="country" data-plugin="select2" class="form-control" required="">
                                <option value="">Choose a Country</option>
                                <option @if($OrderDetail->country_name=='AF'){{'selected'}}@endif value="AF">Afghanistan</option>
                                <option @if($OrderDetail->country_name=='AX'){{'selected'}}@endif value="AX">Aland Islands</option>
                                <option @if($OrderDetail->country_name=='AL'){{'selected'}}@endif value="AL">Albania</option>
                                <option @if($OrderDetail->country_name=='DZ'){{'selected'}}@endif value="DZ">Algeria</option>
                                <option @if($OrderDetail->country_name=='AD'){{'selected'}}@endif value="AD">Andorra</option>
                                <option @if($OrderDetail->country_name=='AO'){{'selected'}}@endif value="AO">Angola</option>
                                <option @if($OrderDetail->country_name=='AI'){{'selected'}}@endif value="AI">Anguilla</option>
                                <option @if($OrderDetail->country_name=='AG'){{'selected'}}@endif value="AG">Antigua And Barbuda</option>
                                <option @if($OrderDetail->country_name=='AA'){{'selected'}}@endif value="AA">APO/FPO</option>
                                <option @if($OrderDetail->country_name=='AR'){{'selected'}}@endif value="AR">Argentina</option>
                                <option @if($OrderDetail->country_name=='AM'){{'selected'}}@endif value="AM">Armenia</option>
                                <option @if($OrderDetail->country_name=='AW'){{'selected'}}@endif value="AW">Aruba</option>
                                <option @if($OrderDetail->country_name=='Australia'){{'selected'}}@endif value="Australia">Australia</option>
                                <option @if($OrderDetail->country_name=='AT'){{'selected'}}@endif value="AT">Austria</option>
                                <option @if($OrderDetail->country_name=='AZ'){{'selected'}}@endif value="AZ">Azerbaijan</option>
                                <option @if($OrderDetail->country_name=='BS'){{'selected'}}@endif value="BS">Bahamas</option>
                                <option @if($OrderDetail->country_name=='BH'){{'selected'}}@endif  value="BH">Bahrain</option>
                                <option @if($OrderDetail->country_name=='BD'){{'selected'}}@endif  value="BD">Bangladesh</option>
                                <option @if($OrderDetail->country_name=='BB'){{'selected'}}@endif  value="BB">Barbados</option>
                                <option @if($OrderDetail->country_name=='BY'){{'selected'}}@endif  value="BY">Belarus</option>
                                <option @if($OrderDetail->country_name=='BE'){{'selected'}}@endif  value="BE">Belgium</option>
                                <option @if($OrderDetail->country_name=='BZ'){{'selected'}}@endif  value="BZ">Belize</option>
                                <option @if($OrderDetail->country_name=='BJ'){{'selected'}}@endif  value="BJ">Benin</option>
                                <option @if($OrderDetail->country_name=='BM'){{'selected'}}@endif  value="BM">Bermuda</option>
                                <option @if($OrderDetail->country_name=='BT'){{'selected'}}@endif  value="BT">Bhutan</option>
                                <option @if($OrderDetail->country_name=='BO'){{'selected'}}@endif  value="BO">Bolivia</option>
                                <option @if($OrderDetail->country_name=='BA'){{'selected'}}@endif  value="BA">Bosnia And Herzegovina</option>
                                <option @if($OrderDetail->country_name=='BW'){{'selected'}}@endif  value="BW">Botswana</option>
                                <option @if($OrderDetail->country_name=='BV'){{'selected'}}@endif  value="BV">Bouvet Island</option>
                                <option @if($OrderDetail->country_name=='BR'){{'selected'}}@endif  value="BR">Brazil</option>
                                <option @if($OrderDetail->country_name=='IO'){{'selected'}}@endif  value="IO">British Indian Ocean Territory</option>
                                <option @if($OrderDetail->country_name=='BN'){{'selected'}}@endif  value="BN">Brunei Darussalam</option>
                                <option @if($OrderDetail->country_name=='BG'){{'selected'}}@endif  value="BG">Bulgaria</option>
                                <option @if($OrderDetail->country_name=='BF'){{'selected'}}@endif  value="BF">Burkina Faso</option>
                                <option @if($OrderDetail->country_name=='BI'){{'selected'}}@endif  value="BI">Burundi</option>
                                <option @if($OrderDetail->country_name=='KH'){{'selected'}}@endif value="KH">Cambodia</option>
                                <option @if($OrderDetail->country_name=='CM'){{'selected'}}@endif value="CM">Cameroon</option>
                                <option @if($OrderDetail->country_name=='CA'){{'selected'}}@endif value="CA">Canada</option>
                                <option @if($OrderDetail->country_name=='CV'){{'selected'}}@endif value="CV">Cape Verde</option>
                                <option @if($OrderDetail->country_name=='KY'){{'selected'}}@endif value="KY">Cayman Islands</option>
                                <option value="CF">Central African Republic</option>
                                <option @if($OrderDetail->country_name=='TD'){{'selected'}}@endif value="TD">Chad</option>
                                <option @if($OrderDetail->country_name=='CL'){{'selected'}}@endif value="CL">Chile</option>
                                <option @if($OrderDetail->country_name=='CN'){{'selected'}}@endif value="CN">China</option>
                                <option @if($OrderDetail->country_name=='CX'){{'selected'}}@endif value="CX">Christmas Island</option>
                                <option @if($OrderDetail->country_name=='CC'){{'selected'}}@endif value="CC">Cocos (Keeling) Islands</option>
                                <option @if($OrderDetail->country_name=='CO'){{'selected'}}@endif value="CO">Colombia</option>
                                <option @if($OrderDetail->country_name=='KM'){{'selected'}}@endif value="KM">Comoros</option>
                                <option @if($OrderDetail->country_name=='CK'){{'selected'}}@endif value="CK">Cook Islands</option>
                                <option @if($OrderDetail->country_name=='CR'){{'selected'}}@endif value="CR">Costa Rica</option>
                                <option @if($OrderDetail->country_name=='HR'){{'selected'}}@endif value="HR">Croatia</option>
                                <option @if($OrderDetail->country_name=='CU'){{'selected'}}@endif value="CU">Cuba</option>
                                <option @if($OrderDetail->country_name=='CY'){{'selected'}}@endif value="CY">Cyprus</option>
                                <option @if($OrderDetail->country_name=='CZ'){{'selected'}}@endif value="CZ">Czech Republic</option>
                                <option @if($OrderDetail->country_name=='DK'){{'selected'}}@endif value="DK">Denmark</option>
                                <option @if($OrderDetail->country_name=='DJ'){{'selected'}}@endif value="DJ">Djibouti</option>
                                <option @if($OrderDetail->country_name=='DM'){{'selected'}}@endif value="DM">Dominica</option>
                                <option @if($OrderDetail->country_name=='DO'){{'selected'}}@endif value="DO">Dominican Republic</option>
                                <option @if($OrderDetail->country_name=='EC'){{'selected'}}@endif value="EC">Ecuador</option>
                                <option @if($OrderDetail->country_name=='EG'){{'selected'}}@endif value="EG">Egypt</option>
                                <option @if($OrderDetail->country_name=='SV'){{'selected'}}@endif value="SV">El Salvador</option>
                                <option @if($OrderDetail->country_name=='GQ'){{'selected'}}@endif value="GQ">Equatorial Guinea</option>
                                <option @if($OrderDetail->country_name=='ER'){{'selected'}}@endif value="ER">Eritrea</option>
                                <option @if($OrderDetail->country_name=='EE'){{'selected'}}@endif value="EE">Estonia</option>
                                <option @if($OrderDetail->country_name=='ET'){{'selected'}}@endif value="ET">Ethiopia</option>
                                <option @if($OrderDetail->country_name=='FK'){{'selected'}}@endif value="FK">Falkland Islands (Malvinas)</option>
                                <option @if($OrderDetail->country_name=='FO'){{'selected'}}@endif value="FO">Faroe Islands</option>
                                <option @if($OrderDetail->country_name=='FJ'){{'selected'}}@endif value="FJ">Fiji</option>
                                <option @if($OrderDetail->country_name=='FI'){{'selected'}}@endif value="FI">Finland</option>
                                <option @if($OrderDetail->country_name=='FR'){{'selected'}}@endif value="FR">France</option>
                                <option @if($OrderDetail->country_name=='GF'){{'selected'}}@endif value="GF">French Guiana</option>
                                <option @if($OrderDetail->country_name=='PF'){{'selected'}}@endif value="PF">French Polynesia</option>
                                <option @if($OrderDetail->country_name=='TF'){{'selected'}}@endif value="TF">French Southern Territories</option>
                                <option @if($OrderDetail->country_name=='GA'){{'selected'}}@endif value="GA">Gabon</option>
                                <option @if($OrderDetail->country_name=='GM'){{'selected'}}@endif value="GM">Gambia</option>
                                <option @if($OrderDetail->country_name=='GE'){{'selected'}}@endif value="GE">Georgia</option>
                                <option @if($OrderDetail->country_name=='DE'){{'selected'}}@endif value="DE">Germany</option>
                                <option @if($OrderDetail->country_name=='GH'){{'selected'}}@endif value="GH">Ghana</option>
                                <option @if($OrderDetail->country_name=='GI'){{'selected'}}@endif value="GI">Gibraltar</option>
                                <option @if($OrderDetail->country_name=='GR'){{'selected'}}@endif value="GR">Greece</option>
                                <option @if($OrderDetail->country_name=='GL'){{'selected'}}@endif value="GL">Greenland</option>
                                <option @if($OrderDetail->country_name=='GD'){{'selected'}}@endif value="GD">Grenada</option>
                                <option @if($OrderDetail->country_name=='GP'){{'selected'}}@endif value="GP">Guadeloupe</option>
                                <option @if($OrderDetail->country_name=='GT'){{'selected'}}@endif value="GT">Guatemala</option>
                                <option @if($OrderDetail->country_name=='GN'){{'selected'}}@endif value="GN">Guinea</option>
                                <option @if($OrderDetail->country_name=='GW'){{'selected'}}@endif value="GW">Guinea Bissau</option>
                                <option @if($OrderDetail->country_name=='GY'){{'selected'}}@endif value="GY">Guyana</option>
                                <option @if($OrderDetail->country_name=='HT'){{'selected'}}@endif value="HT">Haiti</option>
                                <option @if($OrderDetail->country_name=='VA'){{'selected'}}@endif value="VA">Holy See (Vatican City State)</option>
                                <option @if($OrderDetail->country_name=='HN'){{'selected'}}@endif value="HN">Honduras</option>
                                <option @if($OrderDetail->country_name=='HK'){{'selected'}}@endif value="HK">Hong Kong</option>
                                <option @if($OrderDetail->country_name=='HU'){{'selected'}}@endif value="HU">Hungary</option>
                                <option @if($OrderDetail->country_name=='IS'){{'selected'}}@endif value="IS">Iceland</option>
                                <option @if($OrderDetail->country_name=='IN'){{'selected'}}@endif value="IN">India</option>
                                <option @if($OrderDetail->country_name=='ID'){{'selected'}}@endif value="ID">Indonesia</option>
                                <option value="IR">Iran, Islamic Republic Of</option>
                                <option @if($OrderDetail->country_name=='IQ'){{'selected'}}@endif value="IQ">Iraq</option>
                                <option @if($OrderDetail->country_name=='IE'){{'selected'}}@endif value="IE">Ireland</option>
                                <option @if($OrderDetail->country_name=='IM'){{'selected'}}@endif value="IM">Isle of Man</option>
                                <option @if($OrderDetail->country_name=='IL'){{'selected'}}@endif value="IL">Israel</option>
                                <option @if($OrderDetail->country_name=='IT'){{'selected'}}@endif value="IT">Italy</option>
                                <option @if($OrderDetail->country_name=='JM'){{'selected'}}@endif value="JM">Jamaica</option>
                                <option @if($OrderDetail->country_name=='JP'){{'selected'}}@endif value="JP">Japan</option>
                                <option @if($OrderDetail->country_name=='JO'){{'selected'}}@endif value="JO">Jordan</option>
                                <option @if($OrderDetail->country_name=='KZ'){{'selected'}}@endif value="KZ">Kazakhstan</option>
                                <option @if($OrderDetail->country_name=='KE'){{'selected'}}@endif value="KE">Kenya</option>
                                <option @if($OrderDetail->country_name=='KI'){{'selected'}}@endif value="KI">Kiribati</option>
                                <option @if($OrderDetail->country_name=='KP'){{'selected'}}@endif value="KP">Democratic People's Republic of Korea</option>
                                <option @if($OrderDetail->country_name=='KR'){{'selected'}}@endif value="KR">Republic of Korea</option>
                                <option @if($OrderDetail->country_name=='KW'){{'selected'}}@endif value="KW">Kuwait</option>
                                <option @if($OrderDetail->country_name=='KG'){{'selected'}}@endif value="KG">Kyrgyzstan</option>
                                <option @if($OrderDetail->country_name=='LA'){{'selected'}}@endif value="LA">Lao</option>
                                <option @if($OrderDetail->country_name=='LV'){{'selected'}}@endif value="LV">Latvia</option>
                                <option @if($OrderDetail->country_name=='LB'){{'selected'}}@endif value="LB">Lebanon</option>
                                <option @if($OrderDetail->country_name=='LS'){{'selected'}}@endif value="LS">Lesotho</option>
                                <option @if($OrderDetail->country_name=='LR'){{'selected'}}@endif value="LR">Liberia</option>
                                <option @if($OrderDetail->country_name=='LY'){{'selected'}}@endif value="LY">Libyan Arab Jamahiriya</option>
                                <option @if($OrderDetail->country_name=='LI'){{'selected'}}@endif value="LI">Liechtenstein</option>
                                <option @if($OrderDetail->country_name=='LT'){{'selected'}}@endif value="LT">Lithuania</option>
                                <option @if($OrderDetail->country_name=='LU'){{'selected'}}@endif value="LU">Luxembourg</option>
                                <option @if($OrderDetail->country_name=='MO'){{'selected'}}@endif value="MO">Macao</option>
                                <option @if($OrderDetail->country_name=='MK'){{'selected'}}@endif value="MK">Macedonia</option>
                                <option @if($OrderDetail->country_name=='MG'){{'selected'}}@endif value="MG">Madagascar</option>
                                <option @if($OrderDetail->country_name=='MW'){{'selected'}}@endif value="MW">Malawi</option>
                                <option @if($OrderDetail->country_name=='MY'){{'selected'}}@endif value="MY">Malaysia</option>
                                <option @if($OrderDetail->country_name=='MV'){{'selected'}}@endif value="MV">Maldives</option>
                                <option @if($OrderDetail->country_name=='ML'){{'selected'}}@endif value="ML">Mali</option>
                                <option @if($OrderDetail->country_name=='MT'){{'selected'}}@endif value="MT">Malta</option>
                                <option @if($OrderDetail->country_name=='MQ'){{'selected'}}@endif value="MQ">Martinique</option>
                                <option @if($OrderDetail->country_name=='MR'){{'selected'}}@endif value="MR">Mauritania</option>
                                <option @if($OrderDetail->country_name=='MU'){{'selected'}}@endif value="MU">Mauritius</option>
                                <option @if($OrderDetail->country_name=='YT'){{'selected'}}@endif value="YT">Mayotte</option>
                                <option @if($OrderDetail->country_name=='MX'){{'selected'}}@endif value="MX">Mexico</option>
                                <option @if($OrderDetail->country_name=='MD'){{'selected'}}@endif value="MD">Moldova, Republic Of</option>
                                <option @if($OrderDetail->country_name=='MC'){{'selected'}}@endif value="MC">Monaco</option>
                                <option @if($OrderDetail->country_name=='MN'){{'selected'}}@endif value="MN">Mongolia</option>
                                <option @if($OrderDetail->country_name=='ME'){{'selected'}}@endif value="ME">Montenegro</option>
                                <option @if($OrderDetail->country_name=='MS'){{'selected'}}@endif value="MS">Montserrat</option>
                                <option @if($OrderDetail->country_name=='MA'){{'selected'}}@endif value="MA">Morocco</option>
                                <option @if($OrderDetail->country_name=='MZ'){{'selected'}}@endif value="MZ">Mozambique</option>
                                <option @if($OrderDetail->country_name=='MM'){{'selected'}}@endif value="MM">Myanmar</option>
                                <option @if($OrderDetail->country_name=='NA'){{'selected'}}@endif value="NA">Namibia</option>
                                <option @if($OrderDetail->country_name=='GH'){{'selected'}}@endif value="NR">Nauru</option>
                                <option @if($OrderDetail->country_name=='NP'){{'selected'}}@endif value="NP">Nepal</option>
                                <option @if($OrderDetail->country_name=='NL'){{'selected'}}@endif value="NL">Netherlands</option>
                                <option @if($OrderDetail->country_name=='AN'){{'selected'}}@endif value="AN">Netherlands Antilles</option>
                                <option @if($OrderDetail->country_name=='NC'){{'selected'}}@endif value="NC">New Caledonia</option>
                                <option @if($OrderDetail->country_name=='NZ'){{'selected'}}@endif value="NZ">New Zealand</option>
                                <option @if($OrderDetail->country_name=='NI'){{'selected'}}@endif value="NI">Nicaragua</option>
                                <option @if($OrderDetail->country_name=='NE'){{'selected'}}@endif value="NE">Niger</option>
                                <option @if($OrderDetail->country_name=='NG'){{'selected'}}@endif value="NG">Nigeria</option>
                                <option @if($OrderDetail->country_name=='NU'){{'selected'}}@endif value="NU">Niue</option>
                                <option @if($OrderDetail->country_name=='NF'){{'selected'}}@endif value="NF">Norfolk Island</option>
                                <option @if($OrderDetail->country_name=='NO'){{'selected'}}@endif value="NO">Norway</option>
                                <option @if($OrderDetail->country_name=='OM'){{'selected'}}@endif value="OM">Oman</option>
                                <option @if($OrderDetail->country_name=='PK'){{'selected'}}@endif value="PK">Pakistan</option>
                                <option @if($OrderDetail->country_name=='PS'){{'selected'}}@endif value="PS">Palestinian Territory, Occupied</option>
                                <option @if($OrderDetail->country_name=='PA'){{'selected'}}@endif value="PA">Panama</option>
                                <option @if($OrderDetail->country_name=='PG'){{'selected'}}@endif value="PG">Papua New Guinea</option>
                                <option @if($OrderDetail->country_name=='PY'){{'selected'}}@endif value="PY">Paraguay</option>
                                <option @if($OrderDetail->country_name=='PE'){{'selected'}}@endif value="PE">Peru</option>
                                <option @if($OrderDetail->country_name=='PH'){{'selected'}}@endif value="PH">Philippines</option>
                                <option @if($OrderDetail->country_name=='PN'){{'selected'}}@endif value="PN">Pitcairn</option>
                                <option @if($OrderDetail->country_name=='PL'){{'selected'}}@endif value="PL">Poland</option>
                                <option @if($OrderDetail->country_name=='PT'){{'selected'}}@endif value="PT">Portugal</option>
                                <option @if($OrderDetail->country_name=='QA'){{'selected'}}@endif value="QA">Qatar</option>
                                <option @if($OrderDetail->country_name=='RE'){{'selected'}}@endif value="RE">Reunion</option>
                                <option @if($OrderDetail->country_name=='RO'){{'selected'}}@endif value="RO">Romania</option>
                                <option @if($OrderDetail->country_name=='RU'){{'selected'}}@endif value="RU">Russia</option>
                                <option @if($OrderDetail->country_name=='RW'){{'selected'}}@endif value="RW">Rwanda</option>
                                <option @if($OrderDetail->country_name=='SH'){{'selected'}}@endif value="SH">Saint Helena</option>
                                <option @if($OrderDetail->country_name=='KN'){{'selected'}}@endif value="KN">Saint Kitts And Nevis</option>
                                <option @if($OrderDetail->country_name=='LC'){{'selected'}}@endif value="LC">Saint Lucia</option>
                                <option @if($OrderDetail->country_name=='PM'){{'selected'}}@endif value="PM">Saint Pierre And Miquelon</option>
                                <option @if($OrderDetail->country_name=='WS'){{'selected'}}@endif value="WS">Samoa</option>
                                <option @if($OrderDetail->country_name=='SM'){{'selected'}}@endif value="SM">San Marino</option>
                                <option @if($OrderDetail->country_name=='ST'){{'selected'}}@endif value="ST">Sao Tome And Principe</option>
                                <option @if($OrderDetail->country_name=='SA'){{'selected'}}@endif value="SA">Saudi Arabia</option>
                                <option @if($OrderDetail->country_name=='SN'){{'selected'}}@endif value="SN">Senegal</option>
                                <option @if($OrderDetail->country_name=='RS'){{'selected'}}@endif value="RS">Serbia</option>
                                <option @if($OrderDetail->country_name=='SC'){{'selected'}}@endif value="SC">Seychelles</option>
                                <option @if($OrderDetail->country_name=='SL'){{'selected'}}@endif value="SL">Sierra Leone</option>
                                <option @if($OrderDetail->country_name=='SG'){{'selected'}}@endif value="SG">Singapore</option>
                                <option @if($OrderDetail->country_name=='SK'){{'selected'}}@endif value="SK">Slovakia</option>
                                <option @if($OrderDetail->country_name=='SI'){{'selected'}}@endif value="SI">Slovenia</option>
                                <option @if($OrderDetail->country_name=='SB'){{'selected'}}@endif value="SB">Solomon Islands</option>
                                <option @if($OrderDetail->country_name=='SO'){{'selected'}}@endif value="SO">Somalia</option>
                                <option @if($OrderDetail->country_name=='ZA'){{'selected'}}@endif value="ZA">South Africa</option>
                                <option @if($OrderDetail->country_name=='GS'){{'selected'}}@endif value="GS">South Georgia</option>
                                <option @if($OrderDetail->country_name=='KR'){{'selected'}}@endif value="KR">South Korea</option>
                                <option @if($OrderDetail->country_name=='ES'){{'selected'}}@endif value="ES">Spain</option>
                                <option @if($OrderDetail->country_name=='LK'){{'selected'}}@endif value="LK">Sri Lanka</option>
                                <option @if($OrderDetail->country_name=='VC'){{'selected'}}@endif value="VC">St. Vincent</option>
                                <option @if($OrderDetail->country_name=='SD'){{'selected'}}@endif value="SD">Sudan</option>
                                <option @if($OrderDetail->country_name=='SR'){{'selected'}}@endif value="SR">Suriname</option>
                                <option value="SJ">Svalbard And Jan Mayen</option>
                                <option @if($OrderDetail->country_name=='SZ'){{'selected'}}@endif value="SZ">Swaziland</option>
                                <option @if($OrderDetail->country_name=='SE'){{'selected'}}@endif value="SE">Sweden</option>
                                <option @if($OrderDetail->country_name=='CH'){{'selected'}}@endif value="CH">Switzerland</option>
                                <option @if($OrderDetail->country_name=='SY'){{'selected'}}@endif value="SY">Syria</option>
                                <option @if($OrderDetail->country_name=='TW'){{'selected'}}@endif value="TW">Taiwan</option>
                                <option @if($OrderDetail->country_name=='TJ'){{'selected'}}@endif value="TJ">Tajikistan</option>
                                <option @if($OrderDetail->country_name=='TZ'){{'selected'}}@endif value="TZ">Tanzania, United Republic Of</option>
                                <option @if($OrderDetail->country_name=='TH'){{'selected'}}@endif value="TH">Thailand</option>
                                <option @if($OrderDetail->country_name=='TL'){{'selected'}}@endif value="TL">Timor Leste</option>
                                <option @if($OrderDetail->country_name=='TG'){{'selected'}}@endif value="TG">Togo</option>
                                <option @if($OrderDetail->country_name=='TK'){{'selected'}}@endif value="TK">Tokelau</option>
                                <option @if($OrderDetail->country_name=='TO'){{'selected'}}@endif value="TO">Tonga</option>
                                <option @if($OrderDetail->country_name=='TT'){{'selected'}}@endif value="TT">Trinidad and Tobago</option>
                                <option @if($OrderDetail->country_name=='TN'){{'selected'}}@endif value="TN">Tunisia</option>
                                <option @if($OrderDetail->country_name=='TR'){{'selected'}}@endif value="TR">Turkey</option>
                                <option @if($OrderDetail->country_name=='TM'){{'selected'}}@endif value="TM">Turkmenistan</option>
                                <option @if($OrderDetail->country_name=='TC'){{'selected'}}@endif value="TC">Turks And Caicos Islands</option>
                                <option @if($OrderDetail->country_name=='TV'){{'selected'}}@endif value="TV">Tuvalu</option>
                                <option @if($OrderDetail->country_name=='UG'){{'selected'}}@endif value="UG">Uganda</option>
                                <option @if($OrderDetail->country_name=='UA'){{'selected'}}@endif value="UA">Ukraine</option>
                                <option @if($OrderDetail->country_name=='AE'){{'selected'}}@endif value="AE">United Arab Emirates</option>
                                <option @if($OrderDetail->country_name=='GB'){{'selected'}}@endif value="GB">United Kingdom</option>
                                <option @if($OrderDetail->country_name=='US'){{'selected'}}@endif value="US">United States</option>
                                <option @if($OrderDetail->country_name=='UY'){{'selected'}}@endif value="UY">Uruguay</option>
                                <option @if($OrderDetail->country_name=='UZ'){{'selected'}}@endif value="UZ">Uzbekistan</option>
                                <option @if($OrderDetail->country_name=='VU'){{'selected'}}@endif value="VU">Vanuatu</option>
                                <option @if($OrderDetail->country_name=='VE'){{'selected'}}@endif value="VE">Venezuela</option>
                                <option @if($OrderDetail->country_name=='VN'){{'selected'}}@endif value="VN">Viet Nam</option>
                                <option @if($OrderDetail->country_name=='VG'){{'selected'}}@endif value="VG">Virgin Islands, British</option>
                                <option @if($OrderDetail->country_name=='WF'){{'selected'}}@endif value="WF">Wallis And Futuna</option>
                                <option @if($OrderDetail->country_name=='EH'){{'selected'}}@endif value="EH">Western Sahara</option>
                                <option @if($OrderDetail->country_name=='YE'){{'selected'}}@endif value="YE">Yemen</option>
                                <option @if($OrderDetail->country_name=='ZM'){{'selected'}}@endif value="ZM">Zambia</option>
                                <option @if($OrderDetail->country_name=='ZW'){{'selected'}}@endif value="ZW">Zimbabwe</option>
                            </select>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                <button type="button"class="btn btn-success" onclick="saveAddress()">Update</button>
            </div>
        </div>
    </div>
</div>
<!-- Modal for Contact Buyer -->