@extends('backend_layout.app') 
@section('header_css')
    <link src="{{ URL::asset('public/admin-assets/libs/select2/dist/css/select2.css') }}"></link>
@endsection
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
	<li class="breadcrumb-item ">
		<a href="">Home</a>
	</li>
	<li class="breadcrumb-item">
		<a href="#">Orders</a>
	</li>
	<li class="breadcrumb-item active"># {{$OrderDetail->record_id}}</li>
</ol>
<?php
	$date = explode("T",$OrderDetail->created_time);
	$order_date = $date[0];
	$time = rtrim($date[1],".000Z");
	$grandSubTotal = 0;
	$shippingCost  = 0;
	$itemIdArray = [];
?>

<div class="container-fluid">

	<div class="animated fadeIn">
		<div class="row">
			<div class="col-md-8">
				<h5>Orders / # {{$OrderDetail->record_id}}  {{$order_date}}, {{$time}}</h5>
			</div>
			<div class="col-md-4">
				<a href="{{$previous != '' ? route('order.detail',$previous):'javascript:;'}}" class="btn btn-primary waves-effect text-left" title="Previous Order" data-toggle="tooltip"><i class="fa fa-arrow-down"></i></a>
				<a href="{{$next != '' ? route('order.detail',$next):'javascript:;'}}" class="btn btn-primary waves-effect text-left" title="Next Order" data-toggle="tooltip"><i class="fa fa-arrow-up"></i></a>
				<a href="javascript:;" class="btn btn-info waves-effect text-left" title="Print" data-toggle="tooltip"><i class="fa fa-print"></i></a>
				<button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                    Action
                </button>
                <div class="dropdown-menu">
                    <a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#contact_buyer">Contact Buyer</a>
                    <a class="dropdown-item" href="javascript:;" data-toggle="modal" data-target="#force_sync">Synchronize</a>
                </div>
			</div>
		</div>
		<div class="row">
			<div class="col-md-7">
				<?php 
					$is_shipped =1 ;
					for($i=0;$i<count($itemArray);$i++){
						if(count($itemArray[$i]['trackCount'])>0){
							$is_shipped+1;
						}
					}
				?>
				@if($is_shipped < 2)
				<a href="javascript:;" class="btn btn-primary waves-effect text-left" data-toggle="modal" data-target="#add_tracking">Ship Items</a>
				@endif
				@if($OrderDetail->shipped_time != '')
					<a href="javascript:;" onclick="orderShipped({{$OrderDetail->id}},false)" class="btn btn-danger waves-effect text-left" title="Mark Order as UnShipped" data-toggle="tooltip">Mark As UnShipped</a>
				@else
					<a href="javascript:;" onclick="orderShipped({{$OrderDetail->id}},true)" class="btn btn-success waves-effect text-left" title="Mark Order as Shipped" data-toggle="tooltip">Mark As Shipped</a>
				@endif
				@if($OrderDetail->paid_time != '')
				<a href="javascript:;" onclick="recievePayment({{$OrderDetail->id}},false)" class="btn btn-danger waves-effect text-left" title="Mark Order as UnPaid" data-toggle="tooltip">Mark As UnPaid</a>
				@else
					<a href="javascript:;" onclick="recievePayment({{$OrderDetail->id}},true)" class="btn btn-success waves-effect text-left" title="Mark Order as Paid" data-toggle="tooltip">Mark As Paid</a>
				@endif
				@if($OrderDetail->order_status == 'Cancelled')
					<a href="javascript:;" class="btn btn-danger waves-effect text-left" title="Cancel Order" data-toggle="tooltip" disabled="">Cancel Order</a>
				@else
					<a href="javascript:;" class="btn btn-danger waves-effect text-left" title="Cancel Order" data-toggle="modal" data-target="#contact_buyer">Cancel Order</a>
				@endif
			</div>
			<div class="form-group col-md-5">
				<select class="form-control selectpicker" onchange="changeStatus(this.value,'{{$OrderDetail->order_id}}')" data-plugin="select2">
					@foreach($orderStatus as $os)
					<?php
						$selected = ($os->id == $OrderDetail->status)? 'selected':'';
					?>
						<option value="{{$os->id}}" {{$selected}}>{{$os->name}}</option>
					@endforeach
				</select>
			</div>
		</div>


		<div class="row">
			<div class="col-md-9">
				<div class="card card-accent-theme">
					<div class="card-header">
						Item Details
					</div>
					<div class="card-body">
						<?php for($i=0;$i<count($itemArray);$i++){ ?>
							<div class="row">
								<div class="col-md-10">
									<div class="row">
										<div class="col-md-3">
											@if($itemArray[$i]['item']->picture_details != '')
												<img src="{{$itemArray[$i]['item']->picture_details}}" height="100px" width="100px" alt="Product Image">
											@elseif($itemArray[$i]['item']->image != '')
												<img src="url('/public/images/products/'.$itemArray[$i]['item']->image)" height="100px" width="100px" alt="Product Image">
											@else
												<img src="http://via.placeholder.com/100x100">
											@endif
										</div>
										<div class="col-md-9">
											<a href="javascript:;" data-toggle="tooltip" title="{{$itemArray[$i]['item']->title}}">
												{{$itemArray[$i]['item']->title}}
												@if(count($itemArray)>'1')
													<b>
													-  {{ $itemArray[$i]['transaction']->shipping_details }}
													</b>
												@endif
											</a><br>
											<span>
												<b>Item : </b>
												<a href="{{$itemArray[$i]['item']->view_item_url}}" target="_blank" data-toggle="tooltip" title="{{$itemArray[$i]['item']->title}}">
													{{$itemArray[$i]['item']->item_id}}
												</a>
											</span><br>
											@if(!empty($itemArray[$i]['item']->sku))
												<span> <b>SKU : </b>{{$itemArray[$i]['item']->sku}}</span><br>
											@endif
											@if(count($itemArray[$i]['trackCount'])>0)
												@foreach($itemArray[$i]['tracking'] as $tracking)
												<span><b>Ship with : </b>
													{{$tracking->shipping_company_name}} #{{$tracking->tracking_id}}
												</span>
												@endforeach
											@else
												<span><b>Ship with : </b>
													{{$itemArray[$i]['item']->shipping_service}}
												</span>
											@endif
										</div>
									</div>
								</div>
								<div class="col-md-2">
									@if(isset($itemArray[$i]['transaction']->quantity_purchased) && $itemArray[$i]['transaction']->quantity_purchased != '')
										<?php
										$subtotal = $itemArray[$i]['transaction']->quantity_purchased * $itemArray[$i]['item']->item_price;
										$grandSubTotal = $grandSubTotal + $subtotal;
										?>
									@endif
									<span> 
										<b> 
										@if(isset($itemArray[$i]['transaction']->quantity_purchased))
											{{ $itemArray[$i]['transaction']->quantity_purchased}} X $ 
										@else
											1 X $
										@endif
										{{$itemArray[$i]['item']->item_price}}</b>
										
									</span>
								</div>
							</div>
						<?php } ?>
						<hr>
						<div class="row">
							<div class="col-md-8">
								
							</div>
							<div class="col-md-4">
								<span>
									<b>SubTotal : </b>$ {{$OrderDetail->subtotal}}
								</span><br>
								<span>
									<b>Shipping : </b>$ {{$OrderDetail->quantity_purchased * $OrderDetail->shipping_service_cost}}
								</span><br>
								<span>
									<b>Tax &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b>$ 0.00
								</span><br>
								<span>
									<b>Total &nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b>$ {{$OrderDetail->total}}
								</span><br>
								<span>
									<b>Profit&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;: </b>$ {{$profit}}
								</span><br>
							</div>
						</div>
						<hr>
						<div class="row">
							<div class="col-md-12">
								<span>
									<b>eBay Seller ID : </b>{{$OrderDetail->seller_user_id}}
								</span><br>
								<span>
									<b>eBay Order : # </b>{{$OrderDetail->OID}}
								</span><br>
								<span>
									<b>Payment Method : </b>{{$OrderDetail->payment_methods}}
								</span><br>
								@foreach($feedbacks as $feed)
									@if($feed->role == 'Seller')
									<span>
										<b>Buyer's Feedback :</b> {{$feed->comment_text}}
									</span><br>
									@else
										<span>
											<b> Seller's Feedback :</b> {{$feed->comment_text}}
										</span><br>
									@endif
								@endforeach
								<span>
									<b>Buyer Notes : </b>
									@if($OrderDetail->buyer_note != '')
										{{$OrderDetail->buyer_note}}
									@else
										&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;-
									@endif
								</span><br>
								
							</div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card card-accent-theme">
							<div class="card-header">
								Order Notes
							</div>
							<div class="card-body">
								<div class="table-responsive">
		                            <table class="table table-hover">
		                                <thead>
		                                    <tr>
		                                        <th>Note</th>
		                                        <th>Action</th>
		                                    </tr>
		                                </thead>
		                                <tbody>
		                                	@if(count($orderNotes)>0)
												@foreach($orderNotes as $notes)
			                                    <tr>
			                                        <td>{{$notes->note}}</td>
			                                        <td>
			                                        	<a href="javascript:;" onclick="editNote('{{$notes->id}}','{{$notes->note}}')" class="btn btn-info btn-sm">
			                                        		<i class="fa fa-edit"></i>
			                                        	</a>
			                                        	<a href="javascript:;" onclick="deleteNote('{{$notes->id}}')" class="btn btn-danger btn-sm">
			                                        		<i class="fa fa-trash"></i>
			                                        	</a>
			                                        </td>
			                                    </tr>
			                                    @endforeach
			                                @else
			                                	<tr>
													<td colspan="2" align="center">There is no any Order Note</td>
												</tr>
			                                @endif
		                                </tbody>
		                            </table>
		                        </div>
		                    </div>
						</div>
					</div>
				</div>
				<div class="row">
					<div class="col-md-12">
						<div class="card card-accent-theme">
							<div class="card-header">
								Add Order Note
							</div>
							<div class="card-body">
								<div class="row">
									<input type="hidden" name="order_id" id="order_id" value="{{$OrderDetail->order_id}}">
									<textarea class="form-control" name="order_note" id="order_note" rows="3" placeholder="add notes to this order" required=""></textarea>
								</div>
								<div class="row">
									<div class="pull-right">
										<a href="javascript:;" title="Add Note" onclick="add_note(order_note.value,order_id.value)" class="btn btn-success">Add</a>
									</div>
								</div>
		                    </div>
						</div>
					</div>
				</div>
			</div>
			<div class="col-md-3">
				<div class="row">
					<div class="card card-accent-theme">
						<div class="card-header">
							Buyer Details
							<div class="pull-right">
								<a href="javascript:;" data-toggle="modal" data-target="#update_address" title="Edit"><i class="fa fa-edit"></i></a>
							</div>
						</div>
						<div class="card-body">
							<span>
								<i class="fa fa-user"></i> {{$OrderDetail->name}}
							</span><br>
							<span>
								<i class="fa fa-home"></i> 
								{{$OrderDetail->street1}} @if($OrderDetail->street2) , {{$OrderDetail->street2}}@endif
							</span><br>
							<span>
								{{$OrderDetail->city_name}}  
								{{$OrderDetail->state_or_provinance}}
								{{$OrderDetail->postal_code}}
							</span><br>
							<span>
								<i class="fa fa-globe"></i> {{$OrderDetail->country_name}}
							</span><br>
							<span>
								<i class="fa fa-user"></i>  {{$buyerDetail->buyer_user_id}}
							</span><br>
							<span>
								<i class="fa fa-phone"></i>  {{$OrderDetail->phone}}
							</span><br>
							<span>
								<i class="fa fa-envelope"></i> {{$buyerDetail->email}}
							</span>
						</div>
					</div>
				</div>
				<?php for($i=0;$i<count($itemArray);$i++){
					$itemIdArray[$i] = $itemArray[$i]['item']->id;
				?>
					<div class="row">
						<div class="card card-accent-theme">
							<div class="card-header">
								Suppliers Link & Informations<br>
								{{$itemArray[$i]['item']->item_id}}
								<span class="pull-right">
									<a href="javascript:;" onclick="editSuplierData({{$itemArray[$i]['item']->id}})">
										<i class="fa fa-edit"></i>
									</a>
								</span>
							</div>
							<div class="card-body">
								<span>
									<b>Source Link : </b>
									@if($itemArray[$i]['item']->supplier_product_link != '')
									<a href="{{$itemArray[$i]['item']->supplier_product_link}}" target="_blank">
										@php
										$pieces = parse_url($itemArray[$i]['item']->supplier_product_link);
										$domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
										if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
											echo $regs['domain'];
										}
										@endphp
									</a>
									@endif
								</span><br>
								<span>
									<input type="text" name="editSupplierProductLink" id="editSupplierProductLink_{{$itemArray[$i]['item']->id}}" value="{{$itemArray[$i]['item']->supplier_product_link}}" class="form-control" style="display: none;">
								</span><br>
								<span>
									<b>Cost Price : </b> $ {{$itemArray[$i]['item']->supplier_price}}
								</span><br>
								<span>
									<input type="text" class="form-control numberOnly" name="editItemPrice" id="editItemPrice_{{$itemArray[$i]['item']->id}}" value="{{$itemArray[$i]['item']->supplier_price}}" style="display: none;">
								</span><br>
								<span>
									<b>Shipping Cost : </b> $ {{$itemArray[$i]['item']->supplier_shipping}}
								</span><br>
								<span>
									<input type="text" class="form-control numberOnly" name="editShippingCost" id="editShipping_{{$itemArray[$i]['item']->id}}" value="{{$itemArray[$i]['item']->supplier_shipping}}" style="display: none;">
								</span><br>
								<span>
									<b>Source Order Id : </b> {{$itemArray[$i]['item']->supplier_order_id}}
								</span><br>
								<span>
									<input type="text" name="editSourceOrder" id="editSourceOrder_{{$itemArray[$i]['item']->id}}" value="{{$itemArray[$i]['item']->supplier_order_id}}" class="form-control" style="display: none;">
								</span><br>
								<span>
									<a href="javascript:;" style="display: none;" id="save_{{$itemArray[$i]['item']->id}}" class="btn btn-success btn-sm" onclick="SaveSupplierData({{$itemArray[$i]['item']->id}},{{$OrderDetail->order_id}})">Save</a>
								</span>
							</div>
						</div>
					</div>
				<?php } ?>

				<div class="row">
					<div class="card card-accent-theme">
						<div class="card-header">
							Fees And Transaction
							<span class="pull-right">
								<a href="javascript:;" onclick="editFees()">
									<i class="fa fa-edit"></i>
								</a>
							</span>
						</div>
						<div class="card-body">
							<span>
								<b>Ebay Fee : </b> $ {{$OrderDetail->ebay_fee_price}}
							</span><br>
							<span>
								<input type="text" class="form-control numberOnly" name="editEbayFee" id="editEbayFee" value="{{$OrderDetail->ebay_fee_price}}">
							</span><br>
							<span>
								<b>Paypal Fee : </b> $ {{$OrderDetail->paypal_fee_price}}
							</span><br>
							<span>
								<input type="text" name="editPaypalFee" class="form-control numberOnly" id="editPaypalFee" value="{{$OrderDetail->paypal_fee_price}}">
							</span><br>
							<span>
								<b>Paypal Fixed Transaction Fee : </b> $ 0.30
							</span><br>
							<span>
								<b>Paypal Transaction from buyer : </b> 
								<a href="https://www.paypal.com/activity/payment/{{$OrderDetail->reference_id}}" target="_blank">
									{{$OrderDetail->reference_id}}
								</a>
							</span><br>
							<span>
								<input type="text" class="form-control" name="editPaypalTransactionId" id="editPaypalTransactionId" value="{{$OrderDetail->reference_id}}">
							</span><br>
							<span>
								<b>Paypal Transaction to supplier : </b>
								<a href="https://www.paypal.com/activity/payment/{{$OrderDetail->paypal_tansaction_id_supplier}}" target="_blank">
									{{$OrderDetail->paypal_tansaction_id_supplier}}
								</a>
							</span><br>
							<span>
								<input type="text" class="form-control" name="editPaypalTransactionToSupplier" id="editPaypalTransactionToSupplier" value="{{$OrderDetail->paypal_tansaction_id_supplier}}">
							</span><br>
							<span>
								<a href="javascript:;" class="btn btn-success btn-sm" id="saveFee" style="display: none;" onclick="SaveFees({{$OrderDetail->order_id}})">Save</a>
							</span>
						</div>
					</div>
				</div>

				<div class="row">
					<div class="card card-accent-theme">
						<div class="card-header">
							Order Tags
						</div>
						<div class="card-body">
							<div class="row">
								<div class="form-group">
									<label>Order Tags</label>
									<select class="form-control select2" multiple name="seltags" id="seltags">
		                                @foreach($tags as $tag)	
										<?php 
											$selected_tag = []; 
											foreach($selectedtags as $stag){
												$selected_tag[$stag->tag_id] = $stag->tag_id;
											}
											if(in_array($tag->id,$selected_tag)){
												$selected = "selected";
											}else{
												$selected = "";
											}
										?>
										<option value="{{$tag->id}}" {{$selected}}>{{$tag->tag_name}}</option>
										@endforeach
		                            </select>
								</div>
								<div class="form-group">
									<input  class='btn btn-success' onclick="updateTag()" value="Update Tags" type="button">
								</div>
							</div><br>
							<div class="row">
								<div class="form-group">
									<span>Add New Tag</span><br>
									<form  method="post" action="{{ route('addtag') }}">
										{{csrf_field()}}
										<input type="text" class="form-control" name="tag_name" id="tag_name" required="" />
										<input type="submit" name="add_tag" class="btn btn-success btn-sm" value="Add">
									</form>
								</div>
							</div>
						</div>
					</div>
				</div>

			</div>
		</div>

	</div>
</div>

@include('client.order.modal')

@endsection
@section('footer_script')
	<script src="{{ URL::asset('public/admin-assets/js/widgets-satistics-examples.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/multiselect/js/jquery.multi-select.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/bootstrap-tagsinput/dist/tagsinput.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/sweetalert/sweetalert.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/js/form-plugins-example.js') }}"></script>
    <script src="{{ URL::asset('public/admin-assets/libs/select2/dist/js/select2.js') }}"></script>

    <script>
    	$(function () {
			$('#editEbayFee').hide();
			$('#editPaypalFee').hide();
			$('#editPaypalTransactionId').hide();
			$('#editPaypalTransactionToSupplier').hide();
			$('#editItemPrice').hide();
			$('#custom_div1').hide();

			$('.numberOnly').on('keyup keypress keydown',function(e){
			  	if (/\D/g.test(this.value)){
				    this.value = this.value.replace(/[^0-9\.]/g,'');
			  	}
			});
		});


		var token = $('meta[name="csrf-token"]').attr('content');
		var order_id = $('#order_id').val();

		function updateTag(){
			$.post('{{url('order/savetags')}}',{_token:token,tags:$("#seltags").val(),orderid:order_id},function(response){
				swal("Updated!", "Your Order Tags are Updated!", "success");
				alertAjax(response);
			});
		}

		function editSuplierData(id){
			$('#editItemPrice_'+id).toggle();
			$('#editShipping_'+id).toggle();
			$('#editSourceOrder_'+id).toggle();
			$('#editSupplierProductLink_'+id).toggle();
			$('#save_'+id).toggle();
		}

		function SaveSupplierData(product,order_id){
			var token 			= $('meta[name="csrf-token"]').attr('content');
			shippingCost 		= $('#editShipping_'+product).val();
			sourceId 			= $('#editSourceOrder_'+product).val();
			supplierProductLink = $('#editSupplierProductLink_'+product).val();
			price 				= $('#editItemPrice_'+product).val();
			if(shippingCost != ''){
				$.post('{{url('order/saveChanges')}}',{shipping_cost:shippingCost,_token:token,product:product,order:order_id},function(response){
					var jsonObj = $.parseJSON(response);
				});	
			}
			if(sourceId != ''){
				$.post('{{url('order/saveChanges')}}',{supplierOrderId:sourceId,_token:token,product:product,order:order_id},function(response){
					var jsonObj = $.parseJSON(response);
				});
			}
			if(supplierProductLink != ''){
				$.post('{{url('order/saveChanges')}}',{supplierProductLink:supplierProductLink,_token:token,product:product,order:order_id},function(response){
					var jsonObj = $.parseJSON(response);
				});
			}
			if(price != ''){
				$.post('{{url('order/saveChanges')}}',{price:price,_token:token,product:product,order:order_id},function(response){
					var jsonObj = $.parseJSON(response);
				});
			}
			alertSuccessNotification("Supplier Data Updated");
			location.reload();
		}

		function editFees(){
			$('#editEbayFee').toggle();
			$('#editPaypalFee').toggle();
			$('#edit').toggle();
			$('#editPaypalTransactionId').toggle();
			$('#editPaypalTransactionToSupplier').toggle();
			$('#saveFee').toggle();

		}

		function SaveFees(order){
			var token 					= $('meta[name="csrf-token"]').attr('content');
			ebayFee 					= $('#editEbayFee').val();
			paypalFee 					= $('#editPaypalFee').val();
			paypalTransactionId 		= $('#editPaypalTransactionId').val();
			paypalTransactionToSupplier = $('#editPaypalTransactionToSupplier').val();
			if(ebayFee != ''){
				$.post('{{url('order/saveChanges')}}',{ebayFee:ebayFee,_token:token,order:order},function(response){
					var jsonObj = $.parseJSON(response);
				});
			}
			if(paypalFee != ''){
				$.post('{{url('order/saveChanges')}}',{paypalFee:paypalFee,_token:token,order:order},function(response){
					var jsonObj = $.parseJSON(response);
				});
			}
			if(paypalTransactionId != ''){
				$.post('{{url('order/saveChanges')}}',{paypalTransactionId:paypalTransactionId,_token:token,order:order},function(response){
					var jsonObj = $.parseJSON(response);
				});
			}
			if(paypalTransactionToSupplier != ''){
				$.post('{{url('order/saveChanges')}}',{paypalTransactionToSupplier:paypalTransactionToSupplier,_token:token,order:order},function(response){
					var jsonObj = $.parseJSON(response);
				});
			}
			alertSuccessNotification("Fees Successfully Updated");
			location.reload();
		}

		function add_note(note,order_id){
			var token = $('meta[name="csrf-token"]').attr('content');
			if(note != ''){
				$.post('{{route('order.add_note')}}',{id:order_id,note:note,_token:token},function(response){
					alertAjax(response);
				});
			}else{
				$('#error').text('Please Enter Order Note');

			}
		}

		function deleteNote(id){
			if(id != ''){
			    swal({
		            title: "Are you sure?",
		            text: "You will not be able to recover this order note!",
		            type: "warning",
		            showCancelButton: true,
		            confirmButtonClass: 'btn-danger',
		            confirmButtonText: 'Yes, delete it!',
		            closeOnConfirm: false,
		            //closeOnCancel: false
		        },
		        function () {
					$.post('{{route('order.delete_note')}}',{id:id,_token:token},function(response){
		            	swal("Deleted!", "Your order note has been deleted!", "success");
						alertAjax(response);
					});
		        });
			}
		}

		function orderShipped(order_id,isshipped){
			if(isshipped){
				shipped = "Shipped";
				btn_class = 'btn-success';
				btn_text = 'Yes, Mark as Shipped'
			}else{
				shipped = "UnShipped";
				btn_class = 'btn-danger';
				btn_text = 'Yes, Mark as UnShipped'
			}
			swal({
	            title: "Are you sure?",
	            text: "You want want to mark order as "+shipped,
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonClass: btn_class,
	            confirmButtonText: btn_text,
	            closeOnConfirm: false,
	        },
	        function () {
				$.post('{{url('order/change_ebay_status')}}',{_token:token,order_id:order_id,change:'shipped',isshipped:isshipped},function(response){
					swal(shipped, "Your order has been mark"+shipped+"!", "success");
					alertAjax(response);
				});
	        });
		}

		function recievePayment(order_id,ispaid){
			if(ispaid){
				paid = "Paid";
				btn_class = 'btn-success';
				btn_text = 'Yes, Mark as Paid'
			}else{
				paid = "UnPaid";
				btn_class = 'btn-danger';
				btn_text = 'Yes, Mark as UnPaid'
			}
			swal({
	            title: "Are you sure?",
	            text: "You want want to mark order as "+paid,
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonClass: btn_class,
	            confirmButtonText: btn_text,
	            closeOnConfirm: false,
	        },
	        function () {
				$.post('{{url('order/change_ebay_status')}}',{_token:token,order_id:order_id,change:'payment',ispaid:ispaid},function(response){
					swal(paid, "Your order has been mark "+paid+"!", "success");
					alertAjax(response);
				});
	        });
		}

		function changeStatus(status,order){
			$.post('{{url('order/change_status')}}',{status:status,_token:token,order:order},function(response){
				alertAjax(response);
			});
		}

		function contactBuyer(buyer,message){
			var attachment = [];
			/*var files = $('#attach')[0].files;
			for (var i = 0; i < files.length; i++){
				attachment.push(files[i].name);
			}*/
			$.post('{{url('order/contact_buyer')}}',{_token:token,order:order_id,buyer:buyer,message:message},function(response){           
				var jsonObj = $.parseJSON(response);
				if(jsonObj.error == ''){
		        	$('#contact_buyer').modal('hide');
		        	swal("Sent", "Message Successfully Send!", "success");
				}           
		        alertAjax(response);
			});
		}

		function contactBuyerCancelOrder(buyer,message,attachment){
			$.post('{{url('order/contact_buyer')}}',{_token:token,order:order_id,buyer:buyer,message:message,attachment:attachment},function(response){
				var jsonObj = $.parseJSON(response);
				alertAjax(response);
				if(jsonObj.error==''){
					$('#contact_buyer').modal('hide');
					$('#RefundOrder').modal('show');
				}
			});
		}

		function cancelOrder(order,reason){
			swal({
	            title: "Are you sure?",
	            text: "You want want cancel the order!",
	            type: "warning",
	            showCancelButton: true,
	            confirmButtonClass: 'btn-danger',
	            confirmButtonText: "Yes, Cancel Order",
	            closeOnConfirm: false,
	        },
	        function () {
				$.post('{{url('order/cancel_order')}}',{_token:token,order_id:order,reason:reason},function(response){
					$('#RefundOrder').modal('toggle');
					swal("Cancelled", "Order has been Cancelled!", "success");
					alertAjax(response);
				});
	        });
			$.post('{{url('order/cancel_order')}}',{_token:token,order_id:order,reason:reason},function(response){
				$('#RefundOrder').modal('toggle');
				alertAjax(response);
			});
		}

		function synchOrder(){
			var id = '{{ $OrderDetail->OID }}';
			var syncType = $('input[type="radio"][name="syncType"]:checked').val();
			$.post('{{ url('order/synchronize') }}',{_token:token,order_id:id,syncType:syncType,id:order_id},function(response){
				$('#force_sync').modal('hide');
				swal("Synch", "Order has been Synched!", "success");
				alertAjax(response);
			});
		}

		function editNote(id,note){
			$('#editnote').val(note);
			$('#noteid').val(id);
			$('#edit_note').modal('show');
		}

		function saveNote(id,note){
			if(note != ''){
				$.post('{{route('order.update_note')}}',{id:id,note:note,_token:token},function(response){
					$('#edit_note').modal('hide');
					swal("Updated", "Order note has been updated!", "success");
					alertAjax(response);
				});
			}
		}

		var i =1;
		function addMoreTracking(){
			i++;
			var appendControl = '<div class="row" id="row'+i+'"><div class="col-md-5"><div class="form-group"><label>Tracking Id</label><input type="text" id="tracking" name="tracking[]" class="form-control" placeholder="Tracking Number" required=""></div></div><div class="col-sm-5"><div class="form-group"><label>Shipping Company</label><select class="select2 form-control" onchange="isCustom('+i+',this.value)" id="shipping_company_'+i+'" name="shipping_company[]" placeholder="Shipping Company" required=""><option value="USPS">USPS</option><option value="UPS">UPS</option><option value="FedEx">FedEx</option><option value="DHL">DHL</option><option value="Canada Post">Canada Post</option><option value="Royal Mail">Royal Mail</option><option value="Australia Post">Australia Post</option><option value="Couriers Please">Couriers Please</option><option value="Smart Send">Smart Send</option><option value="Interparcel">Interparcel</option><option value="Fastway">Fastway</option><option value="Courier">Courier</option><option value="Other">Other</option></select></div><div class="form-group" id="custom_div'+i+'" style="display:none;" id="custom_company'+i+'"><label>Company Name</label><input type="text" name="company_name[]" class="form-control" id="company_name_'+i+'"></div></div><div class="col-md-2"><div class="form-control"><a href="javascript:;" onclick="removeControl('+i+')" id="row'+i+'"><label>&nbsp;</label><i class="fa fa-minus" ></i></a></div></div></div>';
			$('#addControl').after(appendControl);
		}

		function removeControl(id){
			$('#row'+id+'').remove();  
		}

		function isCustom(id,selectValue){
			if(selectValue == 'Other'){
				$('#custom_div'+id).show();
			}else{
				$('#custom_div'+id).hide();
			}
		}

		function saveAddress(){
			var contact_name 	= $('#contact_name').val();
			var street1 		= $('#street1').val();
			var street2 		= $('#street2').val();
			var city 			= $('#city').val();
			var phone 			= $('#phone').val();
			var state 			= $('#state').val();
			var zip 			= $('#zip').val();
			var country 		= $('#country').val();
			var order_id 		= $('#order_id').val();
			var sec_email       = $('#secondary_email').val();
			var token 			= $('meta[name="csrf-token"]').attr('content');
			$.post('{{ url('order/change_address') }}',{order_id:order_id,contact_name:contact_name,street1:street1,street2:street2,city:city,phone:phone,state:state,zip:zip,secondary_email:sec_email,country:country,_token:token},function(response){
				swal("Updated!", "Shipping Address is Changed!", "success");
				alertAjax(response);
			});
		}
    </script>
@endsection