@extends('backend_layout.app') 
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
	<li class="breadcrumb-item ">
		<a href="">Home</a>
	</li>
	<li class="breadcrumb-item">
		<a href="#">Monitors</a>
	</li>
	<li class="breadcrumb-item active"></li>
</ol>

<div class="container-fluid">
	<div class="animated fadeIn">
		<h3>Monitors</h3>
		<div class="row">
			<div class="col-md-12">
				<div class="card card-accent-theme">
					<div class="card-body">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="price-monitor" data-toggle="tab" href="#price_monitor" role="tab" aria-controls="home" aria-selected="true">Products Monitor</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="orders-monitor" data-toggle="tab" href="#orders_monitor" role="tab" aria-controls="profile" aria-selected="false">Orders Monitor</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="profitable-monitor" data-toggle="tab" href="#profitable_monitor" role="tab" aria-controls="contact" aria-selected="false">Profitable Monitor</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="orders-processor" data-toggle="tab" href="#orders_processor" role="tab" aria-controls="contact" aria-selected="false">Orders Processor</a>
							</li>
						</ul>
						<div class="tab-content" id="myTabContent">
							<div class="tab-pane fade show active" id="price_monitor" role="tabpanel" aria-labelledby="home-tab">
								<br>
								<p class="bg-success text-center" id="use_price_monitor">Monitor is working</p>
								<button type="button" class="btn btn-danger pull-right RbtnMargin" id="start_stop_price_monitor">Stop Monitor</button>
								<h3>Products Monitor <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="This is the main monitor. Its target is to monitor your listings and make sure every listing is profitable for you when somebody purchases it" data-original-title="Price Monitor"><i class="fa fa-question-circle-o"></i> </a> </h3>
								<span class="LastRun">Last Run: </span> <span class="HoursAgo" id="last_run_price_span">1 Minutes ago</span>
								<hr>
								<div class="row">
									<div class="col-md-6">
										<div class="panel panel-primary" data-collapsed="0">
											<div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
												<div class="panel-title">
												<h6>General</h6><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="Look out - Those 'General' settings are used by all monitors." data-original-title="General"><i class="fa fa-question-circle-o"></i> </a>
												</div>
												<div class="panel-options">
													<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
												</div>
											</div>
											<div class="panel-body" id="collapse1">
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Default Break Even</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The break even that will be used when product price changes or when a listing is uploaded" data-original-title="Break Even"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="defult_break_event_input" style="display: none;">
															<label style="padding-left: 0"><input type="radio" value="0" name="dynamic_breakeven"></label>
														</div>
														<span class="fa fa-check-circle-o" id="default_break_event_span"></span>
														<span class="MonitorsContentValue" id="break_even_span" style="display: block;">13.0</span>
														<input class="form-control form-group" type="number" step="0.1" min="10" placeholder="" id="break_even_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Additional $</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Amount of money to add to your listing price when the buying item price changes or when your listing is uploaded" data-original-title="Additional Profit"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="additional_amount_span">0.0</span>
														<input class="form-control form-group" type="number" placeholder="" id="additional_amount_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Additional %</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The percentage of the buying item price to add to your listing price when the buying item price changes or when your listing is uploaded(besided the break even)" data-original-title="Break Even"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="additional_percentage_span">0.0</span>
														<input class="form-control form-group" type="number" placeholder="" id="additional_percentage_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">END Untracked Listings</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="End all listing that are 'untracked' by the monitor. ATTENTION! - Do NOT apply this option if you just moved from other monitor, it will end all your listings on Ebay." data-original-title="End Untracked"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="end_untracked_listing_input" style="display: none;">
															<label><input type="checkbox" id="non_tracable_checkbox_special" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-check-square-o" id="end_untracked_listing_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Not Available Listings</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What the program does with your listing when the buying item is not available anymore" data-original-title="Not Available"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="not_available_listing_span">Put Out Of Stock</span>
														<div class="btn-group" id="not_available_listing_input" style="display: none;">
															<select class="selectpicker form-control form-group">
																<option value="Alert Me" selected="selected">Alert Me</option>
																<option value="Put Out Of Stock">Put Out Of Stock</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">When Product Is Available</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What will happen when a product available again" data-original-title="Product Is Available"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
															<span class="MonitorsContentValue" id="product_available_oos_monitor_span">Raise Quantity</span>
														<div class="btn-group" id="product_available_oos_monitor_input" style="display: none;">
															<select class="selectpicker form-control form-group">
																<option value="Alert Me" selected="selected">Raise Quantity</option>
																<option value="Put Out Of Stock">Alert Me</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Quantity To Raise To</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Raise the listing quantity when a product available again." data-original-title="Quantity To Raise To"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="raise_quantity_oos_monitor_span">3</span>
														<input class="form-control form-group" type="number" placeholder="" id="raise_quantity_oos_monitor_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Dynamic Policy Creation</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="This feature will ignore the policies selected once the item was uploaded, and will match the best policy available according to the supplier's shipping time." data-original-title="Dynamic policy creation"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_dynamic_policy_creation_checkbox" style="display: none;">
															<label><input type="checkbox" id="dynamic_policy_creation" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-check-square-o" id="dynamic_policy_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Minimum Allowed Quantity In Stock</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Minimum Allowed Quantity In Stock" data-original-title="Minimum Allowed Quantity In Stock"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="min_stock_quantity_span">5</span>
														<input class="form-control form-group" type="number" placeholder="" id="min_stock_quantity_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Minimum Profit $ Per Product</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Minimum Profit $ Per Product" data-original-title="Minimum Profit $ Per Product"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="min_profit_per_product_span">0.0</span>
														<input class="form-control form-group" type="number" step="0.1" id="min_profit_per_product_input" style="display: none;">
													</div>
												</div>
											</div>
										</div>
										<div>
											<a href="javascript:;" onclick="productsMonitor()"><i class="fa fa-edit" color:blue"></i></a>
										&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascrip:;" id="save_products_monitor" class="btn btn-sm btn-success" style="display: none;">Save</a>
										</div>
									</div>
									<div class="col-md-6">
										<div class="panel panel-primary" data-collapsed="0">
											<div class="panel-heading" data-toggle="collapse" data-parent="#accordion1" href="#collapse2">
												<div class="panel-title">
													Monitoring Validations <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Those setting are used by the Price Monitor. Those are the checks the program can do for your products." data-original-title="Monitoring Validations"><i class="fa fa-question-circle-o"></i> </a>
												</div>
												<div class="panel-options">
													<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
												</div>
											</div>
											<div class="panel-body" id="collapse2">
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">When Product Is Vero</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What the program does when the buying product becomes VERO" data-original-title="Product Is VERO"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="product_vero_span">Alert Me</span>
														<div class="btn-group" id="product_vero_input" style="display: none;">
															<select class="selectpicker form-control form-group	">
																<option value="Alert Me" selected="selected">Alert Me</option>
																<option value="End Listing">End Listing</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">When Product Is Blocked</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Blocked products are products that you don't want to sell no matter what. You can add as much as you want products like that at the 'Settings' tab." data-original-title="Product Is Blocked"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="blocked_product_span">Alert Me</span>
														<div class="btn-group" id="blocked_product_input" style="display: none;">
															<select class="selectpicker form-control form-group	">
																<option value="Alert Me" selected="selected">Alert Me</option>
																<option value="End Listing">End Listing</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Maximum Shipping Time</span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="maximum_ship_within_days_span">5 days</span>
														<div class="btn-group" id="maximum_ship_within_days_input" style="display: none;">
															<select class="selectpicker form-control form-group	">
																<option value="5 days" selected="selected">5 days</option>
																<option value="1 days">1 days</option>
																<option value="2 days">2 days</option>
																<option value="3 days">3 days</option>
																<option value="4 days">4 days</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Automatic SKU Filling</span>
														<span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If this option checked the uploader will automatically upload products with Source ID as SKU on Ebay" data-original-title="Automatic SKU Filling"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_sku_auto_add_checkbox" style="display: none;">
															<label><input type="checkbox" id="checkbox_auto_add" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-check-square-o" id="checkbox_sku_auto_add_span"></span>
													</div>
												</div>
												<!-- <div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Allow ADD-ON Items</span>
														<span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Allow ADD-ON Items"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_allow_add_on_items_checkbox" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="checkbox_allow_add_on_items_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Allow Prime Pantry</span>
														<span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Allow Prime Pantry"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_allow_prime_pantry_checkbox" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="checkbox_allow_prime_pantry_span"></span>
													</div>
												</div> -->
												<div class="row" style="display: none">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Allow Suppliers Table</span>
														<span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Allow Suppliers Table"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_allow_suppliers_table_checkbox" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="checkbox_allow_suppliers_table_span"></span>
													</div>
												</div>
												<div class="row" style="display: none">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Minimum Amazon Seller Feedbacks</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Minimum Amazon Seller Feedbacks"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="minimum_amz_seller_feedbacks_span">100</span>
														<input class="form-control" type="number" step="1" placeholder="" id="minimum_amz_seller_feedbacks_input" style="display: none;">
													</div>
												</div>
												<div class="row" style="display: none">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Minimum Amazon Seller Feedbacks Percentage</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Minimum Amazon Seller Feedbacks Percentage"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="minimum_amz_seller_feedbacks_percentage_span">90</span>
														<div class="btn-group" id="minimum_amz_seller_feedbacks_percentage_input" style="display: none;">
															<select class="selectpicker form-control">
																<option value="50">50</option>
																<option value="51">51</option>
																<option value="52">52</option>
																<option value="53">53</option>
																<option value="54">54</option>
																<option value="55">55</option>
																<option value="56">56</option>
																<option value="57">57</option>
																<option value="58">58</option>
																<option value="59">59</option>
																<option value="60">60</option>
																<option value="61">61</option>
																<option value="62">62</option>
																<option value="63">63</option>
																<option value="64">64</option>
																<option value="65">65</option>
																<option value="66">66</option>
																<option value="67">67</option>
																<option value="68">68</option>
																<option value="69">69</option>
																<option value="70">70</option>
																<option value="71">71</option>
																<option value="72">72</option>
																<option value="73">73</option>
																<option value="74">74</option>
																<option value="75">75</option>
																<option value="76">76</option>
																<option value="77">77</option>
																<option value="78">78</option>
																<option value="79">79</option>
																<option value="81">81</option>
																<option value="82">82</option>
																<option value="83">83</option>
																<option value="84">84</option>
																<option value="85">85</option>
																<option value="86">86</option>
																<option value="87">87</option>
																<option value="88">88</option>
																<option value="89">89</option>
																<option value="90">90</option>
																<option value="91">91</option>
																<option value="92">92</option>
																<option value="93">93</option>
																<option value="94">94</option>
																<option value="95">95</option>
																<option value="96">96</option>
																<option value="97">97</option>
																<option value="98">98</option>
																<option value="99">99</option>
																<option value="100">100</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row" style="display: none">
													<div class="col-md-6">
														<span class="MonitorsContentKey">First Prime / Cheapest</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="First Prime / Cheapest"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="first_prime_or_cheapest_span">Prime</span>
														<div class="btn-group" id="first_prime_or_cheapest_input" style="display: none;">
															<select class="selectpicker form-control">
																<option value="Prime">Prime</option>
																<option value="Cheapest">Cheapest</option>
															</select>
														</div>
													</div>
												</div>
											</div>
										</div>
									</div>
								</div>
							</div>
							<div id="orders_monitor" class="tab-pane fade in">
								<br>
								<p class="bg-success text-center" id="use_orders_monitor">Monitor is working</p>
								<button type="button" class="btn btn-danger pull-right RbtnMargin" id="start_stop_orders_monitor">Stop Monitor</button>
								<h3>Orders Monitor <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="'Orders Monitor' target is to monitor your orders. When one of your products sold, this monitor will take care of that listing. If you are using the 'Orders Processor', this monitor must be in active state." data-original-title="Orders Monitor"><i class="fa fa-question-circle-o"></i> </a> </h3>
								<span class="LastRun">Last Run: </span> <span class="HoursAgo" id="last_run_orders_span">2 Minutes ago</span>
								<hr>
								<div class="row">
									<div class="col-md-6 col-md-offset-3">
										<div class="panel panel-primary" data-collapsed="0">
											<div class="panel-heading">
												<div class="panel-title" data-toggle="collapse" data-parent="#accordion3" href="#collapse3">
													<h6>General Settings</h6>
												</div>
												<div class="panel-options">
													<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
												</div>
											</div>
											<div class="panel-body" id="collapse3">
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">When Product Is Sold</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What will happen when a product sold" data-original-title="Product Sold"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="product_sold_orders_monitor_span">Raise Quantity</span>
														<div class="btn-group" id="product_sold_orders_monitor_input" style="display: none;">
																<select class="selectpicker form-control form-group">
																	<option value="Raise Quantity" selected="selected">Raise Quantity</option>
																	<option value="Alert Me">Alert Me</option>
																</select>
														</div>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Quantity To Raise To</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Raise the listing quantity when its sold" data-original-title="Quantity To Raise To"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="raise_quantity_orders_monitor_span">3</span>
														<input class="form-control form-group" type="number" placeholder="" id="raise_quantity_orders_monitor_input" style="display: none;">
													</div>
												</div>
												<hr>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">When Product Is Sold Change Price</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="This feature will change all your listings prices after they sold for the first time with the settings below." data-original-title="Change price when product is sold"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_when_product_sold_checkbox" style="display: none;">
																<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="checkbox_when_product_sold_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Start Price Raise After X Sold</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Start Price Raise After X Sold" data-original-title="Start Price Raise After X Sold"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="num_of_sells_for_price_raise_start_span">1</span>
															<input class="form-control form-group" type="number" min="0" id="num_of_sells_for_price_raise_start_input" style="display:none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Raise Only For First Sell</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Raise only for first sell"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_raise_price_for_first_sell_checkbox" style="display: none;">
															<label><input type="radio" value="0" name="raise_price_for_every_sell"></label>
														</div>
														<span class="fa fa-check-circle-o" id="checkbox_raise_price_for_first_sell_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Raise For Each Sell</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Raise for each sell"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_raise_price_for_every_sell_checkbox" style="display: none;">
															<label><input type="radio" value="1" name="raise_price_for_every_sell"></label>
														</div>
														<span class="fa fa-circle-o" id="checkbox_raise_price_for_every_sell_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">When Product Is Sold Raise Additional % By </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="For example: If item's additional percentage is 10% and you write here 15 - the new additional percentage will be 15%" data-original-title="The total price precentage that sold item price will be raised by."> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="change_price_orders_monitor_span">20.0</span>
														<input class="form-control form-group" type="number" placeholder="" id="change_price_orders_monitor_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">When Product Is Sold Raise Additional $ By </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="For example: If item's additional percentage is 10$ and you write here 15 - the new additional percentage will be 15$" data-original-title="The total price precentage that sold item price will be raised by."> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="add_amount_orders_monitor_span">0.0</span>
														<input class="form-control form-group" type="number" placeholder="" id="add_amount_orders_monitor_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Maximum Percentage Raise Limit </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Maximum Percentage Raise Limit" data-original-title="Maximum Percentage Raise Limit"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="max_price_raise_percentage_span">25.0</span>
														<input class="form-control" type="number" step="0.1" placeholder="" id="max_price_raise_percentage_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Maximum Price Raise Limit </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Maximum Price Raise Limit" data-original-title="Maximum Price Raise Limit"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="max_price_raise_ammount_span">10.0</span>
														<input class="form-control" type="number" step="0.1" placeholder="" id="max_price_raise_ammount_input" style="display: none;">
													</div>
												</div>
												<hr>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Drop Price If Product Not Sold</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Drop Price If Product Not Sold"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_not_sold_drop_price_checkbox" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="checkbox_not_sold_drop_price_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Drop Price After X Days</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Drop Price After X Days"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="days_num_for_price_drop_span">5</span>
														<input class="form-control form-group" type="number" step="0.1" id="days_num_for_price_drop_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Drop Price Percentage </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Drop Price Percentage"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="drop_price_percentage_span">0.0</span>
														<input class="form-control form-group" type="number" step="0.1" placeholder="" id="drop_price_percentage_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Drop Price Amount </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Drop Price Amount"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="drop_price_ammount_span">0.0</span>
														<input class="form-control form-group" type="number" step="0.1" placeholder="" id="drop_price_ammount_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Minimum Drop Price Percentage </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Minimum Drop Price Percentage"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="min_drop_price_percentage_span">0.5</span>
														<input class="form-control" type="number" step="0.1" placeholder="" id="min_drop_price_percentage_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Minimum Drop Price Amount </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Minimum Drop Price Amount"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="min_drop_price_ammount_span">0.0</span>
														<input class="form-control" type="number" step="0.1" placeholder="" id="min_drop_price_ammount_input" style="display: none;">
													</div>
												</div>
											</div>
										</div>
										<a href="javascript:;" onclick="ordersMonitor()"><i class="fa fa-edit" color:blue"></i></a>
										&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascrip:;" id="save_orders_monitor" class="btn btn-sm btn-success" style="display: none;">Save</a>
										</div>
									</div>
								</div>
							</div>
							<div id="profitable_monitor" class="tab-pane fade in">
								<br>
								<p class="bg-success text-center" id="use_profitable_items_monitor">Monitor is working</p>
								<button type="button" class="btn btn-danger pull-right RbtnMargin" id="start_stop_profitable_items_monitor">Stop Monitor</button>
								<h3> Profitability Monitor <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="'Profitability' Monitor target is to monitor your listings and end the not profitable ones." data-original-title="Profitability Monitor"><i class="fa fa-question-circle-o"></i> </a> </h3>
								<span class="LastRun">Last Run: </span> <span class="HoursAgo" id="last_run_profitable_span">767 Minutes ago</span>
								<hr>
								<div class="row">
									<div class="col-md-6 col-md-offset-3">
										<div class="panel panel-primary" data-collapsed="0">
											<div class="panel-heading">
												<div class="panel-title" data-toggle="collapse" data-parent="#accordion4" href="#collapse4">
													General Settings
												</div>
												<div class="panel-options">
													<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
												</div>
											</div>
											<div class="panel-body" id="collapse4">
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">When Listing Is Not Profitable:</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What will happen when a listing is not profitable anymore" data-original-title="Listing Not Profitable"> <i class="fa fa-question-circle-o"></i></a></span><br>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="product_not_profitable_span">Alert Me</span>
														<div class="btn-group" id="product_not_profitable_input" style="display: none;">
															<select class="selectpicker form-group form-control">
																<option value="End Listing" selected="selected">End Listing</option>
																<option value="Alert Me">Alert Me</option>
															</select>
														</div><br>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Listing Left Days</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The monitor will check a listing when the listing have X days left. Set how many days left for the listing." data-original-title="Listing Left Days"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="left_day_profitable_monitor_span">2</span>
														<input class="form-control form-group" type="number" min="0" placeholder="" id="left_day_profitable_monitor_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Listing Minimum Sold Quantity</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The monitor will count the listing as not profitable if it has less than X sold quantity. Set the minimum sold quantity." data-original-title="Minimum Sold"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="minimum_sold_profitable_monitor_span">1</span>
														<input class="form-control form-group" type="number" min="0" placeholder="" id="minimum_sold_profitable_monitor_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Listing Minimum Watchers</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The monitor will count the listing as not profitable if it has less than X watchers. Set the minimum watchers quantity." data-original-title="Minimum Watchers"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="minimum_watchers_profitable_monitor_span">1</span>
														<input class="form-control form-group" type="number" min="0" placeholder="" id="minimum_watchers_profitable_monitor_input" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Listing Minimum Views</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The monitor will count the listing as not profitable if it has less than X views. Set the minimum views quantity." data-original-title="Minimum Views"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="minimum_views_profitable_monitor_span">10</span>
														<input class="form-control" type="number" min="0" placeholder="" id="minimum_views_profitable_monitor_input" style="display: none;">
													</div>
												</div>
											</div>
										</div>
										<a href="javascript:;" onclick="profitMonitor()" id="edit_profitable_monitor_input"><i class="fa fa-edit" color:blue"></i></a>
										&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascrip:;" id ="save_profitable_monitor_input" class="btn btn-sm btn-success" style="display: none;">Save</a>
									</div>
								</div>
							</div>
							<div id="orders_processor" class="tab-pane fade in">
								<br>
								<p class="bg-danger text-center" id="use_order_processor_monitor">Monitor is not working</p>
								<button type="button" class="btn btn-success pull-right RbtnMargin" id="start_stop_order_processor_monitor">Start Monitor</button>
								<h3> Orders Processor<a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="This monitor target is to process all your new orders. Purchase the items from Amazon with your amazon users + update tracking numbers when available + send the buyers messages." data-original-title="Orders Processor"> <i class="fa fa-question-circle-o"></i></a> </h3>
								<span class="LastRun">Last Run: </span> <span class="HoursAgo" id="last_run_orders_processor_span">Still not ran</span>
								<hr>
								<div class="row">
									<div class="col-md-6">
										<div class="panel panel-primary" data-collapsed="0">
											<div class="panel-heading" data-toggle="collapse" data-parent="#accordion5" href="#collapse5">
												<div class="panel-title">
													<span><h6>Purchase Settings</h6><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="Those are the settings of your purchase accounts(Amazon accounts that you would like the monitor to purchase the orders with)." data-original-title="Purchase Settings"><i class="fa fa-question-circle-o"></i> </a></span>
												</div>
												<div class="panel-options">
													<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
												</div>
											</div>
												<div class="panel-body" id="collapse5">
												<b>Amazon Accounts</b>
												<div class="row amazon_account_container" >
													<div class="col-md-6">
														<span class="MonitorsContentKey">Purchase Account</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Add Amazon accounts to make the monitor purchase the orders for you with. MAKE SURE YOU HAVE MONEY IN YOUR GIFT BALANCE!" data-original-title="Amazon Accounts"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="id_buying_account_span">Select account</span>
														<div class="btn-group" id="buying_account_input" style="display: none;">
															<select class="selectpicker form-control form-group">
																<option value="Alert Me" selected="selected">Select account</option>
																<option value="temp6137@gmail.com" title="temp6137@gmail.com">temp6137@gmail.com</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row amazon_account_container">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Zip Code</span>

														<span class="MonitorsHelp">
															<a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Enter your main address zip code(Sometimes Amazon asks for registered zip code to make sure you are the owner of the account.)" data-original-title="Zip Code"> 
																<i class="fa fa-question-circle-o"></i>
															</a>
														</span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="zip_code_span">382345</span>
														<input class="form-control form-group" type="number" id="zip_code_input" style="display: none;">
													</div>
												</div>
												<div class="row amazon_account_container">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Country</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Enter your main country" data-original-title="Country"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="country_span">Amazon.com</span>
														<div class="btn-group" id="amazon_country_input" style="display:none;">
															<select class="selectpicker form-control form-group">
																<option value="US" title="United States">Amazon.com</option>
																<option value="UK" title="United Kingdom">Amazon.co.uk</option>
																<option value="DE" title="Germany">Amazon.de</option>
															</select>
														</div>
													</div>

												</div>
												<div class="row amazon_account_container">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Phone Number</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Enter your Amazon registered phone number(Sometimes Amazon asks for registered phone number to make sure you are the owner of the account.)" data-original-title="Phone number"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="phone_number_span">9856451245</span>
														<input class="form-control form-group" type="number" id="phone_number_input"  style="display: none;">
													</div>
												</div>

												<div class="row amazon_account_container">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Full Name
                                                             <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" >
																<i class="fa fa-question-circle-o"></i>
															</a>
														</span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="amazon_full_name_span"></span>
														<input class="form-control form-group" type="text" placeholder="Full Name" id="amazon_full_name_input" style="display: none;">
													</div>
												</div>

												<div class="row amazon_account_container">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Prime Account</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Does your Amazon account is Prime Account? This field is needed to order with prime shipping." data-original-title="Prime Account"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox form-control form-group" id="prime_account_input" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-check-square-o" id="prime_account_span"></span>
													</div>
												</div>
													<a href="javascript:;" onclick="ordersProcessor()"><i class="fa fa-edit" color:blue"></i></a>
													&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascrip:;" class="btn btn-sm btn-success" id="save_amazon_account_input" style="display: none;">Save</a>
											
												<div class="panel-body" id="collapse5">
													<h4><b>Used cards</b></h4>
													<div class="row amazon_account_container" >
														<div class="col-md-3">
														<button type="button" id="start_buying_account_modal" class="btn btn-primary btn-icon icon-left btn-block ">
															Add Card
															<!-- <i class="entypo-user-add"></i> -->
														</button>
														</div>
													</div>
														<div class="row">
														<div class="table-responsive">
						                                        <table class="table">
						                                            <thead>
						                                                <tr>
						                                                    <th>Last14Digits</th>
						                                                    <th>Mon/Year</th>
						                                                    <th>Action</th>
						                                                </tr>
						                                            </thead>
						                                            <tbody>
						                                               
						                                            </tbody>
						                                        </table>
						                                    </div>
													</div>
												</div>
											<div class="form-group amazon_account_container">
													<div class="col-md-12">
														<div class="pull-right">
															<a class="btn btn-link" onclick="remove_amazon_account()">Remove This Account</a>
														</div>
													</div>
												</div>
												<div id="banggood_accounts_list" style="display: none;">
													<h4><b>Banggood Accounts</b></h4>
													<ul id="banggood_accounts_list_ul">
													</ul>
												</div>
												<div class="col-xs-12">
													<button type="button" id="start_buying_account_modal" class="btn btn-primary btn-icon icon-left btn-block">
														Add Purchase Account
														<i class="entypo-user-add"></i>
													</button>
												</div>
											</div>
										</div>
										<div class="panel panel-primary" data-collapsed="0">
											<!-- <div class="panel-heading" data-toggle="collapse" data-parent="#accordion6" href="#collapse6">
												<div class="panel-title">
													<span>Email Accounts (Gmail Only)</span>
												</div>
												<div class="panel-options">
													<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
												</div>
											</div> -->
											<div class="panel-body" id="collapse6">
												<div class="row" id="email_account_container">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Email Account (Gmail Only)</span>
													</div>
													<div class="col-md-6">
														<div class="btn-group">
															<select id="email_account" class="form-control">
																<option>Select account</option>
																<option value="temp6137@gmail.com" title="temp6137@gmail.com">temp6137@gmail.com</option>
															</select>
														</div>
													</div>
												</div>
												<div class="form-group" id="remove_this_account_container">
													<div class="col-md-12">
														<div class="pull-right">
															<a class="btn btn-link" onclick="remove_email_account()">Remove This Account</a>
														</div>
													</div>
												</div>
												<div class="col-xs-12">
													<a href="javascript:;" id="add_email_account" class="btn btn-primary btn-icon icon-left btn-block">
													<span>Add Gmail Account</span>
													<i class="entypo-user-add"></i>
													</a>
												</div>
											</div>
										</div>
									</div>
									<div class="col-md-6">
										<div class="panel panel-primary" data-collapsed="0">
											<div class="panel-heading" data-toggle="collapse" data-parent="#accordion7" href="#collapse7">
												<div class="panel-title">
													<span><h6>Orders Processing</h6> <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Those settings will apply on active orders while processing them." data-original-title="Orders Processing"> <i class="fa fa-question-circle-o"></i> </a></span>
												</div>
												<div class="panel-options">
													<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
						
												</div>
											</div>
											<div class="panel-body" id="collapse7">
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Auto Order</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If active, the monitor will purchase automatically all your new orders with your buying account." data-original-title="Auto Order"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="auto_order_input" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-check-square-o" id="auto_order_span"></span>
													</div>
												</div>

												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Bundle AddOn</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If active, the monitor will purchase only prime products" data-original-title="Order Only Prime"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="bundle_addon_input" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-check-square-o" id="bundle_addon_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Order Only Prime</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If active, the monitor will purchase only prime products" data-original-title="Order Only Prime"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="order_only_prime_input" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-check-square-o" id="order_only_prime_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Hipshipper</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If active, Enable International Order Fulfillment for eBay Sellers By hipshipper.com" data-original-title="Hipshipper Shipping"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="hipshipper_checkbox_input" style="display: none;">
															<label><input type="checkbox" value="0" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="hipshipper_checkbox_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Maximum Product Price For Auto Order</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Set the maximum product price that you allow the monitor to purchase." data-original-title="Maximum Product Price For Auto Order"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="max_price_auto_order_input">300.0</span>
														<input class="form-control form-group" type="number" id="max_price_auto_order_span" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Max Lose Amount</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If the estimate profit from an order is a lose, set the maximum lose amount you accept. If the estimate lose is greater than what you have set, the monitor won't purchase the product." data-original-title="Max Lose Amount"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="lose_max_amount_span">8.0</span>
														<input class="form-control form-group" type="number" id="lose_max_amount_input" style="display:none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Set Order As Shipped</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Choose when the program will set order shipped status on Ebay. Right after the purchase of the product from Amazon or after tracking details are updated." data-original-title="Set Order As Shipped"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="order_shipped_span">After Tracking Updated</span>
														<select id="order_shipped_input" class="selectpicker form-control form-group" style="display: none;">
															<option value="After Purchase" selected="selected">After Purchase</option>
															<option value="After Tracking Updated">After Tracking Updated</option>
														</select>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Gift Message</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="When ordering from amazon, sometimes you can attach a gift message to the buyer. Choose your message." data-original-title="Gift Message"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="gift_message_span"></span>
														<input type="text" id="gift_message_input" class="form-control form-group" data-validate="required" style="display: none;">
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Set Ontrack For Amazon Shipping Carrier</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Set Ontrack For Amazon Shipping Carrier" data-original-title="Set Ontrack For Amazon Shipping Carrier"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox form-control form-group" id="amazon_shipping_ontrack_input" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="amazon_shipping_ontrack_span"></span>
													</div>
												</div>
												<a href="javascript:;"><i class="fa fa-edit" onclick="ordersProcessing()" color:blue"></i></a>
													&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascrip:;" class="btn btn-sm btn-success" id="save_orders_processing" style="display: none;">Save</a>
												<div class="col-xs-12">
													<!-- <button type="button" id="start_messages_modal" class="btn btn-primary btn-icon icon-left btn-block">
														Messages To Buyers
														<i class="fa fa-envelope-o"></i>
													</button> -->

													<a href="#" class="btn btn-primary btn-icon icon-left btn-block" data-toggle="modal" data-target="#buyers_message">Messages To Buyers<i class="fa fa-envelope-o"></i>
													</a>
												</div>
												<div class="col-xs-12">
													<!-- <button type="button" id="start_notifications_modal" class="btn btn-primary btn-icon icon-left btn-block">
														<img src="/neon_dashboard/assets/images/appointment-reminders-xxl.png">
														Notifications
													</button> -->

													<a href="#" class="btn btn-primary btn-icon icon-left btn-block" data-toggle="modal" data-target="#notification_modal">Notifications<i class="fa fa-envelope-o"></i>
													</a>
												</div>
												<div class="col-xs-12" style="display: none;">
													<button type="button" id="gift_cards" class="btn btn-primary btn-icon icon-left btn-block" onclick="get_gift_cards_list();" data-toggle="modal" data-target="#gift_cards_modal">
													Gift Cards List
													</button>
												</div>
											</div>
										</div>
											<div class="panel panel-primary" data-collapsed="0">
											<div class="panel-heading" data-toggle="collapse" data-parent="#accordion8" href="#collapse8">
												<div class="panel-title">
													<span>Cashback Users<a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Those settings select cashback site and user." data-original-title="Cashback Users"> <i class="fa fa-question-circle-o"></i> </a></span>
												</div>
												<div class="panel-options">
													<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
												</div>
											</div>
											<div class="panel-body" id="collapse8">
												
												<!-- <div class="row">
													<div class="col-xs-12">
														<button type="button" id="start_user_cashback_modal" class="btn btn-primary btn-icon icon-left btn-block">
														Add Cashback User
														<i class="entypo-user-add"></i>
														</button>
													</div>
												</div> -->

												<div class="col-xs-12">
													<!-- <button type="button" id="start_messages_modal" class="btn btn-primary btn-icon icon-left btn-block">
														Add Cashback User
														<i class="fa fa-envelope-o"></i>
													</button> -->

													<a href="#" class="btn btn-primary btn-icon icon-left btn-block" data-toggle="modal" data-target="#cashback_modal">
														Add Cashback User
														<i class="fa fa-envelope-o"></i>
													
					                                </a>
												</div>

													

											</div>
										</div>
									</div>
								</div>
								<i class="fa fa-pencil fa-lg RbtnMargin ChangeDetailsIcons" id="change_order_processor_monitor_details" style="display: block;"></i>
								<i class="fa fa-times fa-lg RbtnMargin ChangeDetailsIcons" id="cancel_order_processor_monitor_details" style="display: none;"></i>
								<i class="fa fa-check fa-lg RbtnMargin ChangeDetailsIcons" id="submit_change_order_processor_monitor_details" style="display: none;"></i>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</div>
<!-- sample modal content -->
<div class="modal fade bs-example-modal-default" id="buyers_message" tabindex="-1" role="dialog"  aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" >Messages To Buyers</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="price-monitor" data-toggle="tab" href="javascript:;" role="tab" aria-controls="home" aria-selected="true">After Order Success Message</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="orders-monitor" data-toggle="tab" href="javascript:;" role="tab" aria-controls="profile" aria-selected="false">Tracking Number Update Message</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="profitable-monitor" data-toggle="tab" href="javascript:;" role="tab" aria-controls="contact" aria-selected="false">Thank You Message</a>
						</li>
						
					</ul>

					<div class="col-md-12">
							<span class="MonitorsContentKey"><strong>Feedback:</strong></span>
						
						<div class="row" id="cashback_site_password">
							<div class="modal-body">
								<div class="col-md-12">
									<div class="btn-group" style="width: 100%;">
										<input class="form-control" type="text" placeholder="" id="cashback_site_password">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
							<span class="MonitorsContentKey"><strong>Send US Orders Thank You Message After:</strong></span>
						
						<div class="row" id="cashback_site_password">
							<div class="modal-body">
								<div class="col-md-12">
									<div class="btn-group" style="width: 100%;">
										<input class="form-control" type="text" placeholder="" id="cashback_site_password">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
							<span class="MonitorsContentKey"><strong>Send International Orders Thank You Messages After:</strong></span>
						
						<div class="row" id="cashback_site_password">
							<div class="modal-body">
								<div class="col-md-12">
									<div class="btn-group" style="width: 100%;">
										<input class="form-control" type="text" placeholder="" id="cashback_site_password">
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save Change</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
 <!-- sample modal content Notification -->
<div class="modal fade bs-example-modal-default" id="notification_modal" tabindex="-1" role="dialog"  aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
                <h6 class="modal-title" style="margin: 12px" >Notifications</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
					<div class="checkbox" id="checkbox_hipshipper_checkbox">
						<input type="checkbox" value="0" ><strong>Email When Ordered Sucessfully</strong>
						<br>
						<input type="checkbox" value="0" ><strong>Email When Tracking Number Updated</strong>
						<br>
						<input type="checkbox" value="0" ><strong>Email When Ordered Failed
						</strong>
						<br>
					</div>
					
				</div>
            </div>
            <div class="col-md-6">
							<span class="MonitorsContentKey"><strong>Notifications Email:</strong></span>
						</div>
						<div class="row" id="cashback_site_password">
							<div class="modal-body">
								<div class="col-md-12">
									<div class="btn-group" style="width: 100%;">
										<input class="form-control" type="text" placeholder="" id="cashback_site_password">
									</div>
								</div>
							</div>
						</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save Change</button>
            </div>
          
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- sample modal content cashback -->
<div class="modal fade bs-example-modal-default" id="cashback_modal" tabindex="-1" role="dialog"  aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" >Add Cashback User</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
        	<div class="modal-body"> 
            	<div class="cashback_user_details_container">
					<div class="row">
						<div class="col-md-6">
							<span class="MonitorsContentKey">Cashback Site Username:</span>
						</div>
						<div class="row" id="cashback_site_username">
							<div class="col-md-12">
								<div class="btn-group">
									<input class="form-control" type="text" placeholder="Cashback Site Username" id="cashback_site_username">
								</div>
							</div>
						</div>
					</div>
				</br>
					<div class="row">
						<div class="col-md-6">
							<span class="MonitorsContentKey">Cashback Site Password:</span>
						</div>
						<div class="row" id="cashback_site_password">
							<div class="col-md-12">
								<div class="btn-group">
									<input class="form-control" type="text" placeholder="Cashback Site Password" id="cashback_site_password">
								</div>
							</div>
						</div>
					</div>
				</br>
					<div class="row">
						<div class="col-md-6">
							<span class="MonitorsContentKey">Cashback Site:</span>
						</div>
						<div class="row" id="cashback_site_password">
							<!-- <div class="col-md-12">
								<div class="btn-group">
										<input class="form-control" type="text" placeholder="Cashback Site" id="cashback_site">
								</div>
							</div> -->
							<div class="col-md-12">
								<div class="btn-group">
									<select  class="form-control" id="email_account">
										<option>Cashback Site</option>
										<option value="temp6137@gmail.com" title="temp6137@gmail.com">temp6137@gmail.com</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>	
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save Change</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
</div>
</div>
@endsection

@section('footer_script')
<script>
	function productsMonitor()
	{
		$('#defult_break_event_input').toggle();
		$('#break_even_input').toggle();
		$('#additional_amount_input').toggle();
		$('#additional_percentage_input').toggle();
		$('#end_untracked_listing_input').toggle();
		$('#not_available_listing_input').toggle();
		$('#product_available_oos_monitor_input').toggle();
		$('#raise_quantity_oos_monitor_input').toggle();
		$('#checkbox_dynamic_policy_creation_checkbox').toggle();
		$('#min_stock_quantity_input').toggle();
		$('#min_profit_per_product_input').toggle();
		$('#product_vero_input').toggle();
		$('#blocked_product_input').toggle();
		$('#maximum_ship_within_days_input').toggle();
		$('#checkbox_sku_auto_add_checkbox').toggle();
		$('#save_products_monitor').toggle();

		$('#default_break_event_span').toggle();
		$('#break_even_span').toggle();
		$('#additional_amount_span').toggle();	
		$('#additional_percentage_span').toggle();
		$('#end_untracked_listing_span').toggle();	
		$('#not_available_listing_span').toggle();
		$('#product_available_oos_monitor_span').toggle();
		$('#raise_quantity_oos_monitor_span').toggle();
		$('#dynamic_policy_span').toggle(); 
		$('#min_stock_quantity_span').toggle();
		$('#min_profit_per_product_span').toggle();
		$('#product_vero_span').toggle();
		$('#blocked_product_span').toggle();
		$('#maximum_ship_within_days_span').toggle();
		$('#checkbox_sku_auto_add_span').toggle();
  	}
  	function ordersMonitor()
	{
		$('#product_sold_orders_monitor_input').toggle();
		$('#raise_quantity_orders_monitor_input').toggle();
		$('#checkbox_when_product_sold_checkbox').toggle();
		$('#num_of_sells_for_price_raise_start_input').toggle();
		$('#checkbox_raise_price_for_first_sell_checkbox').toggle();
		$('#checkbox_raise_price_for_every_sell_checkbox').toggle();
		$('#change_price_orders_monitor_input').toggle();
		$('#add_amount_orders_monitor_input').toggle();
		$('#max_price_raise_percentage_input').toggle();
		$('#max_price_raise_ammount_input').toggle();
		$('#checkbox_not_sold_drop_price_checkbox').toggle();
		$('#days_num_for_price_drop_input').toggle();
		$('#drop_price_percentage_input').toggle();
		$('#drop_price_ammount_input').toggle();
		$('#min_drop_price_percentage_input').toggle();
		$('#min_drop_price_ammount_input').toggle();
		$('#save_orders_monitor').toggle();

		$('#product_sold_orders_monitor_span').toggle();
		$('#raise_quantity_orders_monitor_span').toggle();
		$('checkbox_when_product_sold_span').toggle();
		$('#num_of_sells_for_price_raise_start_span').toggle();
		$('#checkbox_raise_price_for_first_sell_span').toggle();
		$('#checkbox_raise_price_for_every_sell_span').toggle();
		$('#change_price_orders_monitor_span').toggle();
		$('#add_amount_orders_monitor_span').toggle();
		$('#max_price_raise_percentage_span').toggle();
		$('#max_price_raise_ammount_span').toggle();
		$('#checkbox_not_sold_drop_price_span').toggle();
		$('#days_num_for_price_drop_span').toggle();
		$('#drop_price_percentage_span').toggle();
		$('#drop_price_ammount_span').toggle();
		$('#min_drop_price_percentage_span').toggle();
		$('#min_drop_price_ammount_span').toggle();

	}                              
	function profitMonitor()
	{
		$('#product_not_profitable_input').toggle();
		$('#left_day_profitable_monitor_input').toggle();
		$('#minimum_sold_profitable_monitor_input').toggle();
		$('#minimum_watchers_profitable_monitor_input').toggle();
		$('#minimum_views_profitable_monitor_input').toggle();
		$('#save_profitable_monitor_input').toggle();

		$('#product_not_profitable_span').toggle();
		$('#left_day_profitable_monitor_span').toggle();
		$('#minimum_sold_profitable_monitor_span').toggle();
		$('#minimum_watchers_profitable_monitor_span').toggle();
		$('#minimum_views_profitable_monitor_span').toggle();
	}
	
	
</script>
@endsection 