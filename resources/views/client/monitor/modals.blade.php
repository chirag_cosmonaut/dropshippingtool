<div class="modal fade bs-example-modal-default" id="buyers_message" tabindex="-1" role="dialog"  aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" >Messages To Buyers</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="container-fluid">
					<ul class="nav nav-tabs" id="myTab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="price-monitor" data-toggle="tab" href="javascript:;" role="tab" aria-controls="home" aria-selected="true">After Order Success Message</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="orders-monitor" data-toggle="tab" href="javascript:;" role="tab" aria-controls="profile" aria-selected="false">Tracking Number Update Message</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="profitable-monitor" data-toggle="tab" href="javascript:;" role="tab" aria-controls="contact" aria-selected="false">Thank You Message</a>
						</li>
						
					</ul>

					<div class="col-md-12">
							<span class="MonitorsContentKey"><strong>Feedback:</strong></span>
						
						<div class="row" id="cashback_site_password">
							<div class="modal-body">
								<div class="col-md-12">
									<div class="btn-group" style="width: 100%;">
										<input class="form-control" type="text" placeholder="" id="cashback_site_password">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
							<span class="MonitorsContentKey"><strong>Send US Orders Thank You Message After:</strong></span>
						
						<div class="row" id="cashback_site_password">
							<div class="modal-body">
								<div class="col-md-12">
									<div class="btn-group" style="width: 100%;">
										<input class="form-control" type="text" placeholder="" id="cashback_site_password">
									</div>
								</div>
							</div>
						</div>
					</div>

					<div class="col-md-12">
							<span class="MonitorsContentKey"><strong>Send International Orders Thank You Messages After:</strong></span>
						
						<div class="row" id="cashback_site_password">
							<div class="modal-body">
								<div class="col-md-12">
									<div class="btn-group" style="width: 100%;">
										<input class="form-control" type="text" placeholder="" id="cashback_site_password">
									</div>
								</div>
							</div>
						</div>
					</div>


				</div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                  <button type="button" class="btn btn-primary">Save Change</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->
 <!-- sample modal content Notification -->
<div class="modal fade bs-example-modal-default" id="notification_modal" tabindex="-1" role="dialog"  aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
                <h6 class="modal-title" style="margin: 12px" >Notifications</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
					<div class="checkbox" id="checkbox_hipshipper_checkbox">
						<input type="checkbox" value="0" ><strong>Email When Ordered Sucessfully</strong>
						<br>
						<input type="checkbox" value="0" ><strong>Email When Tracking Number Updated</strong>
						<br>
						<input type="checkbox" value="0" ><strong>Email When Ordered Failed
						</strong>
						<br>
					</div>
					
				</div>
            </div>
            <div class="col-md-6">
							<span class="MonitorsContentKey"><strong>Notifications Email:</strong></span>
						</div>
						<div class="row" id="cashback_site_password">
							<div class="modal-body">
								<div class="col-md-12">
									<div class="btn-group" style="width: 100%;">
										<input class="form-control" type="text" placeholder="" id="cashback_site_password">
									</div>
								</div>
							</div>
						</div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save Change</button>
            </div>
          
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->

<!-- sample modal content cashback -->
<div class="modal fade bs-example-modal-default" id="cashback_modal" tabindex="-1" role="dialog"  aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" >Add Cashback User</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
        	<div class="modal-body"> 
            	<div class="cashback_user_details_container">
					<div class="row">
						<div class="col-md-6">
							<span class="MonitorsContentKey">Cashback Site Username:</span>
						</div>
						<div class="row" id="cashback_site_username">
							<div class="col-md-12">
								<div class="btn-group">
									<input class="form-control" type="text" placeholder="Cashback Site Username" id="cashback_site_username">
								</div>
							</div>
						</div>
					</div>
				</br>
					<div class="row">
						<div class="col-md-6">
							<span class="MonitorsContentKey">Cashback Site Password:</span>
						</div>
						<div class="row" id="cashback_site_password">
							<div class="col-md-12">
								<div class="btn-group">
									<input class="form-control" type="text" placeholder="Cashback Site Password" id="cashback_site_password">
								</div>
							</div>
						</div>
					</div>
				</br>
					<div class="row">
						<div class="col-md-6">
							<span class="MonitorsContentKey">Cashback Site:</span>
						</div>
						<div class="row" id="cashback_site_password">
							<!-- <div class="col-md-12">
								<div class="btn-group">
										<input class="form-control" type="text" placeholder="Cashback Site" id="cashback_site">
								</div>
							</div> -->
							<div class="col-md-12">
								<div class="btn-group">
									<select  class="form-control" id="email_account">
										<option>Cashback Site</option>
										<option value="temp6137@gmail.com" title="temp6137@gmail.com">temp6137@gmail.com</option>
									</select>
								</div>
							</div>
						</div>
					</div>
					
				</div>
			</div>	
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save Change</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>