@extends('backend_layout.app') 
@section('content')
<?php
	// echo "<pre>";
	// print_r($productMonitorValue);
	// die;
?>
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
	<li class="breadcrumb-item ">
		<a href="">Home</a>
	</li>
	<li class="breadcrumb-item">
		<a href="#">Monitors</a>
	</li>
	<li class="breadcrumb-item active"></li>
</ol>
<div class="container-fluid">
	@if(session('flash_message'))
        <div class="alert alert-success">
            {!! session('flash_message') !!}
        </div>
        @endif
	<div class="row">
		<div class="col-md-12">
			<div class="card card-accent-theme">
				<div class="card-body">
					<h4 class="text-theme">Monitors</h4>
					<ul class="nav nav-pills mb-3" id="pills-tab" role="tablist">
						<li class="nav-item">
							<a class="nav-link active" id="productMonitor" data-toggle="pill" href="#products_contents" role="tab" aria-controls="products_contents" aria-selected="true">Products Monitor</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="ordersMonitor" data-toggle="pill" href="#orders_contents" role="tab" aria-controls="orders_contents" aria-selected="false">Orders Monitor</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#profitable_monitor" role="tab" aria-controls="pills-contact" aria-selected="false">Profitable Monitor</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" id="pills-contact-tab" data-toggle="pill" href="#orders_processor" role="tab" aria-controls="pills-contact" aria-selected="false">Orders Processor</a>
						</li>
					</ul>
					<div class="tab-content" id="pills-tabContent">
						<div class="tab-pane active" id="products_contents" role="tabpanel">
                            @if(isset($productMonitorValue->is_start) && $productMonitorValue->is_start == 1 )
                             <p class="bg-success text-center" id="use_price_monitor">Product Monitor is working</p>
								<a class="btn btn-danger pull-right RbtnMargin" id="product_start_monitor" onclick="startStopMonitor('products_monitor','false')">Stop Monitor</a>
							@else
								<p class="bg-danger text-center" id="use_price_monitor">Product Monitor is not working</p>
								<a class="btn btn-success pull-right RbtnMargin" onclick="startStopMonitor('products_monitor','true')" id="product_start_monitor">Start Monitor</a>
							@endif
							<h4>Products Monitor 
								<a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="This is the main monitor. Its target is to monitor your listings and make sure every listing is profitable for you when somebody purchases it" data-original-title="Price Monitor">
									<i class="fa fa-question-circle-o"></i>
								</a> 
							</h4>
							<span class="LastRun">Last Run: </span> <span class="HoursAgo" id="last_run_price_span">1 Minutes ago</span>
							<hr>
							<div class="row">
								<div class="col-md-6">
									<div class="card card-accent-theme" data-collapsed="0">
										<div class="card-header" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
											<div class="panel-title">
												<h6>General</h6><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="Look out - Those 'General' settings are used by all monitors." data-original-title="General"><i class="fa fa-question-circle-o"></i> </a>
											</div>
											<div class="panel-options">
												<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
											</div>
										</div>
										<div class="card-body" id="collapse1">
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Default Break Even</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The break even that will be used when product price changes or when a listing is uploaded" data-original-title="Break Even"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
							
													<span class="MonitorsContentValue" id="break_even_span" style="display: block;">{{ isset($productMonitorValue->default_break_event)?$productMonitorValue->default_break_event:'' }}</span>
													<input class="form-control form-group" type="number" step="0.1" min="10" placeholder="" id="break_even_input" name="break_event_input" value="{{isset($productMonitorValue->default_break_event)?$productMonitorValue->default_break_event:''}}" style="display: none;">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Additional $</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Amount of money to add to your listing price when the buying item price changes or when your listing is uploaded" data-original-title="Additional Profit"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="additional_amount_span">{{ isset($productMonitorValue->additional_dollar)?$productMonitorValue->additional_dollar:'' }}</span>
													<input class="form-control form-group" type="number" placeholder="" id="additional_amount_input" style="display: none;" value="{{isset($productMonitorValue->additional_dollar)?$productMonitorValue->additional_dollar:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Additional %</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The percentage of the buying item price to add to your listing price when the buying item price changes or when your listing is uploaded(besided the break even)" data-original-title="Break Even"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="additional_percentage_span">{{ isset($productMonitorValue->additional_per)?$productMonitorValue->additional_per:'' }}</span>
													<input class="form-control form-group" type="number" id="additional_percentage_input" style="display:none;" 
													value="{{ isset($productMonitorValue->additional_per)?$productMonitorValue->additional_per:'' }}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">END Untracked Listings</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="End all listing that are 'untracked' by the monitor. ATTENTION! - Do NOT apply this option if you just moved from other monitor, it will end all your listings on Ebay." data-original-title="End Untracked"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<div class="checkbox" id="end_untracked_listing_input" style="display: none;">
														<input type="checkbox" id="non_tracable_checkbox_special" checked="checked" value="{{isset($productMonitorValue->end_untracked_listing)?$productMonitorValue->end_untracked_listing:''}}">
													</div>
													@if(isset($productMonitorValue->end_untracked_listing) && $productMonitorValue->end_untracked_listing == "true")
														<span class="fa fa-check-square-o" id="end_untracked_listing_span"></span>
													@else
														<span class="fa fa-square-o" id="end_untracked_listing_span"></span>
													@endif
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Not Available Listings</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What the program does with your listing when the buying item is not available anymore" data-original-title="Not Available"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="not_available_listing_span">{{ isset($productMonitorValue->not_available_listing)?$productMonitorValue->not_available_listing:'' }}</span>
													<div class="btn-group" id="not_available_listing_input" style="display: none;">
														<select class="selectpicker form-control form-group" id="available_listing_not_input" name="available_listing_not_input" value="{{isset($productMonitorValue->not_available_listing)?$productMonitorValue->not_available_listing:''}}">
															<option value="Alert Me" selected="selected">Alert Me</option>
															<option value="Put Out Of Stock">Put Out Of Stock</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">When Product Is Available</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What will happen when a product available again" data-original-title="Product Is Available"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="product_available_oos_monitor_span">{{ isset($productMonitorValue->when_product_available)?$productMonitorValue->when_product_available:''}}</span>
													<div class="btn-group" id="product_available_oos_monitor_input" style="display: none;">
														<select class="selectpicker form-control form-group" id="available_product_input" name="available_product_input" value="{{isset($productMonitorValue->when_product_available)?$productMonitorValue->when_product_available:''}}">
															<option value="Raise Quantity" selected="selected">Raise Quantity</option>
															<option value="Alert Me">Alert Me</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Quantity To Raise To</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Raise the listing quantity when a product available again." data-original-title="Quantity To Raise To"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="raise_quantity_oos_monitor_span">{{ isset($productMonitorValue->quantity_to_raise_to)?$productMonitorValue->quantity_to_raise_to:'' }}</span>
													<input class="form-control form-group" type="number" placeholder="" id="raise_quantity_oos_monitor_input" name="raise_quantity_oos_monitor_input" style="display: none;" value="{{ isset($productMonitorValue->quantity_to_raise_to)?$productMonitorValue->quantity_to_raise_to:'' }}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Dynamic Policy Creation</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="This feature will ignore the policies selected once the item was uploaded, and will match the best policy available according to the supplier's shipping time." data-original-title="Dynamic policy creation"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<div class="checkbox" id="checkbox_dynamic_policy_creation_checkbox" style="display: none;">
														<label><input type="checkbox" id="dynamic_policy_creation" name="dynamic_policy_creation"  checked="checked" value="{{ isset($productMonitorValue->dynamic_policy_span)?$productMonitorValue->dynamic_policy_span:'' }}"></label>
													</div>
													@if(isset($productMonitorValue->dynamic_policy_span) && $productMonitorValue->dynamic_policy_creation == "true")
														<span class="fa fa-check-square-o" id="dynamic_policy_span"></span>
													@else
														<span class="fa fa-check-o" id="dynamic_policy_span"></span>
													@endif
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Minimum Allowed Quantity In Stock</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Minimum Allowed Quantity In Stock" data-original-title="Minimum Allowed Quantity In Stock"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="min_stock_quantity_span">{{ isset($productMonitorValue->min_allowed_quantity_in_stock)?$productMonitorValue->min_allowed_quantity_in_stock:'' }}</span>
													<input class="form-control form-group" type="number" placeholder="" id="min_stock_quantity_input" name="min_stock_quantity_input" style="display: none;" value="{{isset($productMonitorValue->min_allowed_quantity_in_stock)?$productMonitorValue->min_allowed_quantity_in_stock:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Minimum Profit $ Per Product</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Minimum Profit $ Per Product" data-original-title="Minimum Profit $ Per Product"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="min_profit_per_product_span"></span>
													<input class="form-control form-group" type="number" step="0.1" id="min_profit_per_product_input" style="display: none;" value="{{isset($productMonitorValue->min_profit_dollar_per_product)?$productMonitorValue->min_profit_dollar_per_product:''}}">
												</div>
											</div>
										</div>
										<div class="card-footer">
											<a href="javascript:;" onclick="productsMonitor()"><i class="fa fa-edit" color:blue"></i></a>
											&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascrip:;" id="save_products_monitor" onclick="saveMonitor()" class="btn btn-sm btn-success" style="display:none;">Save</a>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="card card-accent-theme" data-collapsed="0">
										<div class="card-header" data-toggle="collapse" data-parent="#accordion1" href="#collapse2">
											<div class="panel-title">
												<h6>Monitoring Validations </h6><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Those setting are used by the Price Monitor. Those are the checks the program can do for your products." data-original-title="Monitoring Validations"><i class="fa fa-question-circle-o"></i> </a>
											</div>
											<div class="panel-options">
												<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
											</div>
										</div>
										<div class="card-body" id="collapse2">
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">When Product Is Vero</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What the program does when the buying product becomes VERO" data-original-title="Product Is VERO"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="product_vero_span">{{ isset($productMonitorValue->when_product_vero)?$productMonitorValue->when_product_vero:'' }}</span>
													<div class="btn-group" id="product_vero_input" style="display: none;">
														<select class="selectpicker form-control form-group" id="product_vero_value" name="product_vero_value" 
														 value="{{isset($productMonitorValue->when_product_vero)?$productMonitorValue->when_product_vero:''}}">
															<option value="Alert Me" selected="selected">Alert Me</option>
															<option value="End Listing">End Listing</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">When Product Is Blocked</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Blocked products are products that you don't want to sell no matter what. You can add as much as you want products like that at the 'Settings' tab." data-original-title="Product Is Blocked"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="blocked_product_span">{{ isset($productMonitorValue->when_product_blocked)?$productMonitorValue->when_product_blocked:'' }}</span>
													<div class="btn-group" id="blocked_product_input" style="display: none;">
														<select class="selectpicker form-control form-group" id="block_product_value">
															<option value="Alert Me" selected="selected">Alert Me</option>
															<option value="End Listing">End Listing</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Maximum Shipping Time</span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="maximum_ship_within_days_span">5 days</span>
													<div class="btn-group" id="maximum_ship_within_days_input" style="display: none;">
														<select class="selectpicker form-control form-group" id="max_shipping_value" name="max_shipping_value">
															<option value="5 days" selected="selected">{{ isset($productMonitorValue->	max_shipping_time)?$productMonitorValue->	max_shipping_time:'' }}</option>
															<option value="1 days">1 days</option>
															<option value="2 days">2 days</option>
															<option value="3 days">3 days</option>
															<option value="4 days">4 days</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Automatic SKU Filling</span>
													<span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If this option checked the uploader will automatically upload products with Source ID as SKU on Ebay" data-original-title="Automatic SKU Filling"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<div class="checkbox" id="checkbox_sku_auto_add_checkbox" style="display: none;">
														<label><input type="checkbox" id="checkbox_auto_add" checked="checked" value="{{ isset($productMonitorValue->automatic_sku_filling)?$productMonitorValue->automatic_sku_filling:'' }}"></label>
													</div>
													@if(isset($productMonitorValue->automatic_sku_filling) && $productMonitorValue->automatic_sku_filling == "true")
														<span class="fa fa-check-square-o" id="checkbox_sku_auto_add_span"></span>
													@else
														<span class="fa fa-square-o" id="checkbox_sku_auto_add_span"></span>
													@endif
												</div>
											</div>
										</div>
									</div>
		                        </div>
							</div>
						</div>
                        <div class="tab-pane" id="orders_contents" role="tabpanel">
                            @if(isset($ordersMonitorValue->is_start) && $ordersMonitorValue->is_start == 1 )
								<p class="bg-success text-center" id="use_orders_monitor">ORders Monitor is working</p>
								<a class="btn btn-danger pull-right RbtnMargin" id="product_start_monitor" onclick="startStopMonitor('orders_monitor','false')">Stop Monitor</a>
							@else
								<p class="bg-danger text-center" id="use_orders_monitor">Orders Monitor is not working</p>
								<a class="btn btn-success pull-right RbtnMargin" onclick="startStopMonitor('orders_monitor','true')" id="product_start_monitor">Start Monitor</a>
							@endif
							<h4>Orders Monitor <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="'Orders Monitor' target is to monitor your orders. When one of your products sold, this monitor will take care of that listing. If you are using the 'Orders Processor', this monitor must be in active state." data-original-title="Orders Monitor"><i class="fa fa-question-circle-o"></i> </a> </h4>
							<span class="LastRun">Last Run: </span> <span class="HoursAgo" id="last_run_orders_span">2 Minutes ago</span>
							<hr>
							<div class="row">
								<div class="col-md-2"></div>
								<div class="col-md-8">
									<div class="card card-accent-theme" data-collapsed="0">
										<div class="panel-heading">
											<div class="card-header" data-toggle="collapse" data-parent="#accordion3" href="#collapse3">
												<h6>General Settings</h6>
											</div>
											<div class="panel-options">
												<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
											</div>
										</div>
										<div class="panel-body" id="collapse3">
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">When Product Is Sold</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What will happen when a product sold" data-original-title="Product Sold"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="product_sold_orders_monitor_span">{{ isset($ordersMonitorValue->when_product_sold)?$ordersMonitorValue->when_product_sold:'' }}</span>
													<div class="btn-group" id="product_sold_orders_monitor_input" style="display: none;">
														<select class="selectpicker form-control form-group" id="product_sold_value" name="product_sold_value">
															<option value="Raise Quantity" selected="selected">Raise Quantity</option>
															<option value="Alert Me">Alert Me</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Quantity To Raise To</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Raise the listing quantity when its sold" data-original-title="Quantity To Raise To"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="raise_quantity_orders_monitor_span">{{ isset($ordersMonitorValue->quantity_to_raise_to)?$ordersMonitorValue->quantity_to_raise_to:'' }}</span>
													<input class="form-control form-group" type="number" id="raise_quantity_orders_monitor_input" style="display: none;"
													value="{{isset($ordersMonitorValue->quantity_to_raise_to)?$ordersMonitorValue->quantity_to_raise_to:''}}">
												</div>
											</div>
											<hr>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">When Product Is Sold Change Price</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="This feature will change all your listings prices after they sold for the first time with the settings below." data-original-title="Change price when product is sold"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<div class="checkbox" id="checkbox_when_product_sold_checkbox" style="display: none;">
														<label><input type="checkbox" checked="checked" id="product_sold_change_price"></label>
													</div>
													@if(isset($ordersMonitorValue->when_product_sold_change_price) && $ordersMonitorValue->when_product_sold_change_price == "true")
														<span id="checkbox_when_product_sold_span" class="fa fa-check-square-o"></span>
													@else
														<span id="checkbox_when_product_sold_span" class="fa fa-square-o"></span>
													@endif
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Start Price Raise After X Sold</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Start Price Raise After X Sold" data-original-title="Start Price Raise After X Sold"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="num_of_sells_for_price_raise_start_span">{{ isset($ordersMonitorValue->start_price_raise_after_x_sold)?$ordersMonitorValue->start_price_raise_after_x_sold:'' }}</span>
													<input class="form-control form-group" type="number" min="0" id="num_of_sells_for_price_raise_start_input" style="display:none;" value="{{isset($ordersMonitorValue->start_price_raise_after_x_sold)?$ordersMonitorValue->start_price_raise_after_x_sold:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Raise Only For First Sell</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Raise only for first sell"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<div class="checkbox" id="checkbox_raise_price_for_first_sell_checkbox" style="display: none;">
														<label><input type="radio" name="raise_price_for_every_sell" id="raise_price_for_every_sell"></label>
													</div>
													@if(isset($ordersMonitorValue->raise_only_for_first_sell) && $ordersMonitorValue->raise_only_for_first_sell == 1)
														<span id="checkbox_raise_price_for_first_sell_span" class="fa fa-check-circle-o"></span>
													@else
														<span id="checkbox_raise_price_for_first_sell_span" class="fa fa-circle-o" ></span>
													@endif
													
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Raise For Each Sell</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Raise for each sell"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<div class="checkbox" id="checkbox_raise_price_for_every_sell_checkbox" style="display: none;">
														<label><input type="radio" id="raise_price_for_each_sell"></label>
													</div>
													@if(isset($ordersMonitorValue->raise_for_each_sale) && $ordersMonitorValue->raise_for_each_sale == 1)
														<span id="checkbox_raise_price_for_every_sell_span" class="fa fa-check-circle-o"></span>
													@else
														<span id="checkbox_raise_price_for_every_sell_span" class="fa fa-circle-o"></span>
													@endif
													
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">When Product Is Sold Raise Additional % By </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="For example: If item's additional percentage is 10% and you write here 15 - the new additional percentage will be 15%" data-original-title="The total price precentage that sold item price will be raised by."> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="change_price_orders_monitor_span">{{ isset($ordersMonitorValue->raise_additional_per)?$ordersMonitorValue->raise_additional_per:'' }}</span>
													<input class="form-control form-group" type="number" placeholder="" id="change_price_orders_monitor_input" style="display: none;" value="{{isset($ordersMonitorValue->raise_additional_per)?$ordersMonitorValue->raise_additional_per:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">When Product Is Sold Raise Additional $ By </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="For example: If item's additional percentage is 10$ and you write here 15 - the new additional percentage will be 15$" data-original-title="The total price precentage that sold item price will be raised by."> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="add_amount_orders_monitor_span">{{ isset($ordersMonitorValue->raise_additional_dollar)?$ordersMonitorValue->raise_additional_dollar:'' }}</span>
													<input class="form-control form-group" type="number" id="add_amount_orders_monitor_input" style="display: none;" value="{{isset($ordersMonitorValue->raise_additional_dollar)?$ordersMonitorValue->raise_additional_dollar:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Maximum Percentage Raise Limit </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Maximum Percentage Raise Limit" data-original-title="Maximum Percentage Raise Limit"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="max_price_raise_percentage_span">{{ isset($ordersMonitorValue->max_per_raise_limit)?$ordersMonitorValue->max_per_raise_limit:'' }}</span>
													<input class="form-control form-group" type="number" step="0.1" id="max_price_raise_percentage_input" style="display: none;"
													value="{{ isset($ordersMonitorValue->max_per_raise_limit)?$ordersMonitorValue->max_per_raise_limit:'' }}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Maximum Price Raise Limit </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Maximum Price Raise Limit" data-original-title="Maximum Price Raise Limit"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="max_price_raise_ammount_span">{{ isset($ordersMonitorValue->max_price_raise_limit)?$ordersMonitorValue->max_price_raise_limit:'' }}</span>
													<input class="form-control" type="number" step="0.1" id="max_price_raise_ammount_input" style="display:none;"
													value="{{ isset($ordersMonitorValue->max_price_raise_limit)?$ordersMonitorValue->max_price_raise_limit:'' }}">
												</div>
											</div>
											<hr>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Drop Price If Product Not Sold</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Drop Price If Product Not Sold"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<div class="checkbox" id="checkbox_not_sold_drop_price_checkbox" style="display: none;">
														<label><input type="checkbox" value="1" checked="checked" id="drop_price_not_sale"></label>
													</div>
													@if(isset($ordersMonitorValue->drop_price_product_not_sold) && $ordersMonitorValue->drop_price_product_not_sold = "true")
														<span class="fa fa-check-square-o" id="checkbox_not_sold_drop_price_span"></span>
													@else
														<span class="fa fa-square-o" id="checkbox_not_sold_drop_price_span"></span>
													@endif
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Drop Price After X Days</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Drop Price After X Days"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="days_num_for_price_drop_span">{{ isset($ordersMonitorValue->drop_price_afterX_days)?$ordersMonitorValue->drop_price_afterX_days:'' }}</span>
													<input class="form-control form-group" type="number" step="0.1" id="days_num_for_price_drop_input" style="display:none;" value="{{ isset($ordersMonitorValue->drop_price_afterX_days)?$ordersMonitorValue->drop_price_afterX_days:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Drop Price Percentage </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Drop Price Percentage"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="drop_price_percentage_span">{{ isset($ordersMonitorValue->drop_price_per)?$ordersMonitorValue->drop_price_per:'' }}</span>
													<input class="form-control form-group" type="number" step="0.1" placeholder="" id="drop_price_percentage_input" style="display:none;" value="{{ isset($ordersMonitorValue->drop_price_per)?$ordersMonitorValue->drop_price_per:'' }}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Drop Price Amount </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Drop Price Amount"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="drop_price_ammount_span">{{ isset($ordersMonitorValue->drop_price_amount)?$ordersMonitorValue->drop_price_amount:'' }}</span>
													<input class="form-control form-group" type="number" step="0.1" id="drop_price_ammount_input" style="display:none;"
													value="{{isset($ordersMonitorValue->drop_price_amount)?$ordersMonitorValue->drop_price_amount:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Minimum Drop Price Percentage </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Minimum Drop Price Percentage"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="min_drop_price_percentage_span">{{ isset($ordersMonitorValue->min_drop_price_per)?$ordersMonitorValue->min_drop_price_per:''}}</span>
													<input class="form-control form-group" type="number" step="0.1" id="min_drop_price_percentage_input" style="display:none;"
													value="{{isset($ordersMonitorValue->min_drop_price_per)?$ordersMonitorValue->min_drop_price_per:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-8">
													<span class="MonitorsContentKey">Minimum Drop Price Amount </span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Minimum Drop Price Amount"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-4">
													<span class="MonitorsContentValue" id="min_drop_price_ammount_span">{{ isset($ordersMonitorValue->min_drop_price_amount)?$ordersMonitorValue->min_drop_price_amount:''}}</span>
													<input class="form-control" type="number" step="0.1" id="min_drop_price_ammount_input" style="display:none;"
													value="{{ isset($ordersMonitorValue->min_drop_price_amount)?$ordersMonitorValue->min_drop_price_amount:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="card-footer">
									<a href="javascript:;" onclick="ordersMonitor()"><i class="fa fa-edit" color:blue"></i></a>
									&nbsp;&nbsp;&nbsp;&nbsp;
									<a href="javascrip:;" id="save_orders_monitor" class="btn btn-sm btn-success" onclick="saveOrderMonitor()" style="display: none;">Save</a>
									</div>
								</div>
								<div class="col-md-2"></div>
							</div>
                        </div>
                        <div class="tab-pane" id="profitable_monitor" role="tabpanel">                  
							@if(isset($profitMonitorValue->is_start) && $profitMonitorValue->is_start == 1 )
								<p class="bg-success text-center" id="use_profitable_items_monitor">Profitabiliy Monitor is working</p>
								<a class="btn btn-danger pull-right RbtnMargin" id="product_start_monitor" onclick="startStopMonitor('profitable_monitor','false')">Stop Monitor</a>
							@else
								<p class="bg-danger text-center" id="use_profitable_items_monitor">Profitability Monitor is not working</p>
								<a class="btn btn-success pull-right RbtnMargin" onclick="startStopMonitor('profitable_monitor','true')" id="product_start_monitor">Start Monitor</a>
							@endif
							<a href="{{ route('applyProfits') }}" class="btn btn-success pull-right RbtnMargin">Apply Profits</a>
							
							<h4> Profitability Monitor <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="'Profitability' Monitor target is to monitor your listings and end the not profitable ones." data-original-title="Profitability Monitor"><i class="fa fa-question-circle-o"></i> </a> </h4>
							<span class="LastRun">Last Run: </span> <span class="HoursAgo" id="last_run_profitable_span">767 Minutes ago</span>
							<hr>
							<div class="row">
								<div class="col-md-6 col-md-offset-3">
									<div class="card card-accent-theme" data-collapsed="0">
										<div class="panel-heading">
											<div class="card-header" data-toggle="collapse" data-parent="#accordion4" href="#collapse4">
												<h6>General Settings</h6>
											</div>
											<div class="panel-options">
												<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
											</div>
										</div>
										<div class="card-body" id="collapse4">
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">When Listing Is Not Profitable:</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="What will happen when a listing is not profitable anymore" data-original-title="Listing Not Profitable"> <i class="fa fa-question-circle-o"></i></a></span><br>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="product_not_profitable_span">{{ isset($profitMonitorValue->listing_not_profitable)?$profitMonitorValue->listing_not_profitable:'' }}</span>
													<div class="btn-group" id="product_not_profitable_input" style="display: none;">
														<select class="selectpicker form-group form-control" id="profit_listing_not_available" name="profit_listing_not_available">
															<option value="End Listing" selected="selected">End Listing</option>
															<option value="Alert Me">Alert Me</option>
														</select>
													</div><br>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Listing Left Days</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The monitor will check a listing when the listing have X days left. Set how many days left for the listing." data-original-title="Listing Left Days"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="left_day_profitable_monitor_span">{{ isset($profitMonitorValue->listing_left_days)?$profitMonitorValue->listing_left_days:'' }}</span>
													<input class="form-control form-group" type="number" min="0" id="left_day_profitable_monitor_input" style="display:none;
													 " value="{{ isset($profitMonitorValue->listing_left_days)?$profitMonitorValue->listing_left_days:'' }}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Listing Minimum Sold Quantity</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The monitor will count the listing as not profitable if it has less than X sold quantity. Set the minimum sold quantity." data-original-title="Minimum Sold"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="minimum_sold_profitable_monitor_span">{{ isset($profitMonitorValue->listing_min_sold_quantity)?$profitMonitorValue->listing_min_sold_quantity:'' }}</span>
													<input class="form-control form-group" type="number" min="0" id="minimum_sold_profitable_monitor_input" style="display:none;" value="{{ isset($profitMonitorValue->listing_min_sold_quantity)?$profitMonitorValue->listing_min_sold_quantity:'' }}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Listing Minimum Watchers</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The monitor will count the listing as not profitable if it has less than X watchers. Set the minimum watchers quantity." data-original-title="Minimum Watchers"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="minimum_watchers_profitable_monitor_span">{{ isset($profitMonitorValue->listing_min_watchers)?$profitMonitorValue->listing_min_watchers:''}}</span>
													<input class="form-control form-group" type="number" min="0"  id="minimum_watchers_profitable_monitor_input" style="display: none;" value="{{isset($profitMonitorValue->listing_min_watchers)?$profitMonitorValue->listing_min_watchers:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Listing Minimum Views</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The monitor will count the listing as not profitable if it has less than X views. Set the minimum views quantity." data-original-title="Minimum Views"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="minimum_views_profitable_monitor_span">{{ isset($profitMonitorValue->listing_min_views)?$profitMonitorValue->listing_min_views:'' }}</span>
													<input class="form-control" type="number" min="0" id="minimum_views_profitable_monitor_input" style="display:none;" value="{{ isset($profitMonitorValue->listing_min_views)?$profitMonitorValue->listing_min_views:'' }}">
												</div>
											</div>
										</div>
									</div>
									<div class="card-footer">
										<a href="javascript:;" onclick="profitMonitor()" id="edit_profitable_monitor_input"><i class="fa fa-edit" color:blue"></i></a>
										&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascrip:;" id ="save_profitable_monitor_input" onclick="saveProfitMonitors()" class="btn btn-sm btn-success" style="display: none;">Save</a>
									</div>
								</div>
							</div>
                        </div>
                        <div class="tab-pane" id="orders_processor" role="tabpanel">
							@if(isset($orderProcessingValue->is_start) && $orderProcessingValue->is_start == 1 )
								<p class="bg-success text-center" id="use_order_processor_monitor">Orders Processor Monitor is working</p>
								<a class="btn btn-danger pull-right RbtnMargin" id="product_start_monitor" onclick="startStopMonitor('order_processing','false')">Stop Monitor</a>
							@else
								<p class="bg-danger text-center" id="use_order_processor_monitor">Orders Processor Monitor is not working</p>
								<a class="btn btn-success pull-right RbtnMargin" onclick="startStopMonitor('order_processing','true')" id="product_start_monitor">Start Monitor</a>
							@endif
							<h4> Orders Processor<a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="This monitor target is to process all your new orders. Purchase the items from Amazon with your amazon users + update tracking numbers when available + send the buyers messages." data-original-title="Orders Processor"> <i class="fa fa-question-circle-o"></i></a> </h4>
							<span class="LastRun">Last Run: </span> <span class="HoursAgo" id="last_run_orders_processor_span">Still not ran</span>
							<hr>
							<div class="row">
								<div class="col-md-6">
									<div class="card card-accent-theme" data-collapsed="0">
										<div class="panel-heading" data-toggle="collapse" data-parent="#accordion5" href="#collapse5">
											<div class="card-header">
												<span><h6>Purchase Settings</h6><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="top" title="" data-content="Those are the settings of your purchase accounts(Amazon accounts that you would like the monitor to purchase the orders with)." data-original-title="Purchase Settings"><i class="fa fa-question-circle-o"></i> </a></span>
											</div>
											<div class="panel-options">
												<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
											</div>
										</div>
										<div class="card-body" id="collapse5">
											<b>Amazon Accounts</b>
											<div class="row amazon_account_container" >
												<div class="col-md-6">
													<span class="MonitorsContentKey">Purchase Account</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Add Amazon accounts to make the monitor purchase the orders for you with. MAKE SURE YOU HAVE MONEY IN YOUR GIFT BALANCE!" data-original-title="Amazon Accounts"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="id_buying_account_span">Select account</span>
													<div class="btn-group" id="buying_account_input" style="display: none;">
														<select class="selectpicker form-control form-group" id="purchase_account_id">
															<option value="Alert Me" selected="selected">Select account</option>
															<option value="temp6137@gmail.com" title="temp6137@gmail.com">temp6137@gmail.com</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row amazon_account_container">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Zip Code</span>
														<span class="MonitorsHelp">
															<a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Enter your main address zip code(Sometimes Amazon asks for registered zip code to make sure you are the owner of the account.)" data-original-title="Zip Code"> 
																<i class="fa fa-question-circle-o"></i>
															</a>
														</span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="zip_code_span">{{isset($amazonMonitorValue->zip_code)?$amazonMonitorValue->zip_code:''}}</span>
													<input class="form-control form-group" type="number" id="zip_code_input" style="display:none;" value="{{isset($amazonMonitorValue->zip_code)?$amazonMonitorValue->zip_code:''}}">
												</div>
											</div>
											<div class="row amazon_account_container">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Country</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Enter your main country" data-original-title="Country"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="country_span">{{ isset($amazonMonitorValue->country)?$amazonMonitorValue->country:'' }}</span>
													<div class="btn-group" id="amazon_country_input" style="display:none;">
														<select class="selectpicker form-control form-group" id="amazon_account_country">
															<option value="US" title="United States">Amazon.com</option>
															<option value="UK" title="United Kingdom">Amazon.co.uk</option>
															<option value="DE" title="Germany">Amazon.de</option>
														</select>
													</div>
												</div>
											</div>
											<div class="row amazon_account_container">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Phone Number</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Enter your Amazon registered phone number(Sometimes Amazon asks for registered phone number to make sure you are the owner of the account.)" data-original-title="Phone number"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="phone_number_span">{{ isset($amazonMonitorValue->phone_number)?$amazonMonitorValue->phone_number:''}}</span>
														<input class="form-control form-group" type="number" id="phone_number_input" style="display: none;">
												</div>
											</div>
											<div class="row amazon_account_container">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Full Name
                                                        <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" >
															<i class="fa fa-question-circle-o"></i>
														</a>
													</span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="amazon_full_name_span">{{isset($amazonMonitorValue->Full_name)?$amazonMonitorValue->Full_name:''}}</span>
													<input class="form-control form-group" type="text" placeholder="Full Name" id="amazon_full_name_input" style="display:none;" value="{{isset($amazonMonitorValue->Full_name)?$amazonMonitorValue->Full_name:''}}">
												</div>
											</div>
											<div class="row amazon_account_container">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Prime Account</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Does your Amazon account is Prime Account? This field is needed to order with prime shipping." data-original-title="Prime Account"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<div class="checkbox form-control form-group" id="prime_account_input" style="display: none;">
														<label><input type="checkbox" value="1" checked="checked" id="amazon_prime"></label>
													</div>
													@if(isset($amazonMonitorValue->prime_account) && $amazonMonitorValue->prime_account == "true")
														<span class="fa fa-check-square-o" id="prime_account_span"></span>
													@else
														<span class="fa fa-square-o" id="prime_account_span"></span>
													@endif
												</div>
											</div>
											<div class="card-footer">
												<a href="javascript:;" onclick="ordersProcessor()"><i class="fa fa-edit" color:blue"></i></a>
												&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascrip:;" onclick="saveAmazonAccount()" class="btn btn-sm btn-success" id="save_amazon_account_input" style="display: none;">Save</a>
											</div>
											<hr>
											<div class="panel-body" id="collapse5">
												<h6><b>Used cards</b></h6>
												<div class="row amazon_account_container" >
													<div class="col-md-3">
														<button type="button" id="start_buying_account_modal" class="btn btn-primary ">
														Add Card</button>
													</div>
												</div>
												<hr><br>
												<div class="row">
													<div class="table-responsive">
						                                <table class="table">
						                                    <thead>
						                                        <tr>
						                                            <th>Last14Digits</th>
						                                            <th>Mon/Year</th>
						                                            <th>Action</th>
						                                        </tr>
						                                    </thead>
						                                    <tbody></tbody>
						                                </table>
						                            </div>
												</div>
											</div>
											<div class="form-group amazon_account_container">
												<div class="col-md-12">
													<div class="pull-right">
														<a class="btn btn-link"><b>Remove This Account</b></a>
													</div>
												</div>
											</div>
											<div id="banggood_accounts_list" style="display: none;">
												<h4><b>Banggood Accounts</b></h4>
												<ul id="banggood_accounts_list_ul"></ul>
											</div>
											<div class="col-xs-12">
												<button type="button" id="start_buying_account_modal" class="btn btn-primary btn-icon icon-left btn-block">Add Purchase Account
												<i class="entypo-user-add"></i></button>
											</div>
										</div>
										<div class="panel panel-primary" data-collapsed="0">
											<div class="panel-body" id="collapse6">
												<div class="row" id="email_account_container">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Email Account (Gmail Only)</span>
													</div>
													<div class="col-md-6">
														<div class="btn-group">
															<select id="email_account" class="form-control">
																<option>Select account</option>
																<option value="temp6137@gmail.com" title="temp6137@gmail.com">temp6137@gmail.com</option>
															</select>
														</div>
													</div>
												</div>
												<div class="form-group" id="remove_this_account_container">
													<div class="col-md-12">
														<div class="pull-right">
															<a class="btn btn-link"><b>Remove This Account</b></a>
														</div>
													</div>
												</div>
												<div class="col-xs-12">
													<a href="javascript:;" id="add_email_account" class="btn btn-primary btn-icon icon-left btn-block">
														<span>Add Gmail Account</span>
														<i class="entypo-user-add"></i>
													</a>
												</div>
											</div>
										</div>
									</div>
								</div>
								<div class="col-md-6">
									<div class="card card-accent-theme" data-collapsed="0">
										<div class="panel-heading" data-toggle="collapse" data-parent="#accordion7" href="#collapse7">
											<div class="card-header">
												<span><h6>Orders Processing</h6> <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Those settings will apply on active orders while processing them." data-original-title="Orders Processing"> <i class="fa fa-question-circle-o"></i> </a></span>
											</div>
											<div class="panel-options">
												<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
											</div>
										</div>
										<div class="card-body" id="collapse7">
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Auto Order</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If active, the monitor will purchase automatically all your new orders with your buying account." data-original-title="Auto Order"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<div class="checkbox" id="auto_order_input" style="display: none;">
														<label><input type="checkbox" checked="checked" id="auto_order_value"></label>
													</div>
													@if(isset($orderProcessingValue->auto_order) && $orderProcessingValue->auto_order == "true")
														<span class="fa fa-check-square-o" id="auto_order_span"></span>
													@else
														<span class="fa fa-square-o" id="auto_order_span"></span>
													@endif
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Bundle AddOn</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If active, the monitor will purchase only prime products" data-original-title="Order Only Prime"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<div class="checkbox" id="bundle_addon_input" style="display: none;">
														<label><input type="checkbox" checked="checked" id="bundle_addon_value"></label>
													</div>
													@if(isset($orderProcessingValue->bundle_add_on) && $orderProcessingValue->bundle_add_on == "true")
														<span id="bundle_addon_span" class="fa fa-check-square-o"></span>
													@else
														<span id="bundle_addon_span" class="fa fa-square-o"></span>
													@endif
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Order Only Prime</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If active, the monitor will purchase only prime products" data-original-title="Order Only Prime"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<div class="checkbox" id="order_only_prime_input" style="display: none;">
														<label><input type="checkbox"  checked="checked" id="order_prime_value"></label>
													</div>
													@if(isset($orderProcessingValue->oder_only_prime) &&  $orderProcessingValue->oder_only_prime == "true")
														<span id="order_only_prime_span" class="fa fa-check-square-o"></span>
													@else
														<span id="order_only_prime_span" class="fa fa-square-o"></span>
													@endif
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Hipshipper</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If active, Enable International Order Fulfillment for eBay Sellers By hipshipper.com" data-original-title="Hipshipper Shipping"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<div class="checkbox" id="hipshipper_checkbox_input" style="display:none;">
														<label><input type="checkbox" checked="checked" id="hipshipper_value"></label>
													</div>
													@if(isset($orderProcessingValue->hipshipper) &&  $orderProcessingValue->hipshipper == "true")
														<span id="hipshipper_checkbox_span" class="fa fa-check-square-o"></span>
													@else
														<span id="hipshipper_checkbox_span" class="fa fa-square-o"></span>
													@endif
													<span id="hipshipper_checkbox_span">{{ isset($orderProcessingValue->hipshipper)?$orderProcessingValue->hipshipper:'' }}</span>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Maximum Product Price For Auto Order</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Set the maximum product price that you allow the monitor to purchase." data-original-title="Maximum Product Price For Auto Order"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="max_price_auto_order_span">{{ isset($orderProcessingValue->max_product_price_auto_order)?$orderProcessingValue->max_product_price_auto_order:'' }}</span>
													<input class="form-control form-group" type="number" id="max_price_auto_order_input" style="display: none;"
													value="{{isset($orderProcessingValue->max_product_price_auto_order)?$orderProcessingValue->max_product_price_auto_order:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Max Lose Amount</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If the estimate profit from an order is a lose, set the maximum lose amount you accept. If the estimate lose is greater than what you have set, the monitor won't purchase the product." data-original-title="Max Lose Amount"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="lose_max_amount_span">{{ isset($orderProcessingValue->max_lose_amount)?$orderProcessingValue->max_lose_amount:''}}</span>
													<input class="form-control form-group" type="number" id="lose_max_amount_input" style="display:none;"
													value="{{isset($orderProcessingValue->max_lose_amount)?$orderProcessingValue->max_lose_amount:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Set Order As Shipped</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Choose when the program will set order shipped status on Ebay. Right after the purchase of the product from Amazon or after tracking details are updated." data-original-title="Set Order As Shipped"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="order_shipped_span">{{ isset($orderProcessingValue->set_order_shipped)?$orderProcessingValue->set_order_shipped:''}}</span>
													<select id="order_shipped_input" class="selectpicker form-control form-group" style="display: none;">
														<option value="After Purchase" selected="selected">After Purchase</option>
														<option value="After Tracking Updated">After Tracking Updated</option>
													</select>
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Gift Message</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="When ordering from amazon, sometimes you can attach a gift message to the buyer. Choose your message." data-original-title="Gift Message"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<span class="MonitorsContentValue" id="gift_message_span">{{ isset($orderProcessingValue->gift_message)?$orderProcessingValue->gift_message:'' }}</span>
													<input type="text" id="gift_message_input" class="form-control form-group" data-validate="required" style="display:none;" value="{{isset($orderProcessingValue->gift_message)?$orderProcessingValue->gift_message:''}}">
												</div>
											</div>
											<div class="row">
												<div class="col-md-6">
													<span class="MonitorsContentKey">Set Ontrack For Amazon Shipping Carrier</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Set Ontrack For Amazon Shipping Carrier" data-original-title="Set Ontrack For Amazon Shipping Carrier"> <i class="fa fa-question-circle-o"></i></a></span>
												</div>
												<div class="col-md-6">
													<div class="checkbox form-control form-group" id="amazon_shipping_ontrack_input" style="display: none;">
														<label><input type="checkbox" checked="checked" id="set_ontracking_value"></label>
													</div>
													@if(isset($orderProcessingValue->set_ontrack_shipping_carrier) && $orderProcessingValue->set_ontrack_shipping_carrier == "true")
														<span class="fa fa-check-square-o" id="amazon_shipping_ontrack_span"></span>
													@else
														<span class="fa fa-square-o" id="amazon_shipping_ontrack_span"></span>
													@endif
												</div>
											</div>
											<div class="card-footer">
												<a href="javascript:;"><i class="fa fa-edit" onclick="ordersProcessing()" color:blue"></i></a>
												&nbsp;&nbsp;&nbsp;&nbsp;<a href="javascrip:;" class="btn btn-sm btn-success" onclick="saveOrderProcessing()" id="save_orders_processing" style="display:none;">Save</a>
											</div>
											
											<div class="col-xs-12">
												<a href="#" class="btn btn-primary btn-icon icon-left btn-block" data-toggle="modal" data-target="#buyers_message">Messages To Buyers<i class="fa fa-envelope-o"></i></a>
											</div>
											<div class="col-xs-12">
												<a href="#" class="btn btn-primary btn-icon icon-left btn-block" data-toggle="modal" data-target="#notification_modal">Notifications<i class="fa fa-envelope-o"></i></a>
											</div>
											<div class="col-xs-12" style="display: none;">
												<button type="button" id="gift_cards" class="btn btn-primary btn-icon icon-left btn-block" onclick="get_gift_cards_list();" data-toggle="modal" data-target="#gift_cards_modal">
													Gift Cards List</button>
											</div>
										</div>
									</div>
									<div class="panel panel-primary" data-collapsed="0">
										<div class="panel-heading" data-toggle="collapse" data-parent="#accordion8" href="#collapse8">
											<div class="panel-title">
												<span><b>Cashback Users</b><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Those settings select cashback site and user." data-original-title="Cashback Users"> <i class="fa fa-question-circle-o"></i> </a></span>
											</div>
											<div class="panel-options">
												<a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
											</div>
										</div>
										<div class="panel-body" id="collapse8">
											<div class="col-xs-12">
												<a href="#" class="btn btn-primary btn-icon icon-left btn-block" data-toggle="modal" data-target="#cashback_modal">Add Cashback User<i class="fa fa-envelope-o"></i>
												</a>
											</div>
										</div>
									</div>
								</div>                                                                              
                        	</div>
                   		</div>
                   	</div>
				</div>
			</div>
		</div>
	</div>
</div>
@include('client.monitor.modals')
@endsection
@section('footer_script')
<script src="{{ URL::asset('public/admin-assets/js/common.js') }}"></script>
<script>
	function productsMonitor()
	{
		$('#defult_break_event_input').toggle();
		$('#break_even_input').toggle();
		$('#additional_amount_input').toggle();
		$('#additional_percentage_input').toggle();
		$('#end_untracked_listing_input').toggle();
		$('#not_available_listing_input').toggle();
		$('#product_available_oos_monitor_input').toggle();
		$('#raise_quantity_oos_monitor_input').toggle();
		$('#checkbox_dynamic_policy_creation_checkbox').toggle();
		$('#min_stock_quantity_input').toggle();
		$('#min_profit_per_product_input').toggle();
		$('#product_vero_input').toggle();
		$('#blocked_product_input').toggle();
		$('#maximum_ship_within_days_input').toggle();
		$('#checkbox_sku_auto_add_checkbox').toggle();
		$('#save_products_monitor').toggle();

		$('#default_break_event_span').toggle();
		$('#break_even_span').toggle();
		$('#additional_amount_span').toggle();	
		$('#additional_percentage_span').toggle();
		$('#end_untracked_listing_span').toggle();	
		$('#not_available_listing_span').toggle();
		$('#product_available_oos_monitor_span').toggle();
		$('#raise_quantity_oos_monitor_span').toggle();
		$('#dynamic_policy_span').toggle(); 
		$('#min_stock_quantity_span').toggle();
		$('#min_profit_per_product_span').toggle();
		$('#product_vero_span').toggle();
		$('#blocked_product_span').toggle();
		$('#maximum_ship_within_days_span').toggle();
		$('#checkbox_sku_auto_add_span').toggle();
  	}
  	function saveMonitor()
  	{
  		/*Checkbox for Automatic SKU filling*/
  		if($('#checkbox_auto_add').is(":checked")){
                var checked = "true";
        }
        else{
                var checked = "false";
        }
        /*Checkbox for End untracked listing*/
        if($('#non_tracable_checkbox_special').is(":checked")){
        	var tracable ="true";
        }
        else{
        	var tracable ="false";
        }
        /*Checkbox for Dynamic policy creation*/
        if($('#dynamic_policy_creation').is(":checked")){
        	var dynamic = "true";
        }
        else{
        	var dynamic = "false";
        }
  		var default_break_event = $('#break_even_input').val();
  		var additional_dollar = $('#additional_amount_input').val();
  		var additional_per = $('#additional_percentage_input').val();
  		var end_untracked_listing = tracable;
  		var not_available_listing = $('#available_listing_not_input').val();
  		var when_product_available = $('#available_product_input').val();
  		var quantity_to_raise_to = $('#raise_quantity_oos_monitor_input').val();
  		var dynamic_policy_creation = dynamic;
  		var min_allowed_quantity_in_stock = $('#min_stock_quantity_input').val();
  		var min_profit_dollar_per_product = $('#min_profit_per_product_input').val();
  		var when_product_vero = $('#product_vero_value').val();
  		var when_product_blocked = $('#block_product_value').val();
  		var max_shipping_time = $('#max_shipping_value').val();
  		var automatic_sku_filling = checked;

  		var token = $('meta[name="csrf-token"]').attr('content');
  		$.post('{{url("monitor/product_monitor")}}',
  		{
  		 	default_break_event : default_break_event,
  		 	additional_dollar :additional_dollar,
  		 	additional_per : additional_per,
  		 	end_untracked_listing : end_untracked_listing,
  		 	not_available_listing : not_available_listing,
  		 	when_product_available : when_product_available,
  		 	quantity_to_raise_to : quantity_to_raise_to,
  		 	dynamic_policy_creation : dynamic_policy_creation,
  		 	min_allowed_quantity_in_stock : min_allowed_quantity_in_stock,
  		 	min_profit_dollar_per_product : min_profit_dollar_per_product,
  		 	when_product_vero : when_product_vero,
  		 	when_product_blocked : when_product_blocked,
  		 	max_shipping_time : max_shipping_time,
  		 	automatic_sku_filling :automatic_sku_filling,
  		 	_token : token
  		},
  		function(response){
  		 	alertAjax(response);
  		});
  	}
  	function ordersMonitor()
	{
		$('#product_sold_orders_monitor_input').toggle();
		$('#raise_quantity_orders_monitor_input').toggle();
		$('#checkbox_when_product_sold_checkbox').toggle();
		$('#num_of_sells_for_price_raise_start_input').toggle();
		$('#checkbox_raise_price_for_first_sell_checkbox').toggle();
		$('#checkbox_raise_price_for_every_sell_checkbox').toggle();
		$('#change_price_orders_monitor_input').toggle();
		$('#add_amount_orders_monitor_input').toggle();
		$('#max_price_raise_percentage_input').toggle();
		$('#max_price_raise_ammount_input').toggle();
		$('#checkbox_not_sold_drop_price_checkbox').toggle();
		$('#days_num_for_price_drop_input').toggle();
		$('#drop_price_percentage_input').toggle();
		$('#drop_price_ammount_input').toggle();
		$('#min_drop_price_percentage_input').toggle();
		$('#min_drop_price_ammount_input').toggle();
		$('#save_orders_monitor').toggle();

		$('#product_sold_orders_monitor_span').toggle();
		$('#raise_quantity_orders_monitor_span').toggle();
		$('checkbox_when_product_sold_span').toggle();
		$('#num_of_sells_for_price_raise_start_span').toggle();
		$('#checkbox_raise_price_for_first_sell_span').toggle();
		$('#checkbox_raise_price_for_every_sell_span').toggle();
		$('#change_price_orders_monitor_span').toggle();
		$('#add_amount_orders_monitor_span').toggle();
		$('#max_price_raise_percentage_span').toggle();
		$('#max_price_raise_ammount_span').toggle();
		$('#checkbox_not_sold_drop_price_span').toggle();
		$('#days_num_for_price_drop_span').toggle();
		$('#drop_price_percentage_span').toggle();
		$('#drop_price_ammount_span').toggle();
		$('#min_drop_price_percentage_span').toggle();
		$('#min_drop_price_ammount_span').toggle();

	} 
	function saveOrderMonitor()
  	{
  		if($('#product_sold_change_price').is(":checked")){
                var product_sold  = "true";
        }
        else{
                var product_sold  = "false";
        }
       	if($('#drop_price_not_sale').is(":checked")){
                var drop_price = "true";
        }
        else{
                var drop_price  = "false";
        }
        /*Radio button checked for Raise only for first sell*/
        if($('#raise_price_for_every_sell').is(":checked")){
            var raise_first_sell  = 1;
        }
        else{
            var raise_first_sell  = 0;
        }
        /*Radio button checked for Raise for each sell*/
        if($('#raise_price_for_each_sell').is(":checked")){
            var raise_each_sell  = 1;
        }
        else{
            var raise_each_sell  = 0;
        }
  		var when_product_sold = $('#product_sold_value').val();
  		var quantity_to_raise_to = $('#raise_quantity_orders_monitor_input').val();
  		var when_product_sold_change_price = product_sold;
  		var start_price_raise_after_x_sold = $('#num_of_sells_for_price_raise_start_input').val();
  		var raise_only_for_first_sell = raise_first_sell;
  		var raise_for_each_sale = raise_each_sell;
  		var raise_additional_per = $('#change_price_orders_monitor_input').val();
  		var raise_additional_dollar = $('#add_amount_orders_monitor_input').val();
  		var max_per_raise_limit = $('#max_price_raise_percentage_input').val();
  		var max_price_raise_limit = $('#max_price_raise_ammount_input').val();
  		var drop_price_product_not_sold = drop_price;
  		var drop_price_afterX_days = $('#days_num_for_price_drop_input').val();
  		var drop_price_per = $('#drop_price_percentage_input').val();
  		var drop_price_amount = $('#drop_price_ammount_input').val();
  		var min_drop_price_per = $('#min_drop_price_percentage_input').val();
  		var min_drop_price_amount = $('#min_drop_price_ammount_input').val();

  		var token = $('meta[name="csrf-token"]').attr('content');
  		$.post('{{url("monitor/orders_monitor")}}' ,
  		{
  			when_product_sold : when_product_sold,
  			quantity_to_raise_to : quantity_to_raise_to,
  			when_product_sold_change_price : when_product_sold_change_price,
  			start_price_raise_after_x_sold : start_price_raise_after_x_sold,
  			raise_only_for_first_sell : raise_only_for_first_sell,
  			raise_for_each_sale : raise_for_each_sale,
  			raise_additional_per : raise_additional_per,
  			raise_additional_dollar : raise_additional_dollar,
  			max_per_raise_limit : max_per_raise_limit,
  			max_price_raise_limit : max_price_raise_limit,
  			drop_price_product_not_sold : drop_price_product_not_sold,
  			drop_price_afterX_days : drop_price_afterX_days,
  			drop_price_per : drop_price_per,
  			drop_price_amount : drop_price_amount,
  			min_drop_price_per : min_drop_price_per,
  			min_drop_price_amount : min_drop_price_amount,
  			_token : token
  		},
  		function(response){
  			alertAjax(response);
  		});
  	}                            
	function profitMonitor()
	{
		$('#product_not_profitable_input').toggle();
		$('#left_day_profitable_monitor_input').toggle();
		$('#minimum_sold_profitable_monitor_input').toggle();
		$('#minimum_watchers_profitable_monitor_input').toggle();
		$('#minimum_views_profitable_monitor_input').toggle();
		$('#save_profitable_monitor_input').toggle();

		$('#product_not_profitable_span').toggle();
		$('#left_day_profitable_monitor_span').toggle();
		$('#minimum_sold_profitable_monitor_span').toggle();
		$('#minimum_watchers_profitable_monitor_span').toggle();
		$('#minimum_views_profitable_monitor_span').toggle();
	}
	function saveProfitMonitors()
	{
		var listing_not_profitable = $('#profit_listing_not_available').val();
		var listing_left_days = $('#left_day_profitable_monitor_input').val();
		var listing_min_sold_quantity = $('#minimum_sold_profitable_monitor_input').val();
		var listing_min_watchers = $('#minimum_watchers_profitable_monitor_input').val();
		var listing_min_views = $('#minimum_views_profitable_monitor_input').val();

		var token = $('meta[name="csrf-token"]').attr('content');
		$.post('{{url("monitor/profit_monitor")}}' ,
		{
			listing_not_profitable : listing_not_profitable,
			listing_left_days : listing_left_days,
			listing_min_sold_quantity : listing_min_sold_quantity,
			listing_min_watchers : listing_min_watchers,
			listing_min_views : listing_min_views,
			_token : token
		},
		function(response){
			alertAjax(response);
		});
	}
	function ordersProcessor()
	{
		$('#buying_account_input').toggle();
		$('#zip_code_input').toggle();
		$('#amazon_country_input').toggle();
		$('#phone_number_input').toggle();
		$('#amazon_full_name_input').toggle();
		$('#prime_account_input').toggle();
		$('#save_amazon_account_input').toggle();

		$('#id_buying_account_span').toggle();
		$('#zip_code_span').toggle();
		$('#country_span').toggle();
		$('#phone_number_span').toggle();
		$('amazon_full_name_span').toggle();
		$('#prime_account_span').toggle();
	}
	function saveAmazonAccount()
	{
		if($('#amazon_prime').is(":checked")){
                var prime  = "true";
        }
        else{
                var prime  = "false";
        }

		var purchase_account = $('#purchase_account_id').val();
		var zip_code = $('#zip_code_input').val();
		var country = $('#amazon_account_country').val();
		var phone_number = $('#phone_number_input').val();
		var Full_name = $('#amazon_full_name_input').val();
		var prime_account = prime;

		var token = $('meta[name="csrf-token"]').attr('content');
		$.post('{{url("monitor/amazon_account")}}' ,
		{
			purchase_account : purchase_account,
			zip_code : zip_code,
			country : country,
			phone_number : phone_number,
			Full_name : Full_name,
			prime_account : prime_account,
			_token : token
		},
		function(response){
			alertAjax(response);
		});
	}
	function ordersProcessing()
	{
		$('#auto_order_input').toggle();
		$('#bundle_addon_input').toggle();
		$('#order_only_prime_input').toggle();
		$('#hipshipper_checkbox_input').toggle();
		$('#max_price_auto_order_input').toggle();
		$('#lose_max_amount_input').toggle();
		$('#order_shipped_input').toggle();
		$('gift_message_input').toggle();
		$('#amazon_shipping_ontrack_input').toggle();
		$('#save_orders_processing').toggle();

		$('#auto_order_span').toggle();
		$('#bundle_addon_span').toggle();
		$('#order_only_prime_span').toggle();
		$('#hipshipper_checkbox_span').toggle();
		$('#max_price_auto_order_span').toggle();
		$('#lose_max_amount_span').toggle();
		$('#order_shipped_span').toggle();
		$('#gift_message_input').toggle();
		$('#amazon_shipping_ontrack_span').toggle();
	}
	function saveOrderProcessing()
	{
		if($('#auto_order_value').is(":checked")){
        	var auto_order = "true";
        }
        else{
        	var auto_order = "false";
        }
        if($('#bundle_addon_value').is(":checked")){
        	var bundle_addOn = "true";
        }
        else{
        	var bundle_addOn = "false";
        }
        if($('#order_prime_value').is(":checked")){
        	var order_prime  = "true";
        }
        else{
        	var order_prime = "false";
        }
        if($('#hipshipper_value').is(":checked")){
        	var hipshipper_val  = "true";
        }
        else{
        	var hipshipper_val = "false";
        }
        if($('#set_ontracking_value').is(":checked")){
        	var setOnTrack = "true";
        }
        else{
        	var setOnTrack = "false";
        }
		var auto_order = auto_order;
		var bundle_add_on = bundle_addOn;
		var oder_only_prime = order_prime;
		var hipshipper = hipshipper_val;
		var max_product_price_auto_order = $('#max_price_auto_order_input').val();
		var max_lose_amount = $('#lose_max_amount_input').val();
		var set_order_shipped = $('#order_shipped_input').val();
		var gift_message = $('#gift_message_input').val();
		var set_ontrack_shipping_carrier = setOnTrack;

		var token = $('meta[name="csrf-token"]').attr('content');
		$.post('{{url("monitor/order_process")}}' ,
		{
			auto_order : auto_order,
			bundle_add_on : bundle_add_on,
			oder_only_prime : oder_only_prime,
			hipshipper : hipshipper,
			max_product_price_auto_order : max_product_price_auto_order,
			max_lose_amount : max_lose_amount,
			set_order_shipped : set_order_shipped,
			gift_message : gift_message,
			set_ontrack_shipping_carrier : set_ontrack_shipping_carrier,
			_token :token
		},
		function(response){
			alertAjax(response);
		});
	}
	function startStopMonitor(monitorName,isStart)
	{
		var token = $('meta[name="csrf-token"]').attr('content');
		$.post('{{url("monitor/start_stop")}}', 
		{
			monitorName : monitorName,
			_token : token,
			isStart : isStart
		},
		function(response){
            location.reload();
        });
	}
</script>
@endsection