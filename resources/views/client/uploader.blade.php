@extends('backend_layout.app')	

@section('content')
<!-- <link rel="stylesheet" href="admin-assets/libs/tables-datatables/dist/datatables.min.css"/> -->
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">Uploader</a>
    </li>
    <li class="breadcrumb-item active">Uploader Listing</li>
</ol>


<div class="container-fluid">

    <ul class="nav nav-tabs" id="myTab" role="tablist">
        <li class="nav-item">
            <a class="nav-link active" id="home-tab" data-toggle="tab" href="#regular_upload" role="tab" aria-controls="home" aria-selected="true">Regular Upload</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="profile-tab" data-toggle="tab" href="#bulk_upload" role="tab" aria-controls="profile" aria-selected="false">Bulk Upload</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#all_uploadss" role="tab" aria-controls="contact" aria-selected="false">All Uploads</a>
        </li>
        <li class="nav-item">
            <a class="nav-link" id="contact-tab" data-toggle="tab" href="#scheduled_uploads_calendar" role="tab" aria-controls="contact" aria-selected="false">Scheduled Uploads Calendar</a>
        </li>

    </ul>
    <div class="tab-content" id="myTabContent">
        <div class="tab-pane fade show active" id="regular_upload" role="tabpanel" aria-labelledby="home-tab">
          <form action="{{url('uploader/grabDetail')}}" method="post">
            @csrf
            <div class="UploadProductPanelTitle">Grab Product Details <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Enter a product ID to grab it's details." data-original-title="Product Details"><i class="fa fa-question-circle-o"></i>
                </a>
            </div>                
            <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <label for="field-1" class="control-label">Product ID</label>
                    <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The ID of the product you want to get its details" data-original-title="Product ID"><i class="fa fa-question-circle-o"></i>
                    </a>
                </div>
                <div class="col-md-6 col-sm-6 col-xs-6">
                    <input type="text" class="form-control" id="item_to_grab_id" name="item_to_grab_id" placeholder="Product ID or URL">
                </div>
            </div>
        </br>
        <div class="row">
            <div class="col-md-6">
                <div class="form-group">
                    <label for="buying_site" class="control-label">Source Product Site</label>
                    <select id="buying_site" class="selectpicker form-control"><option value="1">Amazon US</option><option value="4">Amazon UK</option><option value="5">Amazon DE</option><option value="14">Walmart</option><option value="15">BangGood</option><option value="16">Aliexpress</option></select>
                </div>
            </div>
            <div class="col-md-6">
                <div class="form-group">
                    <label for="selling_site" class="control-label">Upload Product Site</label>
                    <select id="selling_site" class="selectpicker form-control">
                        <option value="2">Ebay US</option>
                        <option value="3">Ebay UK</option>
                        <option value="9">Ebay DE</option>
                        <option value="10">Ebay FR</option>
                        <option value="11">Ebay CA</option>
                    </select>
                </div>
            </div>
        </div>
            <div class="col-md-12">
                <div class="form-group">
                    <label for="field-1" class="control-label">Template</label>
                    <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The template that will apply on the product's description" data-original-title="Template"><i class="fa fa-question-circle-o"></i>
                    </a>
                    <select id="templates" class="selectpicker form-control"><option value="4996" data_user_profile_id="9816">amz listings</option><option value="121">BestQuality Best price 14 days returns</option><option value="126">No template</option><option value="222">BestQuality Best price 30 days returns</option><option value="507">Simply Blue</option><option value="1440">Limited Time Sale - 30 days Returns</option><option value="1717">Halloween Sale- 30 Days Returns</option><option value="1718">Halloween Sale- 60 Days Returns</option><option value="1720">Limited Time Sale - 60 days Returns</option><option value="2007">Black Friday - 30 Days Returns</option><option value="2008">Black Friday - 60 Days Returns</option><option value="2009">Christmas - 30 Days Returns</option><option value="2010">Christmas - 60 Days Returns</option><option value="3818">Valentine's Day Sale White 30 Days </option><option value="3819">Valentine's Day Sale Red 30 Days</option><option value="4017">Valentine's Day Sale White 60 Days </option><option value="4018">Valentine's Day Sale Red 60 Days</option><option value="4023">China Non Stop 30 Days</option><option value="4025">China Great Wall 30 Days</option></select>
                    <a class="btn btn-default" href="#">Set As Default Template</a>
                    <a href="javascript:;" class="btn btn-default">Add New Template</a>
                    <a href="javascript:;" class="btn btn-default">Edit Templates</a>
                    <a href="javascript:;" class="btn btn-default" id="delete_template" onclick="delete_current_template();">Delete Templates</a>
                </div>
            </div>
            <div class="col-md-12">
                <div class="form-group">
                    <button type="submit" class="btn btn-primary" onclick="">Grab Details
                    </button>
                </div>
            </div>
        </form>
    </div>
<!-- Bulk Upload Start -->
 <div class="tab-pane fade" id="bulk_upload" role="tabpanel" aria-labelledby="profile-tab">
    <div class="panel panel-default" id="bulk_upload_panel">
        <div class="panel-body">
            <div class="row"> 
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="field-1" class="control-label">Products To Upload <span style="color: rgba(255, 7, 34, 0.86)">*</span></label>
                        <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Paste here all the products you would like to upload separated by break lines - one product per line, and each product asin and title separated by commas - ','" data-original-title="Products To Upload"><i class="fa fa-question-circle-o"></i></a>
                        <textarea class="form-control" id="upload_products_ids" rows="3" style="width: 100%"></textarea><br><br>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <br><br>
                        <a class="file-input-wrapper btn form-control file2 inline btn btn-primary"><i class="glyphicon glyphicon-file"></i>Upload Bulk From CSV<input type="file" accept=".txt,.xls,.xlsx,.csv" class="form-control file2 inline btn btn-primary" data-label="<i class='glyphicon glyphicon-file'></i>Upload Bulk From CSV" id="bulk_upload_file" style="left: -159px; top: 16px;display:none "></a>
                        <a href="javascript:;" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Upload from CSV file"><i class="fa fa-question-circle-o"></i></a>
                    </div>
                </div>
                <div class="col-md-4"></div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group" style="">
                        <label for="bulk_buying_site" class="control-label">Source Product Site</label>
                        <select id="bulk_buying_site" class="selectpicker form-control"><option value="1">Amazon US</option><option value="4">Amazon UK</option><option value="5">Amazon DE</option><option value="14">Walmart</option><option value="15">BangGood</option><option value="16">Aliexpress</option></select>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bulk_selling_site" class="control-label">Upload Product Site</label>
                        <select id="bulk_selling_site" class="selectpicker form-control">
                            <option value="2">Ebay US</option>
                            <option value="3">Ebay UK</option>
                            <option value="9">Ebay DE</option>
                            <option value="10">Ebay FR</option>
                            <option value="11">Ebay CA</option>
                        </select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group" id="bulk_break_even_group">
                        <label for="field-1" class="control-label">Break Even <span style="color: rgba(255, 7, 34, 0.86)">*</span></label>
                        <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Break Even - The percentage of the source product price you want your product to cost more." data-original-title="Break Even"><i class="fa fa-question-circle-o"></i>
                        </a>
                        <input type="number" step="0.1" min="10" class="form-control" id="bulk_break_even" placeholder="%">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group" id="bulk_additional_group">
                        <label for="field-1" class="control-label">$ Additional Profit</label>
                        <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Additional money you want to add to the source price (besides the break even)." data-original-title="Additional $"><i class="fa fa-question-circle-o"></i>
                        </a>
                        <input type="number" step="0.1" min="0" class="form-control" placeholder="$" id="bulk_additional_money">
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group" id="bulk_percentage_profit_group">
                        <label for="field-1" class="control-label">% Additional Profit</label>
                        <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Additional percentage you want to add to the final price after calculating the break even" data-original-title="Additional %"><i class="fa fa-question-circle-o"></i>
                        </a>
                        <input type="number" step="0.1" min="0" class="form-control" placeholder="$" id="bulk_percentage_profit">
                    </div>
                </div>
            </div><hr>
            <div class="row">
                <div class="col-md-5">
                    <div class="form-group" style="margin-right: 20px;">
                        <label for="bulk_upload_price_from" class="control-label">Price From </label>
                        <input type="number" step="1" min="0" class="form-control" id="bulk_upload_price_from">
                    </div>
                </div>
                <div class="col-md-5">
                    <div class="form-group">
                        <label for="bulk_upload_price_to" class="control-label">Price To </label>
                        <input type="number" step="1" min="0" class="form-control" id="bulk_upload_price_to">
                    </div> 
                </div>
            </div>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group">
                        <label for="bulk_quantity_to_upload" class="control-label">Quantity <span style="color: rgba(255, 7, 34, 0.86)">*</span></label>
                        <input type="number" step="1" min="0" class="form-control" id="bulk_quantity_to_upload">
                    </div>
                </div>
                <div class="col-md-5">
                   <div class="form-group">
                        <label for="field-1" class="control-label">Template</label>
                        <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="The template that will apply on the products description" data-original-title="Template"><i class="fa fa-question-circle-o"></i>
                        </a>
                        <select id="bulk_templates" class="selectpicker form-control"><option value="4996" data_user_profile_id="9816">amz listings</option><option value="121">BestQuality Best price 14 days returns</option><option value="126">No template</option><option value="222">BestQuality Best price 30 days returns</option><option value="507">Simply Blue</option><option value="1440">Limited Time Sale - 30 days Returns</option><option value="1717">Halloween Sale- 30 Days Returns</option><option value="1718">Halloween Sale- 60 Days Returns</option><option value="1720">Limited Time Sale - 60 days Returns</option><option value="2007">Black Friday - 30 Days Returns</option><option value="2008">Black Friday - 60 Days Returns</option><option value="2009">Christmas - 30 Days Returns</option><option value="2010">Christmas - 60 Days Returns</option><option value="3818">Valentine's Day Sale White 30 Days </option><option value="3819">Valentine's Day Sale Red 30 Days</option><option value="4017">Valentine's Day Sale White 60 Days </option><option value="4018">Valentine's Day Sale Red 60 Days</option><option value="4023">China Non Stop 30 Days</option><option value="4025">China Great Wall 30 Days</option></select>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-md-3">
                    <div class="form-group">
                        <label for="bulk_upload_use_ebay_policy" class="control-label">Set Policy <span style="color: rgba(255, 7, 34, 0.86)">*</span></label>
                        <select id="bulk_upload_use_ebay_policy" class="selectpicker form-control">
                            <option value="1" selected="">Ebay business policies</option>
                            <option value="0">Custom upload</option>
                        </select>
                    </div>
                </div>
            </div>                    
            <div class="row" id="bulk_upload_ebay_business_policies">
                <div class="col-md-4 col-sm-12">
                    <label for="bulk_upload_payment_policy" class="control-label">Payment Policy <span style="color: rgba(255, 7, 34, 0.86)">*</span></label>
                    <select id="bulk_upload_payment_policy" class="selectpicker form-control" style="width: 100%"><option value="101798934024">EASYNC_PAYMENT_PROFILE</option></select>
                </div>
                <div class="col-md-4 col-sm-12">
                    <label for="bulk_upload_return_policy" class="control-label">Return Policy <span style="color: rgba(255, 7, 34, 0.86)">*</span></label>
                    <select id="bulk_upload_return_policy" class="selectpicker form-control" style="width: 100%"><option value="101798933024">EASYNC_RETURN_PROFILE</option></select>
                </div>
                <div class="col-md-4 col-sm-12">
                    <label for="bulk_upload_shipping_policy" class="control-label">Shipping Policy <span style="color: rgba(255, 7, 34, 0.86)">*</span></label>
                    <select id="bulk_upload_shipping_policy" class="selectpicker form-control" style="width: 100%"><option value="103407634024">EASYNC_CHINA</option><option selected="" value="101798935024">EASYNC_SHIPPING_PROFILE</option></select>
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group" >
                        <label for="bulk_upload_tag-selectized" class="control-label">Tag</label>
                        <select class="form-control" data-plugin="select2" data-placeholder="Select a State" data-allow-clear="true">
                            <option label="Select a State"></option>
                            <option value="AK">Alaska</option>
                            <option value="HI">Hawaii</option>                                        
                        </select>
                    </div>
                </div>
                <div class="col-md-4">
                   <label for="bulk_shipping_location_upload" class="control-label">Location Products Shipped From</label>
                   <div class="form-group">
                       <select id="bulk_shipping_country_upload" class="selectpicker form-control">
                            <option value="AF">Afghanistan</option>
                            <option value="AX">Åland Islands</option>
                            <option value="AL">Albania</option>
                            <option value="DZ">Algeria</option>
                            <option value="AS">American Samoa</option>
                            <option value="AD">Andorra</option>
                            <option value="AO">Angola</option>
                            <option value="AI">Anguilla</option>
                            <option value="AQ">Antarctica</option>
                            <option value="AG">Antigua and Barbuda</option>
                            <option value="AR">Argentina</option>
                            <option value="AM">Armenia</option>
                            <option value="AW">Aruba</option>
                            <option value="AU">Australia</option>
                            <option value="AT">Austria</option>
                            <option value="AZ">Azerbaijan</option>                                    
                        </select>
                   </div>   
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="form-control-label"></label><br>
                        <input type="text" id="bulk_shipping_location_upload" class="form-control" data-validate="required">
                    </div>    
                </div>
            </div><br>
            <div class="row">
                <div class="col-md-5">
                    <div>
                        <input id="enable_oos" type="checkbox" value="1" checked="checked">
                        <label for="enable_oos" class="control-label">Enable Out Of Stock Items Upload </label>
                    </div>
                    <div>
                        <input id="bulk_add_product_to_price_monitor" type="checkbox" value="1">
                        <label for="bulk_add_product_to_price_monitor" class="control-label">Add To Price Monitor</label>
                    </div>
                     <div>
                        <input id="bulk_exclude_vero" type="checkbox" value="1">
                        <label for="bulk_exclude_vero" class="control-label">Allow Vero </label>
                    </div>
                    <div>
                        <input id="bulk_exclude_prime" type="checkbox" value="1">
                        <label for="bulk_exclude_prime" class="control-label">Allow Marketplace </label>
                    </div>
                    <div>
                        <input id="bulk_upload_first_upper_letters" type="checkbox">
                        <label for="bulk_upload_first_upper_letters" class="control-label">First Upper Letters </label>
                    </div>
                </div>
                <div class="col-md-5">
                    <div>
                        <input id="bulk_upload_duplicate_main_to_12" type="checkbox">
                        <label for="bulk_upload_duplicate_main_to_12" class="control-label">Duplicate Main Picture To 12</label>
                    </div>
                     <div>
                        <input id="bulk_upload_private_listing" type="checkbox">
                        <label for="bulk_upload_private_listing" class="control-label">Private Listing</label>
                        <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Allow buyers to remain anonymous to other eBay users" data-original-title="Private listing"><i class="fa fa-question-circle-o"></i>
                        </a>
                    </div>
                    <div>
                        <input id="bulk_upload_make_collage" type="checkbox">
                        <label for="bulk_upload_make_collage" class="control-label">Make Collage </label>
                    </div>
                    <div>
                        <input id="bulk_upload_set_watermark_on_all_pictures" disabled="" type="checkbox">
                        <label for="bulk_upload_set_watermark_on_all_pictures" class="control-label">Set Watermark On All Pictures </label>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-12 text-center">
                <button type="button" class="btn btn-info UploadProductButton" onclick="">Upload
                    Products
                </button>
                <button type="button" class="btn btn-success UploadScheduleProductButton" onclick="">Schedule Upload</button>
            </div>
        </div>
        <br>
    </div>
</div>   
<!-- Bulk Product Finish -->
    <div class="tab-pane fade" id="all_uploadss" role="tabpanel" aria-labelledby="contact-tab">
        <div class="col-md-12">
            <div class="card card-accent-theme">
                <div class="card-body">                                    
                    <table class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%" id="uploader">
                        <thead>
                            <tr>
                                <th>Upload ID</th>
                                <th>Creation Date</th>
                                <th>Total Upload Items</th>
                                <th>Successfully Uploaded</th>
                                <th>Failed To Upload</th>
                                <th>Status</th>
                                <th>Upload Details</th>
                                <th>Download ASINS</th>
                                <th>Cancel</th>

                            </tr>
                        </thead>
                        <tbody>
                            <tr>
                                <td>91888</td>
                                <td>2018-03-16 16:20:36</td>
                                <td>2</td>
                                <td>2</td>
                                <td><div style="text-align:center;color:red;">0</div></td>
                                <td><div style="text-align:center; color:green;">Finished</div></td>
                                <td><button type="button" class="btn btn-primary btn-icon icon-left" onclick="show_upload_details(91888)">Show Details<i class="fa fa-info-circle"></i></button></td>
                                <td><div style="text-align:center;"><a href="https://autodstools.com/download_asins/?user_id=6380&amp;upload_id=91888" class="btn btn-primary btn-icon icon-left">Download ASINS<i class="fa fa-cloud-download"></i></a></div>
                                </td>
                                <!-- <td>dsf</td> -->
                                <td><a href="">Cancel</a></td>
                            </tr>                                           
                        </tbody>
                    </table>

                </div>
                <!-- end card-body -->
            </div>
            <!-- end card -->
        </div>
    </div>
    <div class="tab-pane fade" id="scheduled_uploads_calendar" role="tabpanel" aria-labelledby="contact-tab">
        <div id='calendar'></div>
    </div>
  </div>
</div>

</div>
@endsection
@section('footer_script')
<script type="text/javascript">
    $(document).ready(function() {
        $('#uploader').DataTable({
            "scrollX": true,
        });
    });
</script>
<!--Full Calender -->
<script src="{{ URL::asset('public/admin-assets/libs/fullcalendar/lib/jquery-ui.min.js') }}"></script>
<script src="{{ URL::asset('public/admin-assets/libs/fullcalendar/moment.min.js') }}"></script>
<script src="{{ URL::asset('public/admin-assets/libs/fullcalendar/fullcalendar.min.js') }}"></script>
<script src="{{ URL::asset('public/admin-assets/js/example-fullcalendar.js') }}"></script>
<!--select 2 -->
    <script src="{{ URL::asset('public/admin-assets/libs/select2/dist/js/select2.min.js') }}"></script>
    <!-- bootstrap tags input -->
    <script src="{{ URL::asset('public/admin-assets/libs/bootstrap-tagsinput/dist/tagsinput.js') }}"></script>
    <!-- multiselect -->
    <script src="{{ URL::asset('public/admin-assets/libs/multiselect/js/jquery.multi-select.js') }}"></script>
    <!-- typeahead -->
    <script src="{{ URL::asset('public/admin-assets/libs/typeahead/typeahead.js') }}"></script>
    <script>
        $('.select2').select2({});
        $('#my-select').multiSelect();       
         $('#calendar').fullCalendar({
    // put your options and callbacks here
  })
   </script>
@endsection