@extends('backend_layout.app') 

@section('content')
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
	<li class="breadcrumb-item ">
		<a href="">Home</a>
	</li>
	<li class="breadcrumb-item active">Customer Services</li>
</ol>
<div class="container-fluid">
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-md-12">
				<div class="mailbox-sidebar">
					<div class="mailbox-sidebar-toggler navbar-toggler mailbox-sidebar-toggler d-md-none  mr-auto ">
						<i class="mdi mdi-backburger"></i>
					</div>
					<a href="" class=" btn btn-dark btn-sm">
						<i class="mdi mdi-rocket"></i> Compose</a>
					<nav class="sidebar-nav">
						<ul class="nav">
							<li class="nav-item">
								<a class="nav-link " href="#">
									<i class="mdi mdi-inbox"></i> Inbox
									<span class="badge"></span>
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									<i class="mdi mdi-animation"></i> Drafts
								</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									<i class="mdi mdi-inbox-arrow-up"></i> Sent</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									<i class="mdi mdi-delete-empty"></i>Trash</a>
							</li>
							<li class="divider"></li>
							<li class="nav-title">
								My Folders
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									<i class="mdi mdi-folder"></i>Social</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									<i class="mdi mdi-folder"></i>Promotions</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									<i class="mdi mdi-folder"></i>Internet</a>
							</li>
							<li class="divider"></li>
							<li class="nav-title">
								Tags
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									<i class="mdi mdi-bookmark"></i>Facebook</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									<i class="mdi mdi-bookmark"></i>Twitter</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#">
									<i class="mdi mdi-bookmark"></i>Bheance</a>
							</li>
						</ul>
					</nav>
				</div>
                <div class="mailbox-content">
                    <div class="container fluid">
                        <div class="row">
                            <div class="col-md-4">
                                <div class="card mail-list-card">
                                    <div class="card-body">
                                        <form action="#" class="mail-search-form">
                                            <div class="form-row">
                                                <div class="form-group col-md-10 col-sm-10 col-xs-8">
                                                    <input type="text" class="form-control" placeholder="search">
                                                </div>
                                                <div class="col-md-2 col-sm-2 col-xs-2">
                                                    <button type="submit" class="btn btn-theme btn-sm">
                                                        <i class="mdi mdi-magnify"></i>
                                                    </button>
                                                </div>
                                            </div>
                                        </form>
                                        <div class="mail-list-nav">
                                            <div class="mail-list">
                                                <div class="mail-details">
                                                    <img src="http://via.placeholder.com/100x100" class="user-avatar">
                                                    <ul>
                                                        <li class="user-name"><a href="#message_taniya">Taniya Janniffer</a></li>
                                                        <li class="mail-subject">Dashboard Design</li>
                                                        <li class="mail-time">21.00</li>
                                                    </ul>
                                                    <p class="message">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat ex aperiam
                                                        aspernatur quaerat quod deleniti vel autem </p>
                                                </div>
                                            </div>
                                            <div class="mail-list">
                                                <div class="mail-details">
                                                    <img src="http://via.placeholder.com/100x100" alt="" class="user-avatar">
                                                    <ul>
                                                        <li class="user-name">Harry Justin</li>
                                                        <li class="mail-subject">Editing Video</li>
                                                        <li class="mail-time">23.00</li>
                                                    </ul>
                                                    <p class="message">Lorem ipsum dolor, sit amet consectetur adipisicing elit. Placeat ex aperiam
                                                        aspernatur quaerat quod deleniti vel autem </p>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="mail-list-pagination">
                                            <nav aria-label="Page navigation example ">
                                                <ul class="pagination pagination-sm justify-content-center">
                                                    <li class="page-item">
                                                        <a class="page-link" tabindex="-1" href="#"> Previous </a>
                                                    </li>
                                                    <li class="page-item">
                                                        <a class="page-link" href="#">1</a>
                                                    </li>
                                                    <li class="page-item">
                                                        <a class="page-link" href="#">2</a>
                                                    </li>
                                                    <li class="page-item">
                                                        <a class="page-link" href="#">3</a>
                                                    </li>
                                                    <li class="page-item">
                                                        <a class="page-link" href="#">Next</a>
                                                    </li>
                                                </ul>
                                            </nav>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-8">
                                <div class="card ">
                                    <div class="card-body">
                                        <div class="mail-view">
                                            <div class="mail-toolbar">
                                                <ul>
                                                    <li>
                                                        <i class="mdi mdi-dots-vertical-circle"></i>
                                                    </li>
                                                    <li>
                                                        <i class="mdi mdi-printer"></i>
                                                    </li>
                                                    <li>
                                                        <i class="mdi mdi-reply "></i>
                                                    </li>
                                                    <li>
                                                        <i class="mdi mdi-reply-all"></i>
                                                    </li>
                                                    <li>
                                                        <i class="mdi mdi-message-reply-text"></i>
                                                    </li>

                                                </ul>
                                            </div>
                                            <div class="mail-view-header">
                                                <img src="http://via.placeholder.com/100x100" alt="" class="user-avatar">
                                                <ul>
                                                    <li class="user-name">Taniya Janniffer</li>
                                                    <li class="mail-subject">Dashboard Design</li>
                                                    <li class="mail-time">21.00</li>
                                                </ul>
                                            </div>
                                            <div class="mail-body">
                                                <div class="mail-heading"> Design Upwork Design Challenge </div>
                                                <p class="mail-message" id="message_taniya">
                                                    Dear Paul,
                                                    <br>
                                                    <br> Lorem ipsum dolor sit amet, consectetur adipisicing elit. Laudantium modi,
                                                    doloribus maxime qui dolorem rem nulla blanditiis harum! Tenetur repellat
                                                    aliquam deleniti quaerat. Eius fuga hic tenetur? Sit, reprehenderit animi.
                                                    Lorem, ipsum dolor sit amet consectetur adipisicing elit. Magni deserunt,
                                                    praesentium quos aperiam harum, asperiores odio modi quae eum sit quasi.
                                                    Sint quidem corrupti soluta incidunt quia alias, repudiandae quae.

                                                    <br>
                                                    <br> Lorem ipsum dolor sit amet consectetur adipisicing elit. Animi reiciendis
                                                    repellat dolore?
                                                    <br>
                                                    <br>
                                                    <br> Looking Forward from you
                                                    <br> Have a Great Day
                                                    <br>
                                                    <span class="text-theme">Taniya Janniffer</span>
                                                </p>
                                            </div>
                                            <div class="mail-attachments">
                                                <ul>
                                                    <li>
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <img class="card-img-top" src="http://via.placeholder.com/100x100" alt="Card image cap">
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="file-name">
                                                                    <i class="mdi mdi-paperclip"></i> report.pdf
                                                                </div>
                                                                <a href="" class="btn btn-theme btn-sm">
                                                                    <i class="mdi mdi-cloud-download"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                    <li>
                                                        <div class="card">
                                                            <div class="card-body">
                                                                <img class="card-img-top" src="http://via.placeholder.com/100x100" alt="Card image cap">
                                                            </div>
                                                            <div class="card-footer">
                                                                <div class="file-name">
                                                                    <i class="mdi mdi-paperclip"></i> report.doc</div>
                                                                <a href="" class="btn btn-theme btn-sm">
                                                                    <i class="mdi mdi-cloud-download"></i>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </li>
                                                </ul>
                                            </div>
                                           <div class="mail-reply-form">
                                                <form action="#">
                                                    <div class="form-row">
                                                        <div class="form-group col-md-8 col-sm-8">
                                                            <textarea class="form-control" id="exampleFormControlTextarea1" rows="2" style="overflow:hidden" placeholder="send message"></textarea>
                                                        </div>
                                                        <div class="col-md-2 col-sm-2">
                                                            <button type="submit" class="btn btn-theme btn-sm">
                                                                <i class="mdi mdi-send "></i>
                                                            </button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
			</div>
		</div>
	</div>
    <aside class="aside-menu">
            <div class="aside-header bg-theme text-uppercase">Service Panel</div>
            <div class="aside-body">
                <h6 class="text-theme">Light Sidebar</h6>
                <ul class="theme-colors">
                    <li class="theme-blue" onclick="appSwapStyleSheet('../../css/style-blue.css')"></li>
                    <li class="theme-green" onclick="appSwapStyleSheet('../../css/style-green.css')"></li>
                    <li class="theme-red" onclick="appSwapStyleSheet('../../css/style-red.css')"></li>
                    <li class="theme-yellow" onclick="appSwapStyleSheet('../../css/style-yellow.css')"></li>
                    <li class="theme-orange" onclick="appSwapStyleSheet('../../css/style-orange.css')"></li>
                    <li class="theme-teal" onclick="appSwapStyleSheet('../../css/style-teal.css')"></li>
                    <li class="theme-cyan" onclick="appSwapStyleSheet('../../css/style-cyan.css')"></li>
                    <li class="theme-purple" onclick="appSwapStyleSheet('../../css/style-purple.css')"></li>
                    <li class="theme-indigo" onclick="appSwapStyleSheet('../../css/style-indigo.css')"></li>
                    <li class="theme-pink" onclick="appSwapStyleSheet('../../css/style-pink.css')"></li>
                </ul>

                <!-- <h6 class="text-theme">Social Colors</h6> -->
                <ul class="theme-colors">
                    <li class="theme-facebook" onclick="appSwapStyleSheet('../../css/style-facebook.css')"></li>
                    <li class="theme-twitter" onclick="appSwapStyleSheet('../../css/style-twitter.css')"></li>
                    <li class="theme-linkedin" onclick="appSwapStyleSheet('../../css/style-linkedin.css')"></li>
                    <li class="theme-google-plus" onclick="appSwapStyleSheet('../../css/style-google-plus.css')"></li>
                    <li class="theme-flickr" onclick="appSwapStyleSheet('../../css/style-flickr.css')"></li>
                    <li class="theme-tumblr" onclick="appSwapStyleSheet('../../css/style-tumblr.css')"></li>
                    <li class="theme-xing" onclick="appSwapStyleSheet('../../css/style-xing.css')"></li>
                    <li class="theme-github" onclick="appSwapStyleSheet('../../css/style-github.css')"></li>
                    <li class="theme-html5" onclick="appSwapStyleSheet('../../css/style-html5.css')"></li>
                    <li class="theme-openid" onclick="appSwapStyleSheet('../../css/style-openid.css')"></li>
                    <li class="theme-stack-overflow" onclick="appSwapStyleSheet('../../css/style-stack-overflow.css')"></li>
                    <li class="theme-css3" onclick="appSwapStyleSheet('../../css/style-css3.css')"></li>
                    <li class="theme-dribbble" onclick="appSwapStyleSheet('../../css/style-dribbble.css')"></li>
                    <li class="theme-instagram" onclick="appSwapStyleSheet('../../css/style-instagram.css')"></li>
                    <li class="theme-pinterest" onclick="appSwapStyleSheet('../../css/style-pinterest.css')"></li>
                    <li class="theme-vk" onclick="appSwapStyleSheet('../../css/style-vk.css')"></li>
                    <li class="theme-yahoo" onclick="appSwapStyleSheet('../../css/style-yahoo.css')"></li>
                    <li class="theme-behance" onclick="appSwapStyleSheet('../../css/style-behance.css')"></li>
                    <li class="theme-dropbox" onclick="appSwapStyleSheet('../../css/style-dropbox.css')"></li>
                    <li class="theme-reddit" onclick="appSwapStyleSheet('../../css/style-reddit.css')"></li>
                    <li class="theme-spotify" onclick="appSwapStyleSheet('../../css/style-spotify.css')"></li>
                    <li class="theme-vine" onclick="appSwapStyleSheet('../../css/style-vine.css')"></li>
                    <li class="theme-foursquare" onclick="appSwapStyleSheet('../../css/style-foursquare.css')"></li>
                    <li class="theme-vimeo" onclick="appSwapStyleSheet('../../css/style-vimeo.css')"></li>

                </ul>

                <h6 class="text-theme">Dark Sidebar</h6>
                <ul class="theme-colors">
                    <li class="theme-blue" onclick="appSwapStyleSheetDark('../../css/style-blue.css')"></li>
                    <li class="theme-green" onclick="appSwapStyleSheetDark('../../css/style-green.css')"></li>
                    <li class="theme-red" onclick="appSwapStyleSheetDark('../../css/style-red.css')"></li>
                    <li class="theme-yellow" onclick="appSwapStyleSheetDark('../../css/style-yellow.css')"></li>
                    <li class="theme-orange" onclick="appSwapStyleSheetDark('../../css/style-orange.css')"></li>
                    <li class="theme-teal" onclick="appSwapStyleSheetDark('../../css/style-teal.css')"></li>
                    <li class="theme-cyan" onclick="appSwapStyleSheetDark('../../css/style-cyan.css')"></li>
                    <li class="theme-purple" onclick="appSwapStyleSheetDark('../../css/style-purple.css')"></li>
                    <li class="theme-indigo" onclick="appSwapStyleSheetDark('../../css/style-indigo.css')"></li>
                    <li class="theme-pink" onclick="appSwapStyleSheetDark('../../css/style-pink.css')"></li>
                </ul>

                <!-- <h6 class="text-theme">Social Colors</h6> -->
                <ul class="theme-colors">
                    <li class="theme-facebook" onclick="appSwapStyleSheetDark('../../css/style-facebook.css')"></li>
                    <li class="theme-twitter" onclick="appSwapStyleSheetDark('../../css/style-twitter.css')"></li>
                    <li class="theme-linkedin" onclick="appSwapStyleSheetDark('../../css/style-linkedin.css')"></li>
                    <li class="theme-google-plus" onclick="appSwapStyleSheetDark('../../css/style-google-plus.css')"></li>
                    <li class="theme-flickr" onclick="appSwapStyleSheetDark('../../css/style-flickr.css')"></li>
                    <li class="theme-tumblr" onclick="appSwapStyleSheetDark('../../css/style-tumblr.css')"></li>
                    <li class="theme-xing" onclick="appSwapStyleSheetDark('../../css/style-xing.css')"></li>
                    <li class="theme-github" onclick="appSwapStyleSheetDark('../../css/style-github.css')"></li>
                    <li class="theme-html5" onclick="appSwapStyleSheetDark('../../css/style-html5.css')"></li>
                    <li class="theme-openid" onclick="appSwapStyleSheetDark('../../css/style-openid.css')"></li>
                    <li class="theme-stack-overflow" onclick="appSwapStyleSheetDark('../../css/style-stack-overflow.css')"></li>
                    <li class="theme-css3" onclick="appSwapStyleSheetDark('../../css/style-css3.css')"></li>
                    <li class="theme-dribbble" onclick="appSwapStyleSheetDark('../../css/style-dribbble.css')"></li>
                    <li class="theme-instagram" onclick="appSwapStyleSheetDark('../../css/style-instagram.css')"></li>
                    <li class="theme-pinterest" onclick="appSwapStyleSheetDark('../../css/style-pinterest.css')"></li>
                    <li class="theme-vk" onclick="appSwapStyleSheetDark('../../css/style-vk.css')"></li>
                    <li class="theme-yahoo" onclick="appSwapStyleSheetDark('../../css/style-yahoo.css')"></li>
                    <li class="theme-behance" onclick="appSwapStyleSheetDark('../../css/style-behance.css')"></li>
                    <li class="theme-dropbox" onclick="appSwapStyleSheetDark('../../css/style-dropbox.css')"></li>
                    <li class="theme-reddit" onclick="appSwapStyleSheetDark('../../css/style-reddit.css')"></li>
                    <li class="theme-spotify" onclick="appSwapStyleSheetDark('../../css/style-spotify.css')"></li>
                    <li class="theme-vine" onclick="appSwapStyleSheetDark('../../css/style-vine.css')"></li>
                    <li class="theme-foursquare" onclick="appSwapStyleSheetDark('../../css/style-foursquare.css')"></li>
                    <li class="theme-vimeo" onclick="appSwapStyleSheetDark('../../css/style-vimeo.css')"></li>

                </ul>
            </div>
    </aside>

</div>
</div>

@endsection
@section('footer_script')
<script type="text/javascript">
	$(document).ready(function() {
		$('#example').DataTable({
      "scrollX": true,
    });
});
</script> 
@endsection