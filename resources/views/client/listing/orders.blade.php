@extends('backend_layout.app') 

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
	<li class="breadcrumb-item ">
		<a href="">Home</a>
	</li>
	<li class="breadcrumb-item">
		<a href="#">Listing</a>
	</li>
	<li class="breadcrumb-item active">Orders</li>
</ol>


<div class="container-fluid">

	<div class="animated fadeIn">
		<h3>Orders</h3>
		<br/>
		<a href="#" data-toggle="modal" data-target=".bs-example-modal-default_orders">
			<button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">listing change</button></a>
		<div class="row">
			<div class="col-md-12">
				<div class="card card-accent-theme">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table">
								<thead>
									<tr>
										<th><input type="checkbox" name=""></th>
										<th>Edit</th>
										<th><img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Order ID</th>
										<th><img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png"><br>Source Order ID</th>
										<th>Picture</th>
										<th><img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Item ID</th>
										<th><img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png"><br>Source Item ID</th>
										<th><img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Price</th>
										<th><img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png"><br>Purchase Price</th>
										<th>Tax</th>
										<th>Profit</th>
										<th>Buyer Name</th>
										<th>Buyer Username</th>
										<th><img src="https://autodstools.com/neon_dashboard/assets/images/001-sign.png" style="max-width: 100%; max-height:40px; min-width:30px"></th>
										<th>Sold Date</th>
										<th>Status</th>
									</tr>
								</thead>
								<tbody>
									<tr>
										<td>
											<input type="checkbox" name="">
										</td>
										<td><img src="https://autodstools.com/neon_dashboard/assets/images/edit_opened_small3.png"></td>
										<td><a href="">	253495615267-2228919217015</a></td>
										<td><a href="">265258744</a></td>
										<td><img src="https://i.ebayimg.com/00/s/MTYwMFgxNjAw/z/NnQAAOSwYUBa0DuQ/$_3.JPG" width="50"></td>
										<td><a href="">253495615267</a></td>							
										<td><a href="">B075RFBKNS</a></td>
										<td>14.39$</td>
										<td>11.99$</td>
										<td>0.00$</td>
										<td>0.22$</td>
										<td>Kilynne Leslie Beaulieu</td>
										<td>beaulieuk91</td>
										<td>1</td>
										<td>
											<span class="text-muted">
												<i class="fa fa-clock-o"></i> Oct 16, 2017</span>
											</td>
											<td>
												Canceled
												<select class="">
													<option>Change Status</option>
													<option>Ordered</option>
													<option>Shipped</option>
													<option>Completed</option>
													<option>StandBy</option>
													<option>Canceled</option>
												</select>
											</td>
										</tr>
										<tr>
										<td>
											<input type="checkbox" name="">
										</td>
										<td><img src="https://autodstools.com/neon_dashboard/assets/images/edit_opened_small3.png"></td>
										<td><a href="">	253495615267-2228919217015</a></td>
										<td><a href="">265258744</a></td>
										<td><img src="https://i.ebayimg.com/00/s/MTYwMFgxNjAw/z/NnQAAOSwYUBa0DuQ/$_3.JPG" width="50"></td>
										<td><a href="">253495615267</a></td>							
										<td><a href="">B075RFBKNS</a></td>
										<td>14.39$</td>
										<td>11.99$</td>
										<td>0.00$</td>
										<td>0.22$</td>
										<td>Kilynne Leslie Beaulieu</td>
										<td>beaulieuk91</td>
										<td>1</td>
										<td>
											<span class="text-muted">
												<i class="fa fa-clock-o"></i> Oct 16, 2017</span>
											</td>
											<td>
												Canceled
												<select class="">
													<option>Change Status</option>
													<option>Ordered</option>
													<option>Shipped</option>
													<option>Completed</option>
													<option>StandBy</option>
													<option>Canceled</option>
												</select>
											</td>
										</tr>
										<tr>
										<td>
											<input type="checkbox" name="">
										</td>
										<td><img src="https://autodstools.com/neon_dashboard/assets/images/edit_opened_small3.png"></td>
										<td><a href="">	253495615267-2228919217015</a></td>
										<td><a href="">265258744</a></td>
										<td><img src="https://i.ebayimg.com/00/s/MTYwMFgxNjAw/z/NnQAAOSwYUBa0DuQ/$_3.JPG" width="50"></td>
										<td><a href="">253495615267</a></td>							
										<td><a href="">B075RFBKNS</a></td>
										<td>14.39$</td>
										<td>11.99$</td>
										<td>0.00$</td>
										<td>0.22$</td>
										<td>Kilynne Leslie Beaulieu</td>
										<td>beaulieuk91</td>
										<td>1</td>
										<td>
											<span class="text-muted">
												<i class="fa fa-clock-o"></i> Oct 16, 2017</span>
											</td>
											<td>
												Canceled
												<select class="">
													<option>Change Status</option>
													<option>Ordered</option>
													<option>Shipped</option>
													<option>Completed</option>
													<option>StandBy</option>
													<option>Canceled</option>
												</select>
											</td>
										</tr>
										<tr>
											<td>
												<input type="checkbox" name="">
											</td>
											<td><img src="https://autodstools.com/neon_dashboard/assets/images/edit_opened_small3.png"></td>
											<td><a href="">	253495615267-2228919217015</a></td>
											<td><a href="">265258745</a></td>
											<td><img src="https://i.ebayimg.com/00/s/MTYwMFgxNjAw/z/NnQAAOSwYUBa0DuQ/$_3.JPG" width="50"></td>
											<td><a href="">253495615267</a></td>							
											<td><a href="">B075RFBKNS</a></td>
											<td>14.39$</td>
											<td>11.99$</td>
											<td>0.00$</td>
											<td>0.22$</td>
											<td>Kilynne Leslie Beaulieu</td>
											<td>beaulieuk91</td>
											<td>1</td>
											<td>
												<span class="text-muted">
													<i class="fa fa-clock-o"></i> Oct 16, 2017</span>
												</td>
												<td>
													Canceled
													<select class="">
														<option>Change Status</option>
														<option>Ordered</option>
														<option>Shipped</option>
														<option>Completed</option>
														<option>StandBy</option>
														<option>Canceled</option>
													</select>
												</td>
											</tr>
											<tr>
												<td>
													<input type="checkbox" name="">
												</td>
												<td><img src="https://autodstools.com/neon_dashboard/assets/images/edit_opened_small3.png"></td>
												<td><a href="">	253495615267-2228919217015</a></td>
												<td><a href="">2652587446</a></td>
												<td><img src="https://i.ebayimg.com/00/s/MTYwMFgxNjAw/z/NnQAAOSwYUBa0DuQ/$_3.JPG" width="50"></td>
												<td><a href="">253495615267</a></td>							
												<td><a href="">B075RFBKNS</a></td>
												<td>14.39$</td>
												<td>11.99$</td>
												<td>0.00$</td>
												<td>0.22$</td>
												<td>Kilynne Leslie Beaulieu</td>
												<td>beaulieuk91</td>
												<td>1</td>
												<td>
													<span class="text-muted">
														<i class="fa fa-clock-o"></i> Oct 16, 2017</span>
													</td>
													<td>
														Canceled
														<select class="">
															<option>Change Status</option>
															<option>Ordered</option>
															<option>Shipped</option>
															<option>Completed</option>
															<option>StandBy</option>
															<option>Canceled</option>
														</select>
													</td>
												</tr>			
											</tbody>
											<tfoot>
												<th><input type="checkbox" name=""></th>
												<th>Edit</th>
												<th><img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Order ID</th>
												<th><img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png"><br>Source Order ID</th>
												<th>Picture</th>
												<th><img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Item ID</th>
												<th><img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png"><br>Source Order ID</th>
												<th><img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Price</th>
												<th><img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png"><br>Purchase Price</th>
												<th>Tax</th>
												<th>Profit</th>
												<th>Buyer Name</th>
												<th>Buyer Username</th>
												<th><img src="https://autodstools.com/neon_dashboard/assets/images/001-sign.png" style="max-width: 100%; max-height:40px; min-width:30px"></th>
												<th>Sold Date</th>
												<th>Status</th>
											</tfoot>
										</table>
									</div>
								</div>
								<!-- end card-body -->
							</div>
							<!-- end card -->
						</div>
						<!-- end col -->
					</div>
					<!-- end row -->
				</div>
				<!-- end animated fadeIn -->
			</div>
			<!-- end container-fluid -->


			 <!-- sample modal content Notification -->
    <div class="modal fade bs-example-modal-default_orders" tabindex="-1" role="dialog"  aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog modal-dialog-centered-custom">
            <div class="modal-content">
                <div class="modal-header" style="padding: 5px">
                    <h6 class="modal-title" style="margin: 12px" >Change listings</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                    	<ol style="display: none;" class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                    		<li class="breadcrumb-item ">
                    			<a href="">Home</a>
                    		</li>
                    		<li class="breadcrumb-item">
                    			<a href="#">Listing</a>
                    		</li>
                    		<li class="breadcrumb-item active">Monitors</li>
                    	</ol>


<div class="container-fluid">
	<ul class="nav nav-tabs" id="myTab" role="tablist">
		<li class="nav-item">
			<a class="nav-link active" id="price-monitor" data-toggle="tab" href="#price_monitor" role="tab" aria-controls="home" aria-selected="true">Bulk Changes</a>
		</li>
		<li class="nav-item">
			<a class="nav-link" id="orders-monitor" data-toggle="tab" href="#orders_monitor" role="tab" aria-controls="profile" aria-selected="false">All Bulk changed items</a>
		</li>
	</ul>
	<div class="tab-content" id="myTabContent">
		<div class="tab-pane fade show active" id="price_monitor" role="tabpanel" aria-labelledby="home-tab" style="height: 200px;
    overflow-y: scroll;">
<div class="row">
	<div class="col-md-6">
		<div class="card-body">
			<strong>New Break Even:</strong>
			<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><!-- <button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="mdi mdi-chevron-up"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="mdi mdi-chevron-down"></i></button> --></span></div>
		</div>
		<!-- end inside card-body -->
	</div>

	<div class="col-md-6">
		<div class="card-body">
			<strong>New Addition $ Profit:</strong>
			<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><!-- <button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="mdi mdi-chevron-up"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="mdi mdi-chevron-down"></i></button> --></span></div>
		</div>
		<!-- end inside card-body -->
	</div>
	</div>

	<div class="row">
	<div class="col-md-6">
		<div class="card-body">
			<strong>New Additional % Profit:</strong>
			<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><!-- <button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="mdi mdi-chevron-up"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="mdi mdi-chevron-down"></i></button> --></span></div>
		</div>
		<!-- end inside card-body -->
	</div>

	<div class="col-md-6">
		<div class="card-body">
			<strong>New Quantity:</strong>
			<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><!-- <button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="mdi mdi-chevron-up"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="mdi mdi-chevron-down"></i></button> --></span></div>
		</div>
		<!-- end inside card-body -->
	</div>
	</div>


	<div class="row">
	<div class="col-md-6">
		<div class="card-body">
			<strong>New Price:</strong>
			<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><!-- <button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="mdi mdi-chevron-up"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="mdi mdi-chevron-down"></i></button> --></span></div>
		</div>
		<!-- end inside card-body -->
	</div>

	<div class="col-md-6">
		<div class="card-body">
			<strong>Change Current $ Profit By X.X$(+-):	</strong>
			<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><!-- <button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="mdi mdi-chevron-up"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="mdi mdi-chevron-down"></i></button> --></span></div>
		</div>
		<!-- end inside card-body -->
	</div>
	</div>



	<div class="row">
	<div class="col-md-6">
		<div class="card-body">
			<strong>Change Breakeven By X.X %(+-):</strong>
			<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><!-- <button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="mdi mdi-chevron-up"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="mdi mdi-chevron-down"></i></button> --></span></div>
		</div>
		<!-- end inside card-body -->
	</div>

	<div class="col-md-6">
		<div class="card-body">
			<strong>Change Additional % Profit By X.X$(+-):</strong>
			<div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"><!-- <button class="btn btn-default bootstrap-touchspin-up" type="button"><i class="mdi mdi-chevron-up"></i></button><button class="btn btn-default bootstrap-touchspin-down" type="button"><i class="mdi mdi-chevron-down"></i></button> --></span></div>
		</div>
		<!-- end inside card-body -->
	</div>
	</div>
		


	<div class="row">
	<div class="col-md-6">
		<div class="card-body">
			<strong>Change Private Listings</strong>
			<div class="form-group row">
					<div class="col-md-12">
						<select id="select" name="select" class="form-control">
                            <option value="0">Please select</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                        </select>
                    </div>
                </div>
		</div>
		<!-- end inside card-body -->
	</div>

	<div class="col-md-6">
		<div class="card-body">
			<strong>Template</strong>
				<div class="form-group row">
					<div class="col-md-12">
						<select id="select" name="select" class="form-control">
            <option value="0">Please select</option>
            <option value="1">Option #1</option>
            <option value="2">Option #2</option>
            <option value="3">Option #3</option>
           </select>
          </div>
         </div>
        </div>
       </div>
      </div>

<div class="row">
	<div class="col-md-6">
		<div class="card-body">
			<strong>Change Tag</strong>
			<div class="form-group row">
					<div class="col-md-12">
						<select id="select" name="select" class="form-control">
                            <option value="0">Please select</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                        </select>
                    </div>
                </div>
		</div>
		<!-- end inside card-body -->
	</div>

	<div class="col-md-6">
		<div class="card-body">
			<input type="checkbox" id="checkbox1" name="checkbox1" value="option1"><strong>Remove Tag</strong>
				
	</div>

</div>
</div>



<div class="row">
	

	<div class="col-md-6">
		<div class="card-body">
			<input type="checkbox" id="checkbox1" name="checkbox1" value="option1"><strong>Change Policy</strong>
				
	</div>

</div>
</div>

<div class="row">
	

	<div class="col-md-6">
		<div class="card-body">
			<input type="checkbox" id="checkbox1" name="checkbox1" value="option1"><strong>Change Location</strong>
				
	</div>

</div>
</div>




	<div class="row">
	<div class="col-md-6">
		<div class="card-body">
			<strong>Stock Monitoring</strong>
			<div class="form-group row">
					<div class="col-md-12">
						<select id="select" name="select" class="form-control">
                            <option value="0">Please select</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                        </select>
                    </div>
                </div>
		</div>
		<!-- end inside card-body -->
	</div>

	<div class="col-md-6">
		<div class="card-body">
			<strong>Auto Ordering</strong>
				<div class="form-group row">
					<div class="col-md-12">
						<select id="select" name="select" class="form-control">
            <option value="0">Please select</option>
            <option value="1">Option #1</option>
            <option value="2">Option #2</option>
            <option value="3">Option #3</option>
           </select>
          </div>
         </div>
        </div>
       </div>
      </div>



	<div class="row">
	<div class="col-md-6">
		<div class="card-body">
			<strong>Marketplace</strong>
			<div class="form-group row">
					<div class="col-md-12">
						<select id="select" name="select" class="form-control">
                            <option value="0">Please select</option>
                            <option value="1">Option #1</option>
                            <option value="2">Option #2</option>
                            <option value="3">Option #3</option>
                        </select>
                    </div>
                </div>
		</div>
		<!-- end inside card-body -->
	</div>

	<div class="col-md-6">
		<div class="card-body">
			<strong>Price Monitoring</strong>
				<div class="form-group row">
					<div class="col-md-12">
						<select id="select" name="select" class="form-control">
            <option value="0">Please select</option>
            <option value="1">Option #1</option>
            <option value="2">Option #2</option>
            <option value="3">Option #3</option>
           </select>
          </div>
         </div>
        </div>
       </div>
      </div>



<div class="row">
	

	<div class="col-md-6">
		<div class="card-body">
			<input type="checkbox" id="checkbox1" name="checkbox1" value="option1"><strong>Apply to All Listings</strong>
				
	</div>

</div>
</div>
</div>

		<div id="orders_monitor" class="tab-pane fade in">
			<h1>hello</h1>
			<div class="row">
			</div>
		
		</div>
		
	</div>
</div>
 
<div class="end_res">
				<button type="button" class="btn btn-danger" data-dismiss="modal">End Listing</button>
 			<button type="button" class="btn btn-warning" data-dismiss="modal">Remove from monitors</button>
  		<button type="button" class="btn btn-warning" data-dismiss="modal">Relist</button>
</div>


<!-- sample modal content -->
    <div class="modal fade bs-example-modal-default" tabindex="-1" role="dialog"  aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog modal-dialog-centered-custom">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" >Messages To Buyers</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="container-fluid">
						<ul class="nav nav-tabs" id="myTab" role="tablist">
							<li class="nav-item">
								<a class="nav-link active" id="price-monitor" data-toggle="tab" href="#price_monitor" role="tab" aria-controls="home" aria-selected="true">After Order Success Message</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="orders-monitor" data-toggle="tab" href="#orders_monitor" role="tab" aria-controls="profile" aria-selected="false">Tracking Number Update Message</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" id="profitable-monitor" data-toggle="tab" href="#profitable_monitor" role="tab" aria-controls="contact" aria-selected="false">Thank You Message</a>
							</li>
							
						</ul>

						<div class="col-md-12">
								<span class="MonitorsContentKey"><strong>Feedback:</strong></span>
							
							<div class="row" id="cashback_site_password">
								<div class="modal-body">
									<div class="col-md-12">
										<div class="btn-group" style="width: 100%;">
											<input class="form-control" type="text" placeholder="" id="cashback_site_password">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12">
								<span class="MonitorsContentKey"><strong>Send US Orders Thank You Message After:</strong></span>
							
							<div class="row" id="cashback_site_password">
								<div class="modal-body">
									<div class="col-md-12">
										<div class="btn-group" style="width: 100%;">
											<input class="form-control" type="text" placeholder="" id="cashback_site_password">
										</div>
									</div>
								</div>
							</div>
						</div>

						<div class="col-md-12">
								<span class="MonitorsContentKey"><strong>Send International Orders Thank You Messages After:</strong></span>
							
							<div class="row" id="cashback_site_password">
								<div class="modal-body">
									<div class="col-md-12">
										<div class="btn-group" style="width: 100%;">
											<input class="form-control" type="text" placeholder="" id="cashback_site_password">
										</div>
									</div>
								</div>
							</div>
						</div>


					</div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                      <button type="button" class="btn btn-primary">Save Change</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

     <!-- sample modal content Notification -->
    <div class="modal fade bs-example-modal-default_notifiaction" tabindex="-1" role="dialog"  aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog modal-dialog-centered-custom">
            <div class="modal-content">
                <div class="modal-header" style="padding: 5px">
                    <h6 class="modal-title" style="margin: 12px" >Notifications</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="col-md-12">
						<div class="checkbox" id="checkbox_hipshipper_checkbox">
							<input type="checkbox" value="0" ><strong>Email When Ordered Sucessfully</strong>
							<br>
							<input type="checkbox" value="0" ><strong>Email When Tracking Number Updated</strong>
							<br>
							<input type="checkbox" value="0" ><strong>Email When Ordered Failed
							</strong>
							<br>
						</div>
						
					</div>
                </div>
                <div class="col-md-6">
								<span class="MonitorsContentKey"><strong>Notifications Email:</strong></span>
							</div>
							<div class="row" id="cashback_site_password">
								<div class="modal-body">
									<div class="col-md-12">
										<div class="btn-group" style="width: 100%;">
											<input class="form-control" type="text" placeholder="" id="cashback_site_password">
										</div>
									</div>
								</div>
							</div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save Change</button>
                </div>
              
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->




<!-- sample modal content cashback -->
    <div class="modal fade bs-example-modal-default_cashback" tabindex="-1" role="dialog"  aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog modal-dialog-centered-custom">
            <div class="modal-content">
                <div class="modal-header">
                    <h6 class="modal-title" >Add Cashback User</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
            	<div class="modal-body"> 
                	<div class="cashback_user_details_container">
						<div class="row">
							<div class="col-md-6">
								<span class="MonitorsContentKey">Cashback Site Username:</span>
							</div>
							<div class="row" id="cashback_site_username">
								<div class="col-md-12">
									<div class="btn-group">
										<input class="form-control" type="text" placeholder="Cashback Site Username" id="cashback_site_username">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<span class="MonitorsContentKey">Cashback Site Password:</span>
							</div>
							<div class="row" id="cashback_site_password">
								<div class="col-md-12">
									<div class="btn-group">
										<input class="form-control" type="text" placeholder="Cashback Site Password" id="cashback_site_password">
									</div>
								</div>
							</div>
						</div>
						<div class="row">
							<div class="col-md-6">
								<span class="MonitorsContentKey">Cashback Site:</span>
							</div>
							<div class="row" id="cashback_site_password">
								<!-- <div class="col-md-12">
									<div class="btn-group">
											<input class="form-control" type="text" placeholder="Cashback Site" id="cashback_site">
									</div>
								</div> -->
								<div class="col-md-12">
									<div class="btn-group">
										<select  class="col-md-12" id="email_account" class="selectpicker">
											<option>Cashback Site</option>
											<option value="temp6137@gmail.com" title="temp6137@gmail.com">temp6137@gmail.com</option>
										</select>
									</div>
								</div>
							</div>
						</div>
						
					</div>
				</div>	
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save Change</button>
                </div>
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->



                    </div>
                </div>
               

              
                <div class="modal-footer">
                	<button type="button" class="btn btn-default waves-effect text-left" data-dismiss="modal">Close</button>
                    <button type="button" class="btn btn-primary">Save Change</button>
                </div>
              
            </div>
            <!-- /.modal-content -->
        </div>
        <!-- /.modal-dialog -->
    </div>
    <!-- /.modal -->

			@endsection

			@section('footer_script')


			<script src="{{ url('admin-assets/libs/jquery-knob/dist/jquery.knob.min.js') }}"></script>
			<!--morris js -->
			<script src="{{ url('admin-assets/libs/raphael/raphael.min.js') }}"></script>
			<script src="{{ url('admin-assets/libs/charts-morris-chart/morris.min.js') }}"></script>


			<!-- dashboard-ecom script -->
			<script src="{{ url('admin-assets/js/dashboard-ecom-widgets.js') }}"></script>
			@endsection