@extends('backend_layout.app')	
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item active">Product Detail</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
		<div class="card card-accent-theme ">
            <div class="card-body">
                <div class="card w-100">
                    <div class="card-body">
                        <h5 class="card-title">{{$item_details->title}}</h5>
                    </div>
                    <!-- Product basic details -->
                    <div class="row">
                    	<div class="col-md-12">
                    		<div class="row">
                    			<div class="col-md-4">
                    				@if($item_details->picture_details	 != '')
                    					<img src="{{$item_details->image}}" alt="Product image" />
                    				@elseif($item_details->image != '' && file_exixt($item_details->image))
                    					<img src="{{$item_details->image}}" alt="Product Image" />
                    				@else
                    					&nbsp;&nbsp;&nbsp;&nbsp;<img src="http://via.placeholder.com/100x100" />
                    				@endif
                    			</div>
                    			<div class="col-md-8">
                    				<div class="row">
                    					<div class="col-md-4">
                    						<label><b>Product Price :</b></label>
                    					</div>
                    					<div class="col-md-8">
                    						<lable>{{$item_details->current_price}}</lable>
                    					</div>
                    				</div>
                    				<div class="row">
                    					<div class="col-md-4">
                    						<label><b>Condition:</b></label>
                    					</div>
                    					<div class="col-md-8">
                    						<label>{{$item_details->condition_display_name}}</label>
                    					</div>
                    				</div>
                    				<div class="row">
                    					<div class="col-md-4">
                    						<label><b>Shipping:</b></label>
                    					</div>
                    					<div class="col-md-8">
                    						<label>{{$item_details->ship_to_locations}}</label>
                    					</div>
                    				</div>
                    				<div class="row">
                    					<div class="col-md-4">
                    						<label><b>Delivery:</b></label>
                    					</div>
                    					<div class="col-md-8">
                    						<label>Varies</label>
                    					</div>
                    				</div>
                    				<div class="row">
                    					<div class="col-md-4">
                    						<label><b>Payments:</b></label>
                    					</div>
                    					<div class="col-md-8">
                    						<label>{{$paymentMethod}}</label>
                    					</div>
                    				</div>
                    				<div class="row">
                    					<div class="col-md-4">
                    						<label><b>Return Policy:</b></label>
                    					</div>
                    					<div class="col-md-8">
                    						<label>{{$item_details->return_within}}</label>
                    					</div>
                    				</div>
                    			</div>
                    		</div>
                    	</div>
                    </div><br><br>
              		<!-- Starts Description -->
              		<div class="card card-accent-theme">
              			<div class="card-body">
              				<div class="card-header">
              					<h6>Item Specifics</h6>
              				</div>
              				<div class="card-body">
              					<div class="row">
              						@foreach($item_specifics as $item)
				                    <div class="col-md-6">	
				                    	<div class="row">
				                    		<div class="col-md-6">
				                    			<label><b>{{$item->variation}} : </b></label>
				           					</div>
				                    		<div class="col-md-6">
				                    			<label>{{$item->value}}</label><br>
				                    		</div>
				                    	</div>
				                    </div>
				                    @endforeach
              					</div>
              				</div>
              			</div><br>
                    <!-- Starts from Return Policy -->
                    <div class="card card-accent-theme">
                    	<div class="card-body">
           					<div class="card-header">
           						<h6>Return Policy</h6>
           					</div>
           					<div class="card-body">
           						<div class="row">
			                		<div class="col-md-6">
			                			<div class="row">
			                				<div class="col-md-6">
			                					<label><b>Return Accepted?: </b></label>
			                				</div>
			                				<div class="col-md-6">
			                					<label>{{$item_details->returns_accepted_option}}</label>
			                				</div>
			                			</div>
			                			<div class="row">
			                				<div class="col-md-6">
			                					<label><b>Return Days:</b></label>
			                				</div>
			                				<div class="col-md-6">
			                					<label>{{$item_details->return_within}}</label>
			                				</div>
			                			</div>
			                			<div class="row">
			                				<div class="col-md-6">
			                					<label><b>Refund will be given as:</b></label>
			                				</div>
			                				<div class="col-md-6">
			                					<label>{{$item_details->refund_type}}</label>
			                				</div>
			                			</div>
			                			<div class="row">
			                				<div class="col-md-6">
			                					<label><b>Return shipping will be paid by:</b></label>
			                				</div>
			                				<div class="col-md-6">
			                					<label>{{$item_details->return_paid_by}}</label>
			                				</div>
			                			</div>
			                		</div>
			                		<div class="col-md-6">
			                			<div class="row">
			                				<div class="col-md-4">
			                					<label><b>Return policy detail:</b></label>
			                				</div>
			                				<div class="col-md-8">
			                					<label>{{$item_details->return_policy_detail}}</label>
			                				</div>
			                			</div>
			                			<div class="row">
			                				<div class="col-md-4">
			                					<label><b>Restocking chargable fee:</b></label>
			                				</div>
			                				<div class="col-md-8">
			                					<label>{{$item_details->restock_fee}}</label>
			                				</div>
			                			</div>
			                		</div>
			                	</div>
           					</div>
                    	</div>
                    </div><br>
                    <!-- Start shipping Detail policy -->
                    <div class="card card-accent-theme">
                    	<div class="card-body">
                    		<div class="card-header">
                    			<h6>Shipping Details</h6>
                    		</div><br>
                    		<div class="catd-body">
                    			<div class="row">
	                    			<div class="col-md-6">
	                    				<h6>Domestic Shipping</h6>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Shiping service:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->shipping_service}}</label>
	                    					</div>
	                    				</div>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Shipping Type:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->shipping_type}}</label>
	                    					</div>
	                    				</div>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Shipping Cost $:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->shipping_service_cost}}</label>
	                    					</div>
	                    				</div>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Offer for local pickup:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->offer_local_pickup}}</label>
	                    					</div>
	                    				</div>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Local pickup fees:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->local_pickup_fee}} </label>
	                    					</div>
	                    				</div>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Handling time:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>50 $</label>
	                    					</div>
	                    				</div>
	                    			</div>
	                    			<div class="col-md-6">
	                    				<h6>International Shipping</h6>
	                    				@if(empty($item_details->international_shipping_service) && empty($item_details->international_cost))
	                    					<label>No international shipping available</label>
	                    				@else
	                    					<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Shipping Services:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->international_shipping_service}}</label>
	                    					</div>
	                    				</div>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Shipping cost:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->international_cost}}</label>
	                    					</div>
	                    				</div>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Handling cost:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->internationl_handling_cost}} </label>
	                    					</div>
	                    				</div>
	                    				@endif
	                    			</div>
	                    		</div>
                    		</div>
                    	</div>
                    </div><br>
                    <!-- Payment options -->
                    <div class="card card-accent-theme">
                    	<div class="card-body">
                    		<div class="card-header">
                    			<h6>Payment Details</h6>
                    		</div>
                    		<div class="card-body">
                    			<div class="row">
	                    			<div class="col-md-6">
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Payment method:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$paymentMethod}}</label>
	                    					</div>
	                    				</div>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Email for receiving payment:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->paypal_email_address}}</label>
	                    					</div>
	                    				</div>
	                    				<div class="row">
	                    					<div class="col-md-5">
	                    						<label><b>Payment Instruction:</b></label>
	                    					</div>
	                    					<div class="col-md-7">
	                    						<label>{{$item_details->payment_instruction}}</label>
	                    					</div>
	                    				</div>
	                    			</div>
                    				<div class="col-md-6"></div>
                    			</div>
                    		</div>
                    	</div>
                    </div>  
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
@section('footer_script')
@endsection