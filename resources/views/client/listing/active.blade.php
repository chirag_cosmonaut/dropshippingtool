@extends('backend_layout.app') 
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">Listing</a>
    </li>
    <li class="breadcrumb-item active">Active Listing</li>
</ol>

<div class="container-fluid">
    <div class="animated fadeIn">
        @if(session('flash_message'))
        <div class="alert alert-success">
            {!! session('flash_message') !!}
        </div>
        @endif
        <div class="row">
            <div class="col-md-4">
                <h3>Active Listing</h3>
            </div>
            <div class="col-md-8">
                <a href="#" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="#change_listings">Bulk Change</a>
                <a href="#" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="#listing_title">Change Titles</a>
                <a href="#" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="#filter">Filter</a>
                <a href="#" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="#col_customize">ColumnsCustomize</a>
                <a href="{{ url('active-listing/import') }}" class="btn btn-dark waves-effect text-left">Sync With Ebay</a>
                <a href="{{ url('get_price') }}" class="btn btn-dark waves-effect text-left">Get Price</a>
            </div>
        </div><br>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-accent-theme">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table id="example" class="table table-hover dataTable  w-full" data-plugin="dataTable" width="100%">
                                <thead>
                                    <tr>
                                            <!-- <th>
                                                    <input type="checkbox" class="form-control" name="bulk" id="bulk" />
                                            </th> -->
                                        <th>Edit</th>
                                        <th>
                                            <img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Id
                                        </th>
                                        <th>Source Id</th>
                                        <th>Picture</th>
                                        <th>Title</th>
                                        <th>QT</th>
                                        <th>
                                            <img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png"><br>Sell Price
                                        </th>
                                        <th>Source Price</th>
                                        <th>
                                            <img src="https://autodstools.com/neon_dashboard/assets/images/001-sign.png" style="max-width: 100%; max-height:40px; min-width:30px" width="50">
                                        </th>
                                        <th>
                                            <img src="https://autodstools.com/neon_dashboard/assets/images/004-medical.png" style="max-width: 100%; max-height:40px; min-width:30px">
                                        </th>
                                        <th>
                                            <img src="https://autodstools.com/neon_dashboard/assets/images/007-binoculars.png" style="max-width: 100%; max-height:40px; min-width:30px">
                                        </th>
                                        <th>
                                            <img src="https://autodstools.com/neon_dashboard/assets/images/005-coin.png" style="max-width: 100%; max-height:40px; min-width:30px">
                                        </th>
                                        <th>Upload Date</th>
                                        <th>Days Left</th>
                                        <th>OOS Days</th>
                                        <th>DWS</th>
                                        <th>Status</th>
                                    </tr>
                                </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


<div class="modal fade bs-example-modal-default_active" id="listing_title" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
                <h6 class="modal-title" style="margin: 12px" >Change Listings titles</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="col-md-12">
                    <p>There is not any selected item</p>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade bs-example-modal-default"  id="filter" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" >Filter all your active listings</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-2">
                        <label class="form-control-label"><strong>Users</strong></label>
                    </div>
                    <div class="col-md-5">
                        <select class="form-control" id="selectUSer" name="selectUser">
                            <option value="all user">All User</option>
                            <option value="">ABC</option>
                            <option value="">XYZ</option>
                        </select>
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label" for="text-input"><strong>Quantity</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"> 
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label"> To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Sold</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label control="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div> 
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label"> To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="   form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Sell Price</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label"> To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Watchers</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label"> To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Views</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label"> To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                </div> 
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Break Even</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label"> To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                </div> 
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Tag</strong></label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" id="text-input" name="text-input" class="form-control" placeholder="Text">
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Stock</strong></label>
                            </div>
                            <div class="col-md-10">
                                <select id="select" name="select" class="form-control">
                                    <option value="0">Please select</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Auto Ordering</strong></label>
                            </div>
                            <div class="col-md-10">
                                <select id="select" name="select" class="form-control">
                                    <option value="0">Please select</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-control-label"><strong>Price Monitoring</strong></label>
                            </div>
                            <div class="col-md-9">
                                <select id="select" name="select" class="form-control">
                                    <option value="0">Please select</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Profit %</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label"> To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Profit $</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Total Profit $</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label"> To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Update Date</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>No Sold Between</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Sold Between</strong></label>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">From:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                            <div class="col-md-5">
                                <label class="form-control-label">To:</label>
                                <div class="input-group bootstrap-touchspin">
                                    <span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span>
                                    <input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;">
                                    <span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span>
                                    <span class="input-group-btn-vertical"></span>
                                </div>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-12">
                                <input type="checkbox" id="checkbox1" name="checkbox1" value="option1"><strong>&nbsp;&nbsp;Listings With No Tags</strong>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-control-label"><strong>Allow marketplace</strong></label>
                            </div>
                            <div class="col-md-9">
                                 <select id="select" name="select" class="form-control">
                                    <option value="0">Please select</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-3">
                                <label class="form-control-label"><strong>Stock Monitoring</strong></label>
                            </div>
                            <div class="col-md-9">
                                <select id="select" name="select" class="form-control">
                                    <option value="0">Please select</option>
                                    <option value="1">Option #1</option>
                                    <option value="2">Option #2</option>
                                    <option value="3">Option #3</option>
                                </select>
                            </div>
                        </div><br>
                        <div class="row">
                            <div class="col-md-2">
                                <label class="form-control-label"><strong>Supplier</strong></label>
                            </div>
                            <div class="col-md-10">
                                <input type="text" id="text-input" name="text-input" class="form-control" placeholder="Text">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save Change</button>
            </div>
        </div>
    </div>
</div>



<div class="modal fade bs-example-modal-default_active" id="col_customize" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
                <h6 class="modal-title" style="margin: 12px" >Customize columns of active listings table</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Edit
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Sell ID
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Source ID
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked"> Picture
                    </div>
                </div>
            </div>

            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Title
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Quantity
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Sell Price
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Source Price
                    </div>
                </div>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Sold
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Views
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Watchers
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Profit
                    </div>
                </div>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Upload Date
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Days Left
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">OOS Days
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">DWS
                    </div>
                </div>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1">Tag
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1">Note
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Status
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1"> Shop Name
                    </div>
                </div>
            </div>


            <div class="modal-body">
                <div class="row">
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1">Supplier
                    </div>
                    <div class="col-md-3">
                        <input type="checkbox" id="checkbox1" name="checkbox1" value="option1">variant Id
                    </div>

                </div>
            </div>

            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>

        </div>
        <!-- /.modal-content -->
    </div>
</div>





<!-- sample modal content Notification -->
<div class="modal fade bs-example-modal-default" id="change_listings" tabindex="-1" role="dialog"  aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog modal-lg modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
                <h6 class="modal-title" style="margin: 12px" >Change listings</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <ol style="display: none;" class="breadcrumb bc-colored bg-theme" id="breadcrumb">
                        <li class="breadcrumb-item ">
                            <a href="">Home</a>
                        </li>
                        <li class="breadcrumb-item">
                            <a href="#">Listing</a>
                        </li>
                        <li class="breadcrumb-item active">Monitors</li>
                    </ol>


                    <div class="container-fluid">
                        <ul class="nav nav-tabs" id="myTab" role="tablist">
                            <li class="nav-item">
                                <a class="nav-link active" id="price-monitor" data-toggle="tab" href="#price_monitor" role="tab" aria-controls="home" aria-selected="true">Bulk Changes</a>
                            </li>
                            <li class="nav-item">
                                <a class="nav-link" id="orders-monitor" data-toggle="tab" href="#orders_monitor" role="tab" aria-controls="profile" aria-selected="false">All Bulk changed items</a>
                            </li>
                        </ul>
                        <div class="tab-content" id="myTabContent">
                            <div class="tab-pane fade show active" id="price_monitor" role="tabpanel" aria-labelledby="home-tab" style="height: 200px;
                                 overflow-y: scroll;">
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>New Break Even:</strong>
                                            <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>New Addition $ Profit:</strong>
                                            <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>New Additional % Profit:</strong>
                                            <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>New Quantity:</strong>
                                            <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                                        </div>

                                    </div>
                                </div>


                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>New Price:</strong>
                                            <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Change Current $ Profit By X.X$(+-):	</strong>
                                            <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                                        </div>

                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Change Breakeven By X.X %(+-):</strong>
                                            <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                                        </div>

                                    </div>

                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Change Additional % Profit By X.X$(+-):</strong>
                                            <div class="input-group bootstrap-touchspin"><span class="input-group-addon bootstrap-touchspin-prefix" style="display: none;"></span><input id="demo_vertical" type="number" value="" name="demo_vertical" class="form-control" style="display: block;"><span class="input-group-addon bootstrap-touchspin-postfix" style="display: none;"></span><span class="input-group-btn-vertical"></span></div>
                                        </div>

                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Change Private Listings</strong>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <select id="select" name="select" class="form-control">
                                                        <option value="0">Please select</option>
                                                        <option value="1">Option #1</option>
                                                        <option value="2">Option #2</option>
                                                        <option value="3">Option #3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end inside card-body -->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Template</strong>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <select id="select" name="select" class="form-control">
                                                        <option value="0">Please select</option>
                                                        <option value="1">Option #1</option>
                                                        <option value="2">Option #2</option>
                                                        <option value="3">Option #3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Change Tag</strong>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <select id="select" name="select" class="form-control">
                                                        <option value="0">Please select</option>
                                                        <option value="1">Option #1</option>
                                                        <option value="2">Option #2</option>
                                                        <option value="3">Option #3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end inside card-body -->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <input type="checkbox" id="checkbox1" name="checkbox1" value="option1"><strong>Remove Tag</strong>

                                        </div>

                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <input type="checkbox" id="checkbox1" name="checkbox1" value="option1">
                                            <strong>Change Policy</strong>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <input type="checkbox" id="checkbox1" name="checkbox1" value="option1"><strong>Change Location</strong>

                                        </div>

                                    </div>
                                </div>




                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Stock Monitoring</strong>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <select id="select" name="select" class="form-control">
                                                        <option value="0">Please select</option>
                                                        <option value="1">Option #1</option>
                                                        <option value="2">Option #2</option>
                                                        <option value="3">Option #3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end inside card-body -->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Auto Ordering</strong>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <select id="select" name="select" class="form-control">
                                                        <option value="0">Please select</option>
                                                        <option value="1">Option #1</option>
                                                        <option value="2">Option #2</option>
                                                        <option value="3">Option #3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>



                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Marketplace</strong>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <select id="select" name="select" class="form-control">
                                                        <option value="0">Please select</option>
                                                        <option value="1">Option #1</option>
                                                        <option value="2">Option #2</option>
                                                        <option value="3">Option #3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                        <!-- end inside card-body -->
                                    </div>

                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <strong>Price Monitoring</strong>
                                            <div class="form-group row">
                                                <div class="col-md-12">
                                                    <select id="select" name="select" class="form-control">
                                                        <option value="0">Please select</option>
                                                        <option value="1">Option #1</option>
                                                        <option value="2">Option #2</option>
                                                        <option value="3">Option #3</option>
                                                    </select>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="card-body">
                                            <input type="checkbox" id="checkbox1" name="checkbox1" value="option1">
                                            <strong>Apply to All Listings</strong>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="orders_monitor" class="tab-pane fade in">
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="card card-accent-theme">
                                            <div class="card-body">
                                                <table class="table" id="example1">
                                                    <thead>
                                                        <tr>
                                                            <th>Upload ItemID</th>
                                                            <th>Updated Date</th>
                                                            <th>Total Updated</th>
                                                            <th>Items</th>
                                                            <th>Successfully Updated</th>
                                                            <th>Failed To Upload</th>
                                                            <th>Action</th>
                                                            <th>Status</th>
                                                            <th>Upload Details</th>
                                                            <th>Updated Items Details</th>
                                                        </tr>
                                                    </thead>
                                                    <tbody>
                                                        <tr>
                                                            <td>
                                                                <a href="javascript:void(0)">91888</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">2018-03-16 16:20:36</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">2</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)">2</a>
                                                            </td>
                                                            <td>
                                                                <a href="javascript:void(0)"><div style="text-align:center;color:red;">0</div></a>
                                                            </td>
                                                            <td>Herman Beck</td>
                                                            <td>
                                                                $45.00
                                                            </td>
                                                            <td><div style="text-align:center; color:green;">Finished</div></td>
                                                            <td>
                                                                454
                                                            </td>
                                                            <td>EN</td>
                                                        </tr>
                                                    </tbody>
                                                </table>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>


                    <div class="modal-footer">
                        <button type="button" class="btn btn-danger" data-dismiss="modal">End Listing</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Remove from monitors</button>
                        <button type="button" class="btn btn-warning" data-dismiss="modal">Relist</button>
                        <button type="button" class="btn btn-default waves-effect text-left" data-dismiss="modal">Close</button>
                        <button type="button" class="btn btn-primary">Save Change</button>
                    </div>

                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->

@endsection

@section('footer_script')

<script type="text/javascript">
    $(function () {
        $('#example').DataTable({
            processing: true,
            //serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            scrollX: true,
            pageLength: 25,
            bStateSave: true,
            dom: 'Bfrtip',
            buttons: [
                'copy', 'csv', 'excel', 'pdf', 'print'
            ],
            ajax: "{!! route('activelisting.data') !!}",
            columns: [
                /*{ data: 'bulk',"searchable":false,name:'bulk' },*/
                {data: 'edit', "searchable": false, name: 'edit'},
                {data: 'sell_id', "searchable": true, name: 'sell_id'},
                {data: 'source_id', "searchable": true, name: 'source_id'},
                {data: 'picture', "searchable": false, name: 'picture'},
                {data: 'title', "searchable": true, name: 'title'},
                {data: 'quantity_available', "searchable": true, name: 'quantity_available'},
                {data: 'price', "searchable": true, name: 'price'},
                {data: 'source_price', "searchable": true, name: 'source_price'},
                {data: 'amount_sold', "searchable": true, name: 'amount_sold'},
                {data: 'hit_count', "searchable": true, name: 'hit_count'},
                {data: 'watch_count', "searchable": true, name: 'watch_count'},
                {data: 'profit', "searchable": true, name: 'profit'},
                {data: 'upload_date', "searchable": true, name: 'upload_date'},
                {data: 'days_left', "searchable": true, name: 'days_left'},
                {data: 'oos_day', "searchable": true, name: 'oos_day'},
                {data: 'dws', "searchable": true, name: 'dws'},
                {data: 'status', "searchable": false, name: 'status'},
            ]

        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
            }
        });
    })
</script>
@endsection