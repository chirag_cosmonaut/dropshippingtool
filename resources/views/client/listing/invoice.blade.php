@extends('backend_layout.app') 

@section('content')
<link rel="stylesheet" href="admin-assets/libs/tables-datatables/dist/datatables.min.css"/>
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">Invoice</a>
    </li>
    <li class="breadcrumb-item active">Invoice Listing</li>
</ol>


<div class="container-fluid">
    <div class="row">
        <div class="col-md-12">
            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-3">
                    <select style="width: 265px;" id="invoices_months" class="selectpicker form-control"><option value="{&quot;month&quot;:&quot;4/2018&quot;,&quot;id&quot;:37891}">4/2018(Started at: 01/4/2018)</option><option value="{&quot;month&quot;:&quot;3/2018&quot;,&quot;id&quot;:32943}">3/2018(Started at: 15/3/2018)</option></select>
                </div>
                <div class="col-md-3">
                    <span style="font-size: large; color: #123516;">Invoice Status: </span>
                    <span id="invoice_status" style="font-size: large; font-weight: 600; color: #123516;">New</span>
                </div>  
                <div class="col-md-3"></div>
            </div>          
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-6">
            <table id="example1" class="table table-hover dataTable table-striped w-full" data-plugin="dataTable" width="100%">
                <thead>
                    <tr>
                        <th>Item</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Total Price</th>

                    </tr>
                </thead>
                <tfoot>
                    <tr>
                        <th>Item</th>
                        <th>Unit Price</th>
                        <th>Quantity</th>
                        <th>Total Price</th>
                    </tr>
                </tfoot>
                <tbody>
                     <tr>
                        <td>Auto Order</td>                                                                              
                        <td>0.3 $</td>
                        <td>390</td>
                        <td>117.000 $</td>
                    </tr>
                    <tr>
                        <td>Discount - Auto Orders Expert DropShipper Plan</td>                                                                              
                        <td>0.3 $</td>
                        <td>110</td>
                        <td>-33.000 $</td>
                    </tr>
                    <tr>
                        <td>Expert DropShipper Plan</td>                                                                              
                        <td>114.99 $</td>
                        <td>1</td>
                        <td>114.990 $</td>
                    </tr>
                </tbody>
            </table>
        </div>
        <div class="col-md-3">
            
        </div>
    </div>
    <div class="row">
        <div class="col-md-3">
            
        </div>
        <div class="col-md-6">
            <div class="row" style="background-color: #d1f9f7; border: 2px solid #33acbf; border-radius: 10px; -moz-border-radius: 10px;">
                <div class="col-md-3">
                    Total Discount:
                </div>
                <div class="col-md-3">
                    <span id="discount_price">-$33.000</span>
                </div>
                <div class="col-md-6">
                    <span id="discount_desc">110 Auto orders credits (Expert DropShipper Plan). </span>
                </div>
                <br>
            </div>
        </div>
        <div class="col-md-3">
            
        </div>
        
    </div>    
    <div class="row">&nbsp;</div>
    
    <div class="row">
        <div class="col-md-3">
            
        </div>
    <div class="col-md-6">
            <div class="row" style="background-color: #f9e4a6; border: 2px solid #bf891d; border-radius: 10px; -moz-border-radius: 10px;">
                <div class="col-md-3">
                    Note:
                </div>
                <div class="col-md-9">
                    <span id="note_span"><!-- Changed the subscription plan to: Expert DropShipper at the middle of the month. --> </span>
                </div>
                <br>
                <br>
            </div>
        </div>
    <div class="col-md-3">
            
        </div>
    </div>
    <div class="row">&nbsp;</div>
    <div class="row">
        <div class="col-md-3">
            
        </div>
    <div class="col-md-6">
        <div align="center">
        <div class="col-md-6 col-md-offset-6" style="background-color: #dbf9de; border: 2px solid #93bf97; border-radius: 15px; -moz-border-radius: 15px;">
            <div align="center" style="font-size: large; font-weight: 600; color: #123516;">
                Total Sum
            </div>
            <div align="center" id="invoice_sum" style="font-size: large; font-weight: 600; color: #123516;">$114.990</div>
        </div>
        </div>
   </div>
         <div class="col-md-3">
            
        </div>
   </div>
    
</div>
@endsection
@section('footer_script')
<script src="{{  URL::asset('admin-assets/libs/tables-datatables/dist/datatables.min.js') }}"></script>
<!--<script src="{{ url('admin-assets/js/table-datatable-example.js') }}"></script>-->
<script>
$(document).ready(function () {
    $('#example1').DataTable({
        "paging": false,
        "ordering": false,
        "info": false,
        "searching": false
    });
});
</script>   
@endsection