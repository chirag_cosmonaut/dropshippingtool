@extends('backend_layout.app') 

@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
	<li class="breadcrumb-item ">
		<a href="">Home</a>
	</li>
	<li class="breadcrumb-item">
		<a href="#">Listing</a>
	</li>
	<li class="breadcrumb-item active">Untracked Listing</li>
</ol>


<div class="container-fluid">
    
    @if($flash = session('flash_message'))  
        <div class="alert alert-success" role="alert">
            {{ $flash }}
        </div>
    @endif
    @if($flash = session('error_message'))  
        <div class="alert alert-danger" role="alert">
            {{ $flash }}
        </div>
    @endif
	<div class="animated fadeIn">
		<h3>Untracked Listing</h3>
		<br/>
		<a href="javascript:void(0);" class="btn btn-dark waves-effect text-left" data-toggle="modal" data-target="#change_listings"><i class="fa fa-file-o"></i>Update ASINS via file
		</a>
		<div class="row">

			<div class="col-md-12">
				<div class="card card-accent-theme">
					<div class="card-body">
						<div class="table-responsive">
							<table class="table" id="example">
								<thead>
									<tr>
										<th>End Listing</th>
										<th>eBay ID</th>
										<th><img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png" style="max-width: 100%; max-height:40px; min-width:30px" width="50"></th>
										<th>Sell Site</th>
										<th>Picture</th>
										<th>Title</th>
										<th>ASIN</th>
										<th><img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png" style="max-width: 100%; max-height:40px; min-width:30px"></th>
										<th>Source Site</th>
										<th>Status</th>
									</tr>
								</thead>
								
								<tfoot>
									<th>End Listing</th>
									<th>eBay ID</th>
									<th><img src="https://autodstools.com/neon_dashboard/assets/images/ebay_icon_new.png" style="max-width: 100%; max-height:40px; min-width:30px" width="50"></th>
									<th>Sell Site</th>
									<th>Picture</th>
									<th>Title</th>
									<th>ASIN</th>
									<th><img src="https://autodstools.com/neon_dashboard/assets/images/amazon_icon_new2.png" style="max-width: 100%; max-height:40px; min-width:30px"></th>
									<th>Source Site</th>
									<th>Status</th>
								</tfoot>
							</table>
						</div>
					</div>
					<!-- end card-body -->
				</div>
				<!-- end card -->
			</div>
			<!-- end col -->
		</div>
		<!-- end row -->
	</div>
	<!-- end animated fadeIn -->
</div>
<!-- end container-fluid -->
<!-- sample modal content Notification -->
<div class="modal fade bs-example-modal-default" id="change_listings" tabindex="-1" role="dialog"  aria-hidden="true"
     style="display: none;">
    <div class="modal-dialog modal-md modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
                <h6 class="modal-title" style="margin: 12px" >Update ASINS via file</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                	<form action="{{ route('uploadASINFile.data') }}" method="post" enctype='multipart/form-data'>
                		{{ csrf_field() }}
	                    <div class="container-fluid">
	                        <div class="tab-content" id="myTabContent">
	                            <input type="file" name="txt_doc" id="txt_doc"> Upload Txt file
	                        </div>
	                    </div>
	                    <div class="modal-footer">
	                    	<button type="submit" class="btn btn-primary">Upload</button>
	                        <button type="button" class="btn btn-default waves-effect text-left" data-dismiss="modal">Close</button>
	                    </div>
	                </form>
                </div>
                <!-- /.modal-content -->
            </div>
            <!-- /.modal-dialog -->
        </div>
        <!-- /.modal -->
@endsection

@section('footer_script')

<script type="text/javascript">

	$(function () {
        $('#example').DataTable({
            processing: true,
            //serverSide: true,
            responsive: true,
            ordering: false,
            destroy: true,
            scrollX: true,
            pageLength: 25,
            bStateSave: true,
            dom: 'Bfrtip',
            ajax: "{!! route('untrackedlisting.data') !!}",
            columns: [
                {data: 'end', "searchable": false, name: 'end'},
                {data: 'sell_id', "searchable": true, name: 'sell_id', },
                {data: 'sell', "searchable": true, name: 'sell', },
                {data: 'sell_site', "searchable": true, name: 'sell_site'},
                {data: 'picture', "searchable": false, name: 'picture'},
                {data: 'title', "searchable": true, name: 'title'},
                {data: 'asin', "searchable": true, name: 'asin'},
                {data: 'amazon', "searchable": true, name: 'amazon'},
                {data: 'source_site', "searchable": true, name: 'source_site'},
                {data: 'status', "searchable": false, name: 'status'},
            ]

        });
        $.ajaxSetup({
            headers: {
                'X-CSRF-Token': '{{ csrf_token() }}'
            }
        });
    })
</script>
@endsection