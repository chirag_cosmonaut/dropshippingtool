<!-- Modal for change subscription plan  -->
<!-- <div class="modal fade bs-example-modal-default" id="subscriptionPlan" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
                <h6 class="modal-title" style="margin: 12px">Please choose a subscription plan to continue.</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <p style="color: red; text-align: center;padding: 15px;"><strong>Choose a plan or choose features by yourself by clicking on 'Let Me Choose' button down there</strong></p>
                <div class="modal-body">
                        <div class="container-fluid">
                            <ul class="nav nav-tabs" id="myTab" role="tablist">
                                <li class="nav-item">
                                    <a class="nav-link active" id="price-monitor" data-toggle="tab" href="#price_monitor" role="tab" aria-controls="home" aria-selected="true">With Auto-Order</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" id="orders-monitor" data-toggle="tab" href="#orders_monitor" role="tab" aria-controls="profile" aria-selected="false">Without Auto-Order</a>
                                </li>
                            </ul>
                            <div class="tab-content" id="myTabContent">
                                <div class="tab-pane fade show active" id="price_monitor" role="tabpanel" aria-labelledby="home-tab">
                                <div class="card-body">
                                    <div class="row">  
                                        <div class="col-md-6">
                                            <p style="text-align:left;">Purchase</p>
                                            <h1 style="text-align:left;">2400</h1>
                                            <p style="text-align:left;">Listings</p> 
                                        </div>
                                        <div class="col-md-6">
                                             <p style="text-align:left;">Expert DropShipper</p>
                                            <h1 style="text-align:left;">$114.99/ Month</h1> 
                                        </div> 
                                    </div>
                                    <div class="row"> 
                                        <div class="col-md-2"></div> 
                                        <div class="col-md-8">
                                            <input type="range" min="1" max="100" value="50" class="slider" id="myRange" style="width:450px;">
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div>
                                    <br><br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label><i class="fa fa-user"></i> Auto Order Credits: 110</label>
                                            <label>0.3$ per extra auto order</label><br>
                                            <label><i class="fa fa-user"></i> Price monitot</label><br>
                                            <label><i class="fa fa-user"></i> Profitable monitor</label><br>
                                            <label><i class="fa fa-user"></i> Orders processor</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label><i class="fa fa-user"></i> Customer Service</label><br>
                                            <label><i class="fa fa-user"></i> Lister + Bulk lister</label>
                                            <label><i class="fa fa-check"></i> Auto tracking numbers update</label>
                                            <label><i class="fa fa-check"></i> Auto 3 messages to buyers</label>
                                            <label><i class="fa fa-user"></i> Ebay Accounts:5</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div id="orders_monitor" class="tab-pane fade in">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <p style="text-align:left;">Purchase</p>
                                            <h1 style="text-align:left;">100</h1>
                                            <p style="text-align:left;">Listings</p>
                                        </div>
                                        <div class="col-md-6">
                                            <p style="text-align:left;">Just Started</p>
                                            <h1 style="text-align:left;">$7.99 / Month</h1>
                                        </div>
                                    </div>
                                    <div class="row"> 
                                        <div class="col-md-2"></div> 
                                        <div class="col-md-8">
                                            <input type="range" min="1" max="100" value="50" class="slider" id="myRange" style="width:450px;">
                                        </div>
                                        <div class="col-md-2"></div>
                                    </div><br><br>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <label><i class="fa fa-user"></i> Price monitot</label><br>
                                            <label><i class="fa fa-user"></i> Profitable monitor</label><br>
                                            <label><i class="fa fa-user"></i> Customer Service</label>
                                        </div>
                                        <div class="col-md-6">
                                            <label><i class="fa fa-user"></i> Ebay Accounts:5</label><br>
                                            <label><i class="fa fa-user"></i> lister + Bulk lister</label>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <button type="button" class="btn btn-dark waves-effect text-left" data-dismiss="modal">Let me choose</button>
                </div>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary waves-effect text-left" data-dismiss="modal">Close</button>
            <button type="button" class="btn btn-primary">Update Subscription</button>
        </div>
    </div>
</div> -->


<!-- Modal for add ebay accounts -->
<div class="modal fade bs-example-modal-default"  id="add_ebay_account" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-lg modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Connect Ebay User</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-body">
                            <div class="row">
                                <p class="text-center">
                                    <strong> To Start monitor your ebay user, you must connect it to the monitor.</strong> 
                                </p>

                                <p class="text-center">
                                    <strong>Click on "Connect User" to connect your ebay user to the monitor.</strong>
                                </p>                                
                            </div>
                            <div class="row">
                                <form action="{{ route('ebay_channel.getSessionId') }}" method="post">
                                    <div class="col-md-8">
                                        <select id="site" name="site" class="selectpicker form-control">
                                            <option value="0">ebay US</option> 
                                        </select>
                                    </div>
                                    <div class="col-md-4">
                                        <button type="button" class="btn btn-success text-left" data-dismiss="modal">Connect User</button>
                                    </div>
                                </form>
                            </div>
                        </div></br>
                       
                        <div class="col-md-12">
                            <p style="color: blue;">Your information is secured with top secure protocols (HTTP/SSL)</p>
                            <p style="color: blue;">All your user information stays private and will never be shared with anyone.</p> 
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger text-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-theme">Save Change</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal for add VA accounts -->
<div class="modal fade bs-example-modal-default"  id="va_account" tabindex="-1" role="dialog"  aria-hidden="true">
    <div class="modal-dialog modal-lg modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Add VA Account</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-10">
                        <div class="form-group">
                            <input type="text" id="psw" class="form-control" data-validate="required" placeholder="" value="">
                        </div>
                    </div>
                    <div class="col-md-2">
                        <div class="form-group">
                            <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-4">
                        <strong>VA Name:</strong>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="text" id="va_name" class="form-control" data-validate="required" placeholder="" value="">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4">
                        <strong>VA Email:</strong>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="text" id="va_email" class="form-control" data-validate="required" placeholder="" value="">
                            </div>
                        </div> 
                    </div>
                    <div class="col-md-4">
                        <strong>VA Password</strong>
                        <div class="form-group row">
                            <div class="col-md-12">
                                <input type="Password" id="va_pwd" class="form-control" data-validate="required" placeholder="" value="">
                            </div>
                        </div>  
                    </div>
                </div><br>
                <div class="row">
                    <div class="col-md-6">
                        <div class="form-group">
                            <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Enable Dashboard 
                        </div>
                        <div class="form-group">
                            <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Enable Active Listings
                        </div>
                        <div class="form-group">
                            <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Enable Untracked Listings
                        </div>
                        <div class="form-group">
                            <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Enable Orders
                        </div>
                    </div>
                    <div class="col-md-6">
                       <div class="form-group">
                           <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Enable Customer Service
                       </div>
                       <div class="form-group">
                           <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Enable Uploader
                       </div>
                       <div class="form-group">
                           <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Enable Listings End
                       </div>
                       <div class="form-group">
                           <input type="checkbox" id="checkbox1" name="checkbox1" value="option1" checked="checked">Enable Remove from Monitor
                       </div> 
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal for keyword blacklist -->
<div class="modal fade bs-example-modal-default" id="keyword_blacklist" tabindex="-1" role="dialog"  aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
                <h6 class="modal-title" style="margin: 12px" >Keywords Blacklist</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="keyword-block">
                    <ul>
                        <li>guarantee</li>
                        <li>amazon</li>
                        <li>lifetime</li>
                        <li>call us</li>
                        <li>prime</li>
                        <li>contact us</li>
                        <li>Link below</li>
                        <li>get free</li>
                        <li>using code checkout and adding to cart free item </li>
                        <li>unconditionally</li>
                        <li>refund</li>
                    </ul>
                </div>
            </div>
           
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save Change</button>
            </div>
          
        </div>
        /.modal-content
    </div>
    .modal-dialog
</div>


<!-- Modal for Change Password -->
<div class="modal fade bs-example-modal-default"  id="change_password" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog  modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Change Password</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <strong>Current Password:</strong>
                <div class="form-group row">
                    <div class="col-md-12">
                       <input type="text" id="psw1" class="form-control" data-validate="required" placeholder="" value="">
                    </div>
                </div>
                <strong>New Password:</strong>
                <div class="form-group row">
                    <div class="col-md-12">
                       <input type="text" id="psw2" class="form-control" data-validate="required" placeholder="" value="">
                    </div>
                </div>
                <strong>Confirm New Password:</strong>
                <div class="form-group row">
                    <div class="col-md-12">
                       <input type="text" id="psw3" class="form-control" data-validate="required" placeholder="" value="">
                    </div><!--  -->
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                <button type="button" class="btn btn-primary">Save</button>
            </div>
        </div>
    </div>
</div>


<!-- Modal for block products -->
<div class="modal fade bs-example-modal-default" id="blocked_product" tabindex="-1" role="dialog"  aria-hidden="true"
    style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header" style="padding: 5px">
                <h6 class="modal-title" style="margin: 12px" >Blocked Products</h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <p style="margin: 12px"><strong>Current Blocked Products</strong></p>
                </div>
            </div>
            @if(count($blockProduct) > 0)
            <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
				    <div class="table-responsive">
					   <table class="table table-bordered">
						  <thead>
						      <tr>
						        <th>Item id</th>
						        <th>Action</th>
						      </tr>
						  </thead>
						  <tbody>
                            @foreach($blockProduct as $product)
                                <tr>
                                    <td>{{$product->item_id}}</td>
                                    <td><a href="#" data-toggle="tooltip" onclick="blockProducts({{$product->item_id}},'false')" data-original-title="Remove">
                                        <i class="fa fa-close text-danger"></i></a>
                                    </td>
                                </tr>
                            @endforeach                  
                          </tbody>
					   </table>
				    </div>
                </div>
                <div class="col-md-2"></div>
			</div>
            @else
                <div class="row">
                <div class="col-md-2"></div>
                <div class="col-md-8">
                    <div class="table-responsive">
                       <table class="table table-bordered">
                          <thead>
                              <tr>
                                <th>Item id</th>
                                <th>Action</th>
                              </tr>
                          </thead>
                          <tbody>
                            <tr>
                                <td colspan="2">No block products are available</td>
                            </tr>                 
                          </tbody>
                       </table>
                    </div>
                </div>
                <div class="col-md-2"></div>
            </div>
            @endif
            <div class="modal-body">
                <div class="row">
                    <p style="margin: 12px"><strong>New Blocked Products</strong></p>
                    <div class="col-md-3">
                    	<input type="text" id="blockedProductId" name="blockedProductId" class="form-control" data-validate="required" placeholder="Item id">
                    </div>
                    <div class="col-md-3">
                      <a href="javascript:;" onclick="blockProducts(blockedProductId.value,'true')" class="btn btn-success">Add</a>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>

<!-- Modal for default item specifics -->
<div class="modal fade bs-example-modal-default" id="default_specifics" tabindex="-1" role="dialog"  aria-hidden="true"
        style="display: none;">
        <div class="modal-dialog modal-dialog-centered-custom">
            <div class="modal-content">
                <div class="modal-header" style="padding: 5px">
                    <h6 class="modal-title" style="margin: 12px" >Add Item Specifics</h6>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <div class="row">
                       <div class="col-md-5">
                            <label>65 remaining</label>
                            <input type="text" id="spe_name1" class="form-control" data-validate="required" placeholder="Specifics Name" value="">
                        </div>
                        <div class="col-md-5">
                            <label>65 remaining</label>
                            <input type="text" id="spe_name2" class="form-control" data-validate="required" placeholder="temp6813@gmail.com" value="">
                        </div>
                        <div class="col-md-2">
                            <a href="#"><i class="glyphicon glyphicon-plus" style="font-size: 55px;color: green;margin-left:30px;margin: 15px;"></i></a>
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
                   <a class="btn btn-primary" href="javascript:;">Save Change</a>
                </div> 
            </div>
        </div>
</div>


<!-- Modal for Manage tags -->
<div class="modal fade bs-example-modal-default"  id="tags" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Tags</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card-body">
                            <div class="form-group row">
                                <div class="col-md-8">
                                   <select id="select" name="select" class="form-control">
                                        <option value="0">Kevin</option>
                                        <option value="1">Ordered</option>
                                        <option value="2">Shipped</option>
                                        <option value="3">Completed</option>
                                        <option value="3">Standby</option>
                                        <option value="3">Canceled</option>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button type="button" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Close</button>
               
            </div>
        </div>
    </div>
</div>