@extends('backend_layout.app')	

@section('content')
<link rel="stylesheet" href="{{ URL::asset('public/admin-assets/libs/tables-datatables/dist/datatables.min.css') }}"/>
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item">
        <a href="#">Settings</a>
    </li>
    <li class="breadcrumb-item active">Settings Listing</li>
</ol>

<div class="container-fluid">
    @if(session('flash_message'))
        <div class="alert alert-success">
            {!! session('flash_message') !!}
        </div>
    @endif

    <form role="form" method="post" action="{{ route('settings.update')}}">
        {{csrf_field()}}
        <input type="hidden" name="id" value="{{$account->id}}">
        <div class="row">
            <div class="col-md-6">
                <div class="card card-accent-theme" data-collapsed="0">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion" href="#collapse1">
                        <div class="card-header">
                            General
                        </div>
                        <div class="panel-options">
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div>
                    </div>
                    <div class="card-body" id="collapse1">
                        <div class="card-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-6">
                                     <h5 class="text-dark"><strong>Email Address</strong></h5>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" id="user_email" class="form-control" data-validate="required" placeholder="tnguyen2352@gmail.com" value="">
                                </div>
                            </div>
                            </br>
                            <div class="row">
                                <div class="col-md-6">
                                      <h5 class="text-dark"><strong>Subscription</strong></h5>
                                </div>
                                <div class="col-md-6 text-center btn-tool-bar">
                                   <a href="#" data-toggle="modal" class="btn btn-theme btn-round btn-sm custom-btn" data-target="#subscriptionPlan">Change Subscription Plan</a>
                                    <span class="subscription" id="subscription">Current Plan Expert Dropshipper</span>
                                </div>
                            </div>
                            </br>
                            <div class="row">
                                <div class="col-md-6">
                                     <h5 class="text-dark"><strong>Ebay Accounts</strong></h5>
                                </div>
                                <div class="col-md-6 text-center btn-tool-bar">
                                    <!-- <a href="javascript:;" onclick="jQuery('#modal-no-users-exist').modal({backdrop: true, keyboard: true, show: true}); jQuery('#finish_ebay_user_modal').show();" class="btn btn-primary form-control">Add Ebay Account</a> -->
                                    <a href="javasctipt:;" class="btn btn-theme btn-round btn-sm custom-btn" data-toggle="modal" data-target="#add_ebay_account">Add Ebay Account
                                    </a>
                                    <span class="description " id="current_ebay_accounts">
                                        Current Available Account: <strong>{{$accCount}}/5</strong>
                                    </span>
                                </div>
                            </div>
                            </br>
                            <div class="row">
                                <div class="col-md-6 ">
                                     <h5 class="text-dark"><strong>Add VA Account</strong></h5>
                                </div>
                                <div class="col-md-6 text-center btn-tool-bar">
                                    <a href="javasctipt:;" class="btn btn-theme btn-round  custom-btn" data-toggle="modal" data-target="#va_account" data-backdrop="static">Add VA Account
                                    </a>
                                    
                                </div>
                            </div>
                            </br>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="text-dark"><strong>Keywords Blacklist</strong></h5>
                                </div>
                                <div class="col-md-6 text-center btn-tool-bar">
                                    <a href="#" data-toggle="modal" class="btn btn-theme btn-round custom-btn" data-target="#keyword_blacklist">Keywords Blacklist</a>
                                </div>
                            </div>
                            </br>
                            <div class="row">
                                <div class="col-md-6">
                                      <h5 class="text-dark"><strong>Change Password</strong></h5>
                                </div>
                                <div class="col-md-6 text-center btn-tool-bar">
                                     <a href="javasctipt:;" class="btn btn-theme btn-round  custom-btn" data-toggle="modal" data-target="#change_password">Change Password
                                    </a>
                                    <!-- <button type="button" class="btn btn-primary form-control" data-toggle="modal" data-target="#change_password_modal">Change Password</button> -->
                                </div>
                            </div>
                            </br>
                            <div class="row">
                                <div class="col-md-6">
                                      <h5 class="text-dark"><strong>Multiple Account Protection</strong></h5>
                                    <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="If you have multiple Ebay accounts that are connected to each other, this feature will make sure you don't have the same products in more than 1 account." data-original-title="Auto Order"> <i class="fa fa-question-circle-o"></i></a>
                                </div>
                                <div class="col-md-6">
                                    <label><input type="checkbox" id="ebay_accounts_connected"></label>
                                </div>
                            </div> 
                        </div>
                    </div>
                    <div class="card-footer">
                        
                        <div class="pull-right">
                            <a class="btn btn-link" onclick="delete_account()">Delete Account</a>
                        </div>
                        
                    </div>
                </div>
                <div>
                    <a href="http://www.triplemars.com/" target="_blank">
                        <img id="purchase_gift_card" src="{{URL::asset('public/admin-assets/img/Amazon-Gift-Card-GEN85358.png') }}">
                    </a>
                    <div class="form-group default-padding" align="center">
                        <button type="submit" onclick="save_settings()" class="btn btn-success">Save Settings</button>
                    </div>
                </div>
            </div>
            <!-- Another Box Partition -->
            <div class="col-md-6">
                <div class="card card-accent-theme" data-collapsed="0">
                    <div class="panel-heading" data-toggle="collapse" data-parent="#accordion1" href="#collapse2">
                        <div class="card-header">
                            {{ $account->user_id }} Settings 

                        </div>
                        <div class="panel-options">
                            <a href="#" data-rel="collapse"><i class="entypo-down-open"></i></a>
                        </div>
                    </div>
                    
                    <div class="card-body" id="collapse2">
                        <div class="card-body" style="display: block;">
                            <div class="row">
                                <div class="col-md-6">
                                     <h5 class="text-dark"><strong>PayPal Email Address</strong></h5>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name ="email" value="{{$account->email}}" id="paypal_email" class="form-control" data-validate="required" placeholder="Email Address...">
                                    <span class="description">Used when uploading new products.</span>
                                </div>
                            </div></br>
                            <div class="row">
                                <div class="col-md-6">
                                     <h5 class="text-dark"><strong>Ebay Username</strong></h5>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="ebay_user_name" value="{{$account->user_id}}" id="ebay_username" class="form-control" data-validate="required">
                                </div>
                            </div></br>
                            <div class="row">
                                <div class="col-md-6">
                                     <h5 class="text-dark"><strong>Token</strong></h5>
                                </div>
                                <div class="col-md-6">
                                    <input type="text" name="token" value="{{$account->token}}" id="user_token" class="form-control" readonly="">
                                </div>
                            </div></br>
                            <div class="row">
                                <div class="col-md-6">
                                      <h5 class="text-dark"><strong>Fees</strong></h5>
                                    <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="Enter here the amount of fees you are paying for each sale. (For example: PayPal fee, etc..) This field is used only for displaying correct profits calculated with those fees." data-original-title="Fees" ><i class="fa fa-question-circle-o"></i>
                                    </a>
                                </div>
                                <div class="col-md-6">
                                    <input type="number" name="fees" value="{{$account->fees}}" step="any" min="0" id="all_fees" class="form-control" data-validate="required">
                                </div>
                            </div></br>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="text-dark"><strong>Location From Which Products Are Shipped</strong></h5>
                                </div>
                                <div class="col-md-3">
                                    <select id="shipping_country" name="country" class="selectpicker form-control">
                                        <option value="AF" @if($account->country =='AF')selected @endif>Afghanistan</option>
                                        <option value="AX" @if($account->country =='AX')selected @endif>Åland Islands</option>
                                        <option value="AL" @if($account->country =='AL')selected @endif>Albania</option>
                                        <option value="DZ" @if($account->country =='DZ')selected @endif>Algeria</option>
                                        <option value="AS" @if($account->country =='AS')selected @endif>American Samoa</option>
                                        <option value="AD" @if($account->country =='AD')selected @endif>Andorra</option>
                                        <option value="AO" @if($account->country =='AO')selected @endif>Angola</option>
                                        <option value="AI" @if($account->country =='AI')selected @endif>Anguilla</option>
                                        <option value="AQ" @if($account->country =='AQ')selected @endif>Antarctica</option>
                                        <option value="AG" @if($account->country =='AG')selected @endif>Antigua and Barbuda</option>
                                        <option value="AR" @if($account->country =='AR')selected @endif>Argentina</option>
                                        <option value="AM" @if($account->country =='AM')selected @endif>Armenia</option>
                                        <option value="AW" @if($account->country =='AW')selected @endif>Aruba</option>
                                        <option value="AU" @if($account->country =='AU')selected @endif>Australia</option>
                                        <option value="AT" @if($account->country =='AT')selected @endif>Austria</option>
                                        <option value="AZ" @if($account->country =='AZ')selected @endif>Azerbaijan</option>
                                        <option value="BS" @if($account->country =='BS')selected @endif>Bahamas</option>
                                        <option value="BH" @if($account->country =='BH')selected @endif>Bahrain</option>
                                        <option value="BD" @if($account->country =='BD')selected @endif>Bangladesh</option>
                                        <option value="BB" @if($account->country =='BB')selected @endif>Barbados</option>
                                        <option value="BY" @if($account->country =='BY')selected @endif>Belarus</option>
                                        <option value="BE" @if($account->country =='BE')selected @endif>Belgium</option>
                                        <option value="BZ" @if($account->country =='BZ')selected @endif>Belize</option>
                                        <option value="BJ" @if($account->country =='BJ')selected @endif>Benin</option>
                                        <option value="BM" @if($account->country =='BM')selected @endif>Bermuda</option>
                                        <option value="BT" @if($account->country =='BT')selected @endif>Bhutan</option>
                                        <option value="BO" @if($account->country =='BO')selected @endif>Bolivia, Plurinational State of</option>
                                        <option value="BQ" @if($account->country =='BQ')selected @endif>Bonaire, Sint Eustatius and Saba</option>
                                        <option value="BA" @if($account->country =='BA')selected @endif>Bosnia and Herzegovina</option>
                                        <option value="BW" @if($account->country =='BW')selected @endif>Botswana</option>
                                        <option value="BV" @if($account->country =='BV')selected @endif>Bouvet Island</option>
                                        <option value="BR" @if($account->country =='BR')selected @endif>Brazil</option>
                                        <option value="IO" @if($account->country =='IO')selected @endif>British Indian Ocean Territory</option>
                                        <option value="BN" @if($account->country =='BN')selected @endif>Brunei Darussalam</option>
                                        <option value="BG" @if($account->country =='BG')selected @endif>Bulgaria</option>
                                        <option value="BF" @if($account->country =='BF')selected @endif>Burkina Faso</option>
                                        <option value="BI" @if($account->country =='BI')selected @endif>Burundi</option>
                                        <option value="KH" @if($account->country =='KH')selected @endif>Cambodia</option>
                                        <option value="CM" @if($account->country =='CM')selected @endif>Cameroon</option>
                                        <option value="CA" @if($account->country =='CA')selected @endif>Canada</option>
                                        <option value="CV" @if($account->country =='CV')selected @endif>Cape Verde</option>
                                        <option value="KY" @if($account->country =='KY')selected @endif>Cayman Islands</option>
                                        <option value="CF" @if($account->country =='CF')selected @endif>Central African Republic</option>
                                        <option value="TD" @if($account->country =='TD')selected @endif>Chad</option>
                                        <option value="CL" @if($account->country =='CL')selected @endif>Chile</option>
                                        <option value="CN" @if($account->country =='CN')selected @endif>China</option>
                                        <option value="CX" @if($account->country =='CX')selected @endif>Christmas Island</option>
                                        <option value="CC" @if($account->country =='CC')selected @endif>Cocos (Keeling) Islands</option>
                                        <option value="CO" @if($account->country =='CO')selected @endif>Colombia</option>
                                        <option value="KM" @if($account->country =='KM')selected @endif>Comoros</option>
                                        <option value="CG" @if($account->country =='CG')selected @endif>Congo</option>
                                        <option value="CD" @if($account->country =='CD')selected @endif>Congo, the Democratic Republic of the</option>
                                        <option value="CK" @if($account->country =='CK')selected @endif>Cook Islands</option>
                                        <option value="CR" @if($account->country =='CR')selected @endif>Costa Rica</option>
                                        <option value="CI" @if($account->country =='CI')selected @endif>Côte d'Ivoire</option>
                                        <option value="HR" @if($account->country =='HR')selected @endif>Croatia</option>
                                        <option value="CU" @if($account->country =='CU')selected @endif>Cuba</option>
                                        <option value="CW" @if($account->country =='CW')selected @endif>Curaçao</option>
                                        <option value="CY" @if($account->country =='CY')selected @endif>Cyprus</option>
                                        <option value="CZ" @if($account->country =='CZ')selected @endif>Czech Republic</option>
                                        <option value="DK" @if($account->country =='DK')selected @endif>Denmark</option>
                                        <option value="DJ" @if($account->country =='DJ')selected @endif>Djibouti</option>
                                        <option value="DM" @if($account->country =='DM')selected @endif>Dominica</option>
                                        <option value="DO" @if($account->country =='DO')selected @endif>Dominican Republic</option>
                                        <option value="EC" @if($account->country =='EC')selected @endif>Ecuador</option>
                                        <option value="EG" @if($account->country =='EG')selected @endif>Egypt</option>
                                        <option value="SV" @if($account->country =='SV')selected @endif>El Salvador</option>
                                        <option value="GQ" @if($account->country =='GQ')selected @endif>Equatorial Guinea</option>
                                        <option value="ER" @if($account->country =='ER')selected @endif>Eritrea</option>
                                        <option value="EE" @if($account->country =='EE')selected @endif>Estonia</option>
                                        <option value="ET" @if($account->country =='ET')selected @endif>Ethiopia</option>
                                        <option value="FK" @if($account->country =='FK')selected @endif>Falkland Islands (Malvinas)</option>
                                        <option value="FO" @if($account->country =='FO')selected @endif>Faroe Islands</option>
                                        <option value="FJ" @if($account->country =='FJ')selected @endif>Fiji</option>
                                        <option value="FI" @if($account->country =='FI')selected @endif>Finland</option>
                                        <option value="FR" @if($account->country =='FR')selected @endif>France</option>
                                        <option value="GF" @if($account->country =='GF')selected @endif>French Guiana</option>
                                        <option value="PF" @if($account->country =='PF')selected @endif>French Polynesia</option>
                                        <option value="TF" @if($account->country =='TF')selected @endif>French Southern Territories</option>
                                        <option value="GA" @if($account->country =='GA')selected @endif>Gabon</option>
                                        <option value="GM" @if($account->country =='GM')selected @endif>Gambia</option>
                                        <option value="GE" @if($account->country =='GE')selected @endif>Georgia</option>
                                        <option value="DE" @if($account->country =='DE')selected @endif>Germany</option>
                                        <option value="GH" @if($account->country =='GH')selected @endif>Ghana</option>
                                        <option value="GI" @if($account->country =='GI')selected @endif>Gibraltar</option>
                                        <option value="GR" @if($account->country =='GR')selected @endif>Greece</option>
                                        <option value="GL" @if($account->country =='GL')selected @endif>Greenland</option>
                                        <option value="GD" @if($account->country =='GD')selected @endif>Grenada</option>
                                        <option value="GP" @if($account->country =='GP')selected @endif>Guadeloupe</option>
                                        <option value="GU" @if($account->country =='GU')selected @endif>Guam</option>
                                        <option value="GT" @if($account->country =='GT')selected @endif>Guatemala</option>
                                        <option value="GG" @if($account->country =='GG')selected @endif>Guernsey</option>
                                        <option value="GN" @if($account->country =='GN')selected @endif>Guinea</option>
                                        <option value="GW" @if($account->country =='GW')selected @endif>Guinea-Bissau</option>
                                        <option value="GY" @if($account->country =='GY')selected @endif>Guyana</option>
                                        <option value="HT" @if($account->country =='HT')selected @endif>Haiti</option>
                                        <option value="HM" @if($account->country =='HM')selected @endif>Heard Island and McDonald Islands</option>
                                        <option value="VA" @if($account->country =='VA')selected @endif>Holy See (Vatican City State)</option>
                                        <option value="HN" @if($account->country =='HN')selected @endif>Honduras</option>
                                        <option value="HK" @if($account->country =='HK')selected @endif>Hong Kong</option>
                                        <option value="HU" @if($account->country =='HU')selected @endif>Hungary</option>
                                        <option value="IS" @if($account->country =='IS')selected @endif>Iceland</option>
                                        <option value="IN" @if($account->country =='IN')selected @endif>India</option>
                                        <option value="ID" @if($account->country =='ID')selected @endif>Indonesia</option>
                                        <option value="IR" @if($account->country =='IR')selected @endif>Iran, Islamic Republic of</option>
                                        <option value="IQ" @if($account->country =='IQ')selected @endif>Iraq</option>
                                        <option value="IE" @if($account->country =='IE')selected @endif>Ireland</option>
                                        <option value="IM" @if($account->country =='IM')selected @endif>Isle of Man</option>
                                        <option value="IL" @if($account->country =='IL')selected @endif>Israel</option>
                                        <option value="IT" @if($account->country =='IT')selected @endif>Italy</option>
                                        <option value="JM" @if($account->country =='JM')selected @endif>Jamaica</option>
                                        <option value="JP" @if($account->country =='JP')selected @endif>Japan</option>
                                        <option value="JE" @if($account->country =='JE')selected @endif>Jersey</option>
                                        <option value="JO" @if($account->country =='JO')selected @endif>Jordan</option>
                                        <option value="KZ" @if($account->country =='KZ')selected @endif>Kazakhstan</option>
                                        <option value="KE" @if($account->country =='KE')selected @endif>Kenya</option>
                                        <option value="KI" @if($account->country =='KI')selected @endif>Kiribati</option>
                                        <option value="KP" @if($account->country =='KP')selected @endif>Korea, Democratic People's Republic of</option>
                                        <option value="KR" @if($account->country =='KR')selected @endif>Korea, Republic of</option>
                                        <option value="KW" @if($account->country =='KW')selected @endif>Kuwait</option>
                                        <option value="KG" @if($account->country =='KG')selected @endif>Kyrgyzstan</option>
                                        <option value="LA" @if($account->country =='LA')selected @endif>Lao People's Democratic Republic</option>
                                        <option value="LV" @if($account->country =='LV')selected @endif>Latvia</option>
                                        <option value="LB" @if($account->country =='LB')selected @endif>Lebanon</option>
                                        <option value="LS" @if($account->country =='LS')selected @endif>Lesotho</option>
                                        <option value="LR" @if($account->country =='LR')selected @endif>Liberia</option>
                                        <option value="LY" @if($account->country =='LY')selected @endif @if($account->country =='BG')selected @endif>Libya</option>
                                        <option value="LI" @if($account->country =='LI')selected @endif @if($account->country =='BG')selected @endif>Liechtenstein</option>
                                        <option value="LT" @if($account->country =='LT')selected @endif @if($account->country =='BG')selected @endif>Lithuania</option>
                                        <option value="LU" @if($account->country =='LU')selected @endif @if($account->country =='BG')selected @endif>Luxembourg</option>
                                        <option value="MO" @if($account->country =='MO')selected @endif @if($account->country =='BG')selected @endif>Macao</option>
                                        <option value="MK" @if($account->country =='MK')selected @endif @if($account->country =='BG')selected @endif>Macedonia, the former Yugoslav Republic of</option>
                                        <option value="MG" @if($account->country =='MG')selected @endif @if($account->country =='BG')selected @endif>Madagascar</option>
                                        <option value="MW" @if($account->country =='MW')selected @endif @if($account->country =='BG')selected @endif>Malawi</option>
                                        <option value="MY" @if($account->country =='MY')selected @endif @if($account->country =='BG')selected @endif>Malaysia</option>
                                        <option value="MV" @if($account->country =='MV')selected @endif>Maldives</option>
                                        <option value="ML" @if($account->country =='ML')selected @endif>Mali</option>
                                        <option value="MT" @if($account->country =='MT')selected @endif>Malta</option>
                                        <option value="MH" @if($account->country =='MH')selected @endif>Marshall Islands</option>
                                        <option value="MQ" @if($account->country =='MQ')selected @endif>Martinique</option>
                                        <option value="MR" @if($account->country =='MR')selected @endif>Mauritania</option>
                                        <option value="MU" @if($account->country =='MU')selected @endif>Mauritius</option>
                                        <option value="YT" @if($account->country =='YT')selected @endif>Mayotte</option>
                                        <option value="MX" @if($account->country =='MX')selected @endif>Mexico</option>
                                        <option value="FM" @if($account->country =='FM')selected @endif>Micronesia, Federated States of</option>
                                        <option value="MD" @if($account->country =='MD')selected @endif>Moldova, Republic of</option>
                                        <option value="MC" @if($account->country =='MC')selected @endif @if($account->country =='BG')selected @endif>Monaco</option>
                                        <option value="MN" @if($account->country =='MN')selected @endif @if($account->country =='BG')selected @endif>Mongolia</option>
                                        <option value="ME" @if($account->country =='ME')selected @endif @if($account->country =='BG')selected @endif>Montenegro</option>
                                        <option value="MS" @if($account->country =='MS')selected @endif @if($account->country =='BG')selected @endif>Montserrat</option>
                                        <option value="MA" @if($account->country =='MA')selected @endif @if($account->country =='BG')selected @endif>Morocco</option>
                                        <option value="MZ" @if($account->country =='MZ')selected @endif @if($account->country =='BG')selected @endif>Mozambique</option>
                                        <option value="MM" @if($account->country =='MM')selected @endif @if($account->country =='BG')selected @endif>Myanmar</option>
                                        <option value="NA" @if($account->country =='NA')selected @endif @if($account->country =='BG')selected @endif>Namibia</option>
                                        <option value="NR" @if($account->country =='NR')selected @endif @if($account->country =='BG')selected @endif>Nauru</option>
                                        <option value="NP" @if($account->country =='NP')selected @endif @if($account->country =='BG')selected @endif>Nepal</option>
                                        <option value="NL" @if($account->country =='NL')selected @endif @if($account->country =='BG')selected @endif>Netherlands</option>
                                        <option value="NC" @if($account->country =='NC')selected @endif>New Caledonia</option>
                                        <option value="NZ" @if($account->country =='NZ')selected @endif>New Zealand</option>
                                        <option value="NI" @if($account->country =='NI')selected @endif>Nicaragua</option>
                                        <option value="NE" @if($account->country =='NE')selected @endif>Niger</option>
                                        <option value="NG" @if($account->country =='NG')selected @endif>Nigeria</option>
                                        <option value="NU" @if($account->country =='NU')selected @endif>Niue</option>
                                        <option value="NF" @if($account->country =='NF')selected @endif>Norfolk Island</option>
                                        <option value="MP" @if($account->country =='MP')selected @endif>Northern Mariana Islands</option>
                                        <option value="NO" @if($account->country =='NO')selected @endif>Norway</option>
                                        <option value="OM" @if($account->country =='OM')selected @endif>Oman</option>
                                        <option value="PK" @if($account->country =='PK')selected @endif>Pakistan</option>
                                        <option value="PW" @if($account->country =='PW')selected @endif>Palau</option>
                                        <option value="PS" @if($account->country =='PS')selected @endif>Palestinian Territory, Occupied</option>
                                        <option value="PA" @if($account->country =='PA')selected @endif>Panama</option>
                                        <option value="PG" @if($account->country =='PG')selected @endif>Papua New Guinea</option>
                                        <option value="PY" @if($account->country =='PY')selected @endif>Paraguay</option>
                                        <option value="PE" @if($account->country =='PE')selected @endif>Peru</option>
                                        <option value="PH" @if($account->country =='PH')selected @endif>Philippines</option>
                                        <option value="PN" @if($account->country =='PN')selected @endif>Pitcairn</option>
                                        <option value="PL" @if($account->country =='PL')selected @endif>Poland</option>
                                        <option value="PT" @if($account->country =='PT')selected @endif>Portugal</option>
                                        <option value="PR" @if($account->country =='PR')selected @endif>Puerto Rico</option>
                                        <option value="QA" @if($account->country =='QA')selected @endif>Qatar</option>
                                        <option value="RE" @if($account->country =='RE')selected @endif>Réunion</option>
                                        <option value="RO" @if($account->country =='RO')selected @endif>Romania</option>
                                        <option value="RU" @if($account->country =='RU')selected @endif>Russian Federation</option>
                                        <option value="RW" @if($account->country =='RW')selected @endif>Rwanda</option>
                                        <option value="BL" @if($account->country =='BL')selected @endif>Saint Barthélemy</option>
                                        <option value="SH" @if($account->country =='SH')selected @endif>Saint Helena, Ascension and Tristan da Cunha</option>
                                        <option value="KN" @if($account->country =='KN')selected @endif>Saint Kitts and Nevis</option>
                                        <option value="LC" @if($account->country =='LC')selected @endif>Saint Lucia</option>
                                        <option value="MF" @if($account->country =='MF')selected @endif>Saint Martin (French part)</option>
                                        <option value="PM" @if($account->country =='PM')selected @endif>Saint Pierre and Miquelon</option>
                                        <option value="VC" @if($account->country =='VC')selected @endif>Saint Vincent and the Grenadines</option>
                                        <option value="WS" @if($account->country =='WS')selected @endif>Samoa</option>
                                        <option value="SM" @if($account->country =='SM')selected @endif>San Marino</option>
                                        <option value="ST" @if($account->country =='ST')selected @endif>Sao Tome and Principe</option>
                                        <option value="SA" @if($account->country =='SA')selected @endif>Saudi Arabia</option> @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif
                                        <option value="SN" @if($account->country =='SN')selected @endif>Senegal</option>
                                        <option value="RS" @if($account->country =='RS')selected @endif>Serbia</option>
                                        <option value="SC" @if($account->country =='SC')selected @endif>Seychelles</option>
                                        <option value="SL" @if($account->country =='SL')selected @endif>Sierra Leone</option>
                                        <option value="SG" @if($account->country =='SG')selected @endif>Singapore</option>
                                        <option value="SX" @if($account->country =='SX')selected @endif>Sint Maarten (Dutch part)</option>
                                        <option value="SK" @if($account->country =='SK')selected @endif>Slovakia</option>
                                        <option value="SI" @if($account->country =='SI')selected @endif>Slovenia</option>
                                        <option value="SB" @if($account->country =='SB')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Solomon Islands</option>
                                        <option value="SO" @if($account->country =='SO')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Somalia</option>
                                        <option value="ZA" @if($account->country =='ZA')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>South Africa</option>
                                        <option value="GS" @if($account->country =='GS')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>South Georgia and the South Sandwich Islands</option>
                                        <option value="SS" @if($account->country =='SS')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>South Sudan</option>
                                        <option value="ES" @if($account->country =='ES')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Spain</option>
                                        <option value="LK" @if($account->country =='LK')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Sri Lanka</option>
                                        <option value="SD" @if($account->country =='SD')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Sudan</option>
                                        <option value="SR" @if($account->country =='SR')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Suriname</option>
                                        <option value="SJ" @if($account->country =='SJ')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Svalbard and Jan Mayen</option>
                                        <option value="SZ" @if($account->country =='SZ')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Swaziland</option>
                                        <option value="SE" @if($account->country =='SE')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Sweden</option>
                                        <option value="CH" @if($account->country =='CH')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Switzerland</option>
                                        <option value="SY" @if($account->country =='SY')selected @endif @if($account->country =='BG')selected @endif @if($account->country =='BG')selected @endif>Syrian Arab Republic</option>
                                        <option value="TW" @if($account->country =='TW')selected @endif @if($account->country =='BG')selected @endif>Taiwan, Province of China</option>
                                        <option value="TJ" @if($account->country =='TJ')selected @endif @if($account->country =='BG')selected @endif>Tajikistan</option>
                                        <option value="TZ" @if($account->country =='TZ')selected @endif @if($account->country =='BG')selected @endif>Tanzania, United Republic of</option>
                                        <option value="TH" @if($account->country =='TH')selected @endif @if($account->country =='BG')selected @endif>Thailand</option>
                                        <option value="TL" @if($account->country =='TL')selected @endif @if($account->country =='BG')selected @endif>Timor-Leste</option>
                                        <option value="TG" @if($account->country =='TG')selected @endif @if($account->country =='BG')selected @endif>Togo</option>
                                        <option value="TK" @if($account->country =='TK')selected @endif @if($account->country =='BG')selected @endif>Tokelau</option>
                                        <option value="TO" @if($account->country =='TO')selected @endif @if($account->country =='BG')selected @endif>Tonga</option>
                                        <option value="TT" @if($account->country =='TT')selected @endif @if($account->country =='BG')selected @endif>Trinidad and Tobago</option>
                                        <option value="TN" @if($account->country =='TN')selected @endif @if($account->country =='BG')selected @endif>Tunisia</option>
                                        <option value="TR" @if($account->country =='TR')selected @endif @if($account->country =='BG')selected @endif>Turkey</option>
                                        <option value="TM" @if($account->country =='TM')selected @endif @if($account->country =='BG')selected @endif>Turkmenistan</option>
                                        <option value="TC" @if($account->country =='TC')selected @endif @if($account->country =='BG')selected @endif>Turks and Caicos Islands</option>
                                        <option value="TV" @if($account->country =='TV')selected @endif>Tuvalu</option>
                                        <option value="UG" @if($account->country =='UG')selected @endif>Uganda</option>
                                        <option value="UA" @if($account->country =='UA')selected @endif>Ukraine</option>
                                        <option value="AE" @if($account->country =='AE')selected @endif>United Arab Emirates</option>
                                        <option value="GB" @if($account->country =='GB')selected @endif>United Kingdom</option>
                                        <option value="US" @if($account->country =='US')selected @endif>United States</option>
                                        <option value="UM" @if($account->country =='UM')selected @endif>United States Minor Outlying Islands</option>
                                        <option value="UY" @if($account->country =='UY')selected @endif>Uruguay</option>
                                        <option value="UZ" @if($account->country =='UZ')selected @endif>Uzbekistan</option>
                                        <option value="VU" @if($account->country =='VU')selected @endif>Vanuatu</option>
                                        <option value="VE" @if($account->country =='VE')selected @endif>Venezuela, Bolivarian Republic of</option>
                                        <option value="VN" @if($account->country =='VN')selected @endif>Viet Nam</option>
                                        <option value="VG" @if($account->country =='VG')selected @endif>Virgin Islands, British</option>
                                        <option value="VI" @if($account->country =='VI')selected @endif>Virgin Islands, U.S.</option>
                                        <option value="WF" @if($account->country =='WF')selected @endif>Wallis and Futuna</option>
                                        <option value="EH" @if($account->country =='EH')selected @endif>Western Sahara</option>
                                        <option value="YE" @if($account->country =='YE')selected @endif>Yemen</option>
                                        <option value="ZM" @if($account->country =='ZM')selected @endif>Zambia</option>
                                        <option value="ZW" @if($account->country =='ZW')selected @endif>Zimbabwe</option>
                                    </select>
                                </div>
                                <div class="col-md-3">
                                    <input type="text" name="fulfillment_centers" value="{{$account->fulfillment_centers}}" id="shipping_location" class="form-control" data-validate="required" placeholder="Fulfillment Centers">
                                    <span class="description">Used when uploading new products.</span>
                                </div>
                            </div></br>
                            <div class="row">
                                <div class="col-md-6">
                                   <h5 class="text-dark"><strong>Upload Watermark(Suggested size: 250px * 250px, with transparent background)</strong></h5>
                                </div>
                                <div class="col-md-6">
                                   <!--  <div class="thumbnail watermark_thumbnail" onclick=""> -->
                                   <!--     <a href="javascript:void(0)">
                                           <img src="/neon_dashboard/assets/images/no_image.png" id="watermark_image">
                                           <span class="watermark_text">Upload Watermark</span>
                                       </a> -->
                                   <!--  </div> -->
                                    <input type="file" name="image" value="{{$account->image}}" id="add_watermark_image" >
                                </div>
                            </div></br>
                            <div class="row">
                                <div class="col-md-6">
                                        <h5 class="text-dark"><strong>Blocked Products</strong></h5>
                                        <a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="'Blocked products' are products that you want to block from selling. If you add a blocked product(ASIN), the monitor will remove it if it's currently on sell and won't let you add that product in the future." data-original-title="Blocked Products"><i class="fa fa-question-circle-o"></i>
                                        </a>
                                   
                                </div>
                                <div class="col-md-6 text-center btn-tool-bar">
                                    <a href="javascrtipt:;" class="btn btn-theme btn-round custom-btn" data-toggle="modal" data-target="#blocked_product">Blocked Products
                                    </a>
                                </div>
                            </div></br>
                            <div class="row">
                                <div class="col-md-6">
                                  <h5 class="text-dark"><strong>Default Item Specifics</strong></h5>
                                </div>
                                <div class="col-md-6 text-center btn-tool-bar">
                                    <!-- <button type="button" class="btn btn-primary form-control" onclick="" data-toggle="modal" data-target="#default_item_specifics_modal">Default item specifics</button> -->

                                    <a href="#" class="btn btn-theme btn-round custom-btn"  data-toggle="modal" data-target="#default_specifics">Default item specifics</a>
                                       <!--  <button type="button" class="btn btn-primary form-control" onclick="" data-toggle="modal" data-target="#default_item_specifics_modal">Default item specifics</button> -->
                                    <!-- <a href="javascript:;" onclick="" class="btn btn-primary form-control">Blocked Products</a> -->
                                </a>
                                </div>
                            </div></br>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="text-dark"><strong>Manage Tags</strong></h5>
                                </div>
                                <div class="col-md-6 text-center btn-tool-bar">
                                    <a href="javasctipt" class="btn btn-theme btn-round custom-btn" data-toggle="modal" data-target="#tags">Manage Tags
                                    </a>
                                    <!-- <button type="button" class="btn btn-primary form-control" onclick="" data-toggle="modal" data-target="#manage_tags_modal">Manage Tags</button> -->
                                </div>
                            </div></br>
                            <div class="row">
                                <div class="col-md-6">
                                    <h5 class="text-dark"><strong>Refresh Ebay</strong></h5>
                                </div>
                                <div class="col-md-6 text-center btn-tool-bar">
                                    <button type="button" class="btn btn-theme btn-round custom-btn" onclick="">Refresh eBay token</button>
                                </div>
                            </div></br>
                        </div>
                       
                    </div>
                    <div class="card-footer">
                        <div class="pull-right">
                            <a class="btn btn-link" onclick="">Remove Ebay Account</a>
                        </div>
                    </div>
                </div>
            </div>    
        </div>
    </form>
</div>
@include('client.settings.settings_modal')

@endsection
@section('footer_script')
<script src="{{ URL::asset('public/admin-assets/libs/tables-datatables/dist/datatables.min.js') }}"></script>
<!-- <script src="{{ url('admin-assets/js/table-datatable-example.js') }}"></script> -->
 <script>
    $(document).ready(function () {
        $('#example1').DataTable({
            "paging": false,
            "ordering": false,
            "info": false,
            "searching": false
        });
       
    });
    function blockProducts(item_id, isblock)
    {   
        var token = $('meta[name="csrf-token"]').attr('content');
        $.post('{{url("block_product")}}', {blockProductId:item_id,_token:token, isblock:isblock},function(response){
            location.reload();
        });
    }

</script>   
@endsection 





