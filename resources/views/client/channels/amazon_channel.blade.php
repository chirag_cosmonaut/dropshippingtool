@extends('backend_layout.app') 
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
	<li class="breadcrumb-item ">
		<a href="{{ route('dashboard') }}">Home</a>
	</li>
	<li class="breadcrumb-item active">
		<a href="#">Amazon Channel</a>
	</li>
</ol>

<div class="container-fluid">
	<div class="animated fadeIn">
		<div class="row">
			<div class="col-md-12">
				<div class="card">
                    <div class="card-header text-theme">
                        <strong>Connect Amazon User</strong>
                        <small>To start monitor your amazon user, you must connect it to the monitor. Click on "Connect User" to connect your Amazon Seller Central user to the monitor.</small>
                    </div>
                    <div class="card-body">
                    	<form method="post" action="{{ route('connectMWS') }}">
                    		@csrf
	                        <div class="row">
	                            <div class="form-group col-sm-4">
	                                <label for="site">Site</label>
	                                <select class="form-control" id="site">
	                                    <option value="2" selected="">Amazon US</option>
	                                </select>
	                            </div>
	                        </div>
	                        <!--/.row-->

	                        <div class="row">
	                        	<div class="col-sm-4">
	                        		<button type="submit" class="btn btn-theme btn-sm"><i class="fa fa-dot-circle-o"></i> Connect User</button>
	                        	</div>
	                        </div>
	                        <!--/.row-->
                        </form>
                    </div>
                    <!-- end card-body -->
                </div>
				<!-- end card -->
			</div>
			<!-- end col -->
		</div>
	</div>
</div>
@endsection

