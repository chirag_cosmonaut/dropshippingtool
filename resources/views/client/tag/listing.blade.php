@extends('backend_layout.app')  

@section('content')
<link rel="stylesheet" href="{{ URL::asset('public/admin-assets/libs/tables-datatables/dist/datatables.min.css') }}"/>
 <link rel="stylesheet" href="{{URL::asset('public/admin-assets/libs/tables-datatables/dist/datatables.min.JS') }}">
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item active">Tags</li>
</ol>
@if(Session::has('flash_message'))
    <div class="alert alert-success">
        {{ Session::get('flash_message') }}
    </div>
@endif
<div class="container">
    <div class="animated fadeIn">
        <div class="row">
            <div class="col-md-12">
                <div class="card card-accent-theme">
                    <div class="card-body">
                        <h5>Tag Listing</h5><a href="javascript:;" class="btn btn-dark" data-toggle="modal" data-target="#add_tag">Add</a>

                        <table class="table table-hover dataTable table-striped w-full" id="example" data-plugin="dataTable" width="100%">
                            <thead>
                                <tr>
                                    <th>#</th>
                                    <th>Tags Name</th>
                                    <th>Action</th>
                                </tr>
                            </thead>
                            
                        </table>
                    </div>
                </div>  
            </div>
        </div>
    </div>
</div>
<!-- Modal for Add Tags -->
<div class="modal fade bs-example-modal-default"  id="add_tag" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog  modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Add Tag</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <strong>Tags Name:</strong>
                    <div class="form-group row">
                        <div class="col-md-8">
                           <input type="text" id="addTagsId" name="addTagsId" class="form-control" data-validate="required" placeholder="" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-dark waves-effect text-left" onclick="addTags()">Save</a>
                    <a href="javascript:;" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cancel</a>
                </div>
            
        </div>
    </div>
</div>
<!-- Modal for Update Tag -->
<div class="modal fade bs-example-modal-default"  id="update_tag" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog  modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Update Tag</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
                <div class="modal-body">
                    <input type="hidden" id="editId" name="editId" class="form-control" data-validate="required" placeholder="" value="">
                    <strong>Tags Name:</strong>
                    <div class="form-group row">
                        <div class="col-md-8">
                           <input type="text" id="updateTagId" name="updateTagId" class="form-control" data-validate="required" placeholder="" value="">
                        </div>
                    </div>
                </div>
                <div class="modal-footer">
                    <a href="javascript:;" class="btn btn-dark waves-effect text-left" onclick="updateTag()">Update</a>
                    <a href="javascript:;" class="btn btn-danger waves-effect text-left" data-dismiss="modal">Cancel</a>
                </div>
            
        </div>
    </div>
</div>
@endsection
@section('footer_script')
<script type="text/javascript">
    $(document).ready(function () {
        $(document).on('click','.open_modal',function(){
            $("#editId").val($(this).attr('data-val'));
            var token = $('meta[name="csrf-token"]').attr('content');
            $.post('{{url("tag/get_tag")}}',{editTagId:$(this).attr('data-val'),_token:token},function(response){
               $("#updateTagId").val(response);
            });
        });
        $('#example').DataTable({
            processing: true,
            serverSide: true,
            responsive: true,
            ordering: false,
            searchable: true,
            paging :true,
            ajax: '{!! route("tag.data") !!}',
            columns: [
                { data: 'id', name: 'id' },
                { data: 'tag_name',"searchable":true, name: 'tag_name' },
                { data: 'action',"searchable":false, name: 'Action' }
            ]
        });
    });

    function addTags()
    {
        var addTagsId = $('#addTagsId').val();
        var token = $('meta[name="csrf-token"]').attr('content');
        $.post('{{url("tag/add_tag")}}', {addTagsId:addTagsId,_token:token},function(response){
            location.reload();
        });
    }
    function updateTag()
    {
        var updateTagText = $('#updateTagId').val();
        var editTagId = $('#editId').val();
        var token = $('meta[name="csrf-token"]').attr('content');
        $.post('{{url("tag/update_tag")}}',{updateTagId:updateTagText,_token:token,editTagId:editTagId},function(response){
           alert(response);
           return false;
        });
    }

</script> 

@endsection 





