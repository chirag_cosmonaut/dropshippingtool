	<!-- After SKU Filling in Products Monitor -->

										<!-- <div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Allow ADD-ON Items</span>
														<span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Allow ADD-ON Items"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_allow_add_on_items_checkbox" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="checkbox_allow_add_on_items_span"></span>
													</div>
												</div>
												<div class="row">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Allow Prime Pantry</span>
														<span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Allow Prime Pantry"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_allow_prime_pantry_checkbox" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="checkbox_allow_prime_pantry_span"></span>
													</div>
												</div> -->

												<div class="row" style="display: none">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Allow Suppliers Table</span>
														<span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Allow Suppliers Table"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<div class="checkbox" id="checkbox_allow_suppliers_table_checkbox" style="display: none;">
															<label><input type="checkbox" value="1" checked="checked"></label>
														</div>
														<span class="fa fa-square-o" id="checkbox_allow_suppliers_table_span"></span>
													</div>
												</div>

												<div class="row" style="display: none">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Minimum Amazon Seller Feedbacks</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Minimum Amazon Seller Feedbacks"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="minimum_amz_seller_feedbacks_span">100</span>
														<input class="form-control" type="number" step="1" placeholder="" id="minimum_amz_seller_feedbacks_input" style="display: none;">
													</div>
												</div>
												<div class="row" style="display: none">
													<div class="col-md-6">
														<span class="MonitorsContentKey">Minimum Amazon Seller Feedbacks Percentage</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="Minimum Amazon Seller Feedbacks Percentage"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="minimum_amz_seller_feedbacks_percentage_span">90</span>
														<div class="btn-group" id="minimum_amz_seller_feedbacks_percentage_input" style="display: none;">
															<select class="selectpicker form-control">
																<option value="50">50</option>
																<option value="51">51</option>
																<option value="52">52</option>
																<option value="53">53</option>
																<option value="54">54</option>
																<option value="55">55</option>
																<option value="56">56</option>
																<option value="57">57</option>
																<option value="58">58</option>
																<option value="59">59</option>
																<option value="60">60</option>
																<option value="61">61</option>
																<option value="62">62</option>
																<option value="63">63</option>
																<option value="64">64</option>
																<option value="65">65</option>
																<option value="66">66</option>
																<option value="67">67</option>
																<option value="68">68</option>
																<option value="69">69</option>
																<option value="70">70</option>
																<option value="71">71</option>
																<option value="72">72</option>
																<option value="73">73</option>
																<option value="74">74</option>
																<option value="75">75</option>
																<option value="76">76</option>
																<option value="77">77</option>
																<option value="78">78</option>
																<option value="79">79</option>
																<option value="81">81</option>
																<option value="82">82</option>
																<option value="83">83</option>
																<option value="84">84</option>
																<option value="85">85</option>
																<option value="86">86</option>
																<option value="87">87</option>
																<option value="88">88</option>
																<option value="89">89</option>
																<option value="90">90</option>
																<option value="91">91</option>
																<option value="92">92</option>
																<option value="93">93</option>
																<option value="94">94</option>
																<option value="95">95</option>
																<option value="96">96</option>
																<option value="97">97</option>
																<option value="98">98</option>
																<option value="99">99</option>
																<option value="100">100</option>
															</select>
														</div>
													</div>
												</div>
												<div class="row" style="display: none">
													<div class="col-md-6">
														<span class="MonitorsContentKey">First Prime / Cheapest</span><span class="MonitorsHelp"><a href="#/" data-toggle="popover" data-trigger="hover" data-placement="right" title="" data-content="" data-original-title="First Prime / Cheapest"> <i class="fa fa-question-circle-o"></i></a></span>
													</div>
													<div class="col-md-6">
														<span class="MonitorsContentValue" id="first_prime_or_cheapest_span">Prime</span>
														<div class="btn-group" id="first_prime_or_cheapest_input" style="display: none;">
															<select class="selectpicker form-control">
																<option value="Prime">Prime</option>
																<option value="Cheapest">Cheapest</option>
															</select>
														</div>
													</div>
												</div>


							
						