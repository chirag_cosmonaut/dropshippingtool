<header class="app-header navbar">
    <div class="hamburger hamburger--arrowalt-r navbar-toggler mobile-sidebar-toggler d-lg-none mr-auto">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>

    <a class="navbar-brand" href="{{ route('dashboard') }}">
       <img src="{{ url('images/logo2.png') }}" width="80">
    </a> 

    <div class="hamburger hamburger--arrowalt-r navbar-toggler sidebar-toggler d-md-down-none mr-auto">
        <div class="hamburger-box">
            <div class="hamburger-inner"></div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-12">
            <select id="account" name="account" class="form-control">
                @if(count($accounts)>0)
                    @foreach($accounts as $acc)
                        <?php
                            $accSel = ($currentAcc == $acc->id) ? 'selected':''; 
                        ?>
                        <option value="{{ $acc->id }}" {{$accSel}}>{{ $acc->user_id }}</option>
                    @endforeach
                @else
                    <option>No Account Connected</option>
                @endif
            </select>
        </div>
    </div>
    <ul class="nav navbar-nav ">
        <li class="nav-item dropdown">
            <a class="nav-link " href="#" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                <i class="mdi mdi-bell-ring"></i>
                <span class="notification hertbit"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right notification-list animated flipInY nicescroll-box">
                <div class="dropdown-header">
                    <strong>Notification</strong>
                    <span class="badge badge-pill badge-theme pull-right"> new 5</span>
                </div>

                <div class="wrap">
                    <a href="#" class="dropdown-item">
                        <div class="message-box">
                            <div class="u-img">
                                <img src="http://via.placeholder.com/100x100" alt="user" />
                            </div>
                            <div class="u-text">
                                <div class="u-name">
                                    <strong>A New Order has Been Placed </strong>
                                </div>
                                <small>2 minuts ago</small>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="dropdown-item">
                        <div class="message-box">
                            <div class="u-img">
                                <img src="http://via.placeholder.com/100x100" alt="user" />
                            </div>
                            <div class="u-text">
                                <div class="u-name">
                                    <strong>Order Updated</strong>
                                </div>
                                <small>10 minuts ago</small>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="dropdown-item">
                        <div class="message-box">
                            <div class="u-img">
                                <img src="http://via.placeholder.com/100x100" alt="user" />
                            </div>
                            <div class="u-text">
                                <div class="u-name">
                                    <strong>A New Order has Been Placed </strong>
                                </div>
                                <small>30 minuts ago</small>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="dropdown-item">
                        <div class="message-box">
                            <div class="u-img">
                                <img src="http://via.placeholder.com/100x100" alt="user" />
                            </div>
                            <div class="u-text">
                                <div class="u-name">
                                    <strong> Order has Been Rated </strong>
                                </div>
                                <small>32 minuts ago</small>
                            </div>
                        </div>
                    </a>
                </div>

                <div class="dropdown-footer ">
                    <a href="">
                        <strong>See all messages (150) </strong>
                    </a>
                </div>
            </div>
        </li> 

        <li class="nav-item ">
            <a class="nav-link" href="#" data-toggle="dropdown">
                <i class="mdi mdi-forum"></i>
                <span class="notification hertbit"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right message-list animated flipInY nicescroll-box">

                <div class="dropdown-header">
                    <strong>Messages</strong>
                    <span class="badge badge-pill badge-theme pull-right"> new 15</span>
                </div>
                <div class="wrap">
                    <a href="#" class="dropdown-item">
                        <div class="message-box">
                            <div class="u-img">
                                <img src="http://via.placeholder.com/100x100" alt="user" />
                                <span class="notification online"></span>
                            </div>

                            <div class="u-text">
                                <div class="u-name">
                                    <strong>Natalie Wall</strong>
                                </div>
                                <p class="text-muted">Anyways i would like just do it</p>
                                <small>2 minuts ago</small>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="dropdown-item">
                        <div class="message-box">
                            <div class="u-img">
                                <img src="http://via.placeholder.com/100x100" alt="user" />
                                <span class="notification offline"></span>
                            </div>

                            <div class="u-text">
                                <div class="u-name">
                                    <strong>Steve johns</strong>
                                </div>
                                <p class="text-muted">There is Problem inside the Application</p>
                                <small>10 minuts ago</small>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="dropdown-item">
                        <div class="message-box">
                            <div class="u-img">
                                <img src="http://via.placeholder.com/100x100" alt="user" />
                                <span class="notification buzy"></span>
                            </div>
                            <div class="u-text">
                                <div class="u-name">
                                    <strong>Taniya Jan</strong>
                                </div>
                                <p class="text-muted">Please Checkout The Attachment</p>
                                <small>30 minuts ago</small>
                            </div>
                        </div>
                    </a>

                    <a href="#" class="dropdown-item">
                        <div class="message-box">
                            <div class="u-img">
                                <img src="http://via.placeholder.com/100x100" alt="user" />
                                <span class="notification away"></span>
                            </div>
                            <div class="u-text">
                                <div class="u-name">
                                    <strong>Tim Johns</strong>
                                </div>
                                <p class="text-muted">Anyways i would like just do it</p>
                                <small>32 minuts ago</small>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="dropdown-footer ">
                    <a href="">
                        <strong>See all messages (150) </strong>
                    </a>
                </div>
            </div>
        </li>

        <li class="nav-item ">
            <a class="nav-link" href="#" data-toggle="dropdown">
                <i class="mdi mdi-cards"></i>
                <span class="notification hertbit"></span>
            </a>
            <div class="dropdown-menu dropdown-menu-right task-list animated flipInY nicescroll-box">
                <div class="dropdown-header">
                    <strong>Task List</strong>
                    <span class="badge badge-pill badge-theme pull-right"> new 3</span>
                </div>

                <div class="wrap">
                    <a href="#" class="dropdown-item">
                        <strong>Task 1</strong>
                        <small class="pull-right">50% Complete</small>
                        <div class="progress xs">
                            <div class="progress-bar bg-danger" style="width: 50%" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </a>

                    <a href="#" class="dropdown-item">
                        <strong>Task 2</strong>
                        <small class="pull-right">20% Complete</small>

                        <div class="progress xs">
                            <div class="progress-bar bg-success" style="width: 20%" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </a>

                    <a href="#" class="dropdown-item">
                        <strong>Task 3</strong>
                        <small class="pull-right">80% Complete</small>
                        <div class="progress xs ">
                            <div class="progress-bar bg-warning" style="width: 80%" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </a>

                    <a href="#" class="dropdown-item">
                        <strong>Task 4</strong>
                        <small class="pull-right">60% Complete</small>
                        <div class="progress xs ">
                            <div class="progress-bar bg-info" style="width: 60%" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100">
                            </div>
                        </div>
                    </a>
                </div>

                <div class="dropdown-footer ">
                    <a href="">
                        <strong>view all task (20) </strong>
                    </a>
                </div>

            </div>
        </li>

        <li class="nav-item dropdown">
            <a class="btn btn-round btn-theme btn-sm" data-toggle="dropdown" href="#" role="button" aria-haspopup="true" aria-expanded="false">
                <span class="">{!!  "@".str_slug(Auth::user()->name, '-') !!}
                    <i class="fa fa-arrow-down"></i>
                </span>
            </a>
            <div class="dropdown-menu dropdown-menu-right user-menu animated flipInY ">
                <div class="wrap">
                    <div class="dw-user-box">
                        <div class="u-img">
                            <img src="{{ URL::asset('public/images/user/'.Auth::user()->profile_image) }}"  alt="{!!Auth::user()->name !!}" height="100px" widht="100px" >
                        </div>
                        <div class="u-text">
                            <h5>{{ ucwords(Auth::user()->name) }}</h5>
                            <p class="text-muted">{{ Auth::user()->email }}</p>
                            <p><b>Current Plan</b>: Professional </p>
                            <p><b>Monthly Charge</b>: $18.00</p>
                            <a href="{{ url('profile')}}" class="btn btn-round btn-theme btn-sm">View Profile</a>
                        </div>
                    </div>

                    <a class="dropdown-item" href="{{ url('profile') }}">
                        <i class="fa fa-user"></i> Profile
                    </a>
                    <a class="dropdown-item" href="#">
                        <i class="fa fa-user"></i> Billing & Subscription
                    </a>

                    <a class="dropdown-item" href="#">
                        <i class="fa fa-user"></i> Support
                    </a>                            

                    <a class="dropdown-item" href="#">
                        <i class="fa fa-wrench"></i> Settings
                    </a>
                    <div class="divider"></div>

                    <a class="dropdown-item" href="{{ url('signout') }}">
                        <i class="fa fa-lock"></i> Logout
                    </a>
                </div>
            </div>
        </li>
    </ul>
</header>