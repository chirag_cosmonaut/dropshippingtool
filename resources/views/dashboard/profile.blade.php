@extends('layouts.app')

@section('content')

    <!-- main.css | /* Profile Page Section S */ -->
    <section>
        <div class="profile_page">
            <div class="container">
                <div class="row">
                    <div class="col-lg-4 col-md-5">
                        <div class="profile_page_side_section">
                            <div class="user_profile margin-bottom-30">
                                <div class="user_image_section">
                                    <div class="edit_image">
                                        <a href="{{ route('profile-edit') }}" title="Edit Profile Pic">
                                            <i class="fa fa-edit"></i>
                                        </a>
                                    </div>
                                    <div class="profile_pic">
                                        <img src="assets/image/profile.png" alt="Profile">
                                    </div>
                                </div>
                                <div class="user_details">
                                    <h2 class="user_name">{{ ucwords(Auth::user()->name) }}</h2>
                                    <h3 class="user_id">{!!  "@".str_slug(Auth::user()->name, '-') !!}</h3>
                                   
                                    <p> 

                                    <a  href="{{ route('logout') }}"

                                       onclick="event.preventDefault();

                                                     document.getElementById('logout-form').submit();">

                                        {{ __('Logout') }}

                                    </a>
                                         <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            @csrf
                                        </form>
                                    </p>

                                   
                                </div>
                            </div>

                            <div class="follower_section_box">
                                <div class="follower_section">
                                    <div class="number">00</div>
                                    <div class="text">Blogs</div>
                                </div>
                                <div class="follower_section">
                                    <div class="number">90</div>
                                    <div class="text">Connections</div>
                                </div>
                                <div class="follower_section">
                                    <div class="number">1.2 M</div>
                                    <div class="text">Followers</div>
                                </div>
                            </div>

                            <div class="rating_section_box margin-bottom-30">
                                <div class="rating_section">
                                    <div class="row">
                                        <div class="col-sm-4 custom-width">
                                            <div class="title">RATING</div>
                                        </div>
                                        <div class="col-sm-8 custom-width">
                                            <div class="stars">
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star checked"></span>
                                                <span class="fa fa-star-half-o checked"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="rating_section">
                                    <div class="row">
                                        <div class="col-sm-4 custom-width">
                                            <div class="title">GENRE</div>
                                        </div>
                                        <div class="col-sm-8 custom-width">
                                            <div class="details">
                                                Designing, Photoshop, Css, HTML, Javascript.
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="row profile_page_side_section_button">
                                <div class="col-sm-6 custom-padding">
                                    <a href="javascript:;" class="btn compose" title="Compose">
                                        <i class="fa fa-file-text"></i>
                                        <span class="btn_name">COMPOSE</span>
                                    </a>
                                </div>
                                <div class="col-sm-6 custom-padding">
                                    <a href="javascript:;" class="btn draft" title="Draft">
                                        <div>
                                            <i class="fa fa-folder-open"></i>
                                            <span class="btn_name">DRAFT</span>
                                        </div>
                                        <div class="number">02</div>
                                    </a>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-lg-8 col-md-7">
                        <div class="profile_page_main_section margin-bottom-30">
                            <div class="profile_main_title_section">
                                <h2 class="title">
                                    <i class="fa fa-file-text"></i>
                                    Summary
                                </h2>

                                <a href="javascript:;" class="edit_icon" title="edit">
                                    <i class="fa fa-edit"></i>
                                    Edit
                                </a>
                            </div>
                            <div class="profile_main_section">
                                <p class="content">
                                    Hi, This is <span class="user_name">Rajat Suri</span>. I am feeling glad for what I have achieved here. But I think life is more about helping people rather than commanding them. I feel life is also full of variations in designs. In terms of design, nature creates beautiful graphs and sets lot of colors in our personal life.
                                </p>

                                <p class="content">
                                    So yes I get ideas from nature. The way it designs hills, oceans and fills colors into them. I feel lucky for having this life and capable of identifying hidden treasures beneath a wonderful design.
                                </p>
                            </div>
                        </div>
                        <div class="profile_page_main_section">
                            <div class="profile_main_title_section">
                                <h2 class="title">
                                    <i class="fa fa-pencil-square"></i>
                                    Recent Blogs 
                                </h2>

                                <a href="javascript:;" class="edit_icon" title="edit">
                                    <i class="fa fa-edit"></i>
                                    Edit
                                </a>
                            </div>
                            <div class="profile_main_section no_any_content">
                                <p class="content_text">Awww ! no blogs. Write now <span>Just click on compose button</span></p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
