<!DOCTYPE html>

<html lang="{{ app()->getLocale() }}">

<head>

<meta charset="UTF-8"/>

<meta http-equiv="X-UA-Compatible" content="IE=edge"/>

<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1, user-scalable=0"/>

<title>{{ config('app.name', 'DropShipping Tool') }}</title>

<meta name="keywords" content=""/>

<meta name="description" content=""/>

<meta name="author" content=""/>

<meta name="csrf-token" content="">

<!-- OG Meta S -->

    <meta property="og:url" content=""/>

    <meta property="og:type" content="" />

    <meta property="og:title" content=""/>

    <meta property="og:description" content=""/>

    <meta property="og:image" content=""/>

<!-- OG Meta E -->

<!-- Twitter Meta S -->

    <meta name="twitter:card" content=""/>

    <meta name="twitter:title" content=""/>

    <meta name="twitter:url" content=""/>

    <meta name="twitter:description" content=""/>

    <meta name="twitter:image" content=""/>

<!-- Twitter Meta E -->

<!-- Favicon Icon S -->

    <link rel="icon" href="{{URL::asset('public/assets/image/favicon.ico') }}" type="image/x-icon"/>

    <link rel="apple-touch-icon" sizes="144x144" href="{{ URL::asset('public/assets/image/apple-icon-144x144.png') }}"/>

    <link rel="apple-touch-icon" sizes="114x114" href="{{ URL::asset('public/assets/image/apple-icon-114x114.png') }}"/>

    <link rel="apple-touch-icon" sizes="72x72" href="{{URL::asset('public/assets/image/apple-icon-72x72.png') }}"/>

    <link rel="apple-touch-icon" sizes="57x57" href="{{URL::asset('public/assets/image/apple-icon-57x57.png') }}"/>

<!-- Favicon Icon E -->

<!-- Canonical Link S -->

    <link rel="canonical" href=""/>

<!-- Canonical Link E -->

<!-- Style Sheet Link S -->

    <link rel="stylesheet" href="{{URL::asset('public/assets/css/main.css') }}"/>

<!-- Style Sheet Link E -->

<!-- Java Script Link S -->

    <script type="text/javascript" src="{{URL::asset('public/assets/libraries/jquery-3.2.1/js/jquery-3.2.1.min.js') }}"></script>

    <script type="text/javascript">

        /* Google Font API S */

            $(window).on("load", function() {

                WebFontConfig = {

                    google: { families: ['Dosis:200,300,400,500,600,700,800'] }

                };

                (function() {

                    var wf = document.createElement('script');

                    wf.src = ('https:' == document.location.protocol ? 'https' : 'http') + '://ajax.googleapis.com/ajax/libs/webfont/1.5.18/webfont.js';

                    wf.type = 'text/javascript';

                    wf.async = 'true';

                    var s = document.getElementsByTagName('script')[0];

                    s.parentNode.insertBefore(wf, s);

                })();

            });

        /* Google Font API E */

    </script>

<!-- Java Script Link E -->

</head>

<body class=>

<!-- Page Loader S -->
<!-- Page Loader E -->

<!-- Scroll To Top S -->

    <div id="back_top" title="Scroll To Top" style="display: none;">

        <i class="icon fa fa-caret-up"></i>

    </div>

<!-- Scroll To Top E -->

<div id="wrapper">

    @include('partials.header')

    @yield('content')



 </div>
    @include('partials.footer')
<!-- Java Script S -->

    <script src="{{URL::asset('public/assets/libraries/bootstrap/js/popper.min.js') }}"></script>

    <script src="{{URL::asset('public/assets/libraries/bootstrap/js/bootstrap.min.js') }}"></script>

    <script src="{{URL::asset('public/assets/libraries/OwlCarousel2-2.2.1/js/owl.carousel.min.js') }}"></script>

    <script src="{{URL::asset('public/assets/libraries/wow/js/wow.min.js') }}"></script>

    <script src="{{URL::asset('public/assets/libraries/back-top/js/back-top.js') }}"></script>

    <script src="{{URL::asset('public/assets/js/custom.js') }}"></script> 

<!-- Java Script E -->

</body>

</html>