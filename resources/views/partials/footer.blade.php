<footer>

    <!-- main.css | /* Footer Section S */ -->

    <div class="copyright-main">

        <div class="container">
            
            <div class="copyright_section">
                
                <div class="copyright">© Copyright DropShipping Tool 2018. ALL RIGHT RESREVED</div>

                <div class="social_icon">
                    
                    <a href="javascript:;" title="Facebook">
                        
                        <i class="fa fa-facebook"></i>

                    </a>

                    <a href="javascript:;" title="Twitter">
                        
                        <i class="fa fa-twitter"></i>

                    </a>

                    <a href="javascript:;" title="Linkedin">
                        
                        <i class="fa fa-linkedin"></i>

                    </a>

                    <a href="javascript:;" title="instagram">
                        
                        <i class="fa fa-instagram"></i>

                    </a>

                    <a href="javascript:;" title="Google Plus">
                        
                        <i class="fa fa-google-plus"></i>

                    </a>

                </div>

            </div>

        </div>

    </div>

</footer>

</div>