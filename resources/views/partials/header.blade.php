<header>
  <div class="header-main">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <div class="logo">
                        <a href="#" title="Autods Tools">
                            <img src="https://autodstools.com/neon_dashboard/assets/images/Logo_white.png" style="width:80px" alt="Drop Shipping Tool" >
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</header>   