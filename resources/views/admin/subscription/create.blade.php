@extends('backend_layout.app')	
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item active">Add Subscription</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
    	<div class="row">
    		<div class="col-md-6">
    			<div class="card">
				    <div class="card-header text-theme">
				        <strong>Subscription</strong>
				    </div>
				    <div class="card-body">
				    	@if(session('flash_message'))
					        <div class="alert alert-success">
					            {!! session('flash_message') !!}
					        </div>
					    @endif
				        <form action="{{ url('admin/subscription/add') }}" method="post" class="form-horizontal">
				        	{{ csrf_field()}}
				            <div class="form-group row">
				                <label class="col-md-3 form-control-label">Plan Name</label>
				                <div class="col-md-9">
				                    <input type="text" id="name" name="name" class="form-control" placeholder="Subscription name">
				                    
				                </div>
				            </div>
				            <div class="form-group row">
				                <label class="col-md-3 form-control-label">Price</label>
				                <div class="col-md-9">
				                    <input type="number" id="subPrice" name="subPrice" class="form-control">
				   				</div>
				            </div>
				            <div class="form-group row">
				                <label class="col-md-3 form-control-label">Validity</label>
				                <div class="col-md-9">
				                    <input type="text" id="subValidity" name="subValidity" class="form-control">
				   				</div>
				            </div>
				          	<div class="form-group row">
				          		@foreach($result as $r)
				          			<div class="col-md-6">
				          				<input type="checkbox" name="access_list[]" value="{{$r->id}}" /> {{$r->name}} 
   										<br>
				          			</div>
				          		@endforeach
				          	</div>
				            <div class="card-footer">
						        <input type="submit" class="btn btn-primary" value="Save">
						    </div>
				        </form>
				    </div>
				    
				</div>
    		</div>
		</div>
	</div>
</div>
@endsection
@section('footer_script')
@endsection