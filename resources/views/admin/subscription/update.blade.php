@extends('backend_layout.app')	
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item active">Update Subscription</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
    	<div class="row">
    		<div class="col-md-6">
    			<div class="card">
				    <div class="card-header text-theme">
				        <strong>Subscription</strong>
				    </div>
				    <div class="card-body">
				    	@if(session('flash_message'))
					        <div class="alert alert-success">
					            {!! session('flash_message') !!}
					        </div>
					    @endif
				        <form action="{{ url('admin/subscription/update') }}" method="post" class="form-horizontal">
				        	{{ csrf_field()}}
				        	<input type="hidden" id="hidden_id" name="edit_id" value="{{$subscriptionObj->id}}" >
				            <div class="form-group row">
				                <label class="col-md-3 form-control-label">Subscription Name</label>
				                <div class="col-md-9">
				                    <input type="text" id="name" name="edit_name" class="form-control" placeholder="Subscription name" value="{{$subscriptionObj->name}}">
				                    
				                </div>
				            </div>
				            <div class="form-group row">
				                <label class="col-md-3 form-control-label">Price</label>
				                <div class="col-md-9">
				                    <input type="number" id="subPrice" name="edit_subPrice" class="form-control" value="{{$subscriptionObj->price}}">
				   				</div>
				            </div>
				            <div class="form-group row">
				                <label class="col-md-3 form-control-label">Validity</label>
				                <div class="col-md-9">
				                    <input type="text" id="subValidity" name="edit_subValidity" class="form-control" value="{{$subscriptionObj->validity}}">
				   				</div>
				            </div>
				            <div class="form-group row">
				          	 @foreach($result as $r) 
					          	@if(in_array($r->id, $accessList))
					          		<div class="col-md-6">
					          			<input type="checkbox" name="access_list[]" value="{{$r->id}}" checked="checked" /> {{$r->name}} 
	   									<br>
					          		</div>
					          		@else
					          			<div class="col-md-6">
					          			<input type="checkbox" name="access_list[]" value="{{$r->id}}"/> {{$r->name}} 
	   									<br>
					          		</div>
				          		@endif
				          	 @endforeach                              
				          	</div>
				            <div class="card-footer">
						        <input type="submit" class="btn btn-primary" value="Save">
						    </div>
				        </form>
				    </div>
				    
				</div>
    		</div>
		</div>
	</div>
</div>
@endsection
@section('footer_script')
@endsection