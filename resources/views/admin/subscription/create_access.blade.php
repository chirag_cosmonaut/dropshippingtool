@extends('backend_layout.app')	
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item active">Add Access</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
    	<div class="row">
    		<div class="col-md-6">
    			<div class="card">
				    <div class="card-header text-theme">
				        <strong>Subscription Access</strong>
				    </div>
				    <div class="card-body">
				    	@if(session('flash_message'))
					        <div class="alert alert-success">
					            {!! session('flash_message') !!}
					        </div>
					    @endif
				        <form action="{{ url('admin/subscription/create_access') }}" method="post" class="form-horizontal">
				        	{{ csrf_field()}}
				            <div class="form-group row">
				                <label class="col-md-3 form-control-label">Access Name</label>
				                <div class="col-md-9">
				                    <input type="text" id="name" name="name" class="form-control" placeholder="Access name"> 
				                </div>
				            </div>
				            <div class="card-footer">
						        <input type="submit" class="btn btn-primary" value="Save">
						    </div>
				        </form>
				    </div>
				    
				</div>
    		</div>
		</div>
	</div>
</div>
@endsection
@section('footer_script')
@endsection