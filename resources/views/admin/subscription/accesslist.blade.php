@extends('backend_layout.app')	
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item active">Subscription Access</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @if(session('flash_message'))
        <div class="alert alert-success">
            {!! session('flash_message') !!}
        </div>
        @endif
        <div class="row">
            <div class="col-md-4">
                <h3>Access List</h3>
            </div>
             <div class="col-md-8">
              <a href="javascript:;" data-target="#add_access" data-toggle="modal" class="btn btn-theme pull-right">Add Access</a>
             </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-accent-theme">
                    <div class="card-body">
                        <div class="table-responsive">
                            <table class="table" id="access_list" width="80%">
                               <thead>
                                <tr>
                                  <th>#</th>
                                  <th>Access Name</th>
                                  <th>Action</th>
                                </tr>
                               </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal for add access list -->
<div class="modal fade bs-example-modal-default"  id="add_access" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog  modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Subscription Access</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <strong>Access Name:</strong>
                <div class="form-group row">
                    <div class="col-md-12">
                       <input type="text" id="access_name" name="access_name" class="form-control" data-validate="required" placeholder="Access name" value="">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-primary" onclick="saveAccess()">Save</a>
            </div>
        </div>
    </div>
</div>
<!-- Modal end -->
<!-- Edit Access list modal -->
<div class="modal fade bs-example-modal-default"  id="edit_access" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog  modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Update Subscription Access</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <div class="modal-body">
                <strong>Access Name:</strong>
                <div class="form-group row">
                    <div class="col-md-12">
                       <input type="text" id="edit_access" name="edit_access" class="form-control" data-validate="required" placeholder="Access name" value="">
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <a href="javascript:;" class="btn btn-primary">Save</a>
            </div>
        </div>
    </div>
</div>
<!--  -->
@endsection
@section('footer_script')
<script src="{{ URL::asset('public/admin-assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript">
  $(function() {
  $('#access_list').DataTable({
    processing: true,
    serverSide: true,
    responsive: true,
    ordering: true,
    destroy: true,
    scrollX:true,
    pageLength:10,
    bStateSave:false,
    dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
    ajax: '{!! route('subscription_access.data') !!}',
    columns: [
      { data: 'id'  , "searchable":true ,name:'id'},
      { data: 'name',"searchable":true, name: 'name' },
      { data: 'action',"searchable":false, name: 'action' },
    ]
  });
  $.ajaxSetup({
    headers: {
      'X-CSRF-Token': '{{ csrf_token() }}'
    }
  });
})

function saveAccess()
{
  var name = $('#access_name').val();
  var token = $('meta[name="csrf-token"]').attr('content');
  $.post('{{url("admin/subscription/save_access")}}' ,
    {
      access_name : name,
      _token :token
    },
    function(response){
      alertAjax(response);
    });
}
</script>
@endsection