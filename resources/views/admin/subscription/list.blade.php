@extends('backend_layout.app')	
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item active">Subscription</li>
</ol>

<div class="container-fluid">
    <div class="animated fadeIn">
        @if(session('flash_message'))
        <div class="alert alert-success">
            {!! session('flash_message') !!}
        </div>
        @endif
        <div class="row">
            <div class="col-md-4">
                <h3>Subscription Plans</h3>
            </div>
             <div class="col-md-8">
             	<a href="{{ url('admin/subscription/add_subscription') }}" class="btn btn-theme pull-right">Add Plan</a>
             </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-accent-theme">
                    <div class="card-body">
                        <div class="table-responsive">
							<table class="table" id="example" width="100%">
                               <thead>
                               	<tr>
                                  <th>#</th>
                               		<th>Subscription Name</th>
                               		<th>Price</th>
                               		<th>Validity</th>
                               		<!-- <th>Status</th> -->
                               		<th>Action</th>
                               	</tr>
                               </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection

@section('footer_script')
<script src="{{ URL::asset('public/admin-assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript">
$(function() {
	$('#example').DataTable({
		processing: true,
		serverSide: true,
		responsive: true,
		ordering: true,
		destroy: true,
		scrollX:true,
		pageLength:10,
		bStateSave:false,
		dom: 'Bfrtip',
        buttons: [
            'copy', 'csv', 'excel', 'pdf', 'print'
        ],
		ajax: '{!! route('subscription.data') !!}',
		columns: [
      { data: 'id'  , "searchable":true ,name:'id'},
			{ data: 'name',"searchable":true, name: 'name' },
			{ data: 'price',"searchable":true, name: 'price' },
			{ data: 'validity',"searchable":true, name: 'validity' },
		/*	{ data: 'status',"searchable":false, name: 'status' },*/
			{ data: 'action',"searchable":false, name: 'action' },
		]
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': '{{ csrf_token() }}'
		}
	});
})
</script>
@endsection