@extends('backend_layout.app')	
@section('content')
<!-- Breadcrumb -->
<ol class="breadcrumb bc-colored bg-theme" id="breadcrumb">
    <li class="breadcrumb-item ">
        <a href="">Home</a>
    </li>
    <li class="breadcrumb-item active">Users</li>
</ol>
<div class="container-fluid">
    <div class="animated fadeIn">
        @if(session('flash_message'))
        <div class="alert alert-success">
            {!! session('flash_message') !!}
        </div>
        @endif
        <div class="row">
            <div class="col-md-4">
                <h3>User Details</h3>
            </div>
             <div class="col-md-8">
             	<a href="javascript:;" data-target="#add_user" data-toggle="modal" class="btn btn-theme pull-right">Add User</a>
             </div>
        </div>
        <div class="row">
            <div class="col-md-12">
                <div class="card card-accent-theme">
                    <div class="card-body">
                        <div class="table-responsive">
							<table class="table" id="example" width="100%">
                               <thead>
                               	<tr>
                                  <th>#</th>
                               	  <th>User Name</th>
                               	  <th>Email</th>
                               	  <th>Status</th>
                               	  <th>Action</th>
                               	</tr>
                               </thead>
                            </table>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<!-- Modal for Add User -->
<div class="modal fade bs-example-modal-default"  id="add_user" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog  modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Add User</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="{{ url('admin/user/add') }}">
            	{{ csrf_field()}}
            	<div class="modal-body">
	                <strong>Name:</strong>
	                <div class="form-group row">
	                    <div class="col-md-12">
	                       <input type="text" id="name" name="name" class="form-control" data-validate="required" placeholder="Name" value="">
	                    </div>
	                </div>
	                <strong>Email:</strong>
	                <div class="form-group row">
	                    <div class="col-md-12">
	                       <input type="email" id="email" name="email" class="form-control" data-validate="required" placeholder="Email" value="">
	                    </div>
	                </div>
                  <strong>User Role:</strong>
                <div class="form-group row">
                  <div class="col-md-12">
                    <select name="usersRole" class="form-control">
                      @if(count($roles)>0)
                        @foreach($roles as $r)
                            <option value="{{$r->name}}">{{$r->name}}</option>  
                        @endforeach
                      @else
                            <option>No roles</option>
                      @endif
                    </select>
                  </div>
                </div>
            	</div>
	            <div class="modal-footer">
	                <input type="submit" class="btn btn-primary" value="Save" />
	            </div>
            </form>
            
        </div>
    </div>
</div>
<!-- Modal for Edit User -->
<div class="modal fade bs-example-modal-default"  id="edit_user" tabindex="-1" role="dialog"  aria-hidden="true" style="display: none;">
    <div class="modal-dialog  modal-dialog-centered-custom">
        <div class="modal-content">
            <div class="modal-header">
                <h6 class="modal-title" ><p>Edit User</p></h6>
                <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
            </div>
            <form method="post" action="{{ url('admin/user/update') }}">
              {{ csrf_field()}}
              <div class="modal-body">
                  <input type="hidden" name="edit_id" value="">
                  <strong>Name:</strong>
                  <div class="form-group row">
                      <div class="col-md-12">
                         <input type="text" id="edit_name" name="edit_name" class="form-control" data-validate="required" placeholder="Name" value="">
                      </div>
                  </div>
                  <strong>Email:</strong>
                  <div class="form-group row">
                      <div class="col-md-12">
                         <input type="email" id="edit_email" name="edit_email" class="form-control" data-validate="required" placeholder="Email" value="" readonly>
                      </div>
                  </div>
                  <strong>User Role:</strong>
                <div class="form-group row">
                  <div class="col-md-12">
                    <select name="edit_usersRole" value="usersRole" class="form-control">
                      @if(count($roles)>0)
                        @foreach($roles as $r)
                            <option value="{{$r->name}}">{{$r->name}}</option>  
                        @endforeach
                      @else
                            <option>No roles</option>
                      @endif
                    </select>
                  </div>
                </div>
              </div>
              <div class="modal-footer">
                  <input type="submit" class="btn btn-primary" value="Save" />
              </div>
            </form>
            
        </div>
    </div>
</div>
@endsection
@section('footer_script')
<script src="{{ URL::asset('public/admin-assets/libs/bootstrap-datepicker/js/bootstrap-datepicker.min.js') }}"></script>

<script type="text/javascript">
$(function() {
	$('#example').DataTable({
		processing: true,
		serverSide: true,
		responsive: true,
		ordering: true,
		destroy: true,
		scrollX:true,
		pageLength:10,
		bStateSave:false,
		dom: 'Bfrtip',
		ajax: '{!! route('user.data') !!}',
		columns: [
      	{ data: 'id'  , "searchable":true ,name:'id'},
		{ data: 'name',"searchable":true, name:'name' },
		{ data: 'email',"searchable":true,name:'email'},
		{ data: 'status' ,"searchable":false,name:'status'},
		{ data: 'action',"searchable":false, name:'action' },
		]
	});
	$.ajaxSetup({
		headers: {
			'X-CSRF-Token': '{{ csrf_token() }}'
		}
	});
})

</script>
@endsection