<?php

/*
  |--------------------------------------------------------------------------
  | Web Routes
  |--------------------------------------------------------------------------
  |
  | Here is where you can register web routes for your application. These
  | routes are loaded by the RouteServiceProvider within a group which
  | contains the "web" middleware group. Now create something great!
  |
 */



Auth::routes();

/*Verification module*/
Route::post('user/register','Auth\RegisterController@store')->name('user/register');
Route::get('register/verify/{confirmationCode}','Auth\RegisterController@confirm');
Route::post('login/verify_user','Auth\LoginController@signin')->name('login/verify_user');

  

Route::get('/', 'HomeController@index')->name('home');
Route::get('walmart/{item_id}', 'HomeController@getWalmartItemDetail');
Route::get('signout', 'Auth\LoginController@logout');

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/about-us', 'HomeController@about')->name('home');
Route::post('/store-token', 'Client\ChannelController@storeToken');
Route::group(['middleware' => 'web'], function () {
    Route::auth();
    Route::group(['middleware' => 'auth'], function () {
        Route::any('changeAccount', 'Client\ChannelController@switchAccount')->name('change.account');
        // All routes you put here can be accessible to all authenticated users
        Route::get('dashboard', 'Client\DashboardController@index')->name('dashboard');
        Route::any('order-profit', 'Client\DashboardController@getOrderProfitByWeekly')->name('order_profit');
        
        /*Connect eBay Account*/
        Route::get('ebay_channel', 'Client\ChannelController@ebay_channel')->name('ebay_channel');
        Route::post('connect_ebay', 'Client\ChannelController@connectStore')->name('connectStore');
        Route::any('ebay_channel/getSessionId', 'Client\ChannelController@getSessionId')->name('ebay_channel.getSessionId');
        Route::any('ebay_channel/fetchToken', 'Client\ChannelController@fetchToken')->name('ebay_channel.fetch_token');
        Route::any('ebay_channel/privacyPolicy', 'Client\ChannelController@privacyPolicy')->name('ebay_channel.privacy_policy');
        Route::any('ebay_channel/authFail', 'Client\ChannelController@authFail')->name('ebay_channel.auth_fail');
        /*Connect eBay Account*/

        Route::get('amazon_channel', 'Client\ChannelController@amazon_channel')->name('amazon_channel');
        Route::post('connect-mws', 'Client\ChannelController@connectMWS')->name('connectMWS');

        Route::get('untracked-listing', 'Client\UntrackedController@index')->name('untracked-listing');
        Route::any('untrackedlisting/data', 'Client\UntrackedController@listData')->name('untrackedlisting.data');
        Route::any('uploadASINFile', 'Client\UntrackedController@uploadASINFile')->name('uploadASINFile.data');
        /*Active Listing */
        Route::get('active-listing', 'Client\ListingController@getActiveListing')->name('active-listing');
        Route::any('active-listing/import', 'Client\ListingController@ImportProducts')->name('active-listing/import');
        Route::any('activelisting/data', 'Client\ListingController@Products')->name('activelisting.data');
        Route::any('activelisting/edit/{id}','ActiveListingController@edit')->name('activelisting.edit');
        Route::get('get_price', 'Client\ListingController@getPrices');
        /*Active Listing*/


        Route::get('customer-service', 'Client\ListingController@getCustomerService')->name('customer-service');
        Route::get('finder', 'Client\ListingController@getFinder')->name('finder');
        Route::get('uploader', 'Client\ListingController@getUploader')->name('uploader');
        Route::get('invoice', 'Client\ListingController@getInvoice')->name('invoice');
        Route::post('uploader/grabDetail', 'Client\UploadController@grabItem');
        

        /*Monitors*/
        Route::get('apply-profits', 'Client\MonitorController@applyProfits')->name('applyProfits');

        Route::get('monitors', 'Client\MonitorController@getMonitors')->name('monitors');
        Route::post('monitor/product_monitor','Client\MonitorController@saveProductMonitor');
        Route::post('monitor/orders_monitor','Client\MonitorController@saveOrdersMonior');
        Route::post('monitor/profit_monitor', 'Client\MonitorController@saveProfitMonitor');
        Route::post('monitor/amazon_account','Client\MonitorController@saveAmazonAccount');
        Route::post('monitor/order_process', 'Client\MonitorController@saveOrderProcessing');
        Route::post('monitor/start_stop', 'Client\MonitorController@startStopMonitor');
        Route::any('profitable-monitor','Client\MonitorController@profitableMonitor')->name('profitable_monitor');                    
        /*Monitors*/

        /*Tags*/
        Route::get('tag','Client\TagController@getTags');
        Route::get('tag.data', 'Client\TagController@ajaxData')->name('tag.data');
        Route::post('tag/add_tag','Client\TagController@addTags');
        Route::post('tag/update_tag','Client\TagController@updateTag');
        Route::post('tag/get_tag','Client\TagController@getTag');
        Route::get('tag/delete','Client\TagController@destroy')->name('tag.delete');
        /*Tags*/

        /*Order Module*/
        Route::get('order', 'Client\OrderController@OrderList')->name('order');     
        Route::any('order/import', 'Client\OrderController@ImportOrders')->name('order.import');
        Route::get('order/data', 'Client\OrderController@Orders')->name('order.data');
        Route::get('order/detail/{id}','Client\OrderController@orderDetail')->name('order.detail');
        Route::any('order/addtag','Client\TagController@saveTag')->name('addtag');
        Route::any('order/savetags','Client\OrderController@saveTags');
        Route::any('order/saveProfit','Client\OrderController@saveProfit')->name('orders.saveProfit');
        Route::any('order/print/{id}','Client\OrderController@printOrder')->name('order.print');
        Route::any('order/change_address','Client\OrderController@updateShippingAddress');
        Route::any('order/synchronize','Client\OrderController@orderSynchronization')->name('order.synchronize');
        Route::any('order/leave_feedback','Client\OrderController@leaveFeedback');
        Route::any('order/add_note','Client\OrderController@addOrderNote')->name('order.add_note');
        Route::any('order/update_note','Client\OrderController@updateOrderNote')->name('order.update_note');
        Route::any('order/delete_note','Client\OrderController@deleteOrderNote')->name('order.delete_note');
        Route::any('order/update_tracking','Client\OrderController@updateTrackingId')->name('order.update_tracking');
        Route::any('order/change_status','Client\OrderController@changeStatus');
        Route::any('order/change_ebay_status','Client\OrderController@changeEbayStatus');
        Route::any('order/saveChanges','Client\OrderController@saveOrderChanges')->name('orders.saveChanges');
        Route::any('order/contact_buyer','Client\OrderController@contactBuyer');
        Route::any('order/cancel_order','Client\OrderController@cancelOrder');
        /*Order Module*/


        /* General Settings*/
        Route::any('settings', 'Client\SettingsController@getSettings')->name('settings');
        Route::any('settings/update', 'Client\SettingsController@update')->name('settings.update');
        Route::post('block_product','Client\SettingsController@blockProducts');
        /* General Settings*/


        /*User Profile*/
        Route::get('profile/edit','UserController@edit');
        Route::get('profile','UserController@show');
        Route::post('profile/update','UserController@update')->name('profile.update');
        /*User Profile*/
    });


    Route::group(['middleware' => ['auth', 'super-admin']], function () {
        // All routes you put here can only be accessible to users with super-admin role
        Route::get('/admin-panel', 'Admin\DashboardController@index')->name('admin-panel');
        Route::get('/admin-panel', 'Admin\DashboardController@index')->name('admin-panel');
        // Get Data
        Route::get('/admin-panel/genre/getdata', 'Admin\GenreController@getAjaxData')->name('admin-genre.getdata');
        Route::resource('/admin-panel/genre','Admin\GenreController',[
            'names' => [
                'index' => 'admin-genre.index',
                'store' => 'admin-genre.store',
                'create' => 'admin-genre.create',
                'show' => 'admin-genre.show',
                'edit' => 'admin-genre.edit',
                'update' => 'admin-genre.update',
                'destroy' => 'admin-genre.destroy'
            ]
        ]);
/*Admin-Panel Subscription module*/
        Route::get('admin-panel/subscription','Admin\SubscriptionController@index')->name('admin-panel/subscription');
        Route::get('admin-panel/subscription/data','Admin\SubscriptionController@getData')->name('subscription.data');
        Route::get('admin/subscription/add_subscription','Admin\SubscriptionController@viewPage');
        Route::post('admin/subscription/add','Admin\SubscriptionController@create');
        Route::get('admin/subscription/edit/{id}','Admin\SubscriptionController@edit');
        Route::post('admin/subscription/update','Admin\SubscriptionController@update');
        Route::get('admin/subscription/delete/{id}','Admin\SubscriptionController@delete');
        /*Subscription_Access*/
        Route::get('admin-panel/subscription_access','Admin\SubscriptionController@accessList')->name('admin-panel/subscription_access');
         Route::get('admin/subscripion/access_data','Admin\SubscriptionController@getAccess')->name('subscription_access.data');
        Route::get('admin/subscription/add_access','Admin\SubscriptionController@viewAccess');
        Route::post('admin/subscription/create_access','Admin\SubscriptionController@addAccess');
        Route::get('admin/subscription/access_edit/{id}','Admin\SubscriptionController@editAccess');
        Route::get('admin/subscription/access_delete/{id}','Admin\SubscriptionController@deleteAccess');
        Route::post('admin/subscription/access_update','Admin\SubscriptionController@updateAccess');

/*End of Subscription module*/
    });
});


Route::group(['middleware' => ['auth']], function() {
    Route::resource('roles', 'RoleController');
    Route::resource('users', 'UserController');
    Route::resource('products', 'ProductController');
});



Route::get('404', function() {
    return view('error.404');
});
Route::get('/clear', function() {
    $exitCode6 = Artisan::call('cache:clear');
    return redirect('/');
});
Route::get('/cache', function() {
    $exitCode6 = Artisan::call('cache:clear');
    return redirect('/');
});