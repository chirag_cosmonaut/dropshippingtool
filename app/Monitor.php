<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class Monitor extends Model
{


   public function saveProductsMonitor($formData)
   {
      $user_id = Auth()->user()->id;
      $account_id = session()->get('account');
      $product_monitor =DB::table('products_monitor')->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)->first();

      $productMonitorArray = [];
      $productMonitorArray['default_break_event']           = $formData['default_break_event'];
      $productMonitorArray['additional_dollar']             = $formData['additional_dollar'];
      $productMonitorArray['additional_per']                = $formData['additional_per'];
      $productMonitorArray['end_untracked_listing']         = $formData['end_untracked_listing'];
      $productMonitorArray['not_available_listing']         = $formData['not_available_listing'];
      $productMonitorArray['when_product_available']        = $formData['when_product_available'];
      $productMonitorArray['quantity_to_raise_to']          = $formData['quantity_to_raise_to'];
      $productMonitorArray['dynamic_policy_creation']       = $formData['dynamic_policy_creation'];
      $productMonitorArray['min_allowed_quantity_in_stock'] = $formData['min_allowed_quantity_in_stock'];
      $productMonitorArray['min_profit_dollar_per_product'] = $formData['min_profit_dollar_per_product'];
      $productMonitorArray['when_product_vero']             = $formData['when_product_vero'];
      $productMonitorArray['when_product_blocked']          = $formData['when_product_blocked'];
      $productMonitorArray['max_shipping_time']             = $formData['max_shipping_time'];
      $productMonitorArray['automatic_sku_filling']         = $formData['automatic_sku_filling'];
      $productMonitorArray['account_id']                    = $account_id;
      $productMonitorArray['user_id']                       = $user_id;
      if(empty($product_monitor))
      {
         DB::table('products_monitor')->insert($productMonitorArray);
         $message = "Record Inserted Successfully";
      }
      else
      {
         DB::table('products_monitor')->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)->update($productMonitorArray);
         $message = "Record Updated Successfully";
      }
      return $message;
   }
   public function saveOrdersMonior($formData)
   {
      $message = "";
      $error   = "";
      $user_id = Auth()->user()->id;
      $account_id = session()->get('account');
      $order_monitor = DB::table('orders_monitor')->where('user_id',"=",$user_id)->where('account_id',"=",
         $account_id)->first();

      $orderMonitorArray = [];
      $orderMonitorArray['when_product_sold']               = $formData['when_product_sold'];
      $orderMonitorArray['quantity_to_raise_to']            = $formData['quantity_to_raise_to'];
      $orderMonitorArray['when_product_sold_change_price']  = $formData['when_product_sold_change_price'];
      $orderMonitorArray['start_price_raise_after_x_sold']  = $formData['start_price_raise_after_x_sold'];
      $orderMonitorArray['raise_only_for_first_sell']       = $formData['raise_only_for_first_sell'];
      $orderMonitorArray['raise_for_each_sale']             = $formData['raise_for_each_sale'];
      $orderMonitorArray['raise_additional_per']            = $formData['raise_additional_per'];
      $orderMonitorArray['raise_additional_dollar']         = $formData['raise_additional_dollar'];
      $orderMonitorArray['max_per_raise_limit']             = $formData['max_per_raise_limit'];
      $orderMonitorArray['max_price_raise_limit']           = $formData['max_price_raise_limit'];
      $orderMonitorArray['drop_price_product_not_sold']     = $formData['drop_price_product_not_sold'];
      $orderMonitorArray['drop_price_afterX_days']          = $formData['drop_price_afterX_days'];
      $orderMonitorArray['drop_price_per']                  = $formData['drop_price_per'];
      $orderMonitorArray['drop_price_amount']               = $formData['drop_price_amount'];
      $orderMonitorArray['min_drop_price_per']              = $formData['min_drop_price_per'];
      $orderMonitorArray['min_drop_price_amount']           = $formData['min_drop_price_amount'];
      $orderMonitorArray['account_id']                      = $account_id;
      $orderMonitorArray['user_id']                         = $user_id;
      if(empty($order_monitor))
      {
         DB::table('orders_monitor')->insert($orderMonitorArray);
         $message = "Record Inserted Successfully";
      }
      else
      {
         DB::table('orders_monitor')->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)
         ->update($orderMonitorArray);
         $message = "Record Updated Successfully";
      }
      return $message;
   }
   public function saveProfitMonitor($formData)
   {
      $message = "";
      $error = "";
      $user_id = Auth()->user()->id;
      $account_id = session()->get('account');
      $profitable_monitors = DB::table('profitable_monitor')->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)->first();

      $profitableMonitorArray = [];
      $profitableMonitorArray['listing_not_profitable']     = $formData['listing_not_profitable'];
      $profitableMonitorArray['listing_left_days']          = $formData['listing_left_days'];
      $profitableMonitorArray['listing_min_sold_quantity']  = $formData['listing_min_sold_quantity'];
      $profitableMonitorArray['listing_min_watchers']       = $formData['listing_min_watchers'];
      $profitableMonitorArray['listing_min_views']          = $formData['listing_min_views'];
      $profitableMonitorArray['account_id']                 = $account_id;
      $profitableMonitorArray['user_id']                    = $user_id;
      if(empty($profitable_monitors))
      {
         DB::table('profitable_monitor')->insert($profitableMonitorArray);
         $message = "Record Inserted Successfully";
      }
      else
      {
         DB::table('profitable_monitor')->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)
         ->update($profitableMonitorArray);
         $message = "Record Updated Successfully";
      }
      return $message;
   }
   public function saveAmazonAccount($formData)
   {
      $message = "";
      $error = "";
      $user_id = Auth()->user()->id;
      $account_id = session()->get('account');
      $amazon_accounts = DB::table('amazon_account')->where('user_id',"=",$user_id)->where('account_id','=',
         $account_id)->first();

      $amazonAccountArray = [];
      $amazonAccountArray['purchase_account']  = $formData['purchase_account'];
      $amazonAccountArray['zip_code']          = $formData['zip_code'];
      $amazonAccountArray['country']           = $formData['country'];
      $amazonAccountArray['phone_number']      = $formData['phone_number'];
      $amazonAccountArray['Full_name']         = $formData['Full_name'];
      $amazonAccountArray['prime_account']     = $formData['prime_account'];
      $amazonAccountArray['account_id']        = $account_id;
      $amazonAccountArray['user_id']           = $user_id;
      if(empty($amazon_accounts))
      {
         DB::table('amazon_account')->insert($amazonAccountArray);
         $message = "Record Inserted Successfully";
      }
      else
      {
         DB::table('amazon_account')->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)
         ->update($amazonAccountArray);
         $message = "Record Updated Successfully";
      }
      return $message;
   }
   public function saveOrderProcessing($formData)
   {
      $message = "";
      $error = "";
      $user_id = Auth()->user()->id;
      $account_id = session()->get('account');
      $orders_processing = DB::table('order_processing')->where('user_id',"=",$user_id)->where('account_id',"=",
         $account_id)->first();

      $orderProcessingArray = [];
      $orderProcessingArray['auto_order']                   = $formData['auto_order'];
      $orderProcessingArray['bundle_add_on']                = $formData['bundle_add_on'];
      $orderProcessingArray['oder_only_prime']              = $formData['oder_only_prime'];
      $orderProcessingArray['hipshipper']                   = $formData['hipshipper'];
      $orderProcessingArray['max_product_price_auto_order'] = $formData['max_product_price_auto_order'];
      $orderProcessingArray['max_lose_amount']              = $formData['max_lose_amount'];
      $orderProcessingArray['set_order_shipped']            = $formData['set_order_shipped'];
      $orderProcessingArray['gift_message']                 = $formData['gift_message'];
      $orderProcessingArray['set_ontrack_shipping_carrier'] = $formData['set_ontrack_shipping_carrier'];
      $orderProcessingArray['account_id']                   = $account_id;
      $orderProcessingArray['user_id']                      = $user_id;
      if(empty($orders_processing))
      {
         DB::table('order_processing')->insert($orderProcessingArray);
         $message = "Record Inserted Successfully";
      }
      else
      {
         DB::table('order_processing')->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)
         ->update($orderProcessingArray);
         $message = "Record Updated Successfully";
      }
      return $message;
   }
}
