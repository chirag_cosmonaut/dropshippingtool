<?php


namespace App\Http\Controllers;


use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\User;
use Spatie\Permission\Models\Role;
use DB;
use Hash;
use Auth;
use Datatables;




class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = User::orderBy('id','DESC')->paginate(5);
        return view('users.index',compact('data'))
            ->with('i', ($request->input('page', 1) - 1) * 5);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $roles = Role::pluck('name','name')->all();
        return view('users.create',compact('roles'));
    }
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => 'required',
            'email' => 'required|email|unique:users,email'
        ]);
        $input = $request->all();
        $randomString = str_split('abcdefghijklmnopqrstuvwxyz'.'ABCDEFGHIJKLMNOP'.'0123456789'.'@#(&$');
        $var = '';
        foreach(array_rand($randomString,8) as $string)
        {
            $var .= $randomString[$string];
        }
        $input['password'] = Hash::make($var);
        $user = User::create($input);
        $user->assignRole($input['userRole']);
        return redirect()->route('admin/user/list')
                        ->with('success','User created successfully');
    }
    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show()
    {

        $user_id = Auth()->user()->id;
        $user = User::find($user_id);
        return view('profile.profile',compact('user'));
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $user_id = Auth()->user()->id;
        $user = User::find($user_id);
        return view('profile.edit',compact('user'));
       /* $roles = Role::pluck('name','name')->all();
        $userRole = $user->roles->pluck('name','name')->all();
        return view('users.edit',compact('user','roles','userRole'));*/
    }
    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        $user_id = Auth()->user()->id;
        $this->validate($request, [
            'editName' => 'required',
            'editEmail' => 'required'

        ]);
        if ($request->hasFile('profileImage')) 
        {
            $uploadPhoto = $request->file('profileImage');
            $name = str_slug($request['editName']).'.'.$uploadPhoto->getClientOriginalExtension();
            $destinationPath = public_path('/images/user');
            $imagePath = $destinationPath. "/".  $name;
            $uploadPhoto->move($destinationPath, $name);
            $request['profile_image']=$name;
        }
        $user = User::find($user_id);
        $user->name = $request['editName'];
        $user->email =$request['editEmail'];
        $user->profile_image = $request['profile_image'];
        $user->save();
        return redirect('profile/edit');
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $userObj = User::findOrFail($id);
        $userObj->delete();
        return redirect('admin/user/list');
                        
    }
    public function listUser(){
        $roles = DB::table('roles')->select('name')->get();
        return view('admin.user.list',compact('roles') );
    }
    public function getData(Request $request)
    {
        $userList = getAllRecord('users');
        return Datatables::of($userList)
        ->addColumn('action',function($userList){
            $action = '<a href="'.url('admin/user/edit/{id}',$userList->id).'"  data-target="#edit_user" data-toggle="modal" class="btn btn-theme btn-sm"><i class="fa fa-edit"></i></a>'. 
            '<a href="'.url('admin/user/delete/{id}',$userList->id).'"  class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>';
            return $action;
        })
        ->addColumn('status',function($userList){
            if($userList->isActive == 1){
                return '<span class="badge badge-pill badge-success">Active</span>' ;
            }
            else{
                return '<span class="badge badge-pill badge-danger">InActive</span>' ;
            }
        })
        ->rawColumns(['action','status'])
        ->setTotalRecords($userList->count())
        ->make(true);  
    }
    public function editUser($id)
    {
        $userObj = User::find($id);
        return view('admin.user.list',compact('userObj'));
    }
    public function updateUser(Request $request)
    {
        $this->validate($request,[
            'name' =>'required'
        ]);
        $userObj = User::find($request['edit_id']);
        $userObj->name = $request['edit_name'];
        $userObj->save(); 
        return redirect('admin/user/list');
    }
}