<?php

namespace App\Http\Controllers\Auth;

use App\User;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use App\Mail\EmailVerification;
use Illuminate\Support\Facades\Input;
use Mail;


class RegisterController extends Controller
{
    use RegistersUsers;

    public function store(Request $request){
        $request->validate([
            'name'      => 'required|min:2|unique:users',
            'email'     => 'required|email|unique:users',
            'password'  => 'required|confirmed|min:6'
        ]);
        $confirmation_code = str_random(64);
        $userObj            = new User();
        $userObj->name      = Input::get('name');
        $userObj->email     = Input::get('email');
        $userObj->password  = Hash::make(Input::get('password'));
        $userObj->remember_token = $confirmation_code;
        $userObj->isActive  = 0;
        $userObj->save();
        $userObj->assignRole('super-admin');
        $confirmMailArray   = [];
        $confirmMailArray['remember_token'] = $confirmation_code;
        $confirmMailArray["subject"]        = "Verify Your Account";
        $confirmMailArray["from"]           = 'sonali.cosmonautgroup@gmail.com';
        $confirmMailArray["to"]             = $request['email'];
        $settings['txtbody']                = view('email.verify',$confirmMailArray)->render();
        unset($confirmMailArray['txtbody']);
        sendEmail('email.verify',$confirmMailArray); 
        return redirect('login');
    }

    public function confirm($confirmation_code){
        $user = User::where('remember_token',"=",$confirmation_code)->first();
        $user->isActive         = 1;
        $user->remember_token   = null;
        $user->save();
        return redirect('login');
    }
}

   