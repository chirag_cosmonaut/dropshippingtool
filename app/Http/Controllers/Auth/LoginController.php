<?php

namespace App\Http\Controllers\Auth;

use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Foundation\Validation\ValidatesRequests;
use App\Http\Controllers\Redirect;
use App\User;
use App\Accounts;
use Cookie;

class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/dashboard';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function store()
    {
        $rules = [
            'email' => 'required|exists:users',
            'password' => 'required'
        ];
        $input = Input::only('email','password');
        $validator = Validator::make($input, $rules);

        if($validator->fails())
        {
            return Redirect::back()->withInput()->withErrors($validator);
        }
        $credentials = [
            'email' => Input::get('email'),
            'password' => Input::get('password'),
            'isActive' => 1
        ];

        if(! Auth::attempt($credentials))
        {
            return Redirect::back()
            ->withInput()
            ->withErrors([
                'credentials'=> 'We were unable to sign you in.'
            ]);
        }
        Flash::message('Welcome back');
        return Redirect::home();
    }

    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }
    public function logout(Request $request)
    {
        Cookie::make('account', session()->get('account'), 36000);
        Auth::guard('web')->logout();
        return redirect('/');

    }
    public function signin(Request $request)
    {
        $request->validate([
            'email' => 'required|email',
            'password' => 'required|min:6'
        ]);
        $verifyUser = User::where('email',"=", $request['email'])->first();
        if(!empty($verifyUser)){
            if(isset($verifyUser->isAcive) && $verifyUser->isAcive == '0'){
                $request->session()->flash('flash_message', 'Your Account is not verified! Please verified it.');
                return redirect('login');
            }else{
                $authSuccess = Auth::login($verifyUser, true);
                $account = Accounts::where('int_user_id','=',Auth()->user()->id)->first();
                if(!empty($account)){
                    session()->put('account',$account->id);
                }
                $request->session()->flash('flash_message','Your account is verified! Now you can login.');
                return redirect('dashboard');
            } 
        }else{
            $request->session()->flash('flash_message', 'Invalid Credential.');
            return redirect('login');
        }     
    }
}
