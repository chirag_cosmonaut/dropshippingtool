<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActiveListing;
use App\Monitor;
use DB;
use DateTime;
use DatePeriod;
use DateInterval;

class MonitorController extends Controller
{
   public function getMonitors()
   {
      $productMonitorValue  = getRecordByAccount('products_monitor');
      $ordersMonitorValue   = getRecordByAccount('orders_monitor');
      $profitMonitorValue   = getRecordByAccount('profitable_monitor');
      $amazonMonitorValue   = getRecordByAccount('amazon_account');
      $orderProcessingValue = getRecordByAccount('order_processing');
      return view('client.monitor.monitors',compact('productMonitorValue','ordersMonitorValue','profitMonitorValue','amazonMonitorValue','orderProcessingValue'));
   }
   public function saveProductMonitor(Request $request)
   {
      $message = "";
      $error   = "";
      $monitorObj = new Monitor();
      $formData = $request->all();
      $response = $monitorObj->saveProductsMonitor($formData);
     
      $response = [
        'response' => $response,
        'error'    => $error,
      ];
      $request->session()->flash('flash_message',$message);
      echo json_encode($response);
   }
   public function saveOrdersMonior(Request $request)
   {
      $message = "";
      $error   = "";
      $ordersMonitorObj = new Monitor();
      $formData = $request->all();
      $responseOrdersMonitor = $ordersMonitorObj->saveOrdersMonior($formData); 
      $response = [
        'response' => $responseOrdersMonitor,
        'error'    => $error,
      ];
      $request->session()->flash('flash_message',$message);
      echo json_encode($response);
   }
   public function saveProfitMonitor(Request $request)
   {
       $message = "";
      $error = "";
      $profitableMoniorObj = new Monitor();
      $formData = $request->all();
      $resonseSaveProfitable = $profitableMoniorObj->saveProfitMonitor($formData);
      $response = [
        'response' => $resonseSaveProfitable,
        'error'    => $error,
      ];
      $request->session()->flash('flash_message',$message);
      echo json_encode($response);
   }
   public function saveAmazonAccount(Request $request)
   {
      $message = "";
      $error = "";
      $user_id = Auth()->user()->id;
      $account_id = session()->get('account');
      $amazon_accounts = DB::table('amazon_account')->where('user_id',"=",$user_id)->where('account_id','=',
         $account_id)->first();

      $amazonAccountArray = [];
      $amazonAccountArray['purchase_account']  = $request['purchase_account'];
      $amazonAccountArray['zip_code']          = $request['zip_code'];
      $amazonAccountArray['country']           = $request['country'];
      $amazonAccountArray['phone_number']      = $request['phone_number'];
      $amazonAccountArray['Full_name']         = $request['Full_name'];
      $amazonAccountArray['prime_account']     = $request['prime_account'];
      $amazonAccountArray['account_id']        = $account_id;
      $amazonAccountArray['user_id']           = $user_id;
      if(empty($amazon_accounts))
      {
         DB::table('amazon_account')->insert($amazonAccountArray);
         $message = "Record Inserted Successfully";
      }
      else
      {
         DB::table('amazon_account')->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)
         ->update($amazonAccountArray);
         $message = "Record Updated Successfully";
      }
      $response = [
         'response' => $message,
         'error'    => $error,
      ];
      $request->session()->flash('flash message',$message);
      echo json_encode($response);
   }
    public function saveOrderProcessing(Request $request)
   {
      $message = "";
      $error = "";
      $orderProcessingObj = new Monitor();
      $formData = $request->all();
      $responseOrdersProcessing = $orderProcessingObj->saveOrderProcessing($formData);
      $response = [
        'response' => $responseOrdersProcessing,
        'error'    => $error,
      ];
      $request->session()->flash('flash_message',$message);
      echo json_encode($response);
   }
  public function startStopMonitor(Request $request)
  {
      $message = "";
      $error = "";
      $user_id = Auth()->user()->id;
      $account_id = session()->get('account');
      $monitorName = $request->monitorName;
      if($request->isStart=="true"){
        $startStopMonitor = DB::table($monitorName)->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)->update(['is_start'=>1]);
        $message ="Monitor Start";
      }else{
        $startStopMonitor = DB::table($monitorName)->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)->update(['is_start'=>0]);
        $message = "Monitor Stop";
      }
      $response = [
         'response' => $message,
         'error'    => $error,
      ];
      $request->session()->flash('flash message',$message);
      echo json_encode($response);
  }
  
  public function applyProfits(){
      $user_id = Auth()->user()->id;
      $account_id = session()->get('account');
      $profitableMonitor = DB::table('profitable_monitor')->where('user_id',"=",$user_id)->where('account_id',"=",$account_id)->first();
      $activeProducts = ActiveListing::select(['item.item_id as IID', 'item.time_left', 'item.hit_count', 'item.watch_count','selling_status.quantity_sold'])
              ->leftJoin('selling_status', 'selling_status.item_id', 'item.id')
              ->where('item.source_id', '!=', NULL)
              ->where('item.is_delete', '=', 0)
              ->where('item.account_id', '=', $account_id)
              ->where('item.user_id', '=', $user_id)->get();
      if (!empty($activeProducts)) {
          foreach ($activeProducts as $activeProduct) {
              $viewers = isset($activeProduct->hit_count)?$activeProduct->hit_count:0;
              $watchers = isset($activeProduct->watch_count)?$activeProduct->watch_count:0;
              $time_left = isset($activeProduct->time_left)?$activeProduct->time_left:0;
              $quantity_sold = isset($activeProduct->quantity_sold)?$activeProduct->quantity_sold:0;
              // $date = new DateTime();
              // $date->add(new DateInterval($time_left));
              // $left_days = $date->format('d');
              $left_days = 0;
              if (isset($time_left) && $time_left!=NULL && $time_left!='') {
                $interval = new DateInterval($time_left);
                $left_days = $interval->format('%d');
              }
              if (intval($watchers) > intval($profitableMonitor->listing_min_watchers) && intval($viewers) > intval($profitableMonitor->listing_min_views) && intval($left_days) > intval($profitableMonitor->listing_left_days) && intval($quantity_sold) > intval($profitableMonitor->listing_min_sold_quantity)) {
                
              }else{
                DB::table('notifications')->insert(['user_id'=>$user_id,'account_id'=>$account_id,'item_id'=>$activeProduct->IID,'message'=>'Item #'.$activeProduct->IID.' is non Profitable.']);
              }
          }
          $message ="Profitable Moniter applied Successfully.";
      }else{
          $message ="No product available.";
      }  
      session()->flash('flash_message',$message);    
      return redirect('monitors');
  }
}