<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\GrabItemApi;

class UploadController extends Controller
{
	public function grabItem(Request $request)
	{
		
		$item_id = $request['item_to_grab_id'];

		$grabItemApiObj = new GrabItemApi();
		$response 		= $grabItemApiObj->grabitem($item_id);
		echo "<pre>";
		print_r($response);
		exit;
	}
}
