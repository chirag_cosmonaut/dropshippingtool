<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Illuminate\Support\Facades\Redirect;
use App\Helpers\OrderApi;
use App\Helpers\ItemApi;
use App\Helpers\CustomerApi;
use App\Order;
use App\ActiveListing;
use App\Customer;
use App\Tag;
use App\helpers;
use DB;
use Datatables;

class OrderController extends Controller {

    public function OrderList() {
        return view('client.order.list');
    }

    public function ImportOrders(Request $request) {

        $formData = $request->all();
        $from = date('Y-m-d', strtotime($formData['from']));
        $to = date('Y-m-d', strtotime($formData['to']));

        $orderApiObj = new OrderApi();
        $response = $orderApiObj->importOrders($from, $to);
        /* echo "<pre>";
          print_r($response);
          exit(); */
        if ($response->Ack != 'Failure' && $response->OrderArray) {
            $orders = $response->OrderArray->Order;
            $this->saveOrders($orders);
            actionLog("Order Imported.");
            $request->session()->flash('flash_message', 'Orders Imported Succssfully');
        } else {
            $short_message = $response->Errors->ShortMessage;
            $log_message = $response->Errors->LongMessage;
            $error = $response->Errors->ErrorParameters->Value;
            $request->session()->flash('flash_message', $log_message . $error);
        }
        return Redirect('order');
    }

    public function Orders(Request $request) {
        $account_id = session()->get('account');
        $user_id = Auth()->user()->id;
        $orders = Order::select(['orders.*', 'transaction.final_value_fee', 'orders.order_id as OID', 'b1.id as BID', 'shipping_details.selling_manager_sales_record_number as record_id', 'item.item_id as IID', 'item.image', 'item.picture_details', 'shipping_address.name', 'b1.buyer_user_id', 'transaction.transaction_id'])
                ->leftJoin('buyer as b1', 'b1.id', 'orders.buyer_id')
                ->leftJoin('shipping_details', 'shipping_details.order_id', 'orders.id')
                ->leftJoin('shipping_address', 'shipping_address.order_id', 'orders.id')
                ->leftjoin('order_item', 'order_item.order_id', 'orders.id')
                ->leftJoin('item', 'item.id', 'order_item.item_id')
                ->leftJoin('transaction', 'transaction.order_id', 'orders.id')
                ->where('orders.user_id', '=', $user_id)
                ->where('orders.account_id', '=', $account_id)
                ->orderBy('orders.created_time', 'desc')
                ->groupBy('orders.id')
                ->distinct();

        $datatables = Datatables::of($orders)
                ->addColumn('edit', function($orders) {
                    if (!empty($orders)) {
                        return '<a href="' . route("order.detail", $orders->record_id) . '"><i class="fa fa-edit"></i></a>';
                    } else {
                        return '<a href="javascript:;"><i class="fa fa-edit"></i></a>';
                    }
                })
                ->addColumn('bulk', function($orders) {
                    return '<input type="checkbox" onchange="enableBulk();" name="bulk" value="' . $orders->id . '"/>';
                })
                ->addColumn('sales_order_id', function($orders) {
                    return '<a href="javascript:;">' . $orders->order_id . '</a>';
                })
                ->addColumn('source_order_id', function ($orders) {
                    /*$supplier_data = DB::table('order_item')->where('order_id', '=', $orders->id)->first();*/
                    $where = [
                        'order_id' => $orders->id
                    ];
                    $supplier_data = getAllRecord('order_item',$where,true);
                    if ($supplier_data->supplier_order_id != '') {
                        return $supplier_data->supplier_order_id;
                    } else {
                        return '   -   ';
                    }
                })
                ->addColumn('picture', function ($orders) {
                    if ($orders->picture_details != '') {
                        $image = '<img src="' . $orders->picture_details . '" height="100px" width="100px" alt="Product Image">';
                    } elseif ($orders->image != '' && file_exists($orders->image)) {
                        $image = '<img src="' . $orders->image . '" height="100px" width="100px" alt="Product Image">';
                    } else {
                        $image = '<img src="http://via.placeholder.com/100x100" alt="user" />';
                    }
                    return $image;
                })
                ->addColumn('item_id', function ($orders) {
                    return $orders->IID;
                })
                ->addColumn('source_item_id', function ($orders) {
                    $where = [
                        'order_id' => $orders->id
                    ];
                    $product_link = getAllRecord('order_item',$where,true);
                    if ($product_link->supplier_product_link != '') {
                        $domain = getDomain($product_link->supplier_product_link);
                        return '<a href="' . $product_link->supplier_product_link . '" target="_blank" title="Item Link">' . $domain . '</a>';
                    } else {
                        return '   -   ';
                    }
                })
                ->addColumn('purchase_price', function($orders) {
                    $where = [
                        'order_id' => $orders->id
                    ];
                    $supplier_data = getAllRecord('order_item',$where);
                    $cost_price = 0;
                    foreach ($supplier_data as $sd) {
                        $cost_price = $cost_price + floatval($sd->supplier_price);
                    }
                    return $cost_price;
                })
                ->addColumn('tax', function($orders) {
                    return $tax = 0.0;
                })
                ->addColumn('profit', function ($orders) {
                    $profit = getOrderProfit($orders->id);
                    return number_format($profit, 2);
                })
                ->addColumn('buyer_name', function($orders) {
                    return $orders->name;
                })
                ->addColumn('sold', function($orders) {
                    return '-';
                })
                ->addColumn('sold_date', function($orders) {
                    $soldDate = getFormatedDate($orders->created_time);
                    return '<span class="text-muted">
                            <i class="fa fa-clock-o"></i>&nbsp;' . date('M d Y', strtotime($soldDate)) . '
                        </span>';
                })
                ->addColumn('status', function ($orders) {
                    $where = [
                        'status' => 1
                    ];
                    $statuss = getAllRecord('order_status',$where);
                    $statusOptions = '';
                    foreach ($statuss as $st) {
                        $selected = $st->id == $orders->status ? 'selected' : '';
                        $statusOptions .= '<option value="' . $st->id . '" ' . $selected . '>' . $st->name . '</option>';
                    }
                    $status = '<select class="select2 form-control" name="status" onchange="changeStatus(this.value,' . $orders->id . ')">' . $statusOptions . '
				</select>&nbsp;&nbsp;&nbsp;';
                    return $status;
                })
                ->rawColumns(['bulk', 'edit', 'sales_order_id', 'source_order_id', 'item_id', 'picture', 'buyer_name', 'source_item_id', 'purchase_price', 'tax', 'status', 'sold', 'profit', 'sold_date'])
                ->setTotalRecords($orders->count());
        return $datatables->make(true);
    }

    public function orderDetail($id) {
        $where = [
            'selling_manager_sales_record_number' => $id
        ];
        $record_id = getAllRecord('shipping_details',$where,true);

        $next = DB::table('shipping_details')->select('selling_manager_sales_record_number')->where('selling_manager_sales_record_number', '>', $record_id->selling_manager_sales_record_number)->min('selling_manager_sales_record_number');
        $previous = DB::table('shipping_details')->select('selling_manager_sales_record_number')->where('selling_manager_sales_record_number', '<', $record_id->selling_manager_sales_record_number)->max('selling_manager_sales_record_number');

        $orderObj = new Order();
        $OrderDetail = $orderObj->getDetail($record_id->order_id);

        $buyerObj = new Customer();
        $buyerDetail = $buyerObj->getBuyerDetail($record_id->order_id);

        $productObj = new ActiveListing();
        $productDetails = $productObj->getProductDetails($record_id->order_id);
        $itemArray = [];
        $i = 0;
        foreach ($productDetails as $productDetail) {
            $whereTransaction = [
                'item_id'            => $productDetail->item_id,
                'order_line_item_id' => $productDetail->order_line_item_id,
                'order_id'           => $productDetail->order_id
            ];
            $transactionData = getAllRecord('transaction',$whereTransaction,true);
            
           /* $transactionData = DB::table('transaction')->select('transaction.shipping_details', 'transaction.transaction_price', 'transaction.quantity_purchased')
                            ->where('transaction.item_id', '=', $productDetail->item_id)
                            ->where('transaction.order_line_item_id', '=', $productDetail->order_line_item_id)
                            ->where('order_id', '=', $record_id->order_id)->first();*/

           /* $trackingData = DB::table('tracking')->select('*')->where('item_id', '=', $productDetail->id)->where('order_id', '=', $record_id->order_id)->get();*/
            $whereTrack = [
                'item_id' => $productDetail->id,
                'order_id' => $record_id->order_id
            ];
            $trackingData = getAllRecord('tracking',$whereTrack);
            $itemArray[$i] = [
                'item' => $productDetail,
                'transaction' => $transactionData,
                'tracking' => $trackingData,
                'trackCount' => count($trackingData)
            ];
            $i++;
        }
        $refunds = [];
        $refunds = getAllRecord('order_refunds',array('order_id'=> $OrderDetail->id),true);
        /*$refunds = DB::table('order_refunds')->select('order_refunds.refund_amount')->where('order_id', '=', $OrderDetail->id)->first();*/

        if ($OrderDetail->order_status == 'Cancelled') {
            $refunds = getAllRecord('order_refunds',array('order_id'=> $record_id->order_id),true);
        }


        $profit         = getOrderProfit($record_id->order_id);
        $orderStatus    = getOrderStatus();
        $tags           = getTags();
        $selectedtags   = getSelectedTags($record_id->order_id);


        $orderNotes = $orderObj->getOrderNotes($record_id->order_id);
        $trackingData = getAllRecord('order_refunds',array('order_id'=> $record_id->order_id));
        $feedbacks = DB::table('feedback')->select('feedback.comment_text', 'feedback.role')
                        ->leftJoin('transaction', 'transaction.transaction_id', 'feedback.transaction_id')
                        ->where('transaction.order_id', '=', $record_id->order_id)->get();
        /* $actions = DB::table('action_log')->select('action_log.*','users.name')
          ->where('order_id','=',$record_id->order_id)
          ->leftJoin('users','users.id','action_log.user')
          ->get(); */
        /* $Messages = $buyerObj->getOrderMessages($buyerDetail->buyer_user_id,$productDetail->item_id); */

        return view('client.order.detail', compact('next', 'previous', 'OrderDetail', 'buyerDetail', 'itemArray', 'feedbacks', 'trackingData', 'orderNotes', 'selectedtags', 'tags', 'orderStatus', 'profit'));
    }

    public function saveTags(Request $request) {
        $error = "";
        $response = "";
        $message = "";
        if (count($request->tags) >= 1) {
            DB::table('order_tags')->where('order_id', '=', $request->orderid)->delete();
            foreach ($request->tags as $tagid) {
                DB::table('order_tags')->insert(['order_id' => $request->orderid, 'tag_id' => $tagid]);
            }
        }
        $message .= 'Tags Succssfully Updated';
        $request->session()->flash('flash_message', $message);
        $response = [
            'response' => $message,
            'error' => $error
        ];
        echo json_encode($response);
    }

    public function updateShippingAddress(Request $request) {
        $error = '';
        $message = '';
        $formData = $request->all();
        DB::table('shipping_address')->where('order_id', '=', $formData['order_id'])
                ->update([
                    'name' => $formData['contact_name'],
                    'street1' => $formData['street1'],
                    'street2' => $formData['street2'],
                    'city_name' => $formData['city'],
                    'phone' => $formData['phone'],
                    'state_or_provinance' => $formData['state'],
                    'postal_code' => $formData['zip'],
                    'country_name' => $formData['country'],
        ]);
        $buyerId = Order::find($formData['order_id']);
        $buyerObj = Customer::find($buyerId->buyer_id);

        $buyerObj->secondary_email = $formData['secondary_email'];
        $buyerObj->save();
        $message = 'Shipping Address Updated';
        $request->session()->flash('flash_message', $message);
        $response = [
            'error' => $error,
            'response' => $message
        ];
        echo json_encode($response);
    }

    public function addOrderNote(Request $request) {
        $error = '';
        $response = "";
        $order_id = $request->id;
        $note = $request->note;
        if ($note != '' && isset($note)) {
            DB::table('order_notes')->insert([
                'order_id' => $order_id,
                'note' => $note
            ]);
            $msessage = "Order Note Saved Successfully";
        } else {
            $msessage = 'Please Enter Order Note';
        }
        $response = [
            'response' => $msessage,
            'error' => $error
        ];
        $request->session()->flash('flash_message', $msessage);
        echo json_encode($response);
    }

    public function updateOrderNote(Request $request) {
        $error = '';
        $response = "";
        $id = $request->id;
        $note = $request->note;
        if ($note != '' && isset($note)) {
            DB::table('order_notes')
                    ->where('id', '=', $id)
                    ->update([
                        'note' => $note
            ]);
            $msessage = "Order Note Updated Successfully";
        } else {
            $error = 'Please Enter Order Note';
        }

        $response = [
            'response' => $msessage,
            'error' => $error
        ];
        $request->session()->flash('flash_message', $msessage);
        echo json_encode($response);
    }

    public function deleteOrderNote(Request $request) {
        $error = '';
        $response = "";
        $id = $request->id;
        if ($id != '' && isset($id)) {
            DB::table('order_notes')
                    ->where('id', '=', $id)
                    ->update([
                        'is_active' => 0
            ]);
            $msessage = "Order Note Deleted Successfully";
        } else {
            $msessage = 'Please Enter Order Note';
        }

        $response = [
            'response' => $msessage,
            'error' => $error
        ];
        $request->session()->flash('flash_message', $msessage);
        echo json_encode($response);
    }

    public function changeEbayStatus(Request $request) {
        $error = '';
        $response = '';

        $order_id = $request->order_id;
        $change = $request->change;
        $orderObj = Order::select('orders.order_id as OID', 'item.item_id')
                        ->leftJoin('order_item', 'order_item.order_id', 'orders.id')
                        ->leftJoin('item', 'item.id', 'order_item.item_id')
                        ->leftJoin('transaction', 'transaction.order_id', 'orders.id')
                        ->where('orders.id', $order_id)->first();

        $orderApiObj = new OrderApi();
        $response = $orderApiObj->CompleteSale($orderObj, $change, $request);

        $action = '';
        if ($response->Ack != 'Failure') {
            if ($change == 'payment') {
                if ($request->ispaid == 'true') {
                    $action .= 'Paid';
                    DB::table('orders')->where('id', '=', $order_id)
                            ->update([
                                'paid_time' => $response->Timestamp
                    ]);
                } else {
                    $action .= 'UnPaid';
                    DB::table('orders')->where('id', '=', $order_id)
                            ->update([
                                'paid_time' => ''
                    ]);
                }
            } elseif ($change == 'shipped') {
                if ($request->isshipped == 'true') {
                    $action .= 'Shipped';
                    DB::table('orders')->where('id', '=', $order_id)
                            ->update([
                                'shipped_time' => $response->Timestamp
                    ]);
                } else {
                    $action .= 'UnShipped';
                    DB::table('orders')->where('id', '=', $order_id)
                            ->update([
                                'shipped_time' => ''
                    ]);
                }
            }
            $message = "Orders Status Updated On Ebay Succssfully";
            $request->session()->flash('flash_message', $message);
        } else {

            $short_message = $response->Errors->ShortMessage;
            $log_message = $response->Errors->LongMessage;
            $error = $response->Errors->ErrorParameters->Value;
            $message = $log_message . $error;
            $request->session()->flash('flash_message', $message);
            $error = 'Something Wrong with request';
        }

        $response = [
            'error' => $error,
            'response' => $message
        ];
        echo json_encode($response);
    }

    public function changeStatus(Request $request) {
        $error = false;
        $message = '';
        $response = '';

        $order_id = $request->order;
        if (isset($request->reason)) {
            $status = $request->status;
            $reason = $request->reason;
            $order = DB::table('orders')->where('id', '=', $order_id)
                    ->update([
                'order_status' => $status,
                'cancel_reason' => $reason,
            ]);
            $name = Order::findOrFail($order_id);
            $data = DB::table('shipping_details')->select('shipping_details.selling_manager_sales_record_number as record_id')->leftJoin('orders', 'orders.id', 'shipping_details.order_id')->where('orders.id', '=', $order_id)->first();
            $response .= ' Cancelled Order ';
        } elseif (isset($request->status) && !isset($request->reason)) {
            $status = $request->status;
            $item = DB::table('orders')->where('id', '=', $order_id)
                    ->update(['status' => $status]);
            $name = Order::findOrFail($order_id);
            $data = DB::table('shipping_details')->select('shipping_details.selling_manager_sales_record_number as record_id')->leftJoin('orders', 'orders.id', 'shipping_details.order_id')->where('orders.id', '=', $order_id)->first();
            $response .= 'Internal Status Changed for Order ';
        } elseif (isset($request->refundamttype)) {
            $refundamttype = $request->refundamttype;
            $orders = getAllRecord('orders',array('id'=>$order_id),true);
            $refund_amount = $request->refund_amount;
            $jqid_amount = $request->jqid_amount;
            if ($jqid_amount == 'fa') {
                $refund_amount = $orders->total;
                $total = 0.00;
            } else {
                if ($refundamttype == 'Amount') {
                    $total = $orders->total - $refund_amount;
                } elseif ($refundamttype == 'Percentage') {
                    $refund_amount = $orders->total * $refund_amount / 100;
                    $total = $orders->total - $refund_amount;
                } else {
                    $total = $orders->total - $refund_amount;
                }
            }
            if ($orders->refund_amount != null) {
                $refund_amount = $refund_amount + $orders->refund_amount;
            }
            $order = DB::table('orders')->where('id', '=', $order_id)
                    ->update([
                'refund_amount' => $refund_amount,
                'total' => $total
            ]);
            //$this->updateRefundamountOnEbay($refund_amount,$total);
            $name = Order::findOrFail($order_id);
            $response .= ' Status Changed';
            $request->session()->flash('flash_message', ' Order has been Refunded ');
        }
        $request->session()->flash('flash_message', $response);
        $response = array('error' => $error,
            'response' => $response);
        echo json_encode($response);
    }

    public function saveOrderChanges(Request $request) {
        $error = '';
        $response = "";
        if (isset($request->product) && $request->product != '' && isset($request->order) && $request->order != '') {
            $product = $request->product;
            $order_id = $request->order;
            if (isset($request->shipping_cost) && $request->shipping_cost != '') {
                $shippingCost = $request->shipping_cost;
                $update = DB::table('order_item')
                        ->where('item_id', '=', $product)
                        ->where('order_id', '=', $order_id)
                        ->update([
                    'supplier_shipping' => $shippingCost
                ]);
                if ($update) {
                    $message = "Shipping Cost Updated";
                } else {
                    $message = 'Fail to update';
                }
            } elseif (isset($request->supplierProductLink) && $request->supplierProductLink != '') {
                $supplierProductLink = $request->supplierProductLink;
                $update = DB::table('order_item')
                        ->where('item_id', '=', $product)
                        ->where('order_id', '=', $order_id)
                        ->update([
                    'supplier_product_link' => $supplierProductLink
                ]);
                if ($update) {
                    $message = "Item Link Updated";
                } else {
                    $message = 'Fail to update';
                }
            } elseif (isset($request->supplierOrderId) && $request->supplierOrderId != '') {
                $supplierOrderId = $request->supplierOrderId;
                $update = DB::table('order_item')
                        ->where('item_id', '=', $product)
                        ->where('order_id', '=', $order_id)
                        ->update([
                    'supplier_order_id' => $supplierOrderId
                ]);
                if ($update) {
                    $message = "Order Id Updated";
                } else {
                    $message = 'Fail to update';
                }
            } elseif (isset($request->price) && $request->price != '') {
                $price = $request->price;
                $update = DB::table('order_item')
                        ->where('item_id', '=', $product)
                        ->where('order_id', '=', $order_id)
                        ->update([
                    'supplier_price' => $price
                ]);
                if ($update) {
                    $message = "Cost Price Updated";
                } else {
                    $message = 'Fail to update';
                }
            }
        } elseif (isset($request->order) && $request->order != '' && $request->product == '') {
            $order = $request->order;
            if (isset($request->ebayFee) && $request->ebayFee != '') {
                $ebayFee = $request->ebayFee;
                $update = DB::table('orders')->where('id', '=', $order)
                        ->update([
                    'ebay_fee_price' => $ebayFee
                ]);
                if ($update) {
                    $message = "Ebay Fee Updated";
                } else {
                    $message = 'Fail to update';
                }
            } elseif (isset($request->paypalFee) && $request->paypalFee != '') {
                $paypalFee = $request->paypalFee;
                $update = DB::table('orders')->where('id', '=', $order)
                        ->update([
                    'paypal_fee_price' => $paypalFee
                ]);
                if ($update) {
                    $message = "Paypal Fee Updated";
                } else {
                    $message = 'Fail to update';
                }
            } elseif (isset($request->paypalTransactionId) && $request->paypalTransactionId != '') {
                $paypalTransactionId = $request->paypalTransactionId;
                $update = DB::table('order_payment')->where('order_id', '=', $order)
                        ->update([
                    'reference_id' => $paypalTransactionId
                ]);
                if ($update) {
                    $message = "Paypal Transaction Id Updated";
                } else {
                    $message = 'Fail to update';
                }
            } elseif (isset($request->paypalTransactionToSupplier) && $request->paypalTransactionToSupplier != '') {
                $paypalTransactionToSupplier = $request->paypalTransactionToSupplier;
                $update = DB::table('transaction')->where('order_id', '=', $order)
                        ->update([
                    'paypal_tansaction_id_supplier' => $paypalTransactionToSupplier
                ]);
                if ($update) {
                    $message = "Paypal Transaction Id For Supplier Updated";
                } else {
                    $message = 'Fail to update';
                }
            }
        }

        $response = [
            'response' => $message,
            'error' => $error
        ];
        $request->session()->flash('flash_message', $message);
        echo json_encode($response);
    }

    public function contactBuyer(Request $request) {
        $error = '';
        $response = '';

        $order_id = $request->order;
        $message = $request->message;
        $buyer = $request->buyer;
        //$attachment	= $request->attachment;

        $data = DB::table('item')
                ->select('item.item_id')
                ->leftJoin('order_item', 'order_item.item_id', 'item.id')
                ->where('order_item.order_id', '=', $order_id)
                ->first();
        $orderApiObj = new OrderApi();
        $response = $orderApiObj->contactBuyer($data, $message, $buyer);

        /* 'attachment'	=>	json_encode($request->attachment) */
        if ($response->Ack != 'Failure') {
            $sentMessage = DB::table('messages')->insert([
                'item_id' => $data->item_id,
                'sender_id' => 'smart_choice12',
                'sender_email' => 'smartchoicedirectebay@gmail.com',
                'body' => $message,
                'recipient_id' => $buyer,
                'creation_date' => $response->Timestamp,
            ]);
            $message .= "Message Successfully Send";
        } else {
            $short_message = $response->Errors->ShortMessage;
            $log_message = $response->Errors->LongMessage;
            $error = $response->Errors->ErrorParameters->Value;
            $error = $log_message . $error;
            $request->session()->flash('flash_message', $error);
        }
        $response = array('error' => $error,
            'response' => $response);
        echo json_encode($response);
    }

    public function cancelOrder(Request $request) {
        $error = '';
        $response = '';
        $message = '';

        $order_id = $request->order_id;
        $reason = $request->reason;
        $data = getAllRecord('orders',array('id'=>$order_id),true);
        $orderApiObj = new OrderApi();
        $response = $orderApiObj->cancelOrder($data, $reason);
        if ($response->Ack != 'Failure') {
            $order = DB::table('orders')->where('id', '=', $order_id)
                    ->update([
                'order_status' => 'Cancelled',
                'cancel_reason' => $reason,
                'total' => '0.00',
                'status' => 12
            ]);
            $refund = DB::table('order_payment')->insert([
                'order_id' => $order_id,
                'payee' => 'smart_choice12',
                'payment_status' => 'Succeeded',
                'payment_amount' => $data->total
            ]);
            $refund = DB::table('order_refunds')->insert([
                'order_id' => $order_id,
                'refund_amount' => $data->total
            ]);
            $message = "Orders Successfully Cancelled On Ebay ";
        } else {
            $short_message = $response->Errors->ShortMessage;
            $log_message = $response->Errors->LongMessage;
            $message = $response->Errors->ErrorParameters->Value;
            $error = $log_message . $message;
            $request->session()->flash('flash_message', $error);
        }

        $response = [
            'error' => $error,
            'response' => $message
        ];
        echo json_encode($response);
    }

    public function orderSynchronization(Request $request) {
        $error = '';
        $message = '';
        $syncType = $request->syncType;
        if ($syncType == 'EbayToSystem') {
            $order_id = $request->order_id;
            $syncStatus = $this->importOrderTransaction($order_id);
            if ($syncStatus != 'Failed to sync') {
                $message .= 'Order Successfully Synced.';
                $request->session()->flash('flash_message', 'Orders Detail Succssfully Imported');
            } else {
                $error .= $syncStatus;
            }
        } else {
            $order_id = $request->id;
            $this->syncShippingAddress($order_id);
            $syncStatus = $this->syncTracking($order_id);
            if ($syncStatus != 'Fail') {
                $message .= 'System to Ebay is not available Right now';
                $request->session()->flash('flash_message', $message);
            } else {
                $error .= $syncStatus;
            }
        }
        $response = [
            'response' => $message,
            'error' => $error
        ];
        echo json_encode($response);
    }

    public function importOrderTransaction($order_id) {
        $orderApiObj = new OrderApi();
        $response = $orderApiObj->getOrderTransaction($order_id);

        if ($response->Ack != 'Failure' && $response->OrderArray) {
            $orders = $response->OrderArray->Order;
            $isSingle = true;
            $this->saveOrders($orders, $isSingle);
            return 'Orders Detail Succssfully Sync with Ebay to System';
        } else {
            $short_message = $response->Errors->ShortMessage;
            $log_message = $response->Errors->LongMessage;
            $error = $response->Errors->ErrorParameters->Value;
            return 'Failed to sync';
        }
    }

    public function saveOrders($orders, $isSingle = false) {
        $orderObj = new Order();
        foreach ($orders as $order) {
            $tracking_id = '';
            $shipping_company = '';
            $ItemID = [];
            $ItemIdEbay = [];
            $orderArray = [];
            $trackingIdArray = [];
            $shippingCompanyArray = [];
            $transactionId = [];

            $orderArray['order_id'] = $order->OrderID;
            $orderArray['order_status'] = $order->OrderStatus;
            $orderArray['adjustment_amount'] = $order->AdjustmentAmount;
            $orderArray['amount_paid'] = $order->AmountPaid;
            $orderArray['amount_saved'] = $order->AmountSaved;
            $orderArray['created_time'] = $order->CreatedTime;
            $orderArray['payment_methods'] = $order->PaymentMethods;
            $orderArray['seller_email'] = $order->SellerEmail;
            $orderArray['subtotal'] = $order->Subtotal;
            $orderArray['total'] = $order->Total;
            $orderArray['buyer_user_id'] = $order->BuyerUserID;
            $orderArray['eias_token'] = $order->EIASToken;
            $orderArray['payment_hold_status'] = $order->PaymentHoldStatus;
            $orderArray['is_multi_leg_shipping'] = $order->IsMultiLegShipping;
            $orderArray['seller_user_id'] = $order->SellerUserID;
            $orderArray['seller_eias_token'] = $order->SellerEIASToken;
            $orderArray['cancel_status'] = $order->CancelStatus;
            $orderArray['contatin_ebay_plus_transaction'] = $order->ContainseBayPlusTransaction;
            if (isset($order->PaidTime)) {
                $orderArray['paid_time'] = $order->PaidTime;
            }
            if (isset($order->ShippedTime)) {
                $orderArray['shipped_time'] = $order->ShippedTime;
            }
            if (isset($order->BuyerCheckoutMessage)) {
                $orderArray['buyer_note'] = $order->BuyerCheckoutMessage;
            }

            $check_out_status = $order->CheckoutStatus;
            $shipping_details = $order->ShippingDetails;

            $shipping_address = $order->ShippingAddress;

            $shipping_service_selected = $order->ShippingServiceSelected;
            $transaction = $order->TransactionArray->Transaction;

            $OrderId = $orderObj->saveOrder($orderArray, $isSingle);

            foreach ($check_out_status as $cos) {
                $CheckoutStatusArray = [];
                $CheckoutStatusArray['ebay_payment_status'] = $cos->eBayPaymentStatus;
                $CheckoutStatusArray['last_modified_time'] = $cos->LastModifiedTime;
                $CheckoutStatusArray['payment_method'] = $cos->PaymentMethod;
                $CheckoutStatusArray['status'] = $cos->Status;
                $CheckoutStatusArray['integrated_merchant_credit_card_enabled'] = $cos->IntegratedMerchantCreditCardEnabled;
            }

            foreach ($shipping_details as $sd) {
                $shippingDetailsArray = [];
                $shippingDetailsArray['sales_tax'] = $sd->SalesTax;
                $shippingDetailsArray['shipping_service_option'] = $sd->ShippingServiceOptions;
                $shippingDetailsArray['selling_manager_sales_record_number'] = $sd->SellingManagerSalesRecordNumber;
                $shippingDetailsArray['get_it_fast'] = $sd->GetItFast;

                $ShippingServiceOptionArray = [];
                if (count($shippingDetailsArray['shipping_service_option']) > 0) {
                    for ($i = 0; $i < count($shippingDetailsArray['shipping_service_option']); $i++) {
                        $ShippingServiceOptionArray[$i]['shipping_service'] = $shippingDetailsArray['shipping_service_option'][$i]->ShippingService;
                        $ShippingServiceOptionArray[$i]['shipping_service_cost'] = $shippingDetailsArray['shipping_service_option'][$i]->ShippingServiceCost;
                        $ShippingServiceOptionArray[$i]['shipping_service_priority'] = $shippingDetailsArray['shipping_service_option'][$i]->ShippingServicePriority;
                        $ShippingServiceOptionArray[$i]['expedited_service'] = $shippingDetailsArray['shipping_service_option'][$i]->ExpeditedService;
                        $ShippingServiceOptionArray[$i]['shipping_time_min'] = $shippingDetailsArray['shipping_service_option'][$i]->ShippingTimeMin;
                        $ShippingServiceOptionArray[$i]['shipping_time_max'] = $shippingDetailsArray['shipping_service_option'][$i]->ShippingTimeMax;
                    }
                } else {
                    foreach ($shippingDetailsArray['shipping_service_option'] as $sso) {
                        $ShippingServiceOptionArray['shipping_service'] = $sso->ShippingService;
                        $ShippingServiceOptionArray['shipping_service_cost'] = $sso->ShippingServiceCost;
                        $ShippingServiceOptionArray['shipping_service_priority'] = $sso->ShippingServicePriority;
                        $ShippingServiceOptionArray['expedited_service'] = $sso->ExpeditedService;
                        $ShippingServiceOptionArray['shipping_time_min'] = $sso->ShippingTimeMin;
                        $ShippingServiceOptionArray['shipping_time_max'] = $sso->ShippingTimeMax;
                    }
                }
            }

            foreach ($shipping_address as $sa) {
                $ShippingAddressArray = [];
                $ShippingAddressArray['name'] = $sa->Name;
                $ShippingAddressArray['street1'] = $sa->Street1;
                $ShippingAddressArray['street2'] = $sa->Street2;
                $ShippingAddressArray['city_name'] = $sa->CityName;
                $ShippingAddressArray['state_or_provinance'] = $sa->StateOrProvince;
                $ShippingAddressArray['country_name'] = $sa->CountryName;
                $ShippingAddressArray['phone'] = $sa->Phone;
                $ShippingAddressArray['postal_code'] = $sa->PostalCode;
                $ShippingAddressArray['external_address_id'] = $sa->ExternalAddressID;
            }

            foreach ($shipping_service_selected as $sss) {
                $shipping_service = $sss->ShippingService;
            }
            $orderArray['shipping_service_selected'] = $shipping_service;

            if (count($transaction) > 1) {
                for ($i = 0; $i < count($transaction); $i++) {
                    $transactionArray = [];
                    if ($isSingle == false) {
                        foreach ($transaction[$i]->Buyer as $b) {
                            $buyerArray = [];
                            if ($b->Email != 'Invalid Request') {
                                $buyerArray['email'] = $b->Email;
                            }
                            $buyerArray['user_first_name'] = $b->UserFirstName;
                            $buyerArray['user_last_name'] = $b->UserLastName;
                            if ($isSingle == false) {
                                $buyerArray['buyer_user_id'] = $orderArray['buyer_user_id'];
                                $buyerId = $orderObj->saveBuyer($buyerArray, $ShippingAddressArray);
                                $this->getBuyer($orderArray['buyer_user_id']);
                            }
                        }
                    }

                    foreach ($transaction[$i]->ShippingDetails as $sd) {
                        if (isset($sd->ShipmentTrackingDetails)) {
                            $trackingIdArray[$i] = $sd->ShipmentTrackingDetails->ShipmentTrackingNumber;
                            $shippingCompanyArray[$i] = $sd->ShipmentTrackingDetails->ShippingCarrierUsed;
                            $record_id = $sd->SellingManagerSalesRecordNumber;
                        }
                    }
                    foreach ($transaction[$i]->Item as $it) {
                        $itemArray = [];
                        $itemArray['item_id'] = $it->ItemID;
                        $itemArray['site'] = $it->Site;
                        $itemArray['title'] = $it->Title;
                        $itemArray['condition_id'] = $it->ConditionID;
                        $itemArray['condition_display_name'] = $it->ConditionDisplayName;
                        $ItemIdEbay[] = $it->ItemID;
                        $ItemID[] = $orderObj->saveItem($itemArray);
                    }
                    $transactionArray['item_id'] = $it->ItemID;
                    $transactionArray['shipping_details'] = $record_id;
                    $transactionArray['created_date'] = $transaction[$i]->CreatedDate;
                    $transactionArray['quantity_purchased'] = $transaction[$i]->QuantityPurchased;
                    $transactionArray['transaction_id'] = $transaction[$i]->TransactionID;
                    $transactionArray['transaction_price'] = $transaction[$i]->TransactionPrice;
                    $transactionArray['transaction_site_id'] = $transaction[$i]->TransactionSiteID;
                    $transactionArray['platform'] = $transaction[$i]->Platform;
                    $transactionArray['order_line_item_id'] = $transaction[$i]->OrderLineItemID;
                    $transactionArray['ebay_plus_transaction'] = $transaction[$i]->eBayPlusTransaction;
                    $transactionArray['final_value_fee'] = $transaction[$i]->FinalValueFee;
                    $transactionArray['order_id'] = $OrderId;
                    $transactionId[] = $orderObj->saveTransaction($transactionArray);
                }
            } else {
                foreach ($transaction as $t) {
                    $transactionArray = [];
                    if ($isSingle == false) {
                        foreach ($t->Buyer as $b) {
                            $buyerArray = [];
                            if ($b->Email != 'Invalid Request') {
                                $buyerArray['email'] = $b->Email;
                            }
                            $buyerArray['user_first_name'] = $b->UserFirstName;
                            $buyerArray['user_last_name'] = $b->UserLastName;
                            if ($isSingle == false) {
                                $buyerArray['buyer_user_id'] = $orderArray['buyer_user_id'];
                                $buyerId = $orderObj->saveBuyer($buyerArray, $ShippingAddressArray);
                                $this->getBuyer($orderArray['buyer_user_id']);
                            }
                        }
                    }
                    $i = 0;
                    foreach ($t->ShippingDetails as $sd) {
                        if (isset($sd->ShipmentTrackingDetails)) {
                            $trackingIdArray[$i] = $sd->ShipmentTrackingDetails->ShipmentTrackingNumber;
                            $shippingCompanyArray[$i] = $sd->ShipmentTrackingDetails->ShippingCarrierUsed;
                            $i++;
                        }
                        $selling_manager_sales_record_number = $sd->SellingManagerSalesRecordNumber;
                    }
                    foreach ($t->Item as $i) {
                        $itemArray = [];
                        $itemArray['item_id'] = $i->ItemID;
                        $itemArray['site'] = $i->Site;
                        $itemArray['title'] = $i->Title;
                        $itemArray['condition_id'] = $i->ConditionID;
                        $itemArray['condition_display_name'] = $i->ConditionDisplayName;
                        $ItemIdEbay[] = $i->ItemID;
                        $ItemID[] = $orderObj->saveItem($itemArray);
                    }
                    $transactionArray['item_id'] = $i->ItemID;
                    $transactionArray['shipping_details'] = $sd->SellingManagerSalesRecordNumber;
                    $transactionArray['created_date'] = $t->CreatedDate;
                    $transactionArray['quantity_purchased'] = $t->QuantityPurchased;
                    $transactionArray['transaction_id'] = $t->TransactionID;
                    $transactionArray['transaction_price'] = $t->TransactionPrice;
                    $transactionArray['transaction_site_id'] = $t->TransactionSiteID;
                    $transactionArray['platform'] = $t->Platform;
                    $transactionArray['order_line_item_id'] = $t->OrderLineItemID;
                    $transactionArray['ebay_plus_transaction'] = $t->eBayPlusTransaction;
                    $transactionArray['final_value_fee'] = $t->FinalValueFee;
                    $transactionArray['order_id'] = $OrderId;
                    $transactionId[] = $orderObj->saveTransaction($transactionArray);
                }
            }

            if (isset($order->MonetaryDetails->Payments->Payment)) {
                foreach ($order->MonetaryDetails->Payments->Payment as $payment) {
                    $orderpaymentArray = [];
                    $orderpaymentArray['payment_status'] = $payment->PaymentStatus;
                    $orderpaymentArray['payer'] = $payment->Payer;
                    $orderpaymentArray['payee'] = $payment->Payee;
                    $orderpaymentArray['payment_time'] = $payment->PaymentTime;
                    $orderpaymentArray['payment_amount'] = $payment->PaymentAmount;
                    $orderpaymentArray['reference_id'] = $payment->ReferenceID;
                    $orderpaymentArray['fee_or_credit_amount'] = $payment->fee_or_credit_amount;
                }
            }
            if (isset($order->MonetaryDetails->Refunds->Refund)) {
                foreach ($order->MonetaryDetails->Refunds->Refund as $refund) {
                    $orderrefundArray = [];
                    $orderrefundArray['refundstatus'] = $refund->RefundStatus;
                    $orderrefundArray['refund_type'] = $refund->RefundType;
                    $orderrefundArray['refund_to'] = $refund->RefundTo;
                    $orderrefundArray['refund_time'] = $refund->RefundTime;
                    $orderrefundArray['refund_amount'] = ltrim($refund->RefundAmount, '-');
                    $orderrefundArray['reference_id'] = $refund->ReferenceID;
                    $orderrefundArray['fee_or_credit_amount'] = $refund->FeeOrCreditAmount;
                }
            }

            $ShippingServiceOptionArray['order_id'] = $OrderId;
            $service_option_id = $orderObj->saveSeviceOption($ShippingServiceOptionArray);

            $CheckoutStatusArray['order_id'] = $OrderId;
            $checkOutId = $orderObj->saveCheckOutStatus($CheckoutStatusArray);

            $shippingDetailsArray['shipping_service_option'] = $service_option_id;
            $shippingDetailsArray['order_id'] = $OrderId;
            $ShippingDetailID = $orderObj->saveShippingDetails($shippingDetailsArray);

            $ShippingAddressArray['order_id'] = $OrderId;
            $addressId = $orderObj->saveShippingAddress($ShippingAddressArray);
            ;
            if (isset($orderpaymentArray) && $orderpaymentArray != '') {
                $orderpaymentArray['order_id'] = $OrderId;
                $order_payment_id = $orderObj->saveOrderPayment($orderpaymentArray);
            }
            if (isset($orderrefundArray) && $orderrefundArray != '') {
                $orderrefundArray['order_id'] = $OrderId;
                $order_refund_id = $orderObj->saveOrderRefund($orderrefundArray);
            }


            $subtotal = 0;
            $order_shipping_cost = 0.00;
            for ($i = 0; $i < count($ItemID); $i++) {
                $transaction = DB::table('transaction')
                        ->select('transaction_price', 'order_line_item_id')
                        ->where('order_id', '=', $OrderId)
                        ->where('item_id', '=', $ItemIdEbay[$i])
                        ->where('shipping_details', '=', $transactionId[$i])
                        ->first();

                $records = getAllRecord('order_item',array('order_id'=>$OrderId,'item_id'=>$ItemID[$i],'order_line_item_id'=>$transaction->order_line_item_id),true);

                $ItemIdEbay[$i];
                $ItemData = getPrice($ItemIdEbay[$i]);

                $supplierData = DB::table('item')
                        ->select('item.supplier_product_link', 'item.supplier_shipping', 'item.supplier_price', 'selling_status.current_price')
                        ->leftJoin('selling_status', 'selling_status.item_id', 'item.id')
                        ->where('item.id', '=', $ItemID[$i])
                        ->first();

                if (count($records) == 0) {
                    $orderItemArray = [
                        'order_id' => $OrderId,
                        'item_id' => $ItemID[$i],
                        'supplier_product_link' => $supplierData->supplier_product_link,
                        'supplier_price' => $supplierData->supplier_price,
                        'supplier_shipping' => $supplierData->supplier_shipping
                    ];
                    if ($ItemData != 'Not Found') {
                        $orderItemArray['item_price'] = $ItemData;
                    } else {
                        $orderItemArray['item_price'] = $transaction->transaction_price;
                    }
                    $orderItemArray['order_line_item_id'] = $transaction->order_line_item_id;

                    DB::table('order_item')
                            ->insert($orderItemArray);
                    if (count($trackingIdArray) > 0) {
                        DB::table('tracking')->insert([
                            'order_id' => $OrderId,
                            'item_id' => $ItemID[$i],
                            'tracking_id' => $trackingIdArray[$i],
                            'shipping_company_name' => $shippingCompanyArray[$i]
                        ]);
                    }
                } else {
                    $orderItemArray = [];
                    if ($tracking_id != '') {
                        $orderItemArray = ['tracking_id' => $tracking_id];
                    }
                    if ($shipping_company != '') {
                        $orderItemArray = ['shipping_company_name' => $shipping_company];
                    }
                    if ($ItemData != 'Not Found') {
                        $orderItemArray = ['item_price' => $ItemData];
                    } else {
                        $orderItemArray = ['item_price' => $transaction->transaction_price];
                    }
                    $orderItemArray = ['order_line_item_id' => $transaction->order_line_item_id];

                    DB::table('order_item')->where('order_id', '=', $OrderId)
                            ->where('item_id', '=', $ItemID[$i])
                            ->where('order_line_item_id', '=', $transaction->order_line_item_id)
                            ->update($orderItemArray);
                    $data = getAllRecord('tracking',array('order_id'=>$OrderId));

                    if (count($trackingIdArray) > 0) {
                        if (count($data) == count($trackingIdArray)) {
                            DB::table('tracking')->where('order_id', '=', $OrderId)->where('item_id', '=', $ItemID[$i])->update([
                                'tracking_id' => $trackingIdArray[$i],
                                'shipping_company_name' => $shippingCompanyArray[$i]
                            ]);
                        } else {
                            DB::table('tracking')->insert([
                                'order_id' => $OrderId,
                                'item_id' => $ItemID[$i],
                                'tracking_id' => $trackingIdArray[$i],
                                'shipping_company_name' => $shippingCompanyArray[$i]
                            ]);
                        }
                    }
                }
                $items = DB::table('item')->select('selling_status.current_price', 'item_shipping_detail.shipping_service_cost')
                        ->leftJoin('selling_status', 'selling_status.item_id', 'item.id')
                        ->leftJoin('item_shipping_detail', 'item_shipping_detail.item_id', 'item.id')
                        ->where('item.id', '=', $ItemID[$i])
                        ->first();

                $subtotal = $subtotal + $items->current_price;
                $order_shipping_cost = $order_shipping_cost + $items->shipping_service_cost;
            }
            unset($trackingIdArray);
            unset($shippingCompanyArray);

            if ($orderArray['order_status'] != 'Cancelled') {
                $result = Order::select('ebay_fee_price', 'paypal_fee')->where('id', '=', $OrderId)->first();
                if ($result->ebay_fee_price == 0.0 && $result->paypal_fee == 0.0) {
                    DB::table('orders')->where('id', '=', $OrderId)
                            ->update([
                                'ebay_fee_price' => number_format(((floatval($orderArray['total']) * floatval(8.8))) / 100, 2),
                                'paypal_fee_price' => number_format(((floatval($orderArray['total']) * floatval(2.6))) / 100, 2),
                                'order_shipping_cost' => $order_shipping_cost,
                    ]);
                    if ($isSingle == false) {
                        DB::table('orders')->where('id', '=', $OrderId)->update(['buyer_id' => $buyerId]);
                    }
                } else {
                    DB::table('orders')->where('id', '=', $OrderId)
                            ->update([
                                'order_shipping_cost' => $order_shipping_cost,
                    ]);
                    if ($isSingle == false) {
                        DB::table('orders')->where('id', '=', $OrderId)->update(['buyer_id' => $buyerId]);
                    }
                }
            } else {
                DB::table('orders')->where('id', '=', $OrderId)
                        ->update([
                            'ebay_fee_price' => 0,
                            'paypal_fee_price' => 0,
                            'order_shipping_cost' => $order_shipping_cost
                ]);
            }

            unset($OrderArray);
            unset($buyerArray);
            unset($statusArray);
            unset($taxDetailArray);
            unset($ShippingServiceOptionArray);
            unset($CheckoutStatusArray);
            unset($shippingDetailsArray);
            unset($orderpaymentArray);
        }
        $feedback = $this->getFeedbackLeft();
        $feedback = $this->getFeedbackReceived();
        return redirect('order');
    }

    public function getFeedbackLeft() {
        $feedbackType = 'FeedbackLeft';
        $orderApiObj = new OrderApi();
        $response = $orderApiObj->getFeedback($feedbackType);
        if ($response->Ack != 'Failure') {
            $feedbacks = $response->FeedbackDetailArray;
            $this->storeFeedBack($feedbacks);
            return $response = 'FeedBack Stored';
        } else {
            return $response = 'Not Found';
        }
    }

    public function getFeedbackReceived() {
        $feedbackType = 'FeedbackReceived';
        $orderApiObj = new OrderApi();
        $response = $orderApiObj->getFeedback($feedbackType);
        if ($response->Ack != 'Failure') {
            $feedbacks = $response->FeedbackDetailArray;

            $this->storeFeedBack($feedbacks);
            return $response = 'FeedBack Stored';
        } else {
            return $response = 'Not Found';
        }
    }

    public function storeFeedBack($feedback) {
        $orderObj = new Order();

        foreach ($feedback as $fb) {
            for ($i = 0; $i < count($fb->FeedbackDetail); $i++) {

                $feedbackArray = [];
                $feedbackArray['commenting_user'] = $fb->FeedbackDetail[$i]->CommentingUser;
                $feedbackArray['commenting_user_score'] = $fb->FeedbackDetail[$i]->CommentingUserScore;
                $feedbackArray['comment_text'] = $fb->FeedbackDetail[$i]->CommentText;
                $feedbackArray['comment_time'] = $fb->FeedbackDetail[$i]->CommentTime;
                $feedbackArray['comment_type'] = $fb->FeedbackDetail[$i]->CommentType;
                $feedbackArray['item_id'] = $fb->FeedbackDetail[$i]->ItemID;
                $feedbackArray['role'] = $fb->FeedbackDetail[$i]->Role;
                $feedbackArray['feedback_id'] = $fb->FeedbackDetail[$i]->FeedbackID;
                $feedbackArray['transaction_id'] = $fb->FeedbackDetail[$i]->TransactionID;
                $feedbackArray['order_line_item_id'] = $fb->FeedbackDetail[$i]->OrderLineItemID;
                $feedbackArray['item_title'] = $fb->FeedbackDetail[$i]->ItemTitle;
                $feedbackArray['item_price'] = $fb->FeedbackDetail[$i]->ItemPrice;

                $orderObj->storeFeedBack($feedbackArray);
            }
        }
        return $orderObj;
    }

    public function getBuyer($buyerId) {
        $customerApiObj = new CustomerApi();
        $response = $customerApiObj->getCustomerDetail($buyerId);
        $buyerId = 0;
        if ($response->Ack != "Failure") {
            foreach ($response->User as $s) {
                $customerArray = [];
                $customerArray['about_me_page'] = $s->AboutMePage;
                $customerArray['eiast_token'] = $s->EIASToken;
                $customerArray['email'] = $s->Email;
                $customerArray['feedback_score'] = $s->FeedbackScore;
                $customerArray['unique_negative_feedback_count'] = $s->UniqueNegativeFeedbackCount;
                $customerArray['unique_positive_feedback_count'] = $s->UniquePositiveFeedbackCount;
                $customerArray['positive_feedback_percent'] = $s->PositiveFeedbackPercent;
                $customerArray['feedback_private'] = $s->FeedbackPrivate;
                $customerArray['feeddback_rating_star'] = $s->FeedbackRatingStar;
                $customerArray['id_verified'] = $s->IDVerified;
                $customerArray['ebay_good_standing'] = $s->eBayGoodStanding;
                $customerArray['new_user'] = $s->NewUser;
                $customerArray['registration_date'] = $s->RegistrationDate;
                $customerArray['site'] = $s->Site;
                $customerArray['status'] = $s->Status;
                $customerArray['user_id'] = $s->UserID;
                $customerArray['user_id_changed'] = $s->UserIDChanged;
                $customerArray['user_id_last_changed'] = $s->UserIDLastChanged;
                $customerArray['vat_status'] = $s->VATStatus;
                $customerArray['business_role'] = $s->BusinessRole;
                $customerArray['ebay_wiki_read_only'] = $s->eBayWikiReadOnly;
                $customerArray['motors_dealer'] = $s->MotorsDealer;
                $customerArray['unique_neutral_feedback_count'] = $s->UniqueNeutralFeedbackCount;
                $customerArray['enterprise_seller'] = $s->EnterpriseSeller;

                $customerObj = new Customer();
                $buyerId = $customerObj->saveBuyerDetail($customerArray);
                return $buyerId;
            }
        }
        return $buyerId;
    }

}
