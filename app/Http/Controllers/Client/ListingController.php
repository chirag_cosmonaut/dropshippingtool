<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\ItemApi;
use App\ActiveListing;
use Datatables;
use DB;
use Validator;

class ListingController extends Controller {


	public function getActiveListing(){
		return view('client.listing.active');
	}
	public function getCustomerService(){
		return view('client.customer.customer');
	}
	public function getFinder(){
		return view('client.finder');
	}
    public function getUploader(){
    	return view('client.uploader');
    }
    public function getInvoice(){
    	return view('client.listing.invoice');
    }

    /* public function getSettings(){
      echo "hi";exit();
      return view('client.listing.settings');
      } */

    public function getPrices(Request $request) {
        $account_id = session()->get('account');
        $user_id = Auth()->user()->id;
        $products = ActiveListing::select('item.item_id', 'item.id', 'selling_status.current_price')
                        ->leftJoin('selling_status', 'selling_status.item_id', 'item.id')->where('item.account_id', '=', $account_id)->where('item.user_id', '=', $user_id)->get();
        foreach ($products as $pro) {
            $newPrice = getPrice($pro->item_id);
            if ($newPrice != $pro->current_price && $newPrice != 'Not newt_form_set_background(from, background)') {
                DB::table('item_has_price')->insert([
                    'account_id' => $account_id,
                    'item_id' => $pro->item_id,
                    'old_price' => $pro->current_price,
                    'new_price' => $newPrice
                ]);
                DB::table('selling_status')->where('item_id', '=', $pro->id)->update(['current_price' => $newPrice]);
            }
        }
        return redirect('active-listing');
    }

    public function Products(Request $request) {
        $accounts_id = session()->get('account');
        $user_id = Auth()->user()->id;
        $products = ActiveListing::select(['item.title', 'item.status', 'item.id', 'item.sku', 'item.quantity_available', 'selling_status.current_price','selling_status.quantity_sold', 'item.image', 'item.item_id as IID', 'item.created_at', 'item.time_left', 'item.supplier_price', 'item.hit_count', 'item.picture_details', 'listing_details.view_item_url', 'item.supplier_product_link', 'item.hit_counter', 'listing_details.start_time', 'listing_details.end_time', 'item.isOutOfStock', 'item.outOfStockFrom', 'item.watch_count','source_price','source_id', 'item.source_detail_page_url'])
                ->leftJoin('selling_status', 'selling_status.item_id', 'item.id')
                ->leftJoin('listing_details', 'listing_details.item_id', 'item.id')
                ->where('item.source_id', '!=', NULL)
                ->where('item.is_delete', '=', 0)
                ->where('item.account_id', '=', $accounts_id)
                ->where('item.user_id', '=', $user_id)
                ->orderBy('item.id', 'desc');

        return Datatables::of($products)
            ->addColumn('bulk', function($products) {
                return '<input type="checkbox" class="form-control" name="bulk" id="bulk_' . $products->id . '" />';
            })
            ->addColumn('edit', function($products) {
                return '<a href="javascript:;"><i class="fa fa-edit"></i></a>';
            })
            ->addColumn('sell_id', function($products) {
                if ($products->IID != '') {
                    return '<a href="javascript:;" target="_blank" title="' . $products->title . '">' . $products->IID . '</a>';
                } else {
                    return '-';
                }
            })
            ->addColumn('source_id', function($products) {
                if (isset($products->source_detail_page_url) && $products->source_detail_page_url!='' && $products->source_detail_page_url!=NULL) {
                    return '<a href="'.$products->source_detail_page_url.'" target="_blank" title="' . $products->title . '">' . (isset($products->source_id)?$products->source_id:'-') . '</a>';
                }else{
                    return isset($products->source_id)?$products->source_id:'-';
                }
            })
            ->addColumn('picture', function($products) {
                if ($products->picture_details != '') {
                    $image = '<img class="image-popup-vertical-fit" src="' . $products->picture_details . '" height="100px" width="100px" alt="Product Image">';
                } elseif ($products->image != '' && file_exists($products->image)) {
                    $image = '<img class="image-popup-vertical-fit" src="' . $products->image . '" height="100px" width="100px" alt="Product Image">';
                } else {
                    $image = '<img class="image-popup-vertical-fit" src="http://via.placeholder.com/100x100" alt="user" />';
                }
                return $image;
            })
            ->addColumn('title', function($products) {
                if ($products->title != '') {
                    return '<a href="javascript:;" target="_blank" title="' . $products->title . '">' . $products->title . '</a>';
                } else {
                    return '-';
                }
            })
            ->addColumn('price', function($products) {
                $priceUpdate = getTodayPrice($products->IID);
                if (empty($priceUpdate)) {
                    return '$'.$products->current_price;
                } else {
                    $icon = '';
                    $oldPrice = $priceUpdate->old_price;
                    $newPrice = $priceUpdate->new_price;
                    $priceDiff = $newPrice - $oldPrice;
                    $icon .= $oldPrice < $newPrice ? ' <i class="fa fa-arrow-up" style="color:green;"></i> ' : '<i class="fa fa-arrow-down" style="color:red;"></i> ';
                    $price = $products->current_price != '' ? number_format($products->current_price, 2) : '0.00';
                    return '$'.$price . $icon . $priceDiff;
                }
            })
            ->addColumn('source_price', function($products) {
                return '$'.number_format((float)$products->source_price, 2, '.', '');
            })
            ->addColumn('amount_sold', function($products) {
                return isset($products->quantity_sold)?$products->quantity_sold:0;
            })
            ->addColumn('profit', function ($products) {
                 $purchasePrice = $products->supplier_price;
                 $sellPrice = $products->current_price;
                 $ebayFee = (($sellPrice * 8.8) / 100);
                 $paypalFee = (($sellPrice * 2.6) / 100);
                 $transactionFee = 0.30;
                 $profit = $sellPrice - $purchasePrice - $ebayFee - $paypalFee - $transactionFee;
                return '$'.number_format($profit, 2);
            })
            ->addColumn('upload_date', function($products) {
                $date = getFormatedDate($products->start_time);
                return '<span class="text-muted">
                <i class="fa fa-clock-o"></i>&nbsp;' . date('M d Y', strtotime($date)) . '
            </span>';
            })
            ->addColumn('days_left', function($products) {
                return $products->time_left;
            })
            ->addColumn('oos_day', function($products) {
                if ($products->isOutOfStock == '1') {
                    $date1 = date_create($products->outOfStockFrom);
                    $date2 = date_create("2018-06-25");
                    $diff = date_diff($date1, $date2);
                    return $diff->format("%a days");
                } else {
                    return '  -  ';
                }
            })
            ->addColumn('dws', function($products) {
                return '-';
            })
            ->addColumn('status', function($products) {
                if ($products->IID != '') {
                    $status = '<span class="badge badge-boxed  badge-success">Live</span>';
                } else {
                    $status = '<span class="badge badge-boxed  badge-danger">Draft</labespanl>';
                }
                return $status;
            })
            ->rawColumns(['bulk', 'edit', 'sell_id', 'source_id', 'picture', 'title', 'source_price', 'amount_sold', 'profit', 'upload_date', 'days_left', 'oos_day', 'dws', 'status', 'price'])
            ->setTotalRecords($products->count())
            ->make(true);
    }

    public function importProducts(Request $request) {

        $itemApiObj = new ItemApi();
        $response = $itemApiObj->importProducts();
        if ($response->Ack != 'Failure' && $response->ActiveList) {
            $products = $response->ActiveList->ItemArray->Item;
            $this->saveProducts($products);
            $request->session()->flash('flash_message', 'Products Imported Succssfully');
        } else {
            $short_message = '';
            $message = '';
            foreach ($response->Errors as $errors) {
                if ($errors->SeverityCode == 'Error') {
                    $short_message .= '>' . $errors->ShortMessage;
                    $message = $message . '<br>' . $short_message;
                }
            }
            $request->session()->flash('flash_message', $message);
        }
        return redirect('active-listing');
    }

    public function saveProducts($products) {
        foreach ($products as $product) {
            $item_id = $product->ItemID;
            $this->getItem($item_id);
        }
        return redirect('active-listing');
    }

    public function getItem($item_id) {
        $itemApiObj = new ItemApi();
        $response = $itemApiObj->getItem($item_id);
        $message = '';
        $items = $response->Item;
        if ($response->Ack != 'Failure') {
            $item = $this->saveItem($items);
        } else {
            $short_message = '';
            $message = '<ul>';
            foreach ($response->Errors as $errors) {
                if ($errors->SeverityCode == 'Error') {
                    $message .= '<li>' . $errors->LongMessage . '</li>';
                }
            }
            $message .= '</ul>';
        }
        return $message;
    }

    public function saveItem($items, $isOther = false) {
        $productObj = new ActiveListing();
        foreach ($items as $item) {
            $ItemArray = [];
            $ItemArray['buy_it_now_price'] = $item->BuyItNowPrice;
            $ItemArray['country'] = $item->Country;
            $ItemArray['currency'] = $item->Currency;
            $ItemArray['item_id'] = $item->ItemID;
            $ItemArray['listing_duration'] = $item->ListingDuration;
            $ItemArray['listing_type'] = $item->ListingType;
            $ItemArray['location'] = $item->Location;
            $ItemArray['payment_methods'] = json_encode($item->PaymentMethods);
            $ItemArray['category'] = $item->PrimaryCategory->CategoryID;
            $ItemArray['private_listing'] = $item->PrivateListing;
            $ItemArray['quantity_available'] = $item->Quantity;
            $ItemArray['reserve_price'] = $item->ReservePrice;
            $ItemArray['revise_status'] = $item->ReviseStatus;
            $ItemArray['ship_to_locations'] = $item->ShipToLocations;
            $ItemArray['site'] = $item->Site;
            $ItemArray['start_price'] = $item->StartPrice;
            $ItemArray['time_left'] = $item->TimeLeft;
            $ItemArray['title'] = $item->Title;
            $ItemArray['watch_count'] = $item->WatchCount;
            $ItemArray['paypal_email_address'] = $item->PaypalEmailAddress;
            if($item->HitCount < 0){
                $ItemArray['hit_count'] = 0;
            }else{
                $ItemArray['hit_count'] = $item->HitCount;
            }
            $ItemArray['sku'] = $item->SKU;
            $ItemArray['postal_code'] = $item->PostalCode;
            $ItemArray['dispatch_time_max'] = $item->DispatchTimeMax;
            $ItemArray['proxy_item'] = $item->ProxyItem;
            $ItemArray['buyer_guarantee_price'] = $item->BuyerGuaranteePrice;
            $ItemArray['ship_to_register_country'] = $item->BuyerRequirementDetails->ShipToRegistrationCountry;
            $ItemArray['condition_id'] = $item->ConditionID;
            $ItemArray['condition_description'] = $item->ConditionDescription;
            $ItemArray['condition_display_name'] = $item->ConditionDisplayName;
            $ItemArray['hide_from_search'] = $item->HideFromSearch;
            $ItemArray['out_of_stock_control'] = $item->OutOfStockControl;
            $ItemArray['picture_details'] = $item->PictureDetails->GalleryURL;
            $ItemArray['returns_accepted_option'] = $item->ReturnPolicy->ReturnsAcceptedOption;
            $ItemArray['returns_accepted'] = $item->ReturnPolicy->ReturnsAccepted;
            $ItemArray['refund_type'] = $item->ReturnPolicy->RefundOption;
            $ItemArray['return_within'] = $item->ReturnPolicy->ReturnsWithinOption;
            $ItemArray['return_policy_detail'] = $item->ReturnPolicy->Description;
            $ItemArray['return_paid_by'] = $item->ReturnPolicy->ShippingCostPaidByOption;
            $ItemArray['restock_fee'] = $item->ReturnPolicy->RestockingFeeValueOption;
            $ItemArray['shipping_irregular'] = $item->ShippingPackageDetails->ShippingIrregular;
            $ItemArray['shipping_package'] = $item->ShippingPackageDetails->ShippingPackage;
            $ItemArray['weight_major'] = $item->ShippingPackageDetails->WeightMajor;
            $ItemArray['weight_minor'] = $item->ShippingPackageDetails->WeightMinor;

            foreach ($item->ListingDetails as $ld) {
                $ldArray['converted_buy_it_now_price'] = $ld->ConvertedBuyItNowPrice;
                $ldArray['converted_start_price'] = $ld->ConvertedStartPrice;
                $ldArray['converted_reserve_price'] = $ld->ConvertedReservePrice;
                $ldArray['has_reserve_price'] = $ld->HasReservePrice;
                $ldArray['start_time'] = $ld->StartTime;
                $ldArray['end_time'] = $ld->EndTime;
                $ldArray['view_item_url'] = $ld->ViewItemURL;
                $ldArray['view_item_url_for_natural_search'] = $ld->ViewItemURLForNaturalSearch;
            }

            foreach ($item->SellingStatus as $ss) {
                $ssArray['converted_current_price'] = $ss->ConvertedCurrentPrice;
                $ssArray['current_price'] = $ss->CurrentPrice;
                $ssArray['quantity_sold'] = $ss->QuantitySold;
                $ssArray['listing_status'] = $ss->ListingStatus;
            }

            foreach ($item->ShippingDetails as $sd) {
                $shippingArray = [];
                $shippingArray['sales_tax_percent'] = $sd->SalesTax->SalesTaxPercent;
                $shippingArray['shipping_included_in_tax'] = $sd->SalesTax->ShippingIncludedInTax;
                $shippingArray['shipping_service'] = $sd->ShippingServiceOptions->ShippingService;
                $shippingArray['shipping_service_cost'] = $sd->ShippingServiceOptions->ShippingServiceCost;
                $shippingArray['shipping_service_priority'] = $sd->ShippingServiceOptions->ShippingServicePriority;
                $shippingArray['additional_cost'] = $sd->ShippingServiceOptions->ShippingServiceAdditionalCost;
                $shippingArray['free_shipping'] = $sd->ShippingServiceOptions->FreeShipping;
                $shippingArray['shipping_type'] = $sd->ShippingType;
                if (count($sd->ExcludeShipToLocation) > 0) {
                    $ItemArray['international_shipping_country'] = json_encode($sd->ExcludeShipToLocation);
                }
            }

            $ItemArray['current_price'] = $ss->CurrentPrice;
            $product_id = $productObj->saveProduct($ItemArray, $isOther);
            $ldArray['item_id'] = $product_id;
            $listing_id = $productObj->saveListingDetails($ldArray);
            $ssArray['item_id'] = $product_id;
            $selling_status_id = $productObj->saveSallingStatus($ssArray);

            $storeArray['item_id'] = $product_id;
            //$store_id = $productObj->saveStoreDetail($storeArray);
            $shippingArray['item_id'] = $product_id;
            $shipping_id = $productObj->saveshipping_details($shippingArray);

            return $product_id;
        }
    }
}
