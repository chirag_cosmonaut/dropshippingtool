<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Helpers\eBaySession;
use App\Helpers\configration;
use App\Helpers\EbayApi;
use App\Accounts;

class ChannelController extends Controller{

    public function ebay_channel(){
    	return view('client.channels.ebay_channel');
    }

    public function connectStore(Request $request){

    }

    public function storeToken(Request $request){

    }

    public function getSessionId(Request $request){
    	$ebayApiObj    = new EbayApi();
        $submiturl     = $ebayApiObj->getSessionId();

		return redirect($submiturl);
    }

    public function fetchToken(Request $request){
        $userId  =  $request['username'];
        $account = Accounts::where('user_id','=',$userId)->where('int_user_id','=',Auth()->user()->id)->first();
        if(!empty($account)){
            $request->session()->flash('flash_message','Account Already Connected');
            return redirect('/settings');
        }
        $ebayApiObj    = new EbayApi();
        $token         = $ebayApiObj->fetchToken();
        
        $accountId = $this->getAccount($userId);

        $request->session()->flash('flash_message','Ebay Account Successfully Connected');
        return redirect('/settings');
    }

    public function getAccount($userId){
        $ebayApiObj     = new EbayApi();
        $response       = $ebayApiObj->getUser($userId);
        if($response->Ack == "Success" || $response->Ack == "Warning"){
            $account        = $this->saveAccount($response->User);

        }else{
            $short_message = '';
            $message = '';
            foreach($response->Errors as $errors){
                if($errors->SeverityCode=='Error'){
                    $short_message .= '>'.$errors->ShortMessage;
                    $message = $message.'<br>'.$short_message;
                }
            }
            session()->flash('flash_message',$message);
            return redirect('settings');
        }
        return $account;
    }

    public function saveAccount($account){
        foreach($account as $acc){
            $accountArray = [];
            $accountArray['eiast_token']                 = $acc->EIASToken;
            $accountArray['email']                       = $acc->Email;
            $accountArray['feedback_score']              = $acc->FeedbackScore;
            $accountArray['unique_negative_feedback_count'] = $acc->UniqueNegativeFeedbackCount;
            $accountArray['unique_positive_feedback_count'] = $acc->UniquePositiveFeedbackCount;
            $accountArray['positive_feedback_percent']   = $acc->PositiveFeedbackPercent;
            $accountArray['feedback_private']            = $acc->FeedbackPrivate;
            $accountArray['feeddback_rating_star']       = $acc->FeedbackRatingStar;
            $accountArray['id_verified']                 = $acc->IDVerified;
            $accountArray['ebay_good_standing']          = $acc->eBayGoodStanding;
            $accountArray['new_user']                    = $acc->NewUser;
            $accountArray['registration_date']           = $acc->RegistrationDate;
            $accountArray['site']                        = $acc->Site;
            $accountArray['status']                      = $acc->Status;
            $accountArray['user_id']                     = $acc->UserID;
            $accountArray['user_id_changed']             = $acc->UserIDChanged;
            $accountArray['user_id_last_changed']        = $acc->UserIDLastChanged;
            $accountArray['allow_payment_edit']          = $acc->SellerInfo->AllowPaymentEdit;
            $accountArray['checkeout_enabled']           = $acc->SellerInfo->CheckoutEnabled;
            $accountArray['cip_bank_account_stored']     = $acc->SellerInfo->CIPBankAccountStored;
            $accountArray['good_standing']               = $acc->SellerInfo->GoodStanding;
            $accountArray['live_auction_authorized']     = $acc->SellerInfo->LiveAuctionAuthorized;
            $accountArray['merchandizing_pref']          = $acc->SellerInfo->MerchandizingPref;
            $accountArray['qualifies_for_b2b_vat']       = $acc->SellerInfo->QualifiesForB2BVAT;
            $accountArray['seller_guarantee_level']      = $acc->SellerInfo->SellerGuaranteeLevel;
            $accountArray['store_owner']                 = $acc->SellerInfo->StoreOwner;
            $accountArray['payment_method']              = $acc->SellerInfo->PaymentMethod;
            $accountArray['charity_registered']          = $acc->SellerInfo->CharityRegistered;
            $accountArray['safe_payment_exempt']         = $acc->SellerInfo->SafePaymentExempt;
            $accountArray['max_scheduled_minutes']       = $acc->SellerInfo->SchedulingInfo->MaxScheduledMinutes;
            $accountArray['min_scheduled_minutes']       = $acc->SellerInfo->SchedulingInfo->MinScheduledMinutes;
            $accountArray['max_scheduled_items']         = $acc->SellerInfo->SchedulingInfo->MaxScheduledItems;
            $accountArray['transaction_percent']         = $acc->SellerInfo->TransactionPercent;

            $accountObj = new Accounts();
            $accId              = $accountObj->saveAccount($accountArray);
            $newProducts        = $accountObj->newProductMonitor($accId);
            $newOrderMonitors   = $accountObj->newOrderMonitor($accId);
            $newProfitable      = $accountObj->newProfitableMonitor($accId);
            $newOrderProcessing = $accountObj->newOrderProcessing($accId);
            $accountObj->saveToken($accId);
        }
    }

    public function switchAccount(Request $request){
        $error      = '';
        $message    = '';

        $account = $request->account;
        session()->put('account',$account);
        $message = "Account Successfully Switched";
        $request->session('flash_message',$message);
        $response = [
            'error'     => $error,
            'message'   => $message
        ];
        echo json_encode($response);
    }


    /*Do Nothing with These functions (used for Auth-n-Auth)*/
    public function authFail(){
        echo "Failed";
        exit();
    }
    public function privacyPolicy(){
    	return view('client.channels.privacy_policy');
    }
    /*Do Nothing with These functions (used for Auth-n-Auth)*/

    public function amazon_channel(){
        
    }
}