<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use DB;
use Accounts;
use Carbon\Carbon;
use DateTime;
use Date;


class DashboardController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        //$this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(){
        $user_id = Auth()->user()->id;
        $account_id = session()->get('account');
        //active
        $total=DB::table('item')->Where("source_id","!=",NULL)->where(['account_id'=>$account_id,'user_id'=>$user_id,'is_delete'=>0])->count();
        $live =DB::table('item')->Where("source_id","!=",NULL)->Where("item_id","!=","")->where(['account_id'=>$account_id,'user_id'=>$user_id,'is_delete'=>0])->count();
        $draft=DB::table('item')->Where("source_id","!=",NULL)->where("item_id","=","")->where(['account_id'=>$account_id,'user_id'=>$user_id,'is_delete'=>0])->count();
        //untracked
        $untracked=DB::table('item')->Where("source_id","=",NULL)->where(['account_id'=>$account_id,'user_id'=>$user_id,'is_delete'=>0])->count();
        //order
        $order=DB::table('orders')->where(['account_id'=>$account_id,'user_id'=>$user_id])->count();
        $complete=DB::table('orders')->where(['account_id'=>$account_id,'user_id'=>$user_id,'order_status'=>"Completed"])->count();
        $active=DB::table('orders')->where(['account_id'=>$account_id,'user_id'=>$user_id,'order_status'=>"Active"])->count();
        $cancel=DB::table('orders')->where(['account_id'=>$account_id,'user_id'=>$user_id,'order_status'=>"Canceled"])->count();
        //profits
        $week_profit= getOrderProfitByDate('weekly');
        $month_profit = getOrderProfitByDate('monthly');
        $last2_hours = getOrderProfitByDate('hourly');
        return view('client.dashboard',compact('total','live','draft','order','complete','active','cancel','week_profit','month_profit','last2_hours','untracked'));
    }

    public function getOrderProfitByWeekly(Request $request){
        $response = array();
        if (isset($request->week) && intval($request->week) > 0) {
            $response = getOrderProfitByWeekly($request->week);
        }
        echo json_encode(array('response' =>$response['data'] ));
    }

}