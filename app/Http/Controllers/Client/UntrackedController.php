<?php

namespace App\Http\Controllers\client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\ActiveListing;
use Datatables;
use Carbon;
use Validator;
use DB;
use AmazonProduct;
use Hookable;
use ApaiIO\Operations\Lookup;
use Illuminate\Support\Facades\Redirect;

class UntrackedController extends Controller
{
    public function index(){
    	return view('client.listing.untracked');
    }
    public function listData(Request $request)
    {
    	$accounts_id = session()->get('account');
        $user_id = Auth()->user()->id;
        $products = ActiveListing::select(['item.title', 'item.status', 'item.id', 'item.sku', 'item.quantity_available', 'selling_status.current_price', 'item.image', 'item.item_id as IID', 'item.created_at', 'item.time_left', 'item.supplier_price', 'item.hit_count', 'item.picture_details', 'listing_details.view_item_url', 'item.supplier_product_link', 'item.hit_counter', 'listing_details.start_time', 'listing_details.end_time', 'item.isOutOfStock', 'item.outOfStockFrom', 'item.watch_count'])
                ->leftJoin('selling_status', 'selling_status.item_id', 'item.id')
                ->leftJoin('listing_details', 'listing_details.item_id', 'item.id')
                ->where('item.source_id', '=', NULL)
                ->where('item.is_delete', '=', 0)
                ->where('item.account_id', '=', $accounts_id)
                ->where('item.user_id', '=', $user_id)
                ->orderBy('item.id', 'desc');

        return Datatables::of($products)
            ->addColumn('end', function($products) {
                return '<button>End</button>';
            })
            ->addColumn('sell_id', function($products) {
                if ($products->IID != '') {
                    return '<a href="javascript:;" target="_blank" title="' . $products->title . '">' . $products->IID . '</a>';
                } else {
                    return '-';
                }
            })
            ->addColumn('sell', function($products) {
                 return isset($products->site)?$products->site:'-';
            })
            ->addColumn('sell_site', function($products) {
                return 'ebay';
            })
            ->addColumn('picture', function($products) {
                if ($products->picture_details != '') {
                    $image = '<img class="image-popup-vertical-fit" src="' . $products->picture_details . '" height="100px" width="100px" alt="Product Image">';
                } elseif ($products->image != '' && file_exists($products->image)) {
                    $image = '<img class="image-popup-vertical-fit" src="' . $products->image . '" height="100px" width="100px" alt="Product Image">';
                } else {
                    $image = '<img class="image-popup-vertical-fit" src="http://via.placeholder.com/100x100" alt="user" />';
                }
                return $image;
            })
            ->addColumn('title', function($products) {
                if ($products->title != '') {
                    return '<a href="javascript:;" target="_blank" title="' . $products->title . '">' . $products->title . '</a>';
                } else {
                    return '-';
                }
            })
        
            ->addColumn('asin', function($products) {
                return isset($products->source_id)?$products->source_id:'-';
            })
            ->addColumn('amazon', function($products) {
                return '-';
            })
            ->addColumn('source_site', function($products) {
                return 'Amazon';
            })
            ->addColumn('status', function($products) {
                if ($products->IID != '') {
                    $status = '<span class="badge badge-boxed  badge-success">Live</span>';
                } else {
                    $status = '<span class="badge badge-boxed  badge-danger">Draft</labespanl>';
                }
                return $status;
            })
            ->rawColumns(['end', 'sell_id', 'sell', 'sell_site', 'picture', 'title', 'asin', 'amazon', 'source_site', 'status'])
            ->setTotalRecords($products->count())
            ->make(true); 
    }


    public function fileUpload(Request $request)
    {
        $this->validate($request ,[
            'file_name' => 'required|file|mimes:txt'
        ]);

        $user_name = Auth()->user()->name;
        $amazon =$request['source_site'];
        $ebay = $request['destination_site'];

        $trim = str_replace(" ","_",$user_name);
        $path = public_path().'/client/';
        $file = $request->file('file_name');
        $customfilename = $trim.'_'.date('Y_m_d_H_i_s') . '.' . $file->getClientOriginalExtension();
        $destinationPath = $path;
        $file->move($destinationPath, $customfilename);
        $readFile = $this->readData($customfilename);
        for($i=0;$i<count($readFile);$i++)
        {
            $source_id = ActiveListing::where('item_id',"=",$readFile[$i][0])->update(['source_item_id'=> $readFile[$i][1] , 'source_site'=>$amazon ,'destination_site'=>$ebay ]);
        }

        $message = "Updated successfully" ;
        $request->session()->flash('flash_message', $message);
        return redirect('untracked-listing');
    }
    
    public function uploadASINFile(Request $request) {
        $non_profitable = $non_available = [];
        $rules = ['txt_doc' => 'required'];
        $allowed =  array('txt');
        $filename = $_FILES['txt_doc']['name'];
        $ext = pathinfo($filename, PATHINFO_EXTENSION);
        $validator = Validator::make($request->all(), $rules);
        if ($validator->passes()) {
            if ($request->hasFile('txt_doc')) {
                if(in_array($ext,$allowed) && 'txt'==$ext) {
                    $uploadPhoto = $request->file('txt_doc');
                    $file_handle = @fopen($uploadPhoto, "r");
                    while (!feof($file_handle) ) {
                        $line_of_text = fgets($file_handle);
                        if ($line_of_text!='') {
                            $parts = explode(' ', $line_of_text);
                            if (isset($parts[0]) && trim($parts[0])!='' && !empty($parts)) {
                                $items = ActiveListing::select('item.*','selling_status.current_price')->leftJoin('selling_status', 'selling_status.item_id', 'item.id')->where(['item.item_id'=>$parts[0],'item.source_id'=>NULL])->first();
                                if (!empty($items)) {
                                    $productMonitorValue  = getRecordByAccount('products_monitor');
                                    if (!empty($productMonitorValue)) {
                                        if (isset($productMonitorValue->is_start) && $productMonitorValue->is_start==1) {
                                            if (isset($parts[1]) && $parts[1]!='') {
                                                $amazon_res = AmazonProduct::item(trim($parts[1]));
                                            } 
                                            if (!empty($amazon_res)) {
                                                $Items = isset($amazon_res['Items'])?$amazon_res['Items']:[];
                                                $Item = isset($Items['Item'])?$Items['Item']:[];
                                                $DetailPageURL = isset($Item['DetailPageURL'])?$Item['DetailPageURL']:NULL;
                                                $Offers = isset($Item['Offers'])?$Item['Offers']:[];
                                                $Offer = isset($Offers['Offer'])?$Offers['Offer']:[];
                                                $OfferListing = isset($Offer['OfferListing'])?$Offer['OfferListing']:[];
                                                $Price = isset($OfferListing['Price'])?$OfferListing['Price']:[];
                                                $Amount = isset($Price['Amount'])?$Price['Amount']:0;
                                                if (isset($Amount) && $Amount > 0) {
                                                    $price = $Amount/100;
                                                    $source_price = floatval($productMonitorValue->default_break_event)*floatval($price)/100;
                                                    $source_price_fixed = floatval($source_price)+floatval($productMonitorValue->additional_dollar);
                                                    $additional_per = floatval($productMonitorValue->additional_per)*floatval($price)/100;
                                                    $source_price_per = floatval($source_price)+floatval($additional_per);
                                                    $paypal_transaction_per = floatval('2.6')*floatval($price)/100;
                                                    $ebay_fees = floatval('8.8')*floatval($price)/100;
                                                    if (floatval($source_price_fixed) >= $source_price_per) {
                                                        $source_price_main = floatval($source_price_fixed)+$paypal_transaction_per+floatval('0.30')+$ebay_fees;
                                                    }else{
                                                        $source_price_main = floatval($source_price_per)+$paypal_transaction_per+floatval('0.30')+$ebay_fees;
                                                    }
                                                    $current_price = isset($items->current_price)?$items->current_price:'0.00';
                                                    $current_sell_price = floatval($current_price)+floatval($source_price_main);
                                                    $minimun_profit = floatval($source_price)+floatval($ebay_fees)+floatval($paypal_transaction_per)+floatval('0.30');
                                                    if (floatval($minimun_profit) >= $productMonitorValue->min_profit_dollar_per_product) {
                                                        if (isset($parts[1]) && $parts[1]!='') {
                                                            ActiveListing::where('item_id',$parts[0])->update(['source_price'=>$price,'source_id'=>$parts[1],'source_detail_page_url'=>$DetailPageURL]);
                                                            DB::table('selling_status')->where('item_id',$items->id)->update(['current_price'=>$current_sell_price]);
                                                        }
                                                    }else{
                                                        $non_profitable[] = $parts[0];
                                                    }
                                                }
                                            }else{
                                                $non_available[] = $parts[0];
                                            }
                                        }else{
                                            $request->session()->flash('error_message', 'Please apply moniters.');
                                            //return redirect('untracked-listing');
                                        }
                                    }
                                }
                            }
                        }
                        $request->session()->flash('flash_message', 'File Uploaded Successfully.');
                        if (count($non_profitable) > 0) {
                            $request->session()->flash('error_message', ''.count($non_profitable).' Items are not profitable.');
                        }
                        if (count($non_available) > 0) {
                            $request->session()->flash('error_message', ''.implode(" , ", $non_available).' Items are not available on Amazon.');
                        }
                        //return redirect('untracked-listing');
                    }
                    //$request->session()->flash('error_message', 'File is empty.');
                    fclose($file_handle);
                    return redirect('untracked-listing');
                }else{
                    $request->session()->flash('error_message', 'Please upload txt files only.');
                    return redirect('untracked-listing');
                }     
            }else{
                $request->session()->flash('error_message', 'Please select a file to upload.');
                return redirect('untracked-listing');
            }
        }else{
            $request->session()->flash('error_message', 'Please select a file to upload.');
            return redirect('untracked-listing');
        }    
    }
}
