<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Tag;

class TagController extends Controller
{
    public function saveTag(Request $request){
    	$tagObj = new Tag();
    	$tagObj->tag_name = $request['tag_name'];
    	$tagObj->save();   
        $request->session()->flash('flash_message', 'Tag Successfully Added');  
        return back()->withInput();
    }
}
