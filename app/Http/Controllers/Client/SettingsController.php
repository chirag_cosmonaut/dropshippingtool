<?php

namespace App\Http\Controllers\Client;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Accounts;
use App\ActiveListing;

class SettingsController extends Controller
{
	public function getSettings(){
		$accountsObj = new Accounts();
		$account = $accountsObj->getSettings();
	    $accCount=Accounts::Where("int_user_id", "=",Auth()->user()->id)->count();
        $blockProduct = getAllRecord('item',array('is_block'=>1));
		return view('client.settings.settings',['account'=>$account,'actionUrl'=>url('settings/update/{id}'),'accountsObj'=>$accountsObj ,'accCount'=>$accCount , 'blockProduct'=>$blockProduct]);

    }
    
    public function update(Request $request){
        $formData = $request->all();
        if($request['image'] != ''){
            $image = $request->file('image');
            $formData['image'] = time().'.'.$image->getClientOriginalExtension();

            $destinationPath = public_path('/images/accounts');
            $image->move($destinationPath, $formData['image']);
        }else{
            $formData['image'] = '';
        }
        $accountObj = new Accounts();
        $accountObj->updateAccounts($formData);
        $message = 'settings Successfully Updated';
        $request->session()->flash('flash_message',$message);
        
        return redirect('settings');
    }
    public function blockProducts(Request $request)
    {
        $itemId = $request->blockProductId;
        if($request->isblock=="true")
        {
            $block_product = ActiveListing::Where("item_id","=", $itemId)->update(['is_block'=>1]);
        }
        else
        {
            $block_product = ActiveListing::Where("item_id","=", $itemId)->update(['is_block'=>0]);
        }
        
    }
    
    
}
