<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use Datatables;
use App\Subscription;
use Illuminate\Support\Facades\Validator;
use DB;

class SubscriptionController extends Controller
{
    public function index(){
      $subscriptionObj=Subscription::where("is_active" ,"=", 1)->get();
    	return view('admin.subscription.list',compact('subscriptionObj'));
    }
    public function getData(Request $request){
    	$subscriptions = getAllRecord('subscription');
    	return Datatables::of($subscriptions)
		->addColumn('action',function($subscriptions){
			$action = '<a href="'.url('admin/subscription/edit',$subscriptions->id).'"  class="btn btn-theme btn-sm"><i class="fa fa-edit"></i></a>'.'<a href="'.url('admin/subscription/delete',$subscriptions->id).'"  class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>';

			return $action;
		})
		/*->addColumn('status',function($subscriptions){
			
		})*/
		->rawColumns(['action','status'])
        ->setTotalRecords($subscriptions->count())
        ->make(true);
    }
    public function viewPage(){
      $result =DB::select('select id,name from subscription_access');
      return view('admin.subscription.create',compact('result'));
    }
  	public function create(Request $request)
  	{
  		$this->validate($request,[
    		'name'			=>'required',
    		'subPrice'		=>'required',
    		'subValidity' 	=>'required'
    	]);
      $result = $request->input('access_list');

      $subscriptionObj 			     = new Subscription();
    	$subscriptionObj->name   	 = $request['name'];
    	$subscriptionObj->price 	 = $request['subPrice'];
    	$subscriptionObj->validity = $request['subValidity'];
    	$subscriptionObj->save();

      $id = $subscriptionObj->id;
      for($i=0;$i<count($result);$i++)
      {
        DB::table('subscription_has_access')->insert(['subcription_id'=>$id , 'access_id'=>$result[$i] ]) ;
      }
    	$request->session()->flash('message','Subscription details added successfully');
    	return redirect('admin-panel/subscription');
  	}
  	public function edit($id)
  	{
      $subscriptionObj  = Subscription::find($id);

      $subscription_id = $subscriptionObj->id;
      $result = DB::select('select id , name from subscription_access'); 
      $response = DB::table('subscription_has_access')->select('access_id')->where('subcription_id',"=",$subscription_id)->get();
      $accessList = [];
      foreach($response as $res)
      {
        array_push($accessList,$res->access_id);
      }
     
    	return view('admin.subscription.update',compact('subscriptionObj','result','accessList'));
  	}
  	public function update(Request $request)
  	{
  		$this->validate($request,[
  			'edit_name' 		=> 'required',
  			'edit_subPrice'	 	=> 'required',
  			'edit_subValidity' 	=> 'required'
  		]);
      $result = $request->input('access_list');

  		$subscriptionObj  			       = Subscription::find($request['edit_id']);
  		$subscriptionObj->name         = $request['edit_name'];
  		$subscriptionObj->price		     = $request['edit_subPrice'];
  		$subscriptionObj->validity 	   = $request['edit_subValidity'];
  		$subscriptionObj->save();

      $id = $subscriptionObj->id;
      $del_access = DB::table('subscription_has_access')->where('subcription_id',"=",$id)->delete();
      for($i=0;$i<count($result);$i++)
      {
        DB::table('subscription_has_access')->insert(['subcription_id'=>$id , 'access_id'=>$result[$i] ]) ;
      }
      
  		return redirect('admin-panel/subscription');
  	}
    public function destroy($id)
    {
        $subscriptionObj  = Subscription::findOrFail($id);
        $subscriptionObj->is_active = 0;
        $subscriptionObj->save();
        return redirect('admin-panel/subscription');
    }

    /*Functions for Access List*/
    public function accessList(){
      return view('admin.subscription.accesslist');
    }
    public function getAccess(Request $request)
    {
      $subscription_access = getAllRecord('subscription_access');
      return Datatables::of($subscription_access)
      ->addColumn('action',function($subscription_access){
        $action ='<a href="'.url('admin/subscription/access_edit/{id}', $subscription_access->id).'"  class="btn btn-theme btn-sm" data-target="#edit_access" data-toggle = "modal"><i class="fa fa-edit"></i></a>'.'<a href="'.url('admin/subscription/access_delete',$subscription_access->id).'"  class="btn btn-danger btn-sm"><i class="fa fa-close"></i></a>'; 
        return $action;
      })
      ->rawColumns(['action','status'])
        ->setTotalRecords($subscription_access->count())
        ->make(true);
    }
    public function saveAccess(Request $request)
    {
      $message = "";
      $error = "";
      $add_name = $request->access_name;
      $addName = DB::table('subscription_access')->insert([
        'name' =>$add_name,
      ]);
      $message= "Access inserted successfully";
      $response = [
        'response' => $message,
        'error'    => $error,
      ];
      $request->session()->flash('flash_message',$message);
      echo json_encode($response);
    }
}
