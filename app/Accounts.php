<?php
namespace App;

use Illuminate\Database\Eloquent\Model;

use DB;

class Accounts extends Model
{
	protected $table = 'accounts';

	public function getSettings(){
        $currentAccount = session()->get('account');
        $loggedUser     = Auth()->user()->id;
    	$accountsObj    = Accounts::where('int_user_id','=',$loggedUser)->where('id','=',$currentAccount)->first();
        if (empty($accountsObj)) {
           $accountsObj = new Accounts();
        }
    	return $accountsObj;
    }

    public function saveToken($accountId){
        $token = session()->get('ebayToken');

        if(empty($accountId)){
            $accountsObj = new Accounts();
        }else{
            $accountsObj = Accounts::findOrFail($accountId);
        }

        $accountsObj->token = $token;
        $accountsObj->save();
        session()->put('account',$accountId);
        return $accountsObj;
    }


    public function updateAccounts($fields){
        $currentAccount     = session()->get('ebayAccount');
        $loggedUser         = Auth()->user()->id;
    	$accountsObj        = Accounts::findOrFail($currentAccount);
        if(empty($accountsObj)) {
           $accountsObj = new Accounts();
        }
        $accountsObj->int_user_id               = $loggedUser;
        $accountsObj->email                     = $fields['email'];
        $accountsObj->user_id                   = $fields['user_id'];
        $accountsObj->fees                      = $fields['fees'];
        $accountsObj->country                   = $fields['country'];
        $accountsObj->fulfillment_centers       = $fields['fulfillment_centers'];
        $accountsObj->image                     = $fields['image'];
    	$accountsObj->save();
    	return $accountsObj;
    }

    public function saveAccount($fields){
        $loggedUser = Auth()->user()->id;
        $token = session()->get('ebaySession');
        $accMatch = Accounts::where('user_id','=',$fields['user_id'])->where('int_user_id','=',$loggedUser)->first();

        if(empty($accMatch)){
            $accountsObj = new Accounts();
        }else{
            $accountsObj = Accounts::find($accMatch->id);
        }


        $accountsObj->eiast_token      =  $fields['eiast_token'];
        $accountsObj->email            =  $fields['email'];
        $accountsObj->feedback_score   =  $fields['feedback_score'];
        $accountsObj->unique_negative_feedback_count    =  $fields['unique_negative_feedback_count'];
        $accountsObj->unique_positive_feedback_count    =  $fields['unique_positive_feedback_count'];
        $accountsObj->positive_feedback_percent         =  $fields['positive_feedback_percent'];
        $accountsObj->feedback_private          =  $fields['feedback_private'];
        $accountsObj->feeddback_rating_star     =  $fields['feeddback_rating_star'];
        $accountsObj->id_verified               =  $fields['id_verified'];
        $accountsObj->ebay_good_standing        =  $fields['ebay_good_standing'];
        $accountsObj->new_user                  =  $fields['new_user'];
        $accountsObj->registration_date         =  $fields['registration_date'];
        $accountsObj->site                      =  $fields['site'];
        $accountsObj->status                    =  $fields['status'];
        $accountsObj->user_id                   =  $fields['user_id'];
        $accountsObj->user_id_changed           =  $fields['user_id_changed'];
        $accountsObj->user_id_last_changed      =  $fields['user_id_last_changed'];
        $accountsObj->allow_payment_edit        =  $fields['allow_payment_edit'];
        $accountsObj->checkeout_enabled         =  $fields['checkeout_enabled'];
        $accountsObj->cip_bank_account_stored   =  $fields['cip_bank_account_stored'];
        $accountsObj->good_standing             =  $fields['good_standing'];
        $accountsObj->live_auction_authorized   =  $fields['live_auction_authorized'];
        $accountsObj->merchandizing_pref        =  $fields['merchandizing_pref'];
        $accountsObj->qualifies_for_b2b_vat     =  $fields['qualifies_for_b2b_vat'];
        $accountsObj->seller_guarantee_level    =  $fields['seller_guarantee_level'];
        $accountsObj->store_owner               =  $fields['store_owner'];
        $accountsObj->payment_method            =  $fields['payment_method'];
        $accountsObj->charity_registered        =  $fields['charity_registered'];
        $accountsObj->safe_payment_exempt       =  $fields['safe_payment_exempt'];
        $accountsObj->max_scheduled_minutes     =  $fields['max_scheduled_minutes'];
        $accountsObj->min_scheduled_minutes     =  $fields['min_scheduled_minutes'];
        $accountsObj->max_scheduled_items       =  $fields['max_scheduled_items'];
        $accountsObj->transaction_percent       =  $fields['transaction_percent'];
        $accountsObj->int_user_id               =  $loggedUser;
        $accountsObj->token                     =  $token;
        $accountsObj->save();
        return $accountsObj->id;
    }


    public function newProductMonitor($account_id)
    {
        $user_id = Auth()->user()->id;
        $productsMonitor = DB::table('products_monitor')->where(['user_id'=>$user_id,'account_id'=>$account_id])->first();
        if (empty($productsMonitor)) {
            $newProductsMonitor = DB::table('products_monitor')->insert([
               'user_id'                         => $user_id ,
               'account_id'                      => $account_id ,
               'default_break_event'             => 0.0 ,
               'additional_dollar'               => 0.0 ,
               'additional_per'                  => 0.0 ,
               'end_untracked_listing'           => 0 ,
               'not_available_listing'           => 0,
               'when_product_available'          => 0 ,
               'quantity_to_raise_to'            => 0.0,
               'dynamic_policy_creation'         => 0 ,
               'min_allowed_quantity_in_stock'   => 0.0 ,
               'min_profit_dollar_per_product'   => 0.0 ,
               'when_product_vero'               => 'Alert Me' ,
               'when_product_blocked'            => 'Alert Me' ,
               'max_shipping_time'               => 0 ,
               'automatic_sku_filling'           => 0 ,
               'is_start'                        => 0
            ]);
            return $newProductsMonitor;
        }else{
            return $productsMonitor;
        }       
        
    }
    public function newOrderMonitor($account_id)
    {
        $user_id = Auth()->user()->id;
        $OrdersMonitor = DB::table('orders_monitor')->where(['user_id'=>$user_id,'account_id'=>$account_id])->first();
        if (empty($OrdersMonitor)) {
            $newOrdersMonitor = DB::table('orders_monitor')->insert([
                'user_id'                            => $user_id ,
                'account_id'                         => $account_id ,
                'when_product_sold'                  => 'Alert Me' ,
                'quantity_to_raise_to'               =>0.0 , 
                'when_product_sold_change_price'     =>0 ,
                'start_price_raise_after_x_sold'     =>0 ,
                'raise_only_for_first_sell'          =>0 ,
                'raise_for_each_sale'                =>0 ,
                'raise_additional_per'               =>0.0 ,
                'raise_additional_dollar'            =>0.0 ,
                'max_per_raise_limit'                =>0.0 ,
                'max_price_raise_limit'              =>0.0 ,
                'drop_price_product_not_sold'        =>0 ,
                'drop_price_afterX_days'             =>0.0 ,
                'drop_price_per'                     =>0.0 ,
                'drop_price_amount'                  =>0.0 ,
                'min_drop_price_per'                 =>0.0 ,
                'min_drop_price_amount'              =>0.0 ,
                'is_start'                           =>0
            ]);
            return $newOrdersMonitor;
        }else{
            return $OrdersMonitor;
        }
    }
  
    public function newProfitableMonitor($account_id)
    {
        $user_id = Auth()->user()->id;
        $ProfitableMonitors = DB::table('profitable_monitor')->where(['user_id'=>$user_id,'account_id'=>$account_id])->first();
        if (empty($ProfitableMonitors)) {
            $newProfitableMonitors = DB::table('profitable_monitor')->insert([
                'user_id'                   => $user_id ,
                'account_id'                => $account_id ,
                'listing_not_profitable'    => 'Alert Me', 
                'listing_left_days'         => 0,
                'listing_min_sold_quantity' => 0,
                'listing_min_watchers'      => 0,
                'listing_min_views'         => 0,
                'is_start'                  => 0
            ]);
            return $newProfitableMonitors;
        }else{
            return $ProfitableMonitors;
        }   
    }
    
    public function newOrderProcessing($account_id)
    {
        $user_id = Auth()->user()->id;
        $OrdersProcessing = DB::table('order_processing')->where(['user_id'=>$user_id,'account_id'=>$account_id])->first();
        if (empty($OrdersProcessing)) {
            $newOrdersProcessing = DB::table('order_processing')->insert([
                'user_id'                     => $user_id ,
                'account_id'                  => $account_id ,
                'auto_order'                  => 0,
                'bundle_add_on'               => 0, 
                'oder_only_prime'             => 0,
                'hipshipper'                  => 0,
                'max_product_price_auto_order'=> 0.0,
                'max_lose_amount'             => 0.0,
                'set_order_shipped'           => 'Alert Me',
                'gift_message'                => 'Thank You',
                'set_ontrack_shipping_carrier'=> 0,
                'is_start'                    => 0
            ]);         
            return $newOrdersProcessing;
        }else{
            return $OrdersProcessing;
        }
    }
}