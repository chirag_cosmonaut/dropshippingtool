<?php

namespace App\Helpers;

use App\Helpers\eBaySession;
use App\Helpers\EbayApi;

Class ItemApi {

    protected $token;
    protected $creadential;

    public function __construct() {
        $ebayApiObj = new EbayApi();
        $this->token = $ebayApiObj->getToken();
        $this->credential = $ebayApiObj->getCredential();
    }

    public function importProducts() {
        $verb = 'GetMyeBaySelling';
        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
                            <GetMyeBaySellingRequest xmlns="urn:ebay:apis:eBLBaseComponents">
                              	<RequesterCredentials>
                                	<eBayAuthToken>' . $this->token . '</eBayAuthToken>
                              	</RequesterCredentials>
                                <ErrorLanguage>en_US</ErrorLanguage>
                                <WarningLevel>High</WarningLevel>
                              	<ActiveList>
                                	<Sort>TimeLeft</Sort>
                              	</ActiveList>
                            </GetMyeBaySellingRequest>';
        $session = new eBaySession($this->credential['devID'], $this->credential['appID'], $this->credential['certID'], $this->credential['serverUrl'], $this->credential['compatabilityLevel'], $this->credential['siteID'], $verb);
        $responseXml = $session->sendHttpRequest($requestXmlBody);

        if (stristr($responseXml, 'HTTP 404') || $responseXml == '')
            die('<P>Error sending request');

        $response = simplexml_load_string($responseXml);
        return $response;
    }

    public function getItem($item_id) {

        $verb = 'getItem';
        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
                    <GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
                      <RequesterCredentials>
                        <eBayAuthToken>' . $this->token . '</eBayAuthToken>
                      </RequesterCredentials>
                      <ItemID>' . $item_id . '</ItemID>
                      <IncludeWatchCount>true</IncludeWatchCount>
                    </GetItemRequest>';

        $session = new eBaySession($this->credential['devID'], $this->credential['appID'], $this->credential['certID'], $this->credential['serverUrl'], $this->credential['compatabilityLevel'], $this->credential['siteID'], $verb);
        $responseXml = $session->sendHttpRequest($requestXmlBody);
        $response = formatApiResponse($responseXml);
        return $response;
    }

    public function getPrice($id) {

        $verb = 'getItem';
        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
                    <GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
                      <RequesterCredentials>
                        <eBayAuthToken>' . $this->token . '</eBayAuthToken>
                      </RequesterCredentials>
                      <ItemID>' . $id . '</ItemID>
                      <IncludeWatchCount>true</IncludeWatchCount>
                    </GetItemRequest>';
        $session = new eBaySession($this->credential['devID'], $this->credential['appID'], $this->credential['certID'], $this->credential['serverUrl'], $this->credential['compatabilityLevel'], $this->credential['siteID'], $verb);
        $responseXml = $session->sendHttpRequest($requestXmlBody);
        $response = formatApiResponse($responseXml);
        return $response;
    }

}
