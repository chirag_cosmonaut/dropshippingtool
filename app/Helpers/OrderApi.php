<?php

namespace App\Helpers;

use App\Helpers\eBaySession;
use App\Helpers\EbayApi;

Class OrderApi {

    protected $token;
    protected $credential;

    public function __construct() {
        $ebayApiObj = new EbayApi();
        $this->token = $ebayApiObj->getToken();
        $this->credential = $ebayApiObj->getCredential();
    }

    public function importOrders($from, $to) {
        /* date_default_timezone_set('Australia/ACT'); */
        /* $fromDate = date('Y-m-d',time()-259200);
          $toDate = date('Y-m-d'); */
        $CreateTimeFrom = $from . 'T00:00:00.000Z';
        $CreateTimeTo = $to . 'T23:59:59.000Z';
        /* $CreateTimeFrom = '2018-03-01T00:00:00.000Z';
          $CreateTimeTo = '2018-04-01T23:59:59.000Z'; */
        $verb = 'GetOrders';
        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
        <GetOrdersRequest xmlns="urn:ebay:apis:eBLBaseComponents">
                <RequesterCredentials>
                        <eBayAuthToken>' . $this->token . '</eBayAuthToken>
                </RequesterCredentials>
                <ErrorLanguage>en_US</ErrorLanguage>
                <WarningLevel>High</WarningLevel>
                <CreateTimeFrom>' . $CreateTimeFrom . '</CreateTimeFrom>
                <CreateTimeTo>' . $CreateTimeTo . '</CreateTimeTo>
                <OrderStatus>All</OrderStatus>
                <DetailLevel>ReturnAll</DetailLevel>
                <IncludeFinalValueFee>true</IncludeFinalValueFee>
        </GetOrdersRequest>';
        $session = new eBaySession($this->credential['devID'], $this->credential['appID'], $this->credential['certID'], $this->credential['serverUrl'], $this->credential['compatabilityLevel'], $this->credential['siteID'], $verb);
        $responseXml = $session->sendHttpRequest($requestXmlBody);
        $response = formatApiResponse($responseXml);
        return $response;
    }

    public function getFeedback($feedbackType) {

        $verb = 'GetFeedback';
        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
			<GetFeedbackRequest xmlns="urn:ebay:apis:eBLBaseComponents">
			  	<RequesterCredentials>
			    	<eBayAuthToken>' . $this->token . '</eBayAuthToken>
			  	</RequesterCredentials>
			  	<FeedbackType>' . $feedbackType . '</FeedbackType>
			  	<DetailLevel>ReturnAll</DetailLevel>
			  	<WarningLevel>High</WarningLevel>
			</GetFeedbackRequest>';

        $session = new eBaySession($this->credential['devID'], $this->credential['appID'], $this->credential['certID'], $this->credential['serverUrl'], $this->credential['compatabilityLevel'], $this->credential['siteID'], $verb);
        $responseXml = $session->sendHttpRequest($requestXmlBody);
        $response = formatApiResponse($responseXml);
        return $response;
    }

    public function CompleteSale($orderObj, $change, $request) {
        $verb = "CompleteSale";
        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
					 	<CompleteSaleRequest xmlns="urn:ebay:apis:eBLBaseComponents">
					 	<RequesterCredentials>
					  		<eBayAuthToken>' . $this->token . '</eBayAuthToken>
						</RequesterCredentials>
				    	<OrderID>' . $orderObj->OID . '</OrderID>
				    	<ItemID>' . $orderObj->item_id . '</ItemID>';
        if ($change == 'payment') {
            if ($request->ispaid == 'false') {
                $requestXmlBody .= '<Paid>false</Paid>';
            } else {
                $requestXmlBody .= '<Paid>true</Paid>';
            }
        } elseif ($change == 'shipped') {
            if ($request->isshipped == 'false') {
                $requestXmlBody .= '<Shipped>false</Shipped>';
            } else {
                $requestXmlBody .= '<Shipped>true</Shipped>';
            }
        }
        $requestXmlBody .= '</CompleteSaleRequest>';
        $session = new eBaySession($this->credential['devID'], $this->credential['appID'], $this->credential['certID'], $this->credential['serverUrl'], $this->credential['compatabilityLevel'], $this->credential['siteID'], $verb);
        $responseXml = $session->sendHttpRequest($requestXmlBody);
        $response = formatApiResponse($responseXml);
        return $response;
    }

    public function contactBuyer($data, $message, $buyer) {
        $verb = 'AddMemberMessageAAQToPartner';
        $compatabilityLevel = 1043;
        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
					<AddMemberMessageAAQToPartnerRequest xmlns="urn:ebay:apis:eBLBaseComponents">
					  <RequesterCredentials>
						<eBayAuthToken>' . $this->token . '</eBayAuthToken>
					  </RequesterCredentials>
					  <ItemID>' . $data->item_id . '</ItemID>
					  <MemberMessage>
						<Subject>We are Canceling The Order</Subject>
						<Body>' . $message . '</Body>';
        /* $requestXmlBody .= '<MessageMedia>';
          for($i=0;$i<count($attachment);$i++){
          $requestXmlBody .= '<MediaName>'.$attachment[$i].'</MediaName>';
          }
          $requestXmlBody .= '</MessageMedia>'; */
        $requestXmlBody .= '
						<QuestionType>General</QuestionType>
						<RecipientID>' . $buyer . '</RecipientID>
					  </MemberMessage>
					  <Version>1043</Version>
					</AddMemberMessageAAQToPartnerRequest>';
        /* var_dump($requestXmlBody);
          exit(); */
        $session = new eBaySession($this->credential['devID'], $this->credential['appID'], $this->credential['certID'], $this->credential['serverUrl'], $this->credential['compatabilityLevel'], $this->credential['siteID'], $verb);
        $responseXml = $session->sendHttpRequest($requestXmlBody);
        $response = formatApiResponse($responseXml);
        return $response;
    }

    public function cancelOrder($data, $reason) {

        $url = 'https://api.sandbox.ebay.com/post-order/v2/cancellation';
        $postData = json_encode(array(
            "legacyOrderId" => $data->order_id,
            "cancelReason" => $reason,
            "buyerPaid" => false));
        $header = array(
            'Accept: application/json',
            'Authorization: TOKEN ' . $this->token,
            'Content-Type: application/json',
            'X-EBAY-C-MARKETPLACE-ID: EBAY-AU'
        );
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $url);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0);
        curl_setopt($ch, CURLOPT_HTTPHEADER, $header);
        curl_setopt($ch, CURLOPT_POST, 1);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $postData);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        $response1 = curl_exec($ch);

        if (curl_errno($ch)) {
            echo "ERROR:" . curl_error($ch);
        }
        curl_close($ch);
        return $ch;
    }

    public function getOrderTransaction($order_id) {
        $verb = 'GetOrderTransactions';
        $compatabilityLevel = 1043;

        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
						<GetOrderTransactionsRequest xmlns="urn:ebay:apis:eBLBaseComponents">
						  <RequesterCredentials>
						    <eBayAuthToken>' . $this->token . '</eBayAuthToken>
						  </RequesterCredentials>
						  <OrderIDArray>
						    <OrderID>' . $order_id . '</OrderID>
						  </OrderIDArray>
						</GetOrderTransactionsRequest>';
        $session = new eBaySession($this->credential['devID'], $this->credential['appID'], $this->credential['certID'], $this->credential['serverUrl'], $this->credential['compatabilityLevel'], $this->credential['siteID'], $verb);
        $responseXml = $session->sendHttpRequest($requestXmlBody);
        $response = formatApiResponse($responseXml);
        return $response;
    }

}
