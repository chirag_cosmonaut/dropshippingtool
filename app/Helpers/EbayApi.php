<?php

namespace App\Helpers;
use App\Helpers\eBaySession;
use App\Accounts;

class EbayApi{

	/*Sandbox Credentials*/
	private $devID            = '37a1ae64-f1d7-412a-ba3e-ce8e46efb8d0';
	private $appID            = 'testdemo-testdemo-SBX-25d7a0307-65bc2a1c';
	private $certID           = 'SBX-5d7a0307798a-d9b6-4962-93a4-384f';
	private $serverUrl        = 'https://api.sandbox.ebay.com/ws/api.dll';
	private $loginURL         = 'https://signin.sandbox.ebay.com/ws/eBayISAPI.dll';
	private $runame           = 'test_demo-testdemo-testde-kjbipkt';


	/*Production Credentials*/
	/*private $devID            = '37a1ae64-f1d7-412a-ba3e-ce8e46efb8d0';
	private $appID            = 'testdemo-testdemo-PRD-d2ccbdebc-39f51547';
	private $certID           = 'PRD-2ccbdebc4779-ec5f-4b2e-a901-dcde	';
	private $serverUrl        = 'https://api.ebay.com/ws/api.dll';
	private $loginURL         = 'https://signin.ebay.com/ws/eBayISAPI.dll';
	private $runame           = 'test_demo-testdemo-testde-clwrnbqzl';*/

	private $compatabilityLevel = 1057;
	private $siteID           = 0;

	public function getSessionId(){
		$verb             = 'GetSessionID';
		$requestBody     = '<?xml version="1.0" encoding="utf-8" ?>';
		$requestBody     .= '<GetSessionIDRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
		$requestBody     .= "<Version>".$this->compatabilityLevel."</Version>";
		$requestBody     .= "<RuName>".$this->runame."</RuName>";
		$requestBody     .= '</GetSessionIDRequest>';

		$session = new eBaySession($this->devID, $this->appID, $this->certID, $this->serverUrl, $this->compatabilityLevel, $this->siteID, $verb);
		$responseBody = $session->sendHttpRequest($requestBody);
		if(stristr($responseBody, 'HTTP 404') || $responseBody == '')
			die('<P>Error sending request');

		$response = simplexml_load_string($responseBody);
		session()->put('ebaySession',(string)$response->SessionID);
		$sesId = urlencode(session()->get('ebaySession'));
		$submiturl = $this->loginURL.'?SignIn&runame='.$this->runame.'&SessID='.$sesId;
		return $submiturl;
	}

	public function fetchToken(){
		
		$sesId 			  =  session()->get('ebaySession');
		$verb             =  'FetchToken';
        $requestXmlBody   =  '<?xml version="1.0" encoding="utf-8" ?>';
        $requestXmlBody   .= '<FetchTokenRequest xmlns="urn:ebay:apis:eBLBaseComponents">';
        $requestXmlBody   .= "<SessionID>".$sesId."</SessionID>";
        $requestXmlBody   .= '</FetchTokenRequest>';

        $session = new eBaySession($this->devID, $this->appID, $this->certID, $this->serverUrl, $this->compatabilityLevel, $this->siteID, $verb);
        $responseXml = $session->sendHttpRequest($requestXmlBody);

        if(stristr($responseXml, 'HTTP 404') || $responseXml == '')
            die('<P>Error sending request');

        $response = simplexml_load_string($responseXml);
        $token = $response->eBayAuthToken;
        session()->put('ebayToken',(string)$response->eBayAuthToken);
        return $token;
	}

	public function getUser($userId){
		$token = $this->getToken();

   		$verb = 'GetUser'; 
        $requestBody='<?xml version="1.0" encoding="utf-8"?> 
				<GetUserRequest xmlns="urn:ebay:apis:eBLBaseComponents"> 
				  <RequesterCredentials> 
				    <eBayAuthToken>'.$token.'</eBayAuthToken> 
				  </RequesterCredentials> 
				  <UserID>'.$userId.'</UserID>
				</GetUserRequest>';
			
        $session = new eBaySession($this->devID, $this->appID, $this->certID, $this->serverUrl, $this->compatabilityLevel, $this->siteID, $verb);
		$responseBody = $session->sendHttpRequest($requestBody);
		if(stristr($responseBody, 'HTTP 404') || $responseBody == '')
			die('<P>Error sending request');

		$response = simplexml_load_string($responseBody);
        return $response;
	}

	public function getToken(){
		$acc 	= session()->get('account');
		$token1 = Accounts::select('token')->where('id','=',$acc)->first();
		if(empty($token1)){
			$token = session()->get('ebayToken');
			if(empty($token)){
				session()->put('flash_message','Connect Your account to get Data');
				return redirect('ebay_channel');
			}else{
				return $token;
			}
		}else{
			if(empty($token1)){
				session()->put('flash_message','Connect Your account to get Data');
				return redirect('ebay_channel');
			}else{
				return $token1->token;
			}
		}
	}

	public function getCredential(){
		$creadentialArray = [
			'compatabilityLevel'	=> $this->compatabilityLevel,
			'devID'					=> $this->devID,
			'appID'					=> $this->appID,
			'certID'				=> $this->certID,
			'serverUrl'				=> $this->serverUrl,
			'siteID'				=> $this->siteID,
			'loginURL'				=> $this->loginURL,
			'runame'				=> $this->runame,
		];

		return $creadentialArray;
	}
}