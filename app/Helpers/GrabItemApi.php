<?php

namespace App\Helpers;

use App\Helpers\eBaySession;
use App\Helpers\EbayApi;

class GrabItemApi {

    protected $token;
    protected $credential;

    public function __construct() {
        $ebayApiObj = new EbayApi();
        $this->token = $ebayApiObj->getToken();
        $this->credential = $ebayApiObj->getCredential();
    }

    public function grabitem($item_id) {
        $item_id;
        $verb = 'GetItem';
        /*$token = $this->token;*/

        $requestXmlBody = '<?xml version="1.0" encoding="utf-8"?>
							<GetItemRequest xmlns="urn:ebay:apis:eBLBaseComponents">
							  <RequesterCredentials>
							    <eBayAuthToken>' . $this->token . '</eBayAuthToken>
							  </RequesterCredentials>
							  <ItemID>' . $item_id . '</ItemID>
							</GetItemRequest>';

        $session = new eBaySession($this->credential['devID'], $this->credential['appID'], $this->credential['certID'], $this->credential['serverUrl'], $this->credential['compatabilityLevel'], $this->credential['siteID'], $verb);

        $responseXml = $session->sendHttpRequest($requestXmlBody);
        $response = formatApiResponse($responseXml);
        return $response;
    }

}
