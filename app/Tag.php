<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
    protected $table = 'tags';

    public function getIndexRecord(){
    	$tags = Tag::orderBy('id','desc')->where('status','=',1) ;
    	return $tags = $tags->get();
    }
    public function saveForm($fields, $tagObj=null)
    {
    	if($tagObj==null)
    	{
    		$tagObj = new Tag();
    	}
    	
    	$tagObj->tag_name = $fields['tag_name'];
    	
    	$tagObj->save();
    	return $tagObj;
    }
}
