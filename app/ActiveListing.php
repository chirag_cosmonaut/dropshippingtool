<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use DB;

class ActiveListing extends Model {

    protected $table = 'item';
    private $users_id;
    private $accounts_id;

    public function __construct() {
        $this->accounts_id = session()->get('account');
        $this->users_id = Auth()->user()->id;
    }

    public function getProductDetails($id) {
        $details = ActiveListing::select('item.title', 'item.image', 'item.picture_details', 'item.id', 'item.sku', 'item.item_id', 'listing_details.view_item_url', 'item_shipping_detail.shipping_service', 'order_item.supplier_product_link', 'order_item.supplier_price', 'order_item.supplier_shipping', 'order_item.supplier_order_id', 'order_item.item_price', 'order_item.order_line_item_id')
                        ->leftJoin('listing_details', 'listing_details.item_id', 'item.id')
                        ->leftJoin('item_shipping_detail', 'item_shipping_detail.item_id', 'item.id')
                        ->leftJoin('order_item', 'order_item.item_id', 'item.id')
                        ->where('order_item.order_id', '=', $id)->get();
        return $details;
    }

    public function saveProduct($fields, $isOther = false) {

        $matches = ActiveListing::where('item_id', '=', $fields['item_id'])->first();
        if (empty($matches)) {
            $productObj = new ActiveListing();
        } else {
            $productObj = ActiveListing::findOrFail($matches->id);
        }
        if ($isOther == false) {
            $productObj->item_id = $fields['item_id'];
        }
        $productObj->buy_it_now_price = $fields['current_price'];
        $productObj->country = $fields['country'];
        $productObj->currency = $fields['currency'];
        $productObj->listing_duration = $fields['listing_duration'];
        $productObj->listing_type = $fields['listing_type'];
        $productObj->location = $fields['location'];
        $productObj->payment_methods = $fields['payment_methods'];
        $productObj->category = $fields['category'];
        $productObj->private_listing = $fields['private_listing'];
        $productObj->quantity_available = $fields['quantity_available'];
        $productObj->reserve_price = $fields['reserve_price'];
        $productObj->revise_status = $fields['revise_status'];
        $productObj->ship_to_locations = $fields['ship_to_locations'];
        $productObj->site = $fields['site'];
        $productObj->start_price = $fields['start_price'];
        $productObj->time_left = $fields['time_left'];
        $productObj->title = $fields['title'];
        $productObj->watch_count = $fields['watch_count'];
        $productObj->paypal_email_address = $fields['paypal_email_address'];
        $productObj->hit_count = $fields['hit_count'];
        $productObj->sku = $fields['sku'];
        $productObj->postal_code = $fields['postal_code'];
        $productObj->dispatch_time_max = $fields['dispatch_time_max'];
        $productObj->handle_time = $fields['dispatch_time_max'];
        $productObj->ship_to_register_country = $fields['ship_to_register_country'];
        $productObj->condition_id = $fields['condition_id'];
        $productObj->condition_display_name = $fields['condition_display_name'];
        $productObj->condition_description = $fields['condition_description'];
        $productObj->hide_from_search = $fields['hide_from_search'];
        $productObj->out_of_stock_control = $fields['out_of_stock_control'];
        $productObj->picture_details = $fields['picture_details'];
        $productObj->returns_accepted_option = $fields['returns_accepted_option'];
        $productObj->returns_accepted = $fields['returns_accepted_option'];
        $productObj->refund_type = $fields['refund_type'];
        $productObj->return_within = $fields['return_within'];
        $productObj->return_policy_detail = $fields['return_policy_detail'];
        $productObj->return_paid_by = $fields['return_paid_by'];
        $productObj->restock_fee = $fields['restock_fee'];
        $productObj->shipping_irregular = $fields['shipping_irregular'];
        $productObj->shipping_package = $fields['shipping_package'];
        $productObj->weight_major = $fields['weight_major'];
        $productObj->weight_minor = $fields['weight_minor'];
        if (isset($fields['international_shipping_country'])) {
            $productObj->international_shipping_country = $fields['international_shipping_country'];
        }
        $productObj->user_id = $this->users_id;
        $productObj->account_id = $this->accounts_id;
        $productObj->save();
        return $productObj->id;
    }

    public function saveListingDetails($fields) {
        $matches = DB::table('listing_details')->where('item_id', '=', $fields['item_id'])->first();
        $listingArray = [
            'item_id' => $fields['item_id'],
            'converted_buy_it_now_price' => $fields['converted_buy_it_now_price'],
            'start_time' => $fields['start_time'],
            'view_item_url' => $fields['view_item_url'],
            'view_item_url_for_natural_search' => $fields['view_item_url_for_natural_search'],
            'converted_start_price' => $fields['converted_start_price'],
            'converted_reserve_price' => $fields['converted_reserve_price'],
            'has_reserve_price' => $fields['has_reserve_price'],
            'end_time' => $fields['end_time']
        ];
        if (empty($matches)) {
            DB::table('listing_details')->insert($listingArray);
            return DB::getPdo()->lastInsertId();
        } else {
            DB::table('listing_details')
                    ->where('item_id', '=', $fields['item_id'])
                    ->update($listingArray);
            return $matches->id;
        }
    }

    public function saveSallingStatus($fields) {
        $matches = DB::table('selling_status')->where('item_id', '=', $fields['item_id'])->first();
        $sellingArray = [
            'item_id' => $fields['item_id'],
            'converted_current_price' => $fields['converted_current_price'],
            'current_price' => $fields['current_price'],
            'quantity_sold' => $fields['quantity_sold'],
            'listing_status' => $fields['listing_status']
        ];
        if (empty($matches)) {
            DB::table('selling_status')->insert($sellingArray);
            return DB::getPdo()->lastInsertId();
        } else {
            DB::table('selling_status')
                    ->where('item_id', '=', $fields['item_id'])
                    ->update($sellingArray);
            return $matches->id;
        }
    }

    public function saveshipping_details($fields) {
        $matches = DB::table('item_shipping_detail')->where('item_id', '=', $fields['item_id'])->first();
        $shippingArray = [
            'item_id' => $fields['item_id'],
            'sales_tax_percent' => $fields['sales_tax_percent'],
            'shipping_included_in_tax' => $fields['shipping_included_in_tax'],
            'shipping_service' => $fields['shipping_service'],
            'shipping_service_cost' => $fields['shipping_service_cost'],
            'shipping_service_priority' => $fields['shipping_service_priority'],
            'free_shipping' => $fields['free_shipping'],
            'shipping_type' => $fields['shipping_type'],
        ];
        if (empty($matches)) {
            DB::table('item_shipping_detail')->insert($shippingArray);
            return DB::getPdo()->lastInsertId();
        } else {
            DB::table('item_shipping_detail')
                    ->where('item_id', '=', $fields['item_id'])
                    ->update($shippingArray);
            return $matches->id;
        }
    }

}
