<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\ActiveListing;
use DB;

class Order extends Model
{
    protected $table = 'orders';

    private $users_id;
    private $accounts_id;

    public function __construct(){
        $this->accounts_id = session()->get('account');
        $this->users_id    = Auth()->user()->id;
    }

    public function saveOrder($orders,$isSingle){
    	$matches = Order::where('order_id','=',$orders['order_id'])->first();
    	
    	if(empty($matches)){
    		$orderObj = new Order();
    		$orderObj->status 	= 8;
    	}else{
    		$orderObj = Order::find($matches->id);
    	}
    	$orderObj->order_id                   = $orders['order_id'];
    	$orderObj->order_status               = $orders['order_status'];
    	$orderObj->adjustment_amount          = $orders['adjustment_amount'];
    	$orderObj->amount_paid                = $orders['amount_paid'];
    	$orderObj->amount_saved               = $orders['amount_saved'];
    	$orderObj->created_time               = $orders['created_time'];
    	$orderObj->payment_methods            = $orders['payment_methods'];
    	$orderObj->seller_email               = $orders['seller_email'];
    	$orderObj->subtotal                   = $orders['subtotal'];
    	$orderObj->total                      = $orders['total'];
    	$orderObj->eias_token                 = $orders['eias_token'];
    	$orderObj->payment_hold_status        = $orders['payment_hold_status'];
    	$orderObj->is_multi_leg_shipping      = $orders['is_multi_leg_shipping'];
    	$orderObj->seller_user_id             = $orders['seller_user_id'];
    	$orderObj->seller_eias_token          = $orders['seller_eias_token'];
    	$orderObj->cancel_status              = $orders['cancel_status'];
    	$orderObj->contains_ebay_plus_transaction = $orders['contatin_ebay_plus_transaction'];
        
        if(isset($orders['buyer_note'])){
            $orderObj->buyer_note             = $orders['buyer_note'];
        }
        if(isset($orders['paid_time'])){
            $orderObj->paid_time              = $orders['paid_time'];
        }
        if(isset($orders['shiped_time'])){
            $orderObj->shiped_time            = $orders['shiped_time'];
        }
        $orderObj->user_id                    = $this->users_id;
        $orderObj->account_id                 = $this->accounts_id;

        $orderObj->save();
        return $orderObj->id;
    }

    public function saveBuyer($fields,$address){
        $matches = DB::table('buyer')->where('buyer_user_id','=',$fields['buyer_user_id'])->first();
        
        $buyerArray = [];
        if(isset($fields['email'])){
            /*if($fields['email'] != $matches->email){
                $buyerArray['secondary_email']   = $fields['email'];
            }else{
            }*/
                $buyerArray['email'] 		     = $fields['email'];
        }
        $buyerArray['user_first_name'] 		= $fields['user_first_name'];
        $buyerArray['user_last_name'] 		= $fields['user_last_name'];
        $buyerArray['buyer_user_id'] 		= $fields['buyer_user_id'];
        if(isset($fields['phone'])){
            $buyerArray['phone'] 			= $fields['phone'];
        }
        if(isset($fields['street1'])){
            $buyerArray['street1'] 			= $fields['street1'];
        }
        if(isset($fields['street2'])){
            $buyerArray['street2'] 			= $fields['street2'];
        }
        if(isset($fields['city_name'])){
            $buyerArray['city_name'] 		= $fields['city_name'];
        }
        if(isset($fields['state_or_provinance'])){
            $buyerArray['state_or_provinance'] = $fields['state_or_provinance'];
        }
        if(isset($fields['postal_code'])){
            $buyerArray['postal_code'] 		= $fields['postal_code'];
        }
        if(isset($fields['country_name'])){
            $buyerArray['country_name'] 	= $fields['country_name'];
        }
        if(empty($matches)){
        	$orderObj = DB::table('buyer')->insert($buyerArray);
        	return DB::getPdo()->lastInsertId();
        }else{
            $orderObj = DB::table('buyer')
            ->where('id','=',$matches->buyer_user_id)
            ->update($buyerArray);
            return $matches->id;
        }
    }

    public function saveItem($fields){
        $matches = ActiveListing::where('item_id','=',$fields['item_id'])->first();
        if(empty($matches)){
        	$productObj = new ActiveListing();
        }else{
        	$productObj = ActiveListing::find($matches->id);
        }
        $productObj->item_id 		= $fields['item_id'];
        $productObj->site 			= $fields['site'];
        $productObj->title 			= $fields['title'];
        $productObj->condition_id 	= $fields['condition_id'];
        $productObj->condition_display_name 		= $fields['condition_display_name'];
        $productObj->item_id        = $fields['item_id'];
        $productObj->user_id        = $this->users_id;
        $productObj->account_id     = $this->accounts_id;
        $productObj->save();
        return $productObj->id;
    }

    public function saveTransaction($fields){
        $matches = DB::table('transaction')->where('shipping_details','=',$fields['shipping_details'])->first();
        $transactionArray = [];
        $transactionArray['shipping_details'] 	    =	$fields['shipping_details'];
        $transactionArray['created_date'] 		    =	$fields['created_date'];
        $transactionArray['quantity_purchased'] 	=	$fields['quantity_purchased'];
        $transactionArray['transaction_id'] 		=	$fields['transaction_id'];
        $transactionArray['transaction_price'] 	    =	$fields['transaction_price'];
        $transactionArray['transaction_site_id']    =	$fields['transaction_site_id'];
        $transactionArray['platform'] 			    =	$fields['platform'];
        $transactionArray['order_line_item_id'] 	=	$fields['order_line_item_id'];
        $transactionArray['ebay_plus_transaction'] 	=	$fields['ebay_plus_transaction'];
        $transactionArray['final_value_fee'] 	    =	$fields['final_value_fee'];
        $transactionArray['order_id'] 			    =	$fields['order_id'];
        $transactionArray['item_id'] 			    =	$fields['item_id'];
        if(empty($matches)){
        	 DB::table('transaction')->insert($transactionArray);
        	 return $transactionArray['shipping_details'];
        }else{
        	DB::table('transaction')
            ->where('id','=',$matches->id)
            ->update($transactionArray);
            return $transactionArray['shipping_details'];
        }
    }

    public function saveSeviceOption($fields){
        
        $shipArray = [];
        for($i=0;$i<1;$i++){
            $shipArray['shipping_service']          = $fields[$i]['shipping_service'];
            $shipArray['shipping_service_cost']     = $fields[$i]['shipping_service_cost'];
            $shipArray['shipping_service_priority'] = $fields[$i]['shipping_service_priority'];
            $shipArray['expedited_service']         = $fields[$i]['expedited_service'];
            $shipArray['shipping_time_min']         = $fields[$i]['shipping_time_min'];
            $shipArray['shipping_time_max']         = $fields[$i]['shipping_time_max'];
            
        }
        $matches = DB::table('service_option')->where('order_id','=',$fields['order_id'])->first();
        $serviceArray = [];
        $serviceArray['shipping_service'] 			= $shipArray['shipping_service'];
        $serviceArray['shipping_service_cost'] 		= $shipArray['shipping_service_cost'];
        $serviceArray['shipping_service_priority'] 	= $shipArray['shipping_service_priority'];
        $serviceArray['expedited_service'] 			= $shipArray['expedited_service'];
        $serviceArray['shipping_time_min'] 			= $shipArray['shipping_time_min'];
        $serviceArray['shipping_time_max'] 			= $shipArray['shipping_time_max'];
        $serviceArray['order_id'] 					= $fields['order_id'];
        if(empty($matches)){
        	DB::table('service_option')->insert($serviceArray);
        	return DB::getPdo()->lastInsertId();
        }else{
        	DB::table('service_option')
            ->where('id','=',$matches->id)
            ->update($serviceArray);
            return $matches->id;
        } 
    }

    public function saveCheckOutStatus($fields){
    	$matches = DB::table('check_out_status')->where('order_id','=',$fields['order_id'])->first();
    	$checkOutArray = [];
    	$checkOutArray['ebay_payment_status'] 	= $fields['ebay_payment_status'];
    	$checkOutArray['last_modified_time'] 	= $fields['last_modified_time'];
    	$checkOutArray['payment_method'] 		= $fields['payment_method'];
    	$checkOutArray['status'] 				= $fields['status'];
    	$checkOutArray['integrated_merchant_credit_card_enabled'] = $fields['integrated_merchant_credit_card_enabled'];
    	$checkOutArray['order_id'] 				= $fields['order_id'];
        if(empty($matches)){
        	DB::table('check_out_status')->insert($checkOutArray);
        	return DB::getPdo()->lastInsertId();
        }else{
        	DB::table('check_out_status')
            ->where('id','=',$matches->id)
            ->update($checkOutArray);
            return $matches->id;
        }
    }

    public function saveShippingDetails($fields)
    {
        $matches = DB::table('shipping_details')->where('order_id','=',$fields['order_id'])->first();
        $shippingArray = [];
        $shippingArray['sales_tax'] 				= $fields['sales_tax'];
        $shippingArray['shipping_service_option'] 	= $fields['shipping_service_option'];
        $shippingArray['selling_manager_sales_record_number'] = $fields['selling_manager_sales_record_number'];
        $shippingArray['get_it_fast'] 				= $fields['get_it_fast'];
        $shippingArray['order_id'] 					= $fields['order_id'];
        if(empty($matches)){
        	DB::table('shipping_details')->insert($shippingArray);
        	return DB::getPdo()->lastInsertId();
        }else{
        	DB::table('shipping_details')
            ->where('id','=',$matches->id)
            ->update($shippingArray);
            return $matches->id;
        }
    }

    public function saveShippingAddress($fields){
        $matches = DB::table('shipping_address')->where('order_id','=',$fields['order_id'])->first();
        $addressArray = [];
        $addressArray['name'] 				= $fields['name'];
        $addressArray['street1'] 			= $fields['street1'];
        $addressArray['street2'] 			= $fields['street2'];
        $addressArray['city_name'] 			= $fields['city_name'];
        $addressArray['state_or_provinance'] = $fields['state_or_provinance'];
        $addressArray['country_name'] 		= $fields['country_name'];
        $addressArray['phone'] 				= $fields['phone'];
        $addressArray['postal_code'] 		= $fields['postal_code'];
        $addressArray['external_address_id'] = $fields['external_address_id'];
        $addressArray['order_id'] 			= $fields['order_id'];
       	if(empty($matches)){
        	DB::table('shipping_address')->insert($addressArray);
        	return DB::getPdo()->lastInsertId();
        }else{
        	DB::table('shipping_address')
            ->where('id','=',$matches->id)
            ->update($addressArray);
            return $matches->id;
        }
    }

    public function saveOrderPayment($fields){
        $matches = DB::table('order_payment')->where('order_id')->first();
        $paymentArray = [];
        $paymentArray['order_id'] 			= $fields['order_id'];
        $paymentArray['payment_status'] 	= $fields['payment_status'];
        $paymentArray['payer'] 				= $fields['payer'];
        $paymentArray['payee'] 				= $fields['payee'];
        $paymentArray['payment_time'] 		= $fields['payment_time'];
        $paymentArray['payment_amount'] 	= $fields['payment_amount'];
        $paymentArray['reference_id']		= $fields['reference_id'];
        $paymentArray['fee_or_credit_amount'] = $fields['fee_or_credit_amount'];
        if(empty($matches)){
        	DB::table('order_payment')->insert($paymentArray);
        	return DB::getPdo()->lastInsertId();
        }else{
        	DB::table('order_payment')
            ->where('id','=',$matches->id)
            ->update($paymentArray);
            return $matches->id;
        }
    }
    public function saveOrderRefund($fields){
        $matches = DB::table('order_refunds')->where('order_id')->first();
        $refundArray = [];
        $refundArray['order_id'] 				= $fields['order_id'];
        $refundArray['refundstatus'] 			= $fields['refundstatus'];
        $refundArray['refund_type'] 			= $fields['refund_type'];
        $refundArray['refund_to'] 				= $fields['refund_to'];
        $refundArray['refund_time'] 			= $fields['refund_time'];
        $refundArray['refund_amount'] 			= $fields['refund_amount'];
        $refundArray['reference_id'] 			= $fields['reference_id'];
        $refundArray['fee_or_credit_amount'] 	= $fields['fee_or_credit_amount'];
        if(empty($matches)){
        	DB::table('order_refunds')->insert($refundArray);
        	return DB::getPdo()->lastInsertId();
        }else{
        	DB::table('order_refunds')
            ->where('id','=',$matches->id)
            ->update($refundArray);
            return $matches->id;
        } 
    }

    public function storeFeedBack($fields){
        $matchFlag = 0;
        $matches = DB::table('feedback')->where('feedback_id','=',$fields['feedback_id'])->first();
        $feedbackArray = [];
        $feedbackArray['commenting_user'] 		= $fields['commenting_user'];
        $feedbackArray['commenting_user_score'] = $fields['commenting_user_score'];
        $feedbackArray['comment_text'] 			= $fields['comment_text'];
        $feedbackArray['comment_time'] 			= $fields['comment_time'];
        $feedbackArray['comment_type'] 			= $fields['comment_type'];
        $feedbackArray['item_id'] 				= $fields['item_id'];
        $feedbackArray['role'] 					= $fields['role'];
        $feedbackArray['feedback_id'] 			= $fields['feedback_id'];
        $feedbackArray['transaction_id'] 		= $fields['transaction_id'];
        $feedbackArray['order_line_item_id'] 	= $fields['order_line_item_id'];
        $feedbackArray['item_title'] 			= $fields['item_title'];
        $feedbackArray['item_price'] 			= $fields['item_price'];
        if(empty($matches)){
        	DB::table('feedback')->insert($feedbackArray);
        	return DB::getPdo()->lastInsertId();
        }else{
        	DB::table('feedback')
            ->where('id','=',$matches->id)
            ->update($feedbackArray);
            return $matches->id;
        }
    }

    public function getDetail($id){
        $details = DB::table('orders')
            ->select('orders.id as order_id','orders.order_id as OID','orders.created_time','orders.ebay_fee','orders.paypal_fee','orders.subtotal','orders.total','transaction.paypal_transaction_id','transaction.paypal_tansaction_id_supplier','shipping_details.selling_manager_sales_record_number as record_id','orders.payment_methods','orders.seller_user_id','orders.order_status','transaction.quantity_purchased','shipping_address.*','orders.ebay_fee_price','orders.paypal_fee_price','orders.refund_amount','orders.cancel_reason','orders.status','orders.transaction_fee','orders.shipped_time','order_payment.payment_amount','order_payment.payment_time','orders.paid_time','order_payment.reference_id','orders.buyer_note','service_option.shipping_service_cost')
            ->leftJoin('shipping_details','shipping_details.order_id','orders.id')
            ->leftJoin('shipping_address','shipping_address.order_id','orders.id')
            ->leftJoin('service_option','service_option.order_id','orders.id')
            ->leftJoin('order_payment','order_payment.order_id','orders.id')
            ->leftJoin('transaction','transaction.order_id','orders.id')
            ->where('orders.id','=',$id);
        return $details = $details->first();
    }

    public function getOrderNotes($id){
        $notes = DB::table('order_notes')
        ->where('order_id','=',$id)
        ->where('is_active','=',1)
        ->orderBy('id','desc');
        return $notes->get();
    }
}
