<?php
namespace App;
namespace App\Http\Models;

use Illuminate\Database\Eloquent\Model;

use DB;

class Settings extends Model
{
	protected $table = 'settings';

	public function getSettings(){
    	$settings = Settings::first();
    	return $settings;
    }

    public function changeSettings($fields){
    	$settingObj = Settings::findOrFail($fields['id']);
        $settingObj->order_email                = $fields['order_email'];
        $settingObj->place_order                = $fields['place_order'];
        $settingObj->default_currency           = $fields['default_currency'];
        $settingObj->channel                    = $fields['channel'];
        $settingObj->return_within              = $fields['return_within'];
        $settingObj->shipping_service           = $fields['domestic_shipping_service'];
        $settingObj->paypal_email               = $fields['paypal_email'];
        $settingObj->return_policy              = $fields['return_policy'];
        $settingObj->payment_instruction        = $fields['payment_instruction'];        
        $settingObj->ebay_fee                   = $fields['ebay_fee'];
    	$settingObj->paypal_fee                 = $fields['paypal_fee'];
        $settingObj->transaction_fee        = $fields['transaction_fee'];
    	$settingObj->save();
    	return $settingObj;
    }

    public function saveSandboxDetails($fields){
        $user_id = \Auth::user()->id;
        $keys = DB::table('ebay_setting')->where('user','=',$user_id)->where('type','=','sendbox')->get();
        $isEdit=false;
        foreach($keys as $key){
            if($key->user==$user_id){
                $isEdit=true;
            }
        }
        if($isEdit==false){
            $settingObj = DB::table('ebay_setting')->insert([
                            'user'      => $user_id,
                            'app_id'    => $fields['app_id'],
                            'dev_id'    => $fields['dev_id'],
                            'cert_id'   => $fields['cert_id'],
                            'token'     => $fields['token'],
                            'type'      => $fields['type']
                        ]);
            return $settingObj;
        }else{
           $settingObj = DB::table('ebay_setting')
                        ->where('user',$user_id)
                        ->where('type','sendbox')
                        ->update([
                            'user'          => $user_id,
                            'app_id'        => $fields['app_id'],
                            'dev_id'        => $fields['dev_id'],
                            'cert_id'       => $fields['cert_id'],
                            'token'         => $fields['token'],
                            'type'          => $fields['type']
                        ]);
            return $settingObj;
        }
        
    }

    public function saveProductionDetails($fields){
        $user_id = \Auth::user()->id;
        $keys = DB::table('ebay_setting')->where('user','=',$user_id)->where('type','=','production')->get();
        $isEdit=false;
        foreach($keys as $key){
            if($key->user==$user_id){
                $isEdit=true;
            }
        }
        if($isEdit==false){
            $settingObj = DB::table('ebay_setting')->insert([
                            'user' => $user_id,
                            'app_id' => $fields['app_id'],
                            'dev_id' => $fields['dev_id'],
                            'cert_id' => $fields['cert_id'],
                            'token' => $fields['token'],
                            'type' => $fields['type']
                        ]);
            return $settingObj;
        }else{
           $settingObj = DB::table('ebay_setting')
                        ->where('user',$user_id)
                        ->where('type','production')
                        ->update([
                            'user' => $user_id,
                            'app_id' => $fields['app_id'],
                            'dev_id' => $fields['dev_id'],
                            'cert_id' => $fields['cert_id'],
                            'token' => $fields['token'],
                            'type' => $fields['type']
                        ]);
            return $settingObj;
        }
        
    }


    public function getSendboxKey(){
        /*$user_id = \Auth::user()->id;*/
        $settings = DB::table('ebay_setting')
                ->select('ebay_setting.*')
                /*->where('ebay_setting.user','=',$user_id)*/
                ->where('ebay_setting.type','=','sendbox')
                ->first();
        return $settings;
    }

    public function getProductionKey(){
        /*$user_id = \Auth::user()->id;*/
        $settings = DB::table('ebay_setting')
                ->select('ebay_setting.*')
                /*->where('ebay_setting.user','=',$user_id)*/
                ->where('ebay_setting.type','=','production')
                ->first();
        return $settings;
    }

  


}
