<?php

namespace App;
use DB;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $table = 'buyer';
    
    public function getBuyerDetail($id){
        $details = Customer::select('buyer.*')
                    ->leftJoin('orders','orders.buyer_id','buyer.id')
                    ->where('orders.id','=',$id);
        return $details = $details->first();
    }

    public function getOrderMessages($buyer,$item){
        $messages = DB::table('messages')->select('*')
                ->where('item_id','=',$item)
                ->where('sender_id','=',$buyer)
                ->orWhere('recipient_id',$buyer)
                ->orderBy('creation_date','desc');
        return $messages->get();
    }

    public function saveBuyerDetail($buyer){
        $match = Customer::where('buyer_user_id','=',$buyer['user_id'])->first();
        if(empty($match)){
            $customerObj = new Customer();
        }else{
            $customerObj = Customer::find($match->id);
        }
        $customerObj->about_me_page             =  $buyer['about_me_page'];
        $customerObj->eiast_token               =  $buyer['eiast_token'];
        if($buyer['email'] != 'Invalid Request'){
            $customerObj->email                 =  $buyer['email'];
        }
        $customerObj->feedback_score            =  $buyer['feedback_score'];
        $customerObj->unique_negative_feedback_count    =  $buyer['unique_negative_feedback_count'];
        $customerObj->unique_positive_feedback_count    =  $buyer['unique_positive_feedback_count'];
        $customerObj->positive_feedback_percent         =  $buyer['positive_feedback_percent'];
        $customerObj->feedback_private          =  $buyer['feedback_private'];
        $customerObj->feeddback_rating_star     =  $buyer['feeddback_rating_star'];
        $customerObj->id_verified               =  $buyer['id_verified'];
        $customerObj->ebay_good_standing        =  $buyer['ebay_good_standing'];
        $customerObj->new_user                  =  $buyer['new_user'];
        $customerObj->registration_date         =  $buyer['registration_date'];
        $customerObj->site                      =  $buyer['site'];
        $customerObj->status                    =  $buyer['status'];
        $customerObj->user_id_changed           =  $buyer['user_id_changed'];
        $customerObj->user_id_last_changed      =  $buyer['user_id_last_changed'];
        $customerObj->vat_status                =  $buyer['vat_status'];
        $customerObj->business_role             =  $buyer['business_role'];
        $customerObj->ebay_wiki_read_only       =  $buyer['ebay_wiki_read_only'];
        $customerObj->motors_dealer             =  $buyer['motors_dealer'];
        $customerObj->unique_neutral_feedback_count             =  $buyer['unique_neutral_feedback_count'];
        $customerObj->enterprise_seller         =  $buyer['enterprise_seller'];

        $customerObj->save();
        return $customerObj->id;
    }
}
