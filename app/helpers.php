<?php

use App\ActiveListing;
use App\Order;
use App\Helpers\ItemApi;
use App\Tag;

function formatApiResponse($responseXml){
    $responseDoc = new DomDocument();
    $responseDoc->loadXML($responseXml);
    $errors = $responseDoc->getElementsByTagName('Errors');
    $response = simplexml_import_dom($responseDoc);
    return $response;
}

function getAllRecord($table,$condition = null,$isSingle = null,$limit=null){
    $tableData = DB::table($table);
    if(!empty($condition)){
        $tableData->where($condition);
    }
    if ($limit!= null) {
        $tableData->limit($limit);
        $tableData->orderBy('id','desc');
    }
    if($isSingle != null){
        return $tableData = $tableData->first();
    }
    $tableData = $tableData->get();
    return $tableData;
}

function getRecordByAccount($table){
    $user_id        = Auth()->user()->id;
    $account_id     = session()->get('account');
    $records = DB::table($table)->where('user_id',"=",$user_id)->where('account_id',"=",$account_id);
    $records = $records->first();
    return $records;
}

function insertOrUpdate($table,$data,$condition=null){
    if(empty($condition)){
        DB::table($table)->insert($data);
        return DB::getPdo()->lastInsertId();
    }else{
        DB::table($table)->where($condition)->update($data);
    }
}

function getFormatedDate($date){
    $date1 = explode("T", $date);
    return $returnDate = $date1[0];
}

function getFormatedTime($date){
    $dateTime = explode("T", $date);
    return $time = rtrim($dateTime[0],".000Z");
}

function getDomain($url){
    $link   = '';
    $pieces = parse_url($url);
    $domain = isset($pieces['host']) ? $pieces['host'] : $pieces['path'];
    if (preg_match('/(?P<domain>[a-z0-9][a-z0-9\-]{1,63}\.[a-z\.]{2,6})$/i', $domain, $regs)) {
        return $link = $regs['domain'];
    }
}

function getOrderProfit($orderID) {
    $OrderDetail = Order::select('orders.total', 'orders.subtotal', 'orders.transaction_fee', 'orders.ebay_fee_price', 'orders.paypal_fee_price', 'orders.order_status', 'order_payment.payment_amount', 'orders.shipped_time')
                    ->leftJoin('order_payment', 'order_payment.order_id', 'orders.id')
                    ->where('orders.id', '=', $orderID)->first();

    $items = ActiveListing::select('order_item.supplier_price', 'order_item.supplier_shipping', 'order_item.item_price')
            ->leftjoin('order_item', 'order_item.item_id', 'item.id')
            ->where('order_item.order_id', '=', $orderID)
            ->get();
    $supplier_cost = 0;
    $shipping_cost = 0;
    foreach ($items as $item) {
        $supplier_cost = $supplier_cost + $item->supplier_price;
        $shipping_cost = $shipping_cost + $item->supplier_shipping;
    }

    $transactionFee = $OrderDetail->transaction_fee;
    if (isset($OrderDetail->total)) {
        $total_amount = $OrderDetail->total;
    } else {
        $total_amount = 0;
    }
    $ebay_fee = 0;
    if (isset($OrderDetail->ebay_fee_price)) {
        $ebay_fee = $OrderDetail->ebay_fee_price;
    } else {
        $ebay_fee = 0;
    }

    if (isset($OrderDetail->paypal_fee_price)) {
        $paypal_fee = $OrderDetail->paypal_fee_price;
    } else {
        $paypal_fee = 0;
    }
    $refund_amount = 0.00;
    $refunds;
    if ($OrderDetail->order_status != 'Cancelled') {
        return $profit = floatval($total_amount) - floatval($supplier_cost) - floatval($shipping_cost) - floatval($ebay_fee) - floatval($paypal_fee - $transactionFee);
    } elseif ($OrderDetail->order_status == 'Cancelled' && $OrderDetail->shipped_time == NULL) {
        $total_amount = $OrderDetail->payment_amount;
        $refund_amount = 0;
        $supplier_cost = 0;
        $shipping_cost = 0;
        $ebay_fee = 0;
        $paypal_fee = 0;
        $refund = getAllRecord('order_refunds',array('order_id'=>$orderID),true);
        if (!empty($refund)) {
            $refund_amount = $refund->refund_amount;
        } else {
            $refund_amount = 0;
        }
        return $profit = floatval($total_amount) - floatval($refund_amount) - floatval($transactionFee);
    } elseif ($OrderDetail->order_status == 'Cancelled' && $OrderDetail->shipped_time != NULL) {
        $total_amount   = $OrderDetail->payment_amount;
        $refund_amount  = 0;
        $supplier_cost  = 0;
        $shipping_cost  = 0;
        $ebay_fee       = 0;
        $paypal_fee     = 0;
        $refunds = getAllRecord('order_refunds',array('order_id'=>$orderID),true);
        if (!empty($refund)) {
            $refund_amount = $refund->refund_amount;
        } else {
            $refund_amount = 0;
        }
        return $profit = floatval($total_amount) - floatval($refund_amount) - floatval($transactionFee);
    }
}

function getOrderStatus() {
    $status = DB::table('order_status')->where('status', '=', 1)->orderBy('order', 'asc')->get();
    return $status;
}

function getSelectedTags($orderid) {
    $selecredtags = DB::table('order_tags')->where('order_id', '=', $orderid)
            ->get();
    return $selecredtags;
}

function actionLog($action) {
    $user_id    = Auth()->user()->id;
    $account_id = session()->get('account');
    $ip         = $_SERVER['REMOTE_ADDR'];
    DB::table('action_log')->insert([
        'user_id'       => $user_id,
        'account_id'    => $account_id,
        'action'        => $action,
        'ip_address'    => $ip
    ]);
    return $user_id;
}

function getPrice($id) {
    $itemApiObj = new ItemApi();
    $response = $itemApiObj->getPrice($id);
    $items = $response->Item;
    if ($response->Ack != 'Failure') {
        return $itemPrice = floatval($response->Item->StartPrice);
    } else {
        return $itemPrice = 'Not Found';
    }
}

function getTags() {
    $tags = Tag::orderBy('id', 'desc')->where('status', '=', 1);
    return $tags = $tags->get();
}

function getTodayPrice($itemId) {
    $account_id = session()->get('account');
    $price = DB::table('item_has_price')->select('old_price', 'new_price')->where('item_id', '=', $itemId)->where('account_id', '=', $account_id)->where('created_at', 'LIKE', date('Y-m-d') . '%')->first();
    return $price;
}


function sendEmail($view = null, $settings = null){
    if (!empty($settings) && $view != null) {
        $sent = Mail::send($view, $settings, function ($message) use ($settings) {
        $message->from($settings['from']);
        $message->to($settings['to'])->subject($settings['subject']);
        });
    }
}

function getCurrentTimezone(){
    $user_id = Auth()->user()->id;
    $account_id = session()->get('account');
    $row = DB::table('accounts')->select('timezone')->where('int_user_id',"=",$user_id)->where('id',"=",$account_id)->first();
    $timezone = $row->timezone;
    if(empty($timezone)){
        $timezone = "Australia/ACT";
    }
    return $timezone;
}

function getCurrentTime(){
    config(['app.timezone' => getCurrentTimezone()]);
    date_default_timezone_set(getCurrentTimezone());
    $dateTime = date("Y-m-d H:i:s");
    return $dateTime;
}


function calculate_time_span($date){
   $seconds  = strtotime(date('Y-m-d H:i:s')) - strtotime($date);

    $months = floor($seconds / (3600*24*30));
    $day = floor($seconds / (3600*24));
    $hours = floor($seconds / 3600);
    $mins = floor(($seconds - ($hours*3600)) / 60);
    $secs = floor($seconds % 60);

    if($seconds < 60)
        $time = $secs." seconds ago";
    else if($seconds < 60*60 )
        $time = $mins." min ago";
    else if($seconds < 24*60*60)
        $time = $hours." hours ago";
    else if($seconds < 24*60*60)
        $time = $day." day ago";
    else
        $time = $months." month ago";

    return $time;
}

function getOrderProfitByDate($type=null){ 
    $user_id = Auth()->user()->id;
    $account_id = session()->get('account');
    $tableData = DB::table('orders');
    $tableData->select('orders.id');
    $tableData->where('orders.user_id',"=",$user_id);
    $tableData->where('orders.account_id',"=",$account_id);
    if (isset($type) && $type=='monthly') {
        $tableData->whereRaw('DATE_FORMAT(`created_at`,"%Y-%m-%d") >= DATE_SUB(CURDATE(), INTERVAL 1 MONTH)');
    }elseif (isset($type) && $type=='weekly') {
        $tableData->whereRaw('DATE_FORMAT(`created_at`,"%Y-%m-%d") >= DATE_SUB(CURDATE(), INTERVAL 1 WEEK)');
    }elseif (isset($type) && $type=='hourly') {
        $tableData->whereRaw('`created_at` >= DATE_SUB(CURDATE(), INTERVAL 2 HOUR)');
    }
    $tableData = $tableData->get();
    $profit =0;
    if (!empty($tableData)) {
        foreach($tableData as $mprofit)
        {
            $profit = $profit + getOrderProfit($mprofit->id);
        }
    }
    return $profit;
}


function getOrderProfitByWeekly($interval=null){ 
    $user_id = Auth()->user()->id;
    $account_id = session()->get('account');
    $tableData = DB::table('orders');
    $tableData->selectRaw('WEEK(created_at) AS order_week,id');
    $tableData->where('orders.user_id',"=",$user_id);
    $tableData->where('orders.account_id',"=",$account_id);
    if (isset($interval) && $interval!=null) {
        $tableData->whereRaw('DATE_FORMAT(`created_at`,"%Y-%m-%d") >= DATE_SUB(CURDATE(), INTERVAL '.$interval.' MONTH)');
    }
    $tableData->orderBy('order_week','asc');
    $tableData = $tableData->get();
    $profit =0;
    $data = $weeks = [];
    if (!empty($tableData)) {
        foreach($tableData as $mprofit)
        {
            $index = week_exists($mprofit->order_week, $data);
            $tprofit = floatval(getOrderProfit($mprofit->id));
            $profit = floatval($profit) + $tprofit;
            if ($index < 0) {
                $data[] = array('week'=>$mprofit->order_week,'a'=>1,'b'=>(isset($profit) && intval($profit) > 0)?number_format($profit,2):0);
                $weeks[] = $mprofit->order_week;
            }else {
                $data[$index]['a'] +=  1;
                $data[$index]['b'] +=  (isset($profit) && intval($profit) > 0)?:0;
            }
        }
    }
    return array('data'=>$data,'weeks'=>$weeks);
}

// for search if a bank has been added into $amount, returns the key (index)
function week_exists($weekname, $array) {
    $result = -1;
    if (!empty($array)) {
        for($i=0; $i<sizeof($array); $i++) {
            if ($array[$i]['week'] == $weekname) {
                $result = $i;
                break;
            }
        }
    }
    return $result;
}